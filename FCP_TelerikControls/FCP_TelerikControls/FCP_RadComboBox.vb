Imports RenaissanceGlobals

Public Class FCP_RadComboBox

	Public Delegate Function Delegate_SetComboValue(ByRef pCombo As Telerik.WinControls.UI.RadComboBox, ByRef pValue As Object) As Boolean
	Private _ComboSelectString As String
	Private _SelectNoMatch As Boolean

	Public SetValueDelegate As Delegate_SetComboValue
  Public _MasternameCollection As IRadCombo

	Public Sub New()
		MyBase.New()

		' This call is required by the Windows Form Designer.
		InitializeComponent()

		' Add any initialization after the InitializeComponent() call.

		MyBase.ThemeClassName = "Telerik.WinControls.UI.RadComboBox"
		MyBase.ThemeName = "ControlDefault"

		SetValueDelegate = Nothing
		_MasternameCollection = Nothing
		_ComboSelectString = ""
		_SelectNoMatch = False

	End Sub

  Public Sub New(ByRef p_MasternameCollection As IRadCombo)
    Me.New()

    _MasternameCollection = p_MasternameCollection
  End Sub

	Public Property MasternameCollection() As IRadCombo
		' **********************************************************************
		' 
		' 
		' **********************************************************************

		Get
			Return _MasternameCollection
		End Get

		Set(ByVal value As IRadCombo)
			If (value Is Nothing) Then
				If (_MasternameCollection IsNot Nothing) Then
					FCPLostFocus(Nothing, New EventArgs)
					_MasternameCollection.ActiveRadCombo = Nothing
				End If

				_MasternameCollection = value

				If Me.Focused Then
					Call FCPGotFocus(Me, New EventArgs)
				End If
			Else
				If (_MasternameCollection IsNot Nothing) Then
					If (_MasternameCollection IsNot value) Then
						FCPLostFocus(Nothing, New EventArgs)
						_MasternameCollection.ActiveRadCombo = Nothing

						If Me.Focused Then
							Call FCPGotFocus(Me, New EventArgs)
						End If
					End If
				Else
					_MasternameCollection = value
				End If
			End If

		End Set
	End Property

	Public Overrides Property ThemeClassName() As String
		Get
			Return MyBase.ThemeClassName
		End Get
		Set(ByVal value As String)
			MyBase.ThemeClassName = value
		End Set
	End Property

	Public Overloads Property ThemeName() As String
		Get
			Return MyBase.ThemeName
		End Get
		Set(ByVal value As String)
			MyBase.ThemeName = value
		End Set
	End Property

	Public Property SelectNoMatch() As Boolean
		Get
			Return _SelectNoMatch
		End Get
		Set(ByVal value As Boolean)
			_SelectNoMatch = value
		End Set
	End Property

  Public Overloads Property SelectedValue() As Object
    Get
      Return MyBase.SelectedValue
    End Get
    Set(ByVal value As Object)
      Try
        If (value Is Nothing) Then
          Try
            MyBase.SelectedIndex = (-1)
            MyBase.SelectedValue = Nothing
          Catch ex As Exception
          End Try
          MyBase.Text = ""
        ElseIf (Me.Focused) Then
          MyBase.SelectedValue = value
        ElseIf (SetValueDelegate IsNot Nothing) Then
          If SetValueDelegate(Me, value) Then
            MyBase.SelectedValue = value
          Else
            MyBase.SelectedValue = Nothing
          End If
        ElseIf (_MasternameCollection IsNot Nothing) Then
          If ((Me.Items.Count = 1) AndAlso (CType(Me.Items(0), Telerik.WinControls.UI.RadComboBoxItem).Value.Equals(value))) OrElse (Me.Items.Count = _MasternameCollection.Count) Then
            MyBase.SelectedValue = value
          ElseIf _MasternameCollection.Contains(value) Then
            Me.BeginUpdate()
            Me.Items.Clear()
            Me.Items.Add(New Telerik.WinControls.UI.RadComboBoxItem(_MasternameCollection.Item(value).ToString, value))
            Me.EndUpdate()
            MyBase.SelectedValue = value
          Else
            MyBase.SelectedIndex = (-1)
            MyBase.SelectedValue = Nothing
          End If
        End If
      Catch ex As Exception
        MyBase.SelectedValue = value
      End Try

    End Set
  End Property

  Public Overloads Property SelectedIndex() As Integer
    Get
      If (Me.Focused) Then
        Return MyBase.SelectedIndex
      ElseIf (_MasternameCollection IsNot Nothing) Then
        Return _MasternameCollection.IndexOfKey(Me.SelectedValue)
      End If

      Return -1
    End Get
    Set(ByVal value As Integer)
      Try
        If (Me.Focused) Then
          MyBase.SelectedIndex = value
        ElseIf (_MasternameCollection IsNot Nothing) Then
          Dim NewValue As Object

          NewValue = _MasternameCollection.ValueMember(value)

          If (NewValue Is Nothing) Then
            MyBase.SelectedIndex = (-1)
            MyBase.SelectedValue = Nothing
          Else
            Me.SelectedValue = NewValue
          End If
        End If
      Catch ex As Exception
        MyBase.SelectedValue = value
      End Try

    End Set
  End Property

	Public Sub FCPLostFocus(ByVal sender As Object, ByVal e As System.EventArgs)

		If (Me.Created) AndAlso (Not Me.IsDisposed) AndAlso (_MasternameCollection IsNot Nothing) Then
			If (MyBase.Items.Count > 1) Then
				If (MyBase.SelectedValue IsNot Nothing) Then
					Dim ThisItem As Telerik.WinControls.UI.RadComboBoxItem = MyBase.Items(MyBase.SelectedIndex)
					_MasternameCollection.ReplaceComboItem(ThisItem.Value)

					MyBase.SelectedIndex = (-1)
					MyBase.SelectedValue = Nothing
					MyBase.Items.Clear()
					MyBase.Items.Add(ThisItem) ' New Telerik.WinControls.UI.RadComboBoxItem(ThisItem.Text, ThisItem.DescriptionText, ThisItem.Value))
					MyBase.SelectedIndex = 0
				Else
					MyBase.SelectedIndex = (-1)
					MyBase.SelectedValue = Nothing
					MyBase.Items.Clear()
				End If
			End If
		End If

	End Sub

	Private Sub FCPGotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.GotFocus

		If (_MasternameCollection IsNot Nothing) AndAlso (Me.Created) AndAlso (Not Me.IsDisposed) Then
			Dim ItemArray() As Telerik.WinControls.UI.RadComboBoxItem
			ItemArray = _MasternameCollection.RadComboItemArray(_ComboSelectString)

			If (ItemArray IsNot Nothing) AndAlso (ItemArray.Length > MyBase.Items.Count) Then
				If (_MasternameCollection.ActiveRadCombo IsNot Nothing) AndAlso (_MasternameCollection.ActiveRadCombo IsNot Me) Then
					_MasternameCollection.ActiveRadCombo.FCPLostFocus(_MasternameCollection.ActiveRadCombo, New EventArgs)
				End If
				_MasternameCollection.ActiveRadCombo = Me

				If (Me.Items.Count < ItemArray.Length) Then
					If (MyBase.SelectedIndex >= 0) Then
						Dim ThisItem As Telerik.WinControls.UI.RadComboBoxItem = MyBase.Items(MyBase.SelectedIndex)

						MyBase.Items.Clear()
						Try
							MyBase.BeginUpdate()
							MyBase.Items.AddRange(ItemArray)
						Catch ex As Exception
						Finally
							MyBase.EndUpdate()
						End Try
						MyBase.SelectedValue = ThisItem.Value
					Else
						MyBase.Items.Clear()
						Try
							MyBase.BeginUpdate()
							MyBase.Items.AddRange(ItemArray)
						Catch ex As Exception
						Finally
							MyBase.EndUpdate()
						End Try
					End If
				End If

			End If
		End If


	End Sub

	Private Sub thisKeyPressed(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp

		If (_MasternameCollection IsNot Nothing) AndAlso (Me.AutoCompleteMode = Windows.Forms.AutoCompleteMode.None) Then

			If (e.KeyCode = Keys.ShiftKey) OrElse (e.KeyCode = Keys.ControlKey) Or (e.KeyCode = Keys.Alt) Then
				Exit Sub
			End If

			' *******************************************************************************
			' This routine provides the 'Select-as-you-type' functionality on some of the combo boxes.
			' 
			' Note that it will only work for Combo Boxes derived from a Data Table or View.
			' *******************************************************************************

			Dim SelectString As String
			Dim StartingIndex As Integer = 0

      Try

        'Debug.Print("Enter 'thisKeyPressed()'")
        'Debug.Print("  ThisRadComboBox.Text = " & Me.Text)
        'If (Me.SelectedValue IsNot Nothing) Then
        '  Debug.Print("  ThisRadComboBox.SelectedValue = " & Me.SelectedValue.ToString)
        'Else
        '  Debug.Print("  ThisRadComboBox.SelectedValue = <Nothing>")
        'End If
        'Debug.Print("  ThisRadComboBox.Text = " & Me.Text)
        'Debug.Print("  SelectionStart = " & Me.SelectionStart)
        'Debug.Print("  SelectionLength = " & Me.SelectionLength)

        Dim ThisRadComboBox As FCP_TelerikControls.FCP_RadComboBox = Me

        ' Check for <Enter>

        If e.KeyCode = System.Windows.Forms.Keys.Enter Then
          ThisRadComboBox.SelectionStart = 0
          ThisRadComboBox.SelectionLength = ThisRadComboBox.Text.Length
          ' Debug.Print("  Exit (1) 'thisKeyPressed()'")
          Exit Sub
        End If

        If (Me.SelectionStart <= Me.Text.Length) Then
          SelectString = Me.Text.Substring(0, Me.SelectionStart)
        Else
          SelectString = ThisRadComboBox.Text
        End If

        'If (ThisRadComboBox.Tag IsNot Nothing) AndAlso IsNumeric(ThisRadComboBox.Tag) AndAlso (SelectString.Length > 1) Then
        '  StartingIndex = CInt(ThisRadComboBox.Tag)
        'End If

        'Debug.Print("  Initial StartingIndex= " & StartingIndex.ToString)

        If e.KeyCode = System.Windows.Forms.Keys.Back Then
          If SelectString.Length > 0 Then
            SelectString = SelectString.Substring(0, SelectString.Length - 1)
          End If
        End If

        If (e.KeyCode = System.Windows.Forms.Keys.Up) OrElse (e.KeyCode = System.Windows.Forms.Keys.Down) Then
          ' Debug.Print("  Exit (2) 'thisKeyPressed()'")
          Exit Sub
        End If

        ' Search the Combo Data Items for an item starting with the typed text.

        If SelectString.Length <= 0 Then
          If ThisRadComboBox.Items.Count > 0 Then
            ThisRadComboBox.SelectedIndex = 0
            ThisRadComboBox.SelectionStart = 0
            ThisRadComboBox.SelectionLength = ThisRadComboBox.Text.Length
          Else
            MyBase.SelectedIndex = (-1)
            MyBase.SelectedValue = Nothing
            MyBase.SelectionStart = 0
          End If

          ' Debug.Print("  Exit (3) 'thisKeyPressed()'")
          Exit Sub
        End If

        Dim IndexString As String

        Try
          StartingIndex = (-1)
          While (StartingIndex < 0)

            StartingIndex = TopMatchingIndex(SelectString)

            ' Debug.Print("  StartingIndex= " & StartingIndex.ToString)

            If (StartingIndex >= 0) Then
              IndexString = ThisRadComboBox.Items(StartingIndex).Text

              If (ThisRadComboBox.SelectedIndex <> StartingIndex) Then
                ThisRadComboBox.SelectedIndex = StartingIndex
              Else
                ThisRadComboBox.Text = IndexString
              End If


              ThisRadComboBox.SelectionStart = SelectString.Length
              ThisRadComboBox.SelectionLength = IndexString.Length - SelectString.Length

              ' Debug.Print("  IndexString = " & IndexString)
              ' Debug.Print("  SelectString = " & SelectString)
              ' Debug.Print("  ThisRadComboBox.Text = " & ThisRadComboBox.Text)
              ' Debug.Print("  SelectionStart = " & Me.SelectionStart)
              ' Debug.Print("  SelectionLength = " & Me.SelectionLength)
              ' Debug.Print("  SelectString.Length = " & SelectString.Length.ToString)
              ' Debug.Print("  SelectionLength(Set) = " & (IndexString.Length - SelectString.Length).ToString)
              ' Debug.Print("  Exit (4) 'thisKeyPressed()'")
              Exit Sub
            End If

            ' If _noMatch is true, allow entries not in the Items collection.
            If (Me._SelectNoMatch) Then
              MyBase.SelectedIndex = (-1)
              MyBase.SelectedValue = Nothing
              MyBase.SelectionStart = SelectString.Length
              Exit While
            End If

            SelectString = SelectString.Substring(0, SelectString.Length - 1)

            If SelectString.Length <= 0 Then
              If ThisRadComboBox.Items.Count > 0 Then
                ThisRadComboBox.SelectedIndex = 0
                ThisRadComboBox.SelectionStart = 0
                ThisRadComboBox.SelectionLength = ThisRadComboBox.Text.Length
              Else
                MyBase.SelectedIndex = (-1)
                MyBase.SelectedValue = Nothing
                MyBase.SelectionStart = SelectString.Length
              End If

              ' Debug.Print("  Exit (5) 'thisKeyPressed()'")
              Exit Sub
            End If

          End While

        Catch ex As Exception
        End Try

        'Debug.Print("  Exit 'thisKeyPressed()'")

      Catch ex As Exception
      End Try

		End If

	End Sub

	Private Function TopMatchingIndex(ByVal SelectString As String) As Integer

		Dim TopIndex As Integer = 0
		Dim BottomIndex As Integer = Me.Items.Count - 1

		Try
			Dim MidIndex As Integer
			Dim ThisString As String
			Dim CVal As Double

			While (BottomIndex - TopIndex) > 5
				If (BottomIndex - TopIndex) <= 2 Then
					MidIndex = TopIndex + 1
				Else
					MidIndex = CInt((TopIndex + BottomIndex) / 2)
				End If

				ThisString = Me.Items(MidIndex).Text
				CVal = ThisString.CompareTo(SelectString)

				If (CVal = 0) Then
					Return MidIndex
					Exit Function
				Else
					If (CVal > 0) Then
						BottomIndex = MidIndex
					Else
						TopIndex = MidIndex
					End If
				End If

			End While

		Catch ex As Exception
		End Try

		SelectString = SelectString.ToUpper & "*"
		While TopIndex <= BottomIndex
			If Me.Items(TopIndex).Text.ToUpper Like SelectString Then
				Return TopIndex
			End If

			TopIndex += 1
		End While

		Return -1

	End Function

End Class
