Imports System.Data.SqlClient

Public Class AdaptorHandler


  Public Function Set_AdaptorCommands(ByRef pConnection As SqlConnection, ByRef pAdaptor As SqlDataAdapter, ByVal pTableName As String) As Boolean

    Dim SelectCommand As New SqlCommand
    Dim InsertCommand As New SqlCommand
    Dim DeleteCommand As New SqlCommand
    Dim UpdateCommand As New SqlCommand

    SelectCommand.CommandTimeout = 600

    Select Case pTableName.ToUpper

      Case "VANILLA"
        pAdaptor.TableMappings.Clear()

      Case "PERFORMANCE", "TBLPERFORMANCE"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
         New System.Data.Common.DataTableMapping("Table", "Performance", _
         New System.Data.Common.DataColumnMapping() _
         {New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
         New System.Data.Common.DataColumnMapping("ID", "ID"), _
         New System.Data.Common.DataColumnMapping("PerformanceDate", "PerformanceDate"), _
         New System.Data.Common.DataColumnMapping("PerformanceReturn", "PerformanceReturn"), _
         New System.Data.Common.DataColumnMapping("FundsManaged", "FundsManaged"), _
         New System.Data.Common.DataColumnMapping("NAV", "NAV"), _
         New System.Data.Common.DataColumnMapping("Estimate", "Estimate"), _
         New System.Data.Common.DataColumnMapping("LastUpdated", "LastUpdated") _
        })})

        ' Set Select Command

        SelectCommand.CommandText = "[adp_tblPerformance_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
            New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
          New SqlParameter("@ID", System.Data.SqlDbType.Int)).Value = -1
        SelectCommand.Parameters.Add( _
            New SqlParameter("@DATE", System.Data.SqlDbType.DateTime)).Value = DBNull.Value
        SelectCommand.Parameters.Add( _
            New SqlParameter("@Group", System.Data.SqlDbType.NVarChar)).Value = ""

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPerformance_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Date", System.Data.SqlDbType.DateTime, 8, "PerformanceDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Return", System.Data.SqlDbType.Float, 8, "PerformanceReturn"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundsManaged", System.Data.SqlDbType.Float, 8, "FundsManaged"))
        InsertCommand.Parameters.Add(New SqlParameter("@NAV", System.Data.SqlDbType.Float, 8, "NAV"))
        InsertCommand.Parameters.Add(New SqlParameter("@Estimate", System.Data.SqlDbType.Float, 8, "Estimate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPerformance_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Date", System.Data.SqlDbType.DateTime, 8, "PerformanceDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Return", System.Data.SqlDbType.Float, 8, "PerformanceReturn"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundsManaged", System.Data.SqlDbType.Float, 8, "FundsManaged"))
        UpdateCommand.Parameters.Add(New SqlParameter("@NAV", System.Data.SqlDbType.Float, 8, "NAV"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Estimate", System.Data.SqlDbType.Float, 8, "Estimate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPerformance_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))
        DeleteCommand.Parameters.Add(New SqlParameter("@Date", System.Data.SqlDbType.DateTime, 8, "PerformanceDate"))


      Case "INSTRUMENTPERFORMANCE", "TBLINSTRUMENTPERFORMANCE"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "Performance", _
        New System.Data.Common.DataColumnMapping() _
        {New System.Data.Common.DataColumnMapping("ID", "ID"), _
        New System.Data.Common.DataColumnMapping("PerformanceDate", "PerformanceDate"), _
        New System.Data.Common.DataColumnMapping("PerformanceReturn", "PerformanceReturn"), _
        New System.Data.Common.DataColumnMapping("FundsManaged", "FundsManaged"), _
        New System.Data.Common.DataColumnMapping("NAV", "NAV"), _
        New System.Data.Common.DataColumnMapping("Estimate", "Estimate") _
        })})

        ' Set Select Command

        SelectCommand.CommandText = "[adp_tblInstrumentPerformance_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@ID", System.Data.SqlDbType.Int))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@DATE", System.Data.SqlDbType.DateTime))

        ' Leave Update, Insert & Delete commands.


      Case "MASTERNAME", "TBLMASTERNAME"
        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "Mastername", _
        New System.Data.Common.DataColumnMapping() _
        {New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ID", "ID"), _
        New System.Data.Common.DataColumnMapping("Mastername", "Mastername"), _
        New System.Data.Common.DataColumnMapping("DataPeriod", "DataPeriod"), _
        New System.Data.Common.DataColumnMapping("CaptureFromMorningstar", "CaptureFromMorningstar"), _
        New System.Data.Common.DataColumnMapping("IsUsed", "IsUsed") _
        })})

        ' Set Select Command

        SelectCommand.CommandText = "[adp_tblMastername_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@ID", System.Data.SqlDbType.Int))
        'SelectCommand.Parameters.Add( _
        'New SqlParameter("@Mastername", System.Data.SqlDbType.NVarChar))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblMastername_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Mastername", System.Data.SqlDbType.NVarChar, 255, "Mastername"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataPeriod", System.Data.SqlDbType.Int, 4, "DataPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@CaptureFromMorningstar", System.Data.SqlDbType.Bit, 1, "CaptureFromMorningstar"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsUsed", System.Data.SqlDbType.Bit, 1, "IsUsed"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblMastername_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Mastername", System.Data.SqlDbType.NVarChar, 255, "Mastername"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataPeriod", System.Data.SqlDbType.Int, 4, "DataPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CaptureFromMorningstar", System.Data.SqlDbType.Bit, 1, "CaptureFromMorningstar"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsUsed", System.Data.SqlDbType.Bit, 1, "IsUsed"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblMastername_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))


      Case "INFORMATION", "TBLINFORMATION"
        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "Information", _
        New System.Data.Common.DataColumnMapping() _
        {New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ID", "ID"), _
        New System.Data.Common.DataColumnMapping("F1", "F1"), _
        New System.Data.Common.DataColumnMapping("F2", "F2"), _
        New System.Data.Common.DataColumnMapping("F3", "F3"), _
        New System.Data.Common.DataColumnMapping("F4", "F4"), _
        New System.Data.Common.DataColumnMapping("F5", "F5"), _
        New System.Data.Common.DataColumnMapping("F6", "F6"), _
        New System.Data.Common.DataColumnMapping("F7", "F7"), _
        New System.Data.Common.DataColumnMapping("F8", "F8"), _
        New System.Data.Common.DataColumnMapping("F9", "F9"), _
        New System.Data.Common.DataColumnMapping("F10", "F10"), _
        New System.Data.Common.DataColumnMapping("F11", "F11"), _
        New System.Data.Common.DataColumnMapping("F12", "F12"), _
        New System.Data.Common.DataColumnMapping("F13", "F13"), _
        New System.Data.Common.DataColumnMapping("F14", "F14"), _
        New System.Data.Common.DataColumnMapping("F15", "F15"), _
        New System.Data.Common.DataColumnMapping("F16", "F16"), _
        New System.Data.Common.DataColumnMapping("F17", "F17"), _
        New System.Data.Common.DataColumnMapping("F18", "F18"), _
        New System.Data.Common.DataColumnMapping("F19", "F19"), _
        New System.Data.Common.DataColumnMapping("F20", "F20"), _
        New System.Data.Common.DataColumnMapping("F21", "F21"), _
        New System.Data.Common.DataColumnMapping("F22", "F22"), _
        New System.Data.Common.DataColumnMapping("F23", "F23"), _
        New System.Data.Common.DataColumnMapping("F24", "F24"), _
        New System.Data.Common.DataColumnMapping("F25", "F25"), _
        New System.Data.Common.DataColumnMapping("F26", "F26"), _
        New System.Data.Common.DataColumnMapping("F27", "F27"), _
        New System.Data.Common.DataColumnMapping("F28", "F28"), _
        New System.Data.Common.DataColumnMapping("F29", "F29"), _
        New System.Data.Common.DataColumnMapping("F30", "F30"), _
        New System.Data.Common.DataColumnMapping("F31", "F31"), _
        New System.Data.Common.DataColumnMapping("F32", "F32"), _
        New System.Data.Common.DataColumnMapping("F33", "F33"), _
        New System.Data.Common.DataColumnMapping("F34", "F34"), _
        New System.Data.Common.DataColumnMapping("F35", "F35"), _
        New System.Data.Common.DataColumnMapping("F36", "F36"), _
        New System.Data.Common.DataColumnMapping("F37", "F37"), _
        New System.Data.Common.DataColumnMapping("F38", "F38"), _
        New System.Data.Common.DataColumnMapping("F39", "F39"), _
        New System.Data.Common.DataColumnMapping("F40", "F40"), _
        New System.Data.Common.DataColumnMapping("F41", "F41"), _
        New System.Data.Common.DataColumnMapping("LastUpdated", "LastUpdated"), _
        New System.Data.Common.DataColumnMapping("DataVendorName", "DataVendorName"), _
        New System.Data.Common.DataColumnMapping("DataVendorID", "DataVendorID"), _
        New System.Data.Common.DataColumnMapping("Description", "Description"), _
        New System.Data.Common.DataColumnMapping("UserDescription", "UserDescription"), _
        New System.Data.Common.DataColumnMapping("C1", "C1"), _
        New System.Data.Common.DataColumnMapping("C2", "C2"), _
        New System.Data.Common.DataColumnMapping("C3", "C3"), _
        New System.Data.Common.DataColumnMapping("C4", "C4"), _
        New System.Data.Common.DataColumnMapping("C5", "C5"), _
        New System.Data.Common.DataColumnMapping("C6", "C6"), _
        New System.Data.Common.DataColumnMapping("C7", "C7"), _
        New System.Data.Common.DataColumnMapping("C8", "C8"), _
        New System.Data.Common.DataColumnMapping("C9", "C9"), _
        New System.Data.Common.DataColumnMapping("C10", "C10"), _
        New System.Data.Common.DataColumnMapping("C11", "C11"), _
        New System.Data.Common.DataColumnMapping("C12", "C12"), _
        New System.Data.Common.DataColumnMapping("C13", "C13"), _
        New System.Data.Common.DataColumnMapping("C14", "C14"), _
        New System.Data.Common.DataColumnMapping("C15", "C15"), _
        New System.Data.Common.DataColumnMapping("C16", "C16"), _
        New System.Data.Common.DataColumnMapping("C17", "C17"), _
        New System.Data.Common.DataColumnMapping("C18", "C18"), _
        New System.Data.Common.DataColumnMapping("C19", "C19"), _
        New System.Data.Common.DataColumnMapping("C20", "C20"), _
        New System.Data.Common.DataColumnMapping("C21", "C21"), _
        New System.Data.Common.DataColumnMapping("C22", "C22"), _
        New System.Data.Common.DataColumnMapping("C23", "C23"), _
        New System.Data.Common.DataColumnMapping("C24", "C24"), _
        New System.Data.Common.DataColumnMapping("C25", "C25"), _
        New System.Data.Common.DataColumnMapping("C26", "C26"), _
        New System.Data.Common.DataColumnMapping("C27", "C27"), _
        New System.Data.Common.DataColumnMapping("C28", "C28"), _
        New System.Data.Common.DataColumnMapping("C29", "C29"), _
        New System.Data.Common.DataColumnMapping("C30", "C30"), _
        New System.Data.Common.DataColumnMapping("C31", "C31"), _
        New System.Data.Common.DataColumnMapping("C32", "C32"), _
        New System.Data.Common.DataColumnMapping("C33", "C33"), _
        New System.Data.Common.DataColumnMapping("C34", "C34"), _
        New System.Data.Common.DataColumnMapping("C35", "C35"), _
        New System.Data.Common.DataColumnMapping("C36", "C36") _
        })})

        ' Set Select Command

        SelectCommand.CommandText = "[adp_tblInformation_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int))
        'SelectCommand.Parameters.Add( _
        'New SqlParameter("@Description", System.Data.SqlDbType.NVarChar))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblInformation_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))
        InsertCommand.Parameters.Add(New SqlParameter("@F1", System.Data.SqlDbType.NVarChar, 255, "F1"))
        InsertCommand.Parameters.Add(New SqlParameter("@F2", System.Data.SqlDbType.NVarChar, 255, "F2"))
        InsertCommand.Parameters.Add(New SqlParameter("@F3", System.Data.SqlDbType.NVarChar, 255, "F3"))
        InsertCommand.Parameters.Add(New SqlParameter("@F4", System.Data.SqlDbType.NVarChar, 255, "F4"))
        InsertCommand.Parameters.Add(New SqlParameter("@F5", System.Data.SqlDbType.NVarChar, 255, "F5"))
        InsertCommand.Parameters.Add(New SqlParameter("@F6", System.Data.SqlDbType.NVarChar, 255, "F6"))
        InsertCommand.Parameters.Add(New SqlParameter("@F7", System.Data.SqlDbType.NVarChar, 255, "F7"))
        InsertCommand.Parameters.Add(New SqlParameter("@F8", System.Data.SqlDbType.NVarChar, 255, "F8"))
        InsertCommand.Parameters.Add(New SqlParameter("@F9", System.Data.SqlDbType.NVarChar, 255, "F9"))
        InsertCommand.Parameters.Add(New SqlParameter("@F10", System.Data.SqlDbType.NVarChar, 255, "F10"))
        InsertCommand.Parameters.Add(New SqlParameter("@F11", System.Data.SqlDbType.NVarChar, 255, "F11"))
        InsertCommand.Parameters.Add(New SqlParameter("@F12", System.Data.SqlDbType.NVarChar, 255, "F12"))
        InsertCommand.Parameters.Add(New SqlParameter("@F13", System.Data.SqlDbType.NVarChar, 255, "F13"))
        InsertCommand.Parameters.Add(New SqlParameter("@F14", System.Data.SqlDbType.NVarChar, 255, "F14"))
        InsertCommand.Parameters.Add(New SqlParameter("@F15", System.Data.SqlDbType.NVarChar, 255, "F15"))
        InsertCommand.Parameters.Add(New SqlParameter("@F16", System.Data.SqlDbType.NVarChar, 255, "F16"))
        InsertCommand.Parameters.Add(New SqlParameter("@F17", System.Data.SqlDbType.NVarChar, 255, "F17"))
        InsertCommand.Parameters.Add(New SqlParameter("@F18", System.Data.SqlDbType.NVarChar, 255, "F18"))
        InsertCommand.Parameters.Add(New SqlParameter("@F19", System.Data.SqlDbType.NVarChar, 255, "F19"))
        InsertCommand.Parameters.Add(New SqlParameter("@F20", System.Data.SqlDbType.NVarChar, 255, "F20"))
        InsertCommand.Parameters.Add(New SqlParameter("@F21", System.Data.SqlDbType.NVarChar, 255, "F21"))
        InsertCommand.Parameters.Add(New SqlParameter("@F22", System.Data.SqlDbType.NVarChar, 255, "F22"))
        InsertCommand.Parameters.Add(New SqlParameter("@F23", System.Data.SqlDbType.NVarChar, 255, "F23"))
        InsertCommand.Parameters.Add(New SqlParameter("@F24", System.Data.SqlDbType.NVarChar, 255, "F24"))
        InsertCommand.Parameters.Add(New SqlParameter("@F25", System.Data.SqlDbType.NVarChar, 255, "F25"))
        InsertCommand.Parameters.Add(New SqlParameter("@F26", System.Data.SqlDbType.NVarChar, 255, "F26"))
        InsertCommand.Parameters.Add(New SqlParameter("@F27", System.Data.SqlDbType.NVarChar, 255, "F27"))
        InsertCommand.Parameters.Add(New SqlParameter("@F28", System.Data.SqlDbType.NVarChar, 255, "F28"))
        InsertCommand.Parameters.Add(New SqlParameter("@F29", System.Data.SqlDbType.NVarChar, 255, "F29"))
        InsertCommand.Parameters.Add(New SqlParameter("@F30", System.Data.SqlDbType.NVarChar, 255, "F30"))
        InsertCommand.Parameters.Add(New SqlParameter("@F31", System.Data.SqlDbType.NVarChar, 255, "F31"))
        InsertCommand.Parameters.Add(New SqlParameter("@F32", System.Data.SqlDbType.NVarChar, 255, "F32"))
        InsertCommand.Parameters.Add(New SqlParameter("@F33", System.Data.SqlDbType.NVarChar, 255, "F33"))
        InsertCommand.Parameters.Add(New SqlParameter("@F34", System.Data.SqlDbType.NVarChar, 255, "F34"))
        InsertCommand.Parameters.Add(New SqlParameter("@F35", System.Data.SqlDbType.NVarChar, 255, "F35"))
        InsertCommand.Parameters.Add(New SqlParameter("@F36", System.Data.SqlDbType.NVarChar, 255, "F36"))
        InsertCommand.Parameters.Add(New SqlParameter("@F37", System.Data.SqlDbType.NVarChar, 255, "F37"))
        InsertCommand.Parameters.Add(New SqlParameter("@F38", System.Data.SqlDbType.NVarChar, 255, "F38"))
        InsertCommand.Parameters.Add(New SqlParameter("@F39", System.Data.SqlDbType.NVarChar, 255, "F39"))
        InsertCommand.Parameters.Add(New SqlParameter("@F40", System.Data.SqlDbType.NVarChar, 255, "F40"))
        InsertCommand.Parameters.Add(New SqlParameter("@F41", System.Data.SqlDbType.NVarChar, 255, "F41"))
        InsertCommand.Parameters.Add(New SqlParameter("@C1", System.Data.SqlDbType.Bit, 1, "C1"))
        InsertCommand.Parameters.Add(New SqlParameter("@C2", System.Data.SqlDbType.Bit, 1, "C2"))
        InsertCommand.Parameters.Add(New SqlParameter("@C3", System.Data.SqlDbType.Bit, 1, "C3"))
        InsertCommand.Parameters.Add(New SqlParameter("@C4", System.Data.SqlDbType.Bit, 1, "C4"))
        InsertCommand.Parameters.Add(New SqlParameter("@C5", System.Data.SqlDbType.Bit, 1, "C5"))
        InsertCommand.Parameters.Add(New SqlParameter("@C6", System.Data.SqlDbType.Bit, 1, "C6"))
        InsertCommand.Parameters.Add(New SqlParameter("@C7", System.Data.SqlDbType.Bit, 1, "C7"))
        InsertCommand.Parameters.Add(New SqlParameter("@C8", System.Data.SqlDbType.Bit, 1, "C8"))
        InsertCommand.Parameters.Add(New SqlParameter("@C9", System.Data.SqlDbType.Bit, 1, "C9"))
        InsertCommand.Parameters.Add(New SqlParameter("@C10", System.Data.SqlDbType.Bit, 1, "C10"))
        InsertCommand.Parameters.Add(New SqlParameter("@C11", System.Data.SqlDbType.Bit, 1, "C11"))
        InsertCommand.Parameters.Add(New SqlParameter("@C12", System.Data.SqlDbType.Bit, 1, "C12"))
        InsertCommand.Parameters.Add(New SqlParameter("@C13", System.Data.SqlDbType.Bit, 1, "C13"))
        InsertCommand.Parameters.Add(New SqlParameter("@C14", System.Data.SqlDbType.Bit, 1, "C14"))
        InsertCommand.Parameters.Add(New SqlParameter("@C15", System.Data.SqlDbType.Bit, 1, "C15"))
        InsertCommand.Parameters.Add(New SqlParameter("@C16", System.Data.SqlDbType.Bit, 1, "C16"))
        InsertCommand.Parameters.Add(New SqlParameter("@C17", System.Data.SqlDbType.Bit, 1, "C17"))
        InsertCommand.Parameters.Add(New SqlParameter("@C18", System.Data.SqlDbType.Bit, 1, "C18"))
        InsertCommand.Parameters.Add(New SqlParameter("@C19", System.Data.SqlDbType.Bit, 1, "C19"))
        InsertCommand.Parameters.Add(New SqlParameter("@C20", System.Data.SqlDbType.Bit, 1, "C20"))
        InsertCommand.Parameters.Add(New SqlParameter("@C21", System.Data.SqlDbType.Bit, 1, "C21"))
        InsertCommand.Parameters.Add(New SqlParameter("@C22", System.Data.SqlDbType.Bit, 1, "C22"))
        InsertCommand.Parameters.Add(New SqlParameter("@C23", System.Data.SqlDbType.Bit, 1, "C23"))
        InsertCommand.Parameters.Add(New SqlParameter("@C24", System.Data.SqlDbType.Bit, 1, "C24"))
        InsertCommand.Parameters.Add(New SqlParameter("@C25", System.Data.SqlDbType.Bit, 1, "C25"))
        InsertCommand.Parameters.Add(New SqlParameter("@C26", System.Data.SqlDbType.Bit, 1, "C26"))
        InsertCommand.Parameters.Add(New SqlParameter("@C27", System.Data.SqlDbType.Bit, 1, "C27"))
        InsertCommand.Parameters.Add(New SqlParameter("@C28", System.Data.SqlDbType.Bit, 1, "C28"))
        InsertCommand.Parameters.Add(New SqlParameter("@C29", System.Data.SqlDbType.Bit, 1, "C29"))
        InsertCommand.Parameters.Add(New SqlParameter("@C30", System.Data.SqlDbType.Bit, 1, "C30"))
        InsertCommand.Parameters.Add(New SqlParameter("@C31", System.Data.SqlDbType.Bit, 1, "C31"))
        InsertCommand.Parameters.Add(New SqlParameter("@C32", System.Data.SqlDbType.Bit, 1, "C32"))
        InsertCommand.Parameters.Add(New SqlParameter("@C33", System.Data.SqlDbType.Bit, 1, "C33"))
        InsertCommand.Parameters.Add(New SqlParameter("@C34", System.Data.SqlDbType.Bit, 1, "C34"))
        InsertCommand.Parameters.Add(New SqlParameter("@C35", System.Data.SqlDbType.Bit, 1, "C35"))
        InsertCommand.Parameters.Add(New SqlParameter("@C36", System.Data.SqlDbType.Bit, 1, "C36"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataVendorName", System.Data.SqlDbType.NVarChar, 80, "DataVendorName"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataVendorID", System.Data.SqlDbType.NVarChar, 80, "DataVendorID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.NText, Int32.MaxValue, "Description"))
        InsertCommand.Parameters.Add(New SqlParameter("@UserDescription", System.Data.SqlDbType.NText, Int32.MaxValue, "UserDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblInformation_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F1", System.Data.SqlDbType.NVarChar, 255, "F1"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F2", System.Data.SqlDbType.NVarChar, 255, "F2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F3", System.Data.SqlDbType.NVarChar, 255, "F3"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F4", System.Data.SqlDbType.NVarChar, 255, "F4"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F5", System.Data.SqlDbType.NVarChar, 255, "F5"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F6", System.Data.SqlDbType.NVarChar, 255, "F6"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F7", System.Data.SqlDbType.NVarChar, 255, "F7"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F8", System.Data.SqlDbType.NVarChar, 255, "F8"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F9", System.Data.SqlDbType.NVarChar, 255, "F9"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F10", System.Data.SqlDbType.NVarChar, 255, "F10"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F11", System.Data.SqlDbType.NVarChar, 255, "F11"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F12", System.Data.SqlDbType.NVarChar, 255, "F12"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F13", System.Data.SqlDbType.NVarChar, 255, "F13"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F14", System.Data.SqlDbType.NVarChar, 255, "F14"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F15", System.Data.SqlDbType.NVarChar, 255, "F15"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F16", System.Data.SqlDbType.NVarChar, 255, "F16"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F17", System.Data.SqlDbType.NVarChar, 255, "F17"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F18", System.Data.SqlDbType.NVarChar, 255, "F18"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F19", System.Data.SqlDbType.NVarChar, 255, "F19"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F20", System.Data.SqlDbType.NVarChar, 255, "F20"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F21", System.Data.SqlDbType.NVarChar, 255, "F21"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F22", System.Data.SqlDbType.NVarChar, 255, "F22"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F23", System.Data.SqlDbType.NVarChar, 255, "F23"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F24", System.Data.SqlDbType.NVarChar, 255, "F24"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F25", System.Data.SqlDbType.NVarChar, 255, "F25"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F26", System.Data.SqlDbType.NVarChar, 255, "F26"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F27", System.Data.SqlDbType.NVarChar, 255, "F27"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F28", System.Data.SqlDbType.NVarChar, 255, "F28"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F29", System.Data.SqlDbType.NVarChar, 255, "F29"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F30", System.Data.SqlDbType.NVarChar, 255, "F30"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F31", System.Data.SqlDbType.NVarChar, 255, "F31"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F32", System.Data.SqlDbType.NVarChar, 255, "F32"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F33", System.Data.SqlDbType.NVarChar, 255, "F33"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F34", System.Data.SqlDbType.NVarChar, 255, "F34"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F35", System.Data.SqlDbType.NVarChar, 255, "F35"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F36", System.Data.SqlDbType.NVarChar, 255, "F36"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F37", System.Data.SqlDbType.NVarChar, 255, "F37"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F38", System.Data.SqlDbType.NVarChar, 255, "F38"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F39", System.Data.SqlDbType.NVarChar, 255, "F39"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F40", System.Data.SqlDbType.NVarChar, 255, "F40"))
        UpdateCommand.Parameters.Add(New SqlParameter("@F41", System.Data.SqlDbType.NVarChar, 255, "F41"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C1", System.Data.SqlDbType.Bit, 1, "C1"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C2", System.Data.SqlDbType.Bit, 1, "C2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C3", System.Data.SqlDbType.Bit, 1, "C3"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C4", System.Data.SqlDbType.Bit, 1, "C4"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C5", System.Data.SqlDbType.Bit, 1, "C5"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C6", System.Data.SqlDbType.Bit, 1, "C6"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C7", System.Data.SqlDbType.Bit, 1, "C7"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C8", System.Data.SqlDbType.Bit, 1, "C8"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C9", System.Data.SqlDbType.Bit, 1, "C9"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C10", System.Data.SqlDbType.Bit, 1, "C10"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C11", System.Data.SqlDbType.Bit, 1, "C11"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C12", System.Data.SqlDbType.Bit, 1, "C12"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C13", System.Data.SqlDbType.Bit, 1, "C13"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C14", System.Data.SqlDbType.Bit, 1, "C14"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C15", System.Data.SqlDbType.Bit, 1, "C15"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C16", System.Data.SqlDbType.Bit, 1, "C16"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C17", System.Data.SqlDbType.Bit, 1, "C17"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C18", System.Data.SqlDbType.Bit, 1, "C18"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C19", System.Data.SqlDbType.Bit, 1, "C19"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C20", System.Data.SqlDbType.Bit, 1, "C20"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C21", System.Data.SqlDbType.Bit, 1, "C21"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C22", System.Data.SqlDbType.Bit, 1, "C22"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C23", System.Data.SqlDbType.Bit, 1, "C23"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C24", System.Data.SqlDbType.Bit, 1, "C24"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C25", System.Data.SqlDbType.Bit, 1, "C25"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C26", System.Data.SqlDbType.Bit, 1, "C26"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C27", System.Data.SqlDbType.Bit, 1, "C27"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C28", System.Data.SqlDbType.Bit, 1, "C28"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C29", System.Data.SqlDbType.Bit, 1, "C29"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C30", System.Data.SqlDbType.Bit, 1, "C30"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C31", System.Data.SqlDbType.Bit, 1, "C31"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C32", System.Data.SqlDbType.Bit, 1, "C32"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C33", System.Data.SqlDbType.Bit, 1, "C33"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C34", System.Data.SqlDbType.Bit, 1, "C34"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C35", System.Data.SqlDbType.Bit, 1, "C35"))
        UpdateCommand.Parameters.Add(New SqlParameter("@C36", System.Data.SqlDbType.Bit, 1, "C36"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataVendorName", System.Data.SqlDbType.NVarChar, 80, "DataVendorName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataVendorID", System.Data.SqlDbType.NVarChar, 80, "DataVendorID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.NText, Int32.MaxValue, "Description"))
        UpdateCommand.Parameters.Add(New SqlParameter("@UserDescription", System.Data.SqlDbType.NText, Int32.MaxValue, "UserDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblInformation_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ID", System.Data.SqlDbType.Int, 4, "ID"))


			Case "TBLBENCHMARKINDEX"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblBenchmarkIndex", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("BenchmarkID", "BenchmarkID"), _
				New System.Data.Common.DataColumnMapping("BenchmarkDescription", "BenchmarkDescription"), _
				New System.Data.Common.DataColumnMapping("BenchmarkType", "BenchmarkType"), _
				New System.Data.Common.DataColumnMapping("BenchmarkCalculationMethod", "BenchmarkCalculationMethod"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblBenchmarkIndex_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblBenchmarkIndex_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkID", System.Data.SqlDbType.Int, 4, "BenchmarkID"))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkDescription", System.Data.SqlDbType.NVarChar, 100, "BenchmarkDescription"))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkType", System.Data.SqlDbType.Int, 4, "BenchmarkType"))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkCalculationMethod", System.Data.SqlDbType.Int, 4, "BenchmarkCalculationMethod"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand

				UpdateCommand.CommandText = "[adp_tblBenchmarkIndex_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkID", System.Data.SqlDbType.Int, 4, "BenchmarkID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkDescription", System.Data.SqlDbType.NVarChar, 100, "BenchmarkDescription"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkType", System.Data.SqlDbType.Int, 4, "BenchmarkType"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkCalculationMethod", System.Data.SqlDbType.Int, 4, "BenchmarkCalculationMethod"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblBenchmarkIndex_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@BenchmarkID", System.Data.SqlDbType.Int, 4, "BenchmarkID"))


			Case "TBLBENCHMARKITEMS"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblBenchmarkItems", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("BenchmarkItemID", "BenchmarkItemID"), _
				New System.Data.Common.DataColumnMapping("BenchmarkID", "BenchmarkID"), _
				New System.Data.Common.DataColumnMapping("BenchmarkItemType", "BenchmarkItemType"), _
				New System.Data.Common.DataColumnMapping("BenchmarkItemCalculationMethod", "BenchmarkItemCalculationMethod"), _
				New System.Data.Common.DataColumnMapping("BenchmarkInstrumentID", "BenchmarkInstrumentID"), _
				New System.Data.Common.DataColumnMapping("BenchmarkFixedRate", "BenchmarkFixedRate"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblBenchmarkItems_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblBenchmarkItems_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkItemID", System.Data.SqlDbType.Int, 4, "BenchmarkItemID"))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkID", System.Data.SqlDbType.Int, 4, "BenchmarkID"))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkItemType", System.Data.SqlDbType.Int, 4, "BenchmarkItemType"))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkItemCalculationMethod", System.Data.SqlDbType.Int, 4, "BenchmarkItemCalculationMethod"))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkInstrumentID", System.Data.SqlDbType.Int, 4, "BenchmarkInstrumentID"))
				InsertCommand.Parameters.Add(New SqlParameter("@BenchmarkFixedRate", System.Data.SqlDbType.Float, 8, "BenchmarkFixedRate"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand

				UpdateCommand.CommandText = "[adp_tblBenchmarkItems_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkItemID", System.Data.SqlDbType.Int, 4, "BenchmarkItemID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkID", System.Data.SqlDbType.Int, 4, "BenchmarkID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkItemType", System.Data.SqlDbType.Int, 4, "BenchmarkItemType"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkItemCalculationMethod", System.Data.SqlDbType.Int, 4, "BenchmarkItemCalculationMethod"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkInstrumentID", System.Data.SqlDbType.Int, 4, "BenchmarkInstrumentID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BenchmarkFixedRate", System.Data.SqlDbType.Float, 8, "BenchmarkFixedRate"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblBenchmarkItems_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@BenchmarkItemID", System.Data.SqlDbType.Int, 4, "BenchmarkItemID"))


      Case "TBLBESTFX"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblBestFX", _
        New System.Data.Common.DataColumnMapping() { _
        New System.Data.Common.DataColumnMapping("FXID", "FXID"), _
        New System.Data.Common.DataColumnMapping("FXCurrencyCode", "FXCurrencyCode"), _
        New System.Data.Common.DataColumnMapping("FXDate", "FXDate"), _
        New System.Data.Common.DataColumnMapping("FXRate", "FXRate") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblBestFX_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime))
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        SelectCommand.Parameters("@ValueDate").Value = CDate("1 Jan 1900")


      Case "TBLBESTMARKETPRICE"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblBestMarketPrice", _
        New System.Data.Common.DataColumnMapping() { _
        New System.Data.Common.DataColumnMapping("MarketInstrumentID", "MarketInstrumentID"), _
        New System.Data.Common.DataColumnMapping("PriceDate", "PriceDate"), _
        New System.Data.Common.DataColumnMapping("PriceDataType", "PriceDataType"), _
        New System.Data.Common.DataColumnMapping("PriceValue", "PriceValue"), _
        New System.Data.Common.DataColumnMapping("TextValue", "TextValue") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[spu_BestMarketPrice]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime))
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        SelectCommand.Parameters("@ValueDate").Value = CDate("1 Jan 1900")


      Case "TBLBOOKMARK"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblBookmark", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("BookmarkID", "BookmarkID"), _
        New System.Data.Common.DataColumnMapping("BookmarkUserID", "BookmarkUserID"), _
        New System.Data.Common.DataColumnMapping("BookmarkDescription", "BookmarkDescription"), _
        New System.Data.Common.DataColumnMapping("BookmarkDate", "BookmarkDate"), _
        New System.Data.Common.DataColumnMapping("BookmarkWholeDay", "BookmarkWholeDay"), _
        New System.Data.Common.DataColumnMapping("BookmarkVisibleToAll", "BookmarkVisibleToAll"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("Updatable", "Updatable"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblBookmark_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblBookmark_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@BookmarkID", System.Data.SqlDbType.Int, 4, "BookmarkID"))
        InsertCommand.Parameters.Add(New SqlParameter("@BookmarkUserID", System.Data.SqlDbType.Int, 4, "BookmarkUserID"))
        InsertCommand.Parameters.Add(New SqlParameter("@BookmarkDescription", System.Data.SqlDbType.NVarChar, 100, "BookmarkDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@BookmarkDate", System.Data.SqlDbType.DateTime, 8, "BookmarkDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@BookmarkWholeDay", System.Data.SqlDbType.Int, 4, "BookmarkWholeDay"))
        InsertCommand.Parameters.Add(New SqlParameter("@BookmarkVisibleToAll", System.Data.SqlDbType.Int, 4, "BookmarkVisibleToAll"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblBookmark_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@BookmarkID", System.Data.SqlDbType.Int, 4, "BookmarkID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BookmarkUserID", System.Data.SqlDbType.Int, 4, "BookmarkUserID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BookmarkDescription", System.Data.SqlDbType.NVarChar, 100, "BookmarkDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BookmarkDate", System.Data.SqlDbType.DateTime, 8, "BookmarkDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BookmarkWholeDay", System.Data.SqlDbType.Int, 4, "BookmarkWholeDay"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BookmarkVisibleToAll", System.Data.SqlDbType.Int, 4, "BookmarkVisibleToAll"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblBookmark_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@BookmarkID", System.Data.SqlDbType.Int, 4, "BookmarkID"))


			Case "TBLBROKER"

				pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblBroker", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("BrokerID", "BrokerID"), _
        New System.Data.Common.DataColumnMapping("BrokerDescription", "BrokerDescription"), _
        New System.Data.Common.DataColumnMapping("NLS_Culture", "NLS_Culture"), _
        New System.Data.Common.DataColumnMapping("NumericPrecision", "NumericPrecision"), _
        New System.Data.Common.DataColumnMapping("IsEMSX", "IsEMSX"), _
        New System.Data.Common.DataColumnMapping("EMSX_Broker_ID", "EMSX_Broker_ID"), _
        New System.Data.Common.DataColumnMapping("BIC_Code", "BIC_Code"), _
        New System.Data.Common.DataColumnMapping("ValidOrderTypes", "ValidOrderTypes"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblBroker_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblBroker_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@BrokerID", System.Data.SqlDbType.Int, 4, "BrokerID"))
				InsertCommand.Parameters.Add(New SqlParameter("@BrokerDescription", System.Data.SqlDbType.NVarChar, 200, "BrokerDescription"))
				InsertCommand.Parameters.Add(New SqlParameter("@NLS_Culture", System.Data.SqlDbType.NVarChar, 50, "NLS_Culture"))
				InsertCommand.Parameters.Add(New SqlParameter("@NumericPrecision", System.Data.SqlDbType.Int, 4, "NumericPrecision"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsEMSX", System.Data.SqlDbType.Bit, 1, "IsEMSX"))
        InsertCommand.Parameters.Add(New SqlParameter("@EMSX_Broker_ID", System.Data.SqlDbType.VarChar, 50, "EMSX_Broker_ID"))
        InsertCommand.Parameters.Add(New SqlParameter("@BIC_Code", System.Data.SqlDbType.NVarChar, 50, "BIC_Code"))
        InsertCommand.Parameters.Add(New SqlParameter("@ValidOrderTypes", System.Data.SqlDbType.NVarChar, 200, "ValidOrderTypes"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand

				UpdateCommand.CommandText = "[adp_tblBroker_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@BrokerID", System.Data.SqlDbType.Int, 4, "BrokerID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BrokerDescription", System.Data.SqlDbType.NVarChar, 200, "BrokerDescription"))
				UpdateCommand.Parameters.Add(New SqlParameter("@NLS_Culture", System.Data.SqlDbType.NVarChar, 50, "NLS_Culture"))
				UpdateCommand.Parameters.Add(New SqlParameter("@NumericPrecision", System.Data.SqlDbType.Int, 4, "NumericPrecision"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsEMSX", System.Data.SqlDbType.Bit, 1, "IsEMSX"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EMSX_Broker_ID", System.Data.SqlDbType.VarChar, 50, "EMSX_Broker_ID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BIC_Code", System.Data.SqlDbType.NVarChar, 50, "BIC_Code"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ValidOrderTypes", System.Data.SqlDbType.NVarChar, 200, "ValidOrderTypes"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblBroker_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@BrokerID", System.Data.SqlDbType.Int, 4, "BrokerID"))


			Case "TBLBROKERACCOUNTS"

				pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblBrokerAccounts", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("VeniceAccountID", "VeniceAccountID"), _
        New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
        New System.Data.Common.DataColumnMapping("InstrumentTypeID", "InstrumentTypeID"), _
        New System.Data.Common.DataColumnMapping("Exchange", "Exchange"), _
        New System.Data.Common.DataColumnMapping("CurrencyID", "CurrencyID"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("BrokerID", "BrokerID"), _
        New System.Data.Common.DataColumnMapping("GenerateAdminMatchingFile", "GenerateAdminMatchingFile"), _
        New System.Data.Common.DataColumnMapping("UseSimpleCommissionRate", "UseSimpleCommissionRate"), _
        New System.Data.Common.DataColumnMapping("CommissionRate", "CommissionRate"), _
        New System.Data.Common.DataColumnMapping("AccountDescription", "AccountDescription"), _
        New System.Data.Common.DataColumnMapping("AccountNumber", "AccountNumber"), _
        New System.Data.Common.DataColumnMapping("BrokerPriority", "BrokerPriority"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblBrokerAccounts_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblBrokerAccounts_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@VeniceAccountID", System.Data.SqlDbType.Int, 4, "VeniceAccountID"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentTypeID", System.Data.SqlDbType.Int, 4, "InstrumentTypeID"))
				InsertCommand.Parameters.Add(New SqlParameter("@Exchange", System.Data.SqlDbType.Int, 4, "Exchange"))
				InsertCommand.Parameters.Add(New SqlParameter("@CurrencyID", System.Data.SqlDbType.Int, 4, "CurrencyID"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				InsertCommand.Parameters.Add(New SqlParameter("@BrokerID", System.Data.SqlDbType.Int, 4, "BrokerID"))
        InsertCommand.Parameters.Add(New SqlParameter("@GenerateAdminMatchingFile", System.Data.SqlDbType.Bit, 1, "GenerateAdminMatchingFile"))
        InsertCommand.Parameters.Add(New SqlParameter("@UseSimpleCommissionRate", System.Data.SqlDbType.Bit, 1, "UseSimpleCommissionRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@CommissionRate", System.Data.SqlDbType.Float, 8, "CommissionRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@AccountDescription", System.Data.SqlDbType.NVarChar, 200, "AccountDescription"))
				InsertCommand.Parameters.Add(New SqlParameter("@AccountNumber", System.Data.SqlDbType.NVarChar, 50, "AccountNumber"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand

				UpdateCommand.CommandText = "[adp_tblBrokerAccounts_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@VeniceAccountID", System.Data.SqlDbType.Int, 4, "VeniceAccountID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentTypeID", System.Data.SqlDbType.Int, 4, "InstrumentTypeID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Exchange", System.Data.SqlDbType.Int, 4, "Exchange"))
				UpdateCommand.Parameters.Add(New SqlParameter("@CurrencyID", System.Data.SqlDbType.Int, 4, "CurrencyID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BrokerID", System.Data.SqlDbType.Int, 4, "BrokerID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GenerateAdminMatchingFile", System.Data.SqlDbType.Bit, 1, "GenerateAdminMatchingFile"))
        UpdateCommand.Parameters.Add(New SqlParameter("@UseSimpleCommissionRate", System.Data.SqlDbType.Bit, 1, "UseSimpleCommissionRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CommissionRate", System.Data.SqlDbType.Float, 8, "CommissionRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AccountDescription", System.Data.SqlDbType.NVarChar, 200, "AccountDescription"))
				UpdateCommand.Parameters.Add(New SqlParameter("@AccountNumber", System.Data.SqlDbType.NVarChar, 50, "AccountNumber"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblBrokerAccounts_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@VeniceAccountID", System.Data.SqlDbType.Int, 4, "VeniceAccountID"))


			Case "TBLCHANGECONTROL"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblChangeControl", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("ChangeID", "ChangeID"), _
				New System.Data.Common.DataColumnMapping("Product", "Product"), _
				New System.Data.Common.DataColumnMapping("Date_Raised", "Date_Raised"), _
				New System.Data.Common.DataColumnMapping("User_Raised", "User_Raised"), _
				New System.Data.Common.DataColumnMapping("Change_Category", "Change_Category"), _
				New System.Data.Common.DataColumnMapping("Change_Priority", "Change_Priority"), _
				New System.Data.Common.DataColumnMapping("Change_ReferenceNumber", "Change_ReferenceNumber"), _
				New System.Data.Common.DataColumnMapping("Text_Change", "Text_Change"), _
				New System.Data.Common.DataColumnMapping("Text_Detail", "Text_Detail"), _
				New System.Data.Common.DataColumnMapping("Text_ScopeOfChange", "Text_ScopeOfChange"), _
				New System.Data.Common.DataColumnMapping("Text_Dependencies", "Text_Dependencies"), _
				New System.Data.Common.DataColumnMapping("Text_Restrictions", "Text_Restrictions"), _
				New System.Data.Common.DataColumnMapping("Text_ImplementationPlan", "Text_ImplementationPlan"), _
				New System.Data.Common.DataColumnMapping("Text_TestPlan", "Text_TestPlan"), _
				New System.Data.Common.DataColumnMapping("Date_ReviewedIT", "Date_ReviewedIT"), _
				New System.Data.Common.DataColumnMapping("Date_ReviewRejected", "Date_ReviewRejected"), _
				New System.Data.Common.DataColumnMapping("Date_AuthorisedIT", "Date_AuthorisedIT"), _
				New System.Data.Common.DataColumnMapping("Date_AuthorisedOwner", "Date_AuthorisedOwner"), _
				New System.Data.Common.DataColumnMapping("Date_AuthorisedBusiness", "Date_AuthorisedBusiness"), _
				New System.Data.Common.DataColumnMapping("Date_AuthoriseRejected", "Date_AuthoriseRejected"), _
				New System.Data.Common.DataColumnMapping("Date_AcceptedIT", "Date_AcceptedIT"), _
				New System.Data.Common.DataColumnMapping("Date_AcceptedOwner", "Date_AcceptedOwner"), _
				New System.Data.Common.DataColumnMapping("Date_AcceptedBusiness", "Date_AcceptedBusiness"), _
				New System.Data.Common.DataColumnMapping("User_ReviewedIT", "User_ReviewedIT"), _
				New System.Data.Common.DataColumnMapping("User_ReviewRejected", "User_ReviewRejected"), _
				New System.Data.Common.DataColumnMapping("User_AuthorisedIT", "User_AuthorisedIT"), _
				New System.Data.Common.DataColumnMapping("User_AuthorisedOwner", "User_AuthorisedOwner"), _
				New System.Data.Common.DataColumnMapping("User_AuthorisedBusiness", "User_AuthorisedBusiness"), _
				New System.Data.Common.DataColumnMapping("User_AuthoriseRejected", "User_AuthoriseRejected"), _
				New System.Data.Common.DataColumnMapping("User_AcceptedIT", "User_AcceptedIT"), _
				New System.Data.Common.DataColumnMapping("User_AcceptedOwner", "User_AcceptedOwner"), _
				New System.Data.Common.DataColumnMapping("User_AcceptedBusiness", "User_AcceptedBusiness"), _
				New System.Data.Common.DataColumnMapping("Flag_ReviewedIT", "Flag_ReviewedIT"), _
				New System.Data.Common.DataColumnMapping("Flag_ReviewRejected", "Flag_ReviewRejected"), _
				New System.Data.Common.DataColumnMapping("Flag_AuthorisedIT", "Flag_AuthorisedIT"), _
				New System.Data.Common.DataColumnMapping("Flag_AuthorisedOwner", "Flag_AuthorisedOwner"), _
				New System.Data.Common.DataColumnMapping("Flag_AuthorisedBusiness", "Flag_AuthorisedBusiness"), _
				New System.Data.Common.DataColumnMapping("Flag_AuthoriseRejected", "Flag_AuthoriseRejected"), _
				New System.Data.Common.DataColumnMapping("Flag_AcceptedIT", "Flag_AcceptedIT"), _
				New System.Data.Common.DataColumnMapping("Flag_AcceptedOwner", "Flag_AcceptedOwner"), _
				New System.Data.Common.DataColumnMapping("Flag_AcceptedBusiness", "Flag_AcceptedBusiness"), _
				New System.Data.Common.DataColumnMapping("appCurrentUser", "appCurrentUser"), _
				New System.Data.Common.DataColumnMapping("Status", "Status"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblChangeControl_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblChangeControl_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@ChangeID", System.Data.SqlDbType.Int, 4, "ChangeID"))
				InsertCommand.Parameters.Add(New SqlParameter("@Product", System.Data.SqlDbType.NVarChar, 50, "Product"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_Raised", System.Data.SqlDbType.DateTime, 8, "Date_Raised"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_Raised", System.Data.SqlDbType.NVarChar, 50, "User_Raised"))
				InsertCommand.Parameters.Add(New SqlParameter("@Change_Category", System.Data.SqlDbType.NVarChar, 50, "Change_Category"))
				InsertCommand.Parameters.Add(New SqlParameter("@Change_Priority", System.Data.SqlDbType.Int, 4, "Change_Priority"))
				InsertCommand.Parameters.Add(New SqlParameter("@Change_ReferenceNumber", System.Data.SqlDbType.NVarChar, 50, "Change_ReferenceNumber"))
				InsertCommand.Parameters.Add(New SqlParameter("@Text_Change", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_Change"))
				InsertCommand.Parameters.Add(New SqlParameter("@Text_Detail", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_Detail"))
				InsertCommand.Parameters.Add(New SqlParameter("@Text_ScopeOfChange", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_ScopeOfChange"))
				InsertCommand.Parameters.Add(New SqlParameter("@Text_Dependencies", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_Dependencies"))
				InsertCommand.Parameters.Add(New SqlParameter("@Text_Restrictions", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_Restrictions"))
				InsertCommand.Parameters.Add(New SqlParameter("@Text_ImplementationPlan", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_ImplementationPlan"))
				InsertCommand.Parameters.Add(New SqlParameter("@Text_TestPlan", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_TestPlan"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_ReviewedIT", System.Data.SqlDbType.DateTime, 8, "Date_ReviewedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_ReviewRejected", System.Data.SqlDbType.DateTime, 8, "Date_ReviewRejected"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_AuthorisedIT", System.Data.SqlDbType.DateTime, 8, "Date_AuthorisedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_AuthorisedOwner", System.Data.SqlDbType.DateTime, 8, "Date_AuthorisedOwner"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_AuthorisedBusiness", System.Data.SqlDbType.DateTime, 8, "Date_AuthorisedBusiness"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_AuthoriseRejected", System.Data.SqlDbType.DateTime, 8, "Date_AuthoriseRejected"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_AcceptedIT", System.Data.SqlDbType.DateTime, 8, "Date_AcceptedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_AcceptedOwner", System.Data.SqlDbType.DateTime, 8, "Date_AcceptedOwner"))
				InsertCommand.Parameters.Add(New SqlParameter("@Date_AcceptedBusiness", System.Data.SqlDbType.DateTime, 8, "Date_AcceptedBusiness"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_ReviewedIT", System.Data.SqlDbType.NVarChar, 50, "User_ReviewedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_ReviewRejected", System.Data.SqlDbType.NVarChar, 50, "User_ReviewRejected"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_AuthorisedIT", System.Data.SqlDbType.NVarChar, 50, "User_AuthorisedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_AuthorisedOwner", System.Data.SqlDbType.NVarChar, 50, "User_AuthorisedOwner"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_AuthorisedBusiness", System.Data.SqlDbType.NVarChar, 50, "User_AuthorisedBusiness"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_AuthoriseRejected", System.Data.SqlDbType.NVarChar, 50, "User_AuthoriseRejected"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_AcceptedIT", System.Data.SqlDbType.NVarChar, 50, "User_AcceptedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_AcceptedOwner", System.Data.SqlDbType.NVarChar, 50, "User_AcceptedOwner"))
				InsertCommand.Parameters.Add(New SqlParameter("@User_AcceptedBusiness", System.Data.SqlDbType.NVarChar, 50, "User_AcceptedBusiness"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_ReviewedIT", System.Data.SqlDbType.Bit, 1, "Flag_ReviewedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_ReviewRejected", System.Data.SqlDbType.Bit, 1, "Flag_ReviewRejected"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_AuthorisedIT", System.Data.SqlDbType.Bit, 1, "Flag_AuthorisedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_AuthorisedOwner", System.Data.SqlDbType.Bit, 1, "Flag_AuthorisedOwner"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_AuthorisedBusiness", System.Data.SqlDbType.Bit, 1, "Flag_AuthorisedBusiness"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_AuthoriseRejected", System.Data.SqlDbType.Bit, 1, "Flag_AuthoriseRejected"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_AcceptedIT", System.Data.SqlDbType.Bit, 1, "Flag_AcceptedIT"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_AcceptedOwner", System.Data.SqlDbType.Bit, 1, "Flag_AcceptedOwner"))
				InsertCommand.Parameters.Add(New SqlParameter("@Flag_AcceptedBusiness", System.Data.SqlDbType.Bit, 1, "Flag_AcceptedBusiness"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblChangeControl_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@ChangeID", System.Data.SqlDbType.Int, 4, "ChangeID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Product", System.Data.SqlDbType.NVarChar, 50, "Product"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_Raised", System.Data.SqlDbType.DateTime, 8, "Date_Raised"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_Raised", System.Data.SqlDbType.NVarChar, 50, "User_Raised"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Change_Category", System.Data.SqlDbType.NVarChar, 50, "Change_Category"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Change_Priority", System.Data.SqlDbType.Int, 4, "Change_Priority"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Change_ReferenceNumber", System.Data.SqlDbType.NVarChar, 50, "Change_ReferenceNumber"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Text_Change", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_Change"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Text_Detail", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_Detail"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Text_ScopeOfChange", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_ScopeOfChange"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Text_Dependencies", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_Dependencies"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Text_Restrictions", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_Restrictions"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Text_ImplementationPlan", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_ImplementationPlan"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Text_TestPlan", System.Data.SqlDbType.Text, Int32.MaxValue, "Text_TestPlan"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_ReviewedIT", System.Data.SqlDbType.DateTime, 8, "Date_ReviewedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_ReviewRejected", System.Data.SqlDbType.DateTime, 8, "Date_ReviewRejected"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_AuthorisedIT", System.Data.SqlDbType.DateTime, 8, "Date_AuthorisedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_AuthorisedOwner", System.Data.SqlDbType.DateTime, 8, "Date_AuthorisedOwner"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_AuthorisedBusiness", System.Data.SqlDbType.DateTime, 8, "Date_AuthorisedBusiness"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_AuthoriseRejected", System.Data.SqlDbType.DateTime, 8, "Date_AuthoriseRejected"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_AcceptedIT", System.Data.SqlDbType.DateTime, 8, "Date_AcceptedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_AcceptedOwner", System.Data.SqlDbType.DateTime, 8, "Date_AcceptedOwner"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Date_AcceptedBusiness", System.Data.SqlDbType.DateTime, 8, "Date_AcceptedBusiness"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_ReviewedIT", System.Data.SqlDbType.NVarChar, 50, "User_ReviewedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_ReviewRejected", System.Data.SqlDbType.NVarChar, 50, "User_ReviewRejected"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_AuthorisedIT", System.Data.SqlDbType.NVarChar, 50, "User_AuthorisedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_AuthorisedOwner", System.Data.SqlDbType.NVarChar, 50, "User_AuthorisedOwner"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_AuthorisedBusiness", System.Data.SqlDbType.NVarChar, 50, "User_AuthorisedBusiness"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_AuthoriseRejected", System.Data.SqlDbType.NVarChar, 50, "User_AuthoriseRejected"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_AcceptedIT", System.Data.SqlDbType.NVarChar, 50, "User_AcceptedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_AcceptedOwner", System.Data.SqlDbType.NVarChar, 50, "User_AcceptedOwner"))
				UpdateCommand.Parameters.Add(New SqlParameter("@User_AcceptedBusiness", System.Data.SqlDbType.NVarChar, 50, "User_AcceptedBusiness"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_ReviewedIT", System.Data.SqlDbType.Bit, 1, "Flag_ReviewedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_ReviewRejected", System.Data.SqlDbType.Bit, 1, "Flag_ReviewRejected"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_AuthorisedIT", System.Data.SqlDbType.Bit, 1, "Flag_AuthorisedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_AuthorisedOwner", System.Data.SqlDbType.Bit, 1, "Flag_AuthorisedOwner"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_AuthorisedBusiness", System.Data.SqlDbType.Bit, 1, "Flag_AuthorisedBusiness"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_AuthoriseRejected", System.Data.SqlDbType.Bit, 1, "Flag_AuthoriseRejected"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_AcceptedIT", System.Data.SqlDbType.Bit, 1, "Flag_AcceptedIT"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_AcceptedOwner", System.Data.SqlDbType.Bit, 1, "Flag_AcceptedOwner"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Flag_AcceptedBusiness", System.Data.SqlDbType.Bit, 1, "Flag_AcceptedBusiness"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblChangeControl_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@ChangeID", System.Data.SqlDbType.Int, 4, "CurrencyID"))


      Case "TBLCOMPOUNDTRANSACTION" ' 

        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblCompoundTransaction", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("AdminStatus", "AdminStatus"), _
				New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
				New System.Data.Common.DataColumnMapping("TransactionCompoundID", "TransactionCompoundID"), _
				New System.Data.Common.DataColumnMapping("TransactionTemplateID", "TransactionTemplateID"), _
				New System.Data.Common.DataColumnMapping("TransactionTicket", "TransactionTicket"), _
				New System.Data.Common.DataColumnMapping("TransactionFund", "TransactionFund"), _
				New System.Data.Common.DataColumnMapping("TransactionInstrument", "TransactionInstrument"), _
				New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
				New System.Data.Common.DataColumnMapping("TransactionValueorAmount", "TransactionValueorAmount"), _
				New System.Data.Common.DataColumnMapping("TransactionUnits", "TransactionUnits"), _
				New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
				New System.Data.Common.DataColumnMapping("TransactionCosts", "TransactionCosts"), _
				New System.Data.Common.DataColumnMapping("TransactionCostPercent", "TransactionCostPercent"), _
				New System.Data.Common.DataColumnMapping("TransactionEffectivePrice", "TransactionEffectivePrice"), _
				New System.Data.Common.DataColumnMapping("TransactionEffectiveWatermark", "TransactionEffectiveWatermark"), _
				New System.Data.Common.DataColumnMapping("TransactionSpecificInitialEqualisationValue", "TransactionSpecificInitialEqualisationValue"), _
				New System.Data.Common.DataColumnMapping("TransactionCounterparty", "TransactionCounterparty"), _
				New System.Data.Common.DataColumnMapping("TransactionInvestment", "TransactionInvestment"), _
				New System.Data.Common.DataColumnMapping("TransactionExecution", "TransactionExecution"), _
				New System.Data.Common.DataColumnMapping("TransactionDataEntry", "TransactionDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionDecisionDate", "TransactionDecisionDate"), _
				New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
				New System.Data.Common.DataColumnMapping("TransactionFIFOValueDate", "TransactionFIFOValueDate"), _
				New System.Data.Common.DataColumnMapping("TransactionEffectiveValueDate", "TransactionEffectiveValueDate"), _
				New System.Data.Common.DataColumnMapping("TransactionSettlementDate", "TransactionSettlementDate"), _
				New System.Data.Common.DataColumnMapping("TransactionConfirmationDate", "TransactionConfirmationDate"), _
				New System.Data.Common.DataColumnMapping("TransactionCostIsPercent", "TransactionCostIsPercent"), _
				New System.Data.Common.DataColumnMapping("TransactionExemptFromUpdate", "TransactionExemptFromUpdate"), _
				New System.Data.Common.DataColumnMapping("TransactionIsTransfer", "TransactionIsTransfer"), _
				New System.Data.Common.DataColumnMapping("TransactionSpecificInitialEqualisationFlag", "TransactionSpecificInitialEqualisationFlag"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalAmount", "TransactionFinalAmount"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalPrice", "TransactionFinalPrice"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalCosts", "TransactionFinalCosts"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalSettlement", "TransactionFinalSettlement"), _
				New System.Data.Common.DataColumnMapping("TransactionInstructionFlag", "TransactionInstructionFlag"), _
				New System.Data.Common.DataColumnMapping("TransactionBankConfirmation", "TransactionBankConfirmation"), _
				New System.Data.Common.DataColumnMapping("TransactionInitialDataEntry", "TransactionInitialDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionAmountConfirmed", "TransactionAmountConfirmed"), _
				New System.Data.Common.DataColumnMapping("TransactionSettlementConfirmed", "TransactionSettlementConfirmed"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalDataEntry", "TransactionFinalDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionTradeStatusID", "TransactionTradeStatusID"), _
				New System.Data.Common.DataColumnMapping("TransactionComment", "TransactionComment"), _
				New System.Data.Common.DataColumnMapping("TransactionGroup", "TransactionGroup"), _
				New System.Data.Common.DataColumnMapping("ParameterCount", "ParameterCount"), _
				New System.Data.Common.DataColumnMapping("Parameter1_Name", "Parameter1_Name"), _
				New System.Data.Common.DataColumnMapping("Parameter1_Type", "Parameter1_Type"), _
				New System.Data.Common.DataColumnMapping("Parameter1_Value", "Parameter1_Value"), _
				New System.Data.Common.DataColumnMapping("Parameter2_Name", "Parameter2_Name"), _
				New System.Data.Common.DataColumnMapping("Parameter2_Type", "Parameter2_Type"), _
				New System.Data.Common.DataColumnMapping("Parameter2_Value", "Parameter2_Value"), _
				New System.Data.Common.DataColumnMapping("Parameter3_Name", "Parameter3_Name"), _
				New System.Data.Common.DataColumnMapping("Parameter3_Type", "Parameter3_Type"), _
				New System.Data.Common.DataColumnMapping("Parameter3_Value", "Parameter3_Value"), _
				New System.Data.Common.DataColumnMapping("Parameter4_Name", "Parameter4_Name"), _
				New System.Data.Common.DataColumnMapping("Parameter4_Type", "Parameter4_Type"), _
				New System.Data.Common.DataColumnMapping("Parameter4_Value", "Parameter4_Value"), _
				New System.Data.Common.DataColumnMapping("Parameter5_Name", "Parameter5_Name"), _
				New System.Data.Common.DataColumnMapping("Parameter5_Type", "Parameter5_Type"), _
				New System.Data.Common.DataColumnMapping("Parameter5_Value", "Parameter5_Value"), _
				New System.Data.Common.DataColumnMapping("CompoundTransactionGroup", "CompoundTransactionGroup"), _
				New System.Data.Common.DataColumnMapping("CompoundTransactionComment", "CompoundTransactionComment"), _
				New System.Data.Common.DataColumnMapping("CompoundTransactionFlags", "CompoundTransactionFlags"), _
				New System.Data.Common.DataColumnMapping("CompoundTransactionPosted", "CompoundTransactionPosted"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblCompoundTransaction_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblCompoundTransaction_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminStatus", System.Data.SqlDbType.Int, 4, "AdminStatus"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCompoundID", System.Data.SqlDbType.Int, 4, "TransactionCompoundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionTemplateID", System.Data.SqlDbType.Int, 4, "TransactionTemplateID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionTicket", System.Data.SqlDbType.NVarChar, 50, "TransactionTicket"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFund", System.Data.SqlDbType.Int, 4, "TransactionFund"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionInstrument", System.Data.SqlDbType.Int, 4, "TransactionInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionValueorAmount", System.Data.SqlDbType.NVarChar, 10, "TransactionValueorAmount"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionUnits", System.Data.SqlDbType.Float, 8, "TransactionUnits"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCosts", System.Data.SqlDbType.Float, 8, "TransactionCosts"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCostPercent", System.Data.SqlDbType.Float, 8, "TransactionCostPercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectivePrice", System.Data.SqlDbType.Float, 8, "TransactionEffectivePrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveWatermark", System.Data.SqlDbType.Float, 8, "TransactionEffectiveWatermark"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationValue", System.Data.SqlDbType.Float, 8, "TransactionSpecificInitialEqualisationValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCounterparty", System.Data.SqlDbType.Int, 4, "TransactionCounterparty"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionInvestment", System.Data.SqlDbType.NVarChar, 50, "TransactionInvestment"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionExecution", System.Data.SqlDbType.NVarChar, 50, "TransactionExecution"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionDataEntry", System.Data.SqlDbType.NVarChar, 50, "TransactionDataEntry"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionDecisionDate", System.Data.SqlDbType.DateTime, 8, "TransactionDecisionDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFIFOValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionFIFOValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionEffectiveValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSettlementDate", System.Data.SqlDbType.DateTime, 8, "TransactionSettlementDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionConfirmationDate", System.Data.SqlDbType.DateTime, 8, "TransactionConfirmationDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCostIsPercent", System.Data.SqlDbType.Bit, 1, "TransactionCostIsPercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionExemptFromUpdate", System.Data.SqlDbType.Bit, 1, "TransactionExemptFromUpdate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionIsTransfer", System.Data.SqlDbType.Bit, 1, "TransactionIsTransfer"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationFlag", System.Data.SqlDbType.Bit, 1, "TransactionSpecificInitialEqualisationFlag"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalAmount", System.Data.SqlDbType.Int, 4, "TransactionFinalAmount"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalPrice", System.Data.SqlDbType.Int, 4, "TransactionFinalPrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalCosts", System.Data.SqlDbType.Int, 4, "TransactionFinalCosts"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalSettlement", System.Data.SqlDbType.Int, 4, "TransactionFinalSettlement"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionInstructionFlag", System.Data.SqlDbType.Int, 4, "TransactionInstructionFlag"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionBankConfirmation", System.Data.SqlDbType.Int, 4, "TransactionBankConfirmation"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionInitialDataEntry", System.Data.SqlDbType.Int, 4, "TransactionInitialDataEntry"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionAmountConfirmed", System.Data.SqlDbType.Int, 4, "TransactionAmountConfirmed"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSettlementConfirmed", System.Data.SqlDbType.Int, 4, "TransactionSettlementConfirmed"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalDataEntry", System.Data.SqlDbType.Int, 4, "TransactionFinalDataEntry"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionTradeStatusID", System.Data.SqlDbType.Int, 4, "TransactionTradeStatusID"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionComment", System.Data.SqlDbType.NVarChar, 500, "TransactionComment"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionGroup", System.Data.SqlDbType.NVarChar, 50, "TransactionGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@ParameterCount", System.Data.SqlDbType.Int, 4, "ParameterCount"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter1_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter1_Name"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter1_Type", System.Data.SqlDbType.Int, 4, "Parameter1_Type"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter1_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter1_Value"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter2_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter2_Name"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter2_Type", System.Data.SqlDbType.Int, 4, "Parameter2_Type"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter2_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter2_Value"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter3_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter3_Name"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter3_Type", System.Data.SqlDbType.Int, 4, "Parameter3_Type"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter3_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter3_Value"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter4_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter4_Name"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter4_Type", System.Data.SqlDbType.Int, 4, "Parameter4_Type"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter4_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter4_Value"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter5_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter5_Name"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter5_Type", System.Data.SqlDbType.Int, 4, "Parameter5_Type"))
        InsertCommand.Parameters.Add(New SqlParameter("@Parameter5_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter5_Value"))
        InsertCommand.Parameters.Add(New SqlParameter("@CompoundTransactionGroup", System.Data.SqlDbType.Int, 4, "CompoundTransactionGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@CompoundTransactionComment", System.Data.SqlDbType.NVarChar, 50, "CompoundTransactionComment"))
        InsertCommand.Parameters.Add(New SqlParameter("@CompoundTransactionFlags", System.Data.SqlDbType.Int, 4, "CompoundTransactionFlags"))
        InsertCommand.Parameters.Add(New SqlParameter("@CompoundTransactionPosted", System.Data.SqlDbType.Int, 4, "CompoundTransactionPosted"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))
        InsertCommand.Parameters.Add(New SqlParameter("@rvStatusString", System.Data.SqlDbType.NVarChar, 200, System.Data.ParameterDirection.Output, True, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))


        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblCompoundTransaction_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminStatus", System.Data.SqlDbType.Int, 4, "AdminStatus"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCompoundID", System.Data.SqlDbType.Int, 4, "TransactionCompoundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionTemplateID", System.Data.SqlDbType.Int, 4, "TransactionTemplateID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionTicket", System.Data.SqlDbType.NVarChar, 50, "TransactionTicket"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFund", System.Data.SqlDbType.Int, 4, "TransactionFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionInstrument", System.Data.SqlDbType.Int, 4, "TransactionInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionValueorAmount", System.Data.SqlDbType.NVarChar, 10, "TransactionValueorAmount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionUnits", System.Data.SqlDbType.Float, 8, "TransactionUnits"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCosts", System.Data.SqlDbType.Float, 8, "TransactionCosts"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCostPercent", System.Data.SqlDbType.Float, 8, "TransactionCostPercent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCounterparty", System.Data.SqlDbType.Int, 4, "TransactionCounterparty"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionEffectivePrice", System.Data.SqlDbType.Float, 8, "TransactionEffectivePrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveWatermark", System.Data.SqlDbType.Float, 8, "TransactionEffectiveWatermark"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationValue", System.Data.SqlDbType.Float, 8, "TransactionSpecificInitialEqualisationValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionInvestment", System.Data.SqlDbType.NVarChar, 50, "TransactionInvestment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionExecution", System.Data.SqlDbType.NVarChar, 50, "TransactionExecution"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionDataEntry", System.Data.SqlDbType.NVarChar, 50, "TransactionDataEntry"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionDecisionDate", System.Data.SqlDbType.DateTime, 8, "TransactionDecisionDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFIFOValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionFIFOValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionEffectiveValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSettlementDate", System.Data.SqlDbType.DateTime, 8, "TransactionSettlementDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionConfirmationDate", System.Data.SqlDbType.DateTime, 8, "TransactionConfirmationDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCostIsPercent", System.Data.SqlDbType.Bit, 1, "TransactionCostIsPercent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionExemptFromUpdate", System.Data.SqlDbType.Bit, 1, "TransactionExemptFromUpdate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionIsTransfer", System.Data.SqlDbType.Bit, 1, "TransactionIsTransfer"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationFlag", System.Data.SqlDbType.Bit, 1, "TransactionSpecificInitialEqualisationFlag"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalAmount", System.Data.SqlDbType.Int, 4, "TransactionFinalAmount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalPrice", System.Data.SqlDbType.Int, 4, "TransactionFinalPrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalCosts", System.Data.SqlDbType.Int, 4, "TransactionFinalCosts"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalSettlement", System.Data.SqlDbType.Int, 4, "TransactionFinalSettlement"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionInstructionFlag", System.Data.SqlDbType.Int, 4, "TransactionInstructionFlag"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionBankConfirmation", System.Data.SqlDbType.Int, 4, "TransactionBankConfirmation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionInitialDataEntry", System.Data.SqlDbType.Int, 4, "TransactionInitialDataEntry"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionAmountConfirmed", System.Data.SqlDbType.Int, 4, "TransactionAmountConfirmed"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSettlementConfirmed", System.Data.SqlDbType.Int, 4, "TransactionSettlementConfirmed"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalDataEntry", System.Data.SqlDbType.Int, 4, "TransactionFinalDataEntry"))
				UpdateCommand.Parameters.Add(New SqlParameter("@TransactionTradeStatusID", System.Data.SqlDbType.Int, 4, "TransactionTradeStatusID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@TransactionComment", System.Data.SqlDbType.NVarChar, 500, "TransactionComment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionGroup", System.Data.SqlDbType.NVarChar, 50, "TransactionGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ParameterCount", System.Data.SqlDbType.Int, 4, "ParameterCount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter1_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter1_Name"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter1_Type", System.Data.SqlDbType.Int, 4, "Parameter1_Type"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter1_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter1_Value"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter2_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter2_Name"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter2_Type", System.Data.SqlDbType.Int, 4, "Parameter2_Type"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter2_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter2_Value"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter3_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter3_Name"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter3_Type", System.Data.SqlDbType.Int, 4, "Parameter3_Type"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter3_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter3_Value"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter4_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter4_Name"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter4_Type", System.Data.SqlDbType.Int, 4, "Parameter4_Type"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter4_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter4_Value"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter5_Name", System.Data.SqlDbType.NVarChar, 50, "Parameter5_Name"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter5_Type", System.Data.SqlDbType.Int, 4, "Parameter5_Type"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Parameter5_Value", System.Data.SqlDbType.NVarChar, 50, "Parameter5_Value"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CompoundTransactionGroup", System.Data.SqlDbType.Int, 4, "CompoundTransactionGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CompoundTransactionComment", System.Data.SqlDbType.NVarChar, 50, "CompoundTransactionComment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CompoundTransactionFlags", System.Data.SqlDbType.Int, 4, "CompoundTransactionFlags"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CompoundTransactionPosted", System.Data.SqlDbType.Int, 4, "CompoundTransactionPosted"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))
        UpdateCommand.Parameters.Add(New SqlParameter("@rvStatusString", System.Data.SqlDbType.NVarChar, 200, System.Data.ParameterDirection.Output, True, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))


        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblCompoundTransaction_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))


      Case "TBLCOUNTERPARTY"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblCounterparty", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("CounterpartyId", "CounterpartyId"), _
        New System.Data.Common.DataColumnMapping("CounterpartyName", "CounterpartyName"), _
        New System.Data.Common.DataColumnMapping("CounterpartyIsMarketCounterparty", "CounterpartyIsMarketCounterparty"), _
        New System.Data.Common.DataColumnMapping("CounterpartyPFPCID", "CounterpartyPFPCID"), _
        New System.Data.Common.DataColumnMapping("CounterpartyInvestorGroup", "CounterpartyInvestorGroup"), _
        New System.Data.Common.DataColumnMapping("CounterpartyManagementFee", "CounterpartyManagementFee"), _
        New System.Data.Common.DataColumnMapping("CounterpartyPerformanceFee", "CounterpartyPerformanceFee"), _
        New System.Data.Common.DataColumnMapping("CounterpartyPerformanceFeeThreshold", "CounterpartyPerformanceFeeThreshold"), _
        New System.Data.Common.DataColumnMapping("Fee_ManagementEquityRebate", "Fee_ManagementEquityRebate"), _
        New System.Data.Common.DataColumnMapping("Fee_ManagementDebtRebate", "Fee_ManagementDebtRebate"), _
        New System.Data.Common.DataColumnMapping("Fee_PerformanceEquityRebate", "Fee_PerformanceEquityRebate"), _
        New System.Data.Common.DataColumnMapping("Fee_PerformanceDebtRebate", "Fee_PerformanceDebtRebate"), _
        New System.Data.Common.DataColumnMapping("Fee_ManagementMarketingToFCAM", "Fee_ManagementMarketingToFCAM"), _
        New System.Data.Common.DataColumnMapping("Fee_PerformanceMarketingToFCAM", "Fee_PerformanceMarketingToFCAM"), _
        New System.Data.Common.DataColumnMapping("Fee_ManagementMarketingToOthers", "Fee_ManagementMarketingToOthers"), _
        New System.Data.Common.DataColumnMapping("Fee_PerformanceMarketingToOthers", "Fee_PerformanceMarketingToOthers"), _
        New System.Data.Common.DataColumnMapping("CounterpartyFundID", "CounterpartyFundID"), _
        New System.Data.Common.DataColumnMapping("CounterpartyLeverageProvider", "CounterpartyLeverageProvider"), _
        New System.Data.Common.DataColumnMapping("CounterpartyAddress1", "CounterpartyAddress1"), _
        New System.Data.Common.DataColumnMapping("CounterpartyAddress2", "CounterpartyAddress2"), _
        New System.Data.Common.DataColumnMapping("CounterpartyAddress3", "CounterpartyAddress3"), _
        New System.Data.Common.DataColumnMapping("CounterpartyAddress4", "CounterpartyAddress4"), _
        New System.Data.Common.DataColumnMapping("CounterpartyAddress5", "CounterpartyAddress5"), _
        New System.Data.Common.DataColumnMapping("CounterpartyPostCode", "CounterpartyPostCode"), _
        New System.Data.Common.DataColumnMapping("CounterpartyContactName", "CounterpartyContactName"), _
        New System.Data.Common.DataColumnMapping("CounterpartyAccountName", "CounterpartyAccountName"), _
        New System.Data.Common.DataColumnMapping("CounterpartyEMail", "CounterpartyEMail"), _
        New System.Data.Common.DataColumnMapping("CounterpartyPhoneNumber", "CounterpartyPhoneNumber"), _
        New System.Data.Common.DataColumnMapping("CounterpartyFaxNumber", "CounterpartyFaxNumber"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblCounterparty_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblCounterparty_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyID", System.Data.SqlDbType.Int, 4, "CounterpartyID"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyName", System.Data.SqlDbType.NVarChar, 100, "CounterpartyName"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyIsMarketCounterparty", System.Data.SqlDbType.Bit, 1, "CounterpartyIsMarketCounterparty"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyPFPCID", System.Data.SqlDbType.NVarChar, 50, "CounterpartyPFPCID"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyInvestorGroup", System.Data.SqlDbType.Int, 4, "CounterpartyInvestorGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyManagementFee", System.Data.SqlDbType.Float, 8, "CounterpartyManagementFee"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyPerformanceFee", System.Data.SqlDbType.Float, 8, "CounterpartyPerformanceFee"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyPerformanceFeeThreshold", System.Data.SqlDbType.Float, 8, "CounterpartyPerformanceFeeThreshold"))
        InsertCommand.Parameters.Add(New SqlParameter("@Fee_ManagementEquityRebate", System.Data.SqlDbType.Float, 8, "Fee_ManagementEquityRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Fee_ManagementDebtRebate", System.Data.SqlDbType.Float, 8, "Fee_ManagementDebtRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Fee_PerformanceEquityRebate", System.Data.SqlDbType.Float, 8, "Fee_PerformanceEquityRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Fee_PerformanceDebtRebate", System.Data.SqlDbType.Float, 8, "Fee_PerformanceDebtRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Fee_ManagementMarketingToFCAM", System.Data.SqlDbType.Float, 8, "Fee_ManagementMarketingToFCAM"))
        InsertCommand.Parameters.Add(New SqlParameter("@Fee_PerformanceMarketingToFCAM", System.Data.SqlDbType.Float, 8, "Fee_PerformanceMarketingToFCAM"))
        InsertCommand.Parameters.Add(New SqlParameter("@Fee_ManagementMarketingToOthers", System.Data.SqlDbType.Float, 8, "Fee_ManagementMarketingToOthers"))
        InsertCommand.Parameters.Add(New SqlParameter("@Fee_PerformanceMarketingToOthers", System.Data.SqlDbType.Float, 8, "Fee_PerformanceMarketingToOthers"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyFundID", System.Data.SqlDbType.Int, 4, "CounterpartyFundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyLeverageProvider", System.Data.SqlDbType.Int, 4, "CounterpartyLeverageProvider"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress1", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress1"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress2", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress2"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress3", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress3"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress4", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress4"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress5", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress5"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyPostCode", System.Data.SqlDbType.NVarChar, 50, "CounterpartyPostCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyContactName", System.Data.SqlDbType.NVarChar, 100, "CounterpartyContactName"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyAccountName", System.Data.SqlDbType.NVarChar, 100, "CounterpartyAccountName"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyEmail", System.Data.SqlDbType.NVarChar, 50, "CounterpartyEmail"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyPhoneNumber", System.Data.SqlDbType.NVarChar, 50, "CounterpartyPhoneNumber"))
        InsertCommand.Parameters.Add(New SqlParameter("@CounterpartyFaxNumber", System.Data.SqlDbType.NVarChar, 50, "CounterpartyFaxNumber"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblCounterparty_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyID", System.Data.SqlDbType.Int, 4, "CounterpartyID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyName", System.Data.SqlDbType.NVarChar, 100, "CounterpartyName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyIsMarketCounterparty", System.Data.SqlDbType.Bit, 1, "CounterpartyIsMarketCounterparty"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyPFPCID", System.Data.SqlDbType.NVarChar, 50, "CounterpartyPFPCID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyInvestorGroup", System.Data.SqlDbType.Int, 4, "CounterpartyInvestorGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyManagementFee", System.Data.SqlDbType.Float, 8, "CounterpartyManagementFee"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyPerformanceFee", System.Data.SqlDbType.Float, 8, "CounterpartyPerformanceFee"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyPerformanceFeeThreshold", System.Data.SqlDbType.Float, 8, "CounterpartyPerformanceFeeThreshold"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Fee_ManagementEquityRebate", System.Data.SqlDbType.Float, 8, "Fee_ManagementEquityRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Fee_ManagementDebtRebate", System.Data.SqlDbType.Float, 8, "Fee_ManagementDebtRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Fee_PerformanceEquityRebate", System.Data.SqlDbType.Float, 8, "Fee_PerformanceEquityRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Fee_PerformanceDebtRebate", System.Data.SqlDbType.Float, 8, "Fee_PerformanceDebtRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Fee_ManagementMarketingToFCAM", System.Data.SqlDbType.Float, 8, "Fee_ManagementMarketingToFCAM"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Fee_PerformanceMarketingToFCAM", System.Data.SqlDbType.Float, 8, "Fee_PerformanceMarketingToFCAM"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Fee_ManagementMarketingToOthers", System.Data.SqlDbType.Float, 8, "Fee_ManagementMarketingToOthers"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Fee_PerformanceMarketingToOthers", System.Data.SqlDbType.Float, 8, "Fee_PerformanceMarketingToOthers"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyFundID", System.Data.SqlDbType.Int, 4, "CounterpartyFundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyLeverageProvider", System.Data.SqlDbType.Int, 4, "CounterpartyLeverageProvider"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress1", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress1"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress2", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress3", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress3"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress4", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress4"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyAddress5", System.Data.SqlDbType.NVarChar, 50, "CounterpartyAddress5"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyPostCode", System.Data.SqlDbType.NVarChar, 50, "CounterpartyPostCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyContactName", System.Data.SqlDbType.NVarChar, 100, "CounterpartyContactName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyAccountName", System.Data.SqlDbType.NVarChar, 100, "CounterpartyAccountName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyEmail", System.Data.SqlDbType.NVarChar, 50, "CounterpartyEmail"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyPhoneNumber", System.Data.SqlDbType.NVarChar, 50, "CounterpartyPhoneNumber"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CounterpartyFaxNumber", System.Data.SqlDbType.NVarChar, 50, "CounterpartyFaxNumber"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblCounterparty_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@CounterpartyID", System.Data.SqlDbType.Int, 4, "CounterpartyID"))


      Case "TBLCOVARIANCE"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblCovariance", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("CovID", "CovID"), _
        New System.Data.Common.DataColumnMapping("CovListID", "CovListID"), _
        New System.Data.Common.DataColumnMapping("PertracID1", "PertracID1"), _
        New System.Data.Common.DataColumnMapping("PertracID2", "PertracID2"), _
        New System.Data.Common.DataColumnMapping("Covariance", "Covariance"), _
        New System.Data.Common.DataColumnMapping("Correlation", "Correlation"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblCovariance_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblCovariance_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@CovID", System.Data.SqlDbType.Int, 4, "CovID"))
        InsertCommand.Parameters.Add(New SqlParameter("@CovListID", System.Data.SqlDbType.Int, 4, "CovListID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PertracID1", System.Data.SqlDbType.Int, 4, "PertracID1"))
        InsertCommand.Parameters.Add(New SqlParameter("@PertracID2", System.Data.SqlDbType.Int, 4, "PertracID2"))
        InsertCommand.Parameters.Add(New SqlParameter("@Covariance", System.Data.SqlDbType.Float, 8, "Covariance"))
        InsertCommand.Parameters.Add(New SqlParameter("@Correlation", System.Data.SqlDbType.Float, 8, "Correlation"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblCovariance_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@CovID", System.Data.SqlDbType.Int, 4, "CovID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CovListID", System.Data.SqlDbType.Int, 4, "CovListID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PertracID1", System.Data.SqlDbType.Int, 4, "PertracID1"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PertracID2", System.Data.SqlDbType.Int, 4, "PertracID2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Covariance", System.Data.SqlDbType.Float, 8, "Covariance"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Correlation", System.Data.SqlDbType.Float, 8, "Correlation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblCovariance_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@CovID", System.Data.SqlDbType.Int, 4, "CovID"))


      Case "TBLCOVARIANCELIST"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblCovarianceList", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("CovListID", "CovListID"), _
        New System.Data.Common.DataColumnMapping("ListName", "ListName"), _
        New System.Data.Common.DataColumnMapping("GroupListID", "GroupListID"), _
        New System.Data.Common.DataColumnMapping("DateFrom", "DateFrom"), _
        New System.Data.Common.DataColumnMapping("DateTo", "DateTo"), _
        New System.Data.Common.DataColumnMapping("Lamda", "Lamda"), _
        New System.Data.Common.DataColumnMapping("DataPeriod", "DataPeriod"), _
        New System.Data.Common.DataColumnMapping("IsAnnualised", "IsAnnualised"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblCovarianceList_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblCovarianceList_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@CovListID", System.Data.SqlDbType.Int, 4, "CovListID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ListName", System.Data.SqlDbType.NVarChar, 100, "ListName"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupListID", System.Data.SqlDbType.Int, 4, "GroupListID"))
        InsertCommand.Parameters.Add(New SqlParameter("@DateFrom", System.Data.SqlDbType.DateTime, 8, "DateFrom"))
        InsertCommand.Parameters.Add(New SqlParameter("@DateTo", System.Data.SqlDbType.DateTime, 8, "DateTo"))
        InsertCommand.Parameters.Add(New SqlParameter("@Lamda", System.Data.SqlDbType.Float, 8, "Lamda"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataPeriod", System.Data.SqlDbType.Int, 4, "DataPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsAnnualised", System.Data.SqlDbType.Bit, 1, "IsAnnualised"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblCovarianceList_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@CovListID", System.Data.SqlDbType.Int, 4, "CovListID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ListName", System.Data.SqlDbType.NVarChar, 100, "ListName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupListID", System.Data.SqlDbType.Int, 4, "GroupListID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DateFrom", System.Data.SqlDbType.DateTime, 8, "DateFrom"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DateTo", System.Data.SqlDbType.DateTime, 8, "DateTo"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Lamda", System.Data.SqlDbType.Float, 8, "Lamda"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataPeriod", System.Data.SqlDbType.Int, 4, "DataPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsAnnualised", System.Data.SqlDbType.Bit, 1, "IsAnnualised"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblCovarianceList_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@CovListID", System.Data.SqlDbType.Int, 4, "CovListID"))


      Case "TBLCTA_SIMULATION"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblCTA_Simulation", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("SimulationID", "SimulationID"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("InterestRateID", "InterestRateID"), _
        New System.Data.Common.DataColumnMapping("SimulationName", "SimulationName"), _
        New System.Data.Common.DataColumnMapping("SimulationFolder", "SimulationFolder"), _
        New System.Data.Common.DataColumnMapping("SimulationDefinition", "SimulationDefinition"), _
        New System.Data.Common.DataColumnMapping("SimulationUser", "SimulationUser"), _
        New System.Data.Common.DataColumnMapping("SimulationDateEntered", "SimulationDateEntered"), _
        New System.Data.Common.DataColumnMapping("SimulationDateDeleted", "SimulationDateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblCTA_Simulation_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblCTA_Simulation_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@SimulationID", System.Data.SqlDbType.Int, 4, "SimulationID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InterestRateID", System.Data.SqlDbType.Int, 4, "InterestRateID"))
        InsertCommand.Parameters.Add(New SqlParameter("@SimulationName", System.Data.SqlDbType.NVarChar, 200, "SimulationName"))
        InsertCommand.Parameters.Add(New SqlParameter("@SimulationFolder", System.Data.SqlDbType.Int, 4, "SimulationFolder"))
        InsertCommand.Parameters.Add(New SqlParameter("@SimulationDataPeriod", System.Data.SqlDbType.Int, 4, "SimulationDataPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@SimulationDefinition", System.Data.SqlDbType.Text, Int32.MaxValue, "SimulationDefinition"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblCTA_Simulation_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@SimulationID", System.Data.SqlDbType.Int, 4, "SimulationID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InterestRateID", System.Data.SqlDbType.Int, 4, "InterestRateID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SimulationName", System.Data.SqlDbType.NVarChar, 200, "SimulationName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SimulationFolder", System.Data.SqlDbType.Int, 4, "SimulationFolder"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SimulationDataPeriod", System.Data.SqlDbType.Int, 4, "SimulationDataPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SimulationDefinition", System.Data.SqlDbType.Text, Int32.MaxValue, "SimulationDefinition"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblCTA_Simulation_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@SimulationID", System.Data.SqlDbType.Int, 4, "SimulationID"))


      Case "TBLCTA_FOLDERS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblCTA_Folders", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FolderID", "FolderID"), _
        New System.Data.Common.DataColumnMapping("ParentFolderId", "ParentFolderId"), _
        New System.Data.Common.DataColumnMapping("FolderName", "FolderName"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblCTA_Folders_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblCTA_Folders_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FolderID", System.Data.SqlDbType.Int, 4, "FolderID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ParentFolderId", System.Data.SqlDbType.Int, 4, "ParentFolderId"))
        InsertCommand.Parameters.Add(New SqlParameter("@FolderName", System.Data.SqlDbType.NVarChar, 50, "FolderName"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblCTA_Folders_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FolderID", System.Data.SqlDbType.Int, 4, "FolderID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ParentFolderId", System.Data.SqlDbType.Int, 4, "ParentFolderId"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FolderName", System.Data.SqlDbType.NVarChar, 50, "FolderName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblCTA_Folders_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FolderID", System.Data.SqlDbType.Int, 4, "FolderID"))


      Case "TBLCURRENCY"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblCurrency", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("CurrencyID", "CurrencyID"), _
        New System.Data.Common.DataColumnMapping("CurrencyCashInstrumentID", "CurrencyCashInstrumentID"), _
        New System.Data.Common.DataColumnMapping("CurrencyFuturesNominalInstrumentID", "CurrencyFuturesNominalInstrumentID"), _
        New System.Data.Common.DataColumnMapping("DefaultExpenseInstrument", "DefaultExpenseInstrument"), _
        New System.Data.Common.DataColumnMapping("DefaultFeeInstrument", "DefaultFeeInstrument"), _
        New System.Data.Common.DataColumnMapping("DefaultBrokerageExpense", "DefaultBrokerageExpense"), _
        New System.Data.Common.DataColumnMapping("DefaultRetrocessionInstrument", "DefaultRetrocessionInstrument"), _
        New System.Data.Common.DataColumnMapping("DefaultInitialMarginInstrument", "DefaultInitialMarginInstrument"), _
        New System.Data.Common.DataColumnMapping("DefaultMovementCommissionInstrument", "DefaultMovementCommissionInstrument"), _
        New System.Data.Common.DataColumnMapping("FXForwardRiskInstrument", "FXForwardRiskInstrument"), _
        New System.Data.Common.DataColumnMapping("CurrencyCode", "CurrencyCode"), _
        New System.Data.Common.DataColumnMapping("CurrencyDescription", "CurrencyDescription"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblCurrency_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblCurrency_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@CurrencyID", System.Data.SqlDbType.Int, 4, "CurrencyID"))
        InsertCommand.Parameters.Add(New SqlParameter("@CurrencyCashInstrumentID", System.Data.SqlDbType.Int, 4, "CurrencyCashInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@CurrencyFuturesNominalInstrumentID", System.Data.SqlDbType.Int, 4, "CurrencyFuturesNominalInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@DefaultExpenseInstrument", System.Data.SqlDbType.Int, 4, "DefaultExpenseInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@DefaultFeeInstrument", System.Data.SqlDbType.Int, 4, "DefaultFeeInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@DefaultBrokerageExpense", System.Data.SqlDbType.Int, 4, "DefaultBrokerageExpense"))
        InsertCommand.Parameters.Add(New SqlParameter("@DefaultRetrocessionInstrument", System.Data.SqlDbType.Int, 4, "DefaultRetrocessionInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@DefaultInitialMarginInstrument", System.Data.SqlDbType.Int, 4, "DefaultInitialMarginInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@DefaultMovementCommissionInstrument", System.Data.SqlDbType.Int, 4, "DefaultMovementCommissionInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXForwardRiskInstrument", System.Data.SqlDbType.Int, 4, "FXForwardRiskInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@CurrencyCode", System.Data.SqlDbType.NVarChar, 50, "CurrencyCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@CurrencyDescription", System.Data.SqlDbType.NVarChar, 50, "CurrencyDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblCurrency_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@CurrencyID", System.Data.SqlDbType.Int, 4, "CurrencyID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CurrencyCashInstrumentID", System.Data.SqlDbType.Int, 4, "CurrencyCashInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CurrencyFuturesNominalInstrumentID", System.Data.SqlDbType.Int, 4, "CurrencyFuturesNominalInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DefaultExpenseInstrument", System.Data.SqlDbType.Int, 4, "DefaultExpenseInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DefaultFeeInstrument", System.Data.SqlDbType.Int, 4, "DefaultFeeInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DefaultBrokerageExpense", System.Data.SqlDbType.Int, 4, "DefaultBrokerageExpense"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DefaultRetrocessionInstrument", System.Data.SqlDbType.Int, 4, "DefaultRetrocessionInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DefaultInitialMarginInstrument", System.Data.SqlDbType.Int, 4, "DefaultInitialMarginInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DefaultMovementCommissionInstrument", System.Data.SqlDbType.Int, 4, "DefaultMovementCommissionInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXForwardRiskInstrument", System.Data.SqlDbType.Int, 4, "FXForwardRiskInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CurrencyCode", System.Data.SqlDbType.NVarChar, 50, "CurrencyCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CurrencyDescription", System.Data.SqlDbType.NVarChar, 50, "CurrencyDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand

        DeleteCommand.CommandText = "[adp_tblCurrency_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@CurrencyID", System.Data.SqlDbType.Int, 4, "CurrencyID"))


      Case "TBLDATATASK"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblDataTask", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("TaskID", "TaskID"), _
        New System.Data.Common.DataColumnMapping("TaskDescription", "TaskDescription"), _
        New System.Data.Common.DataColumnMapping("TaskType", "TaskType"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("SecurityTicker", "SecurityTicker"), _
        New System.Data.Common.DataColumnMapping("TaskDefinition", "TaskDefinition"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblDataTask_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblDataTask_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@TaskID", System.Data.SqlDbType.Int, 4, "TaskID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TaskDescription", System.Data.SqlDbType.NVarChar, 50, "TaskDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@TaskType", System.Data.SqlDbType.Int, 4, "TaskType"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@SecurityTicker", System.Data.SqlDbType.NVarChar, 50, "SecurityTicker"))
        InsertCommand.Parameters.Add(New SqlParameter("@TaskDefinition", System.Data.SqlDbType.Text, Int32.MaxValue, "TaskDefinition"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblDataTask_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@TaskID", System.Data.SqlDbType.Int, 4, "TaskID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TaskDescription", System.Data.SqlDbType.NVarChar, 50, "TaskDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TaskType", System.Data.SqlDbType.Int, 4, "TaskType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SecurityTicker", System.Data.SqlDbType.NVarChar, 50, "SecurityTicker"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TaskDefinition", System.Data.SqlDbType.Text, Int32.MaxValue, "TaskDefinition"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblDataTask_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@TaskID", System.Data.SqlDbType.Int, 4, "TaskID"))


      Case "TBLEXPOSURE"  ' To do : Update, Insert & Delete commands.

        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblExposure", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("expID", "expID"), _
				New System.Data.Common.DataColumnMapping("expDate", "expDate"), _
				New System.Data.Common.DataColumnMapping("expIsDetailRow", "expIsDetailRow"), _
				New System.Data.Common.DataColumnMapping("expFund", "expFund"), _
				New System.Data.Common.DataColumnMapping("expLimitID", "expLimitID"), _
				New System.Data.Common.DataColumnMapping("expLimitType", "expLimitType"), _
				New System.Data.Common.DataColumnMapping("expLimitCharacteristic", "expLimitCharacteristic"), _
				New System.Data.Common.DataColumnMapping("expLimitInstrument", "expLimitInstrument"), _
				New System.Data.Common.DataColumnMapping("expLimitSector", "expLimitSector"), _
				New System.Data.Common.DataColumnMapping("expLimitSectorName", "expLimitSectorName"), _
				New System.Data.Common.DataColumnMapping("expLimitDealingPeriod", "expLimitDealingPeriod"), _
				New System.Data.Common.DataColumnMapping("expLimitGreaterOrLessThan", "expLimitGreaterOrLessThan"), _
				New System.Data.Common.DataColumnMapping("expLimit", "expLimit"), _
				New System.Data.Common.DataColumnMapping("expDetailInstrument", "expDetailInstrument"), _
				New System.Data.Common.DataColumnMapping("expExposure", "expExposure"), _
				New System.Data.Common.DataColumnMapping("expUsage", "expUsage"), _
				New System.Data.Common.DataColumnMapping("expStatus", "expStatus"), _
				New System.Data.Common.DataColumnMapping("expDateCalculated", "expDateCalculated"), _
				New System.Data.Common.DataColumnMapping("expKnowledgeDate", "expKnowledgeDate"), _
				New System.Data.Common.DataColumnMapping("expKnowledgeDateWholeday", "expKnowledgeDateWholeday"), _
				New System.Data.Common.DataColumnMapping("expSystemKnowledgeDate", "expSystemKnowledgeDate"), _
				New System.Data.Common.DataColumnMapping("expSystemKnowledgeDateWholeday", "expSystemKnowledgeDateWholeday"), _
				New System.Data.Common.DataColumnMapping("expFlags", "expFlags") _
				})})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblExposure_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblExposure_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))
        InsertCommand.Parameters.Add(New SqlParameter("@expID", System.Data.SqlDbType.Int, 4, "expID"))
        InsertCommand.Parameters.Add(New SqlParameter("@expDate", System.Data.SqlDbType.DateTime, 8, "expDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@expFund", System.Data.SqlDbType.Int, 4, "expFund"))
				InsertCommand.Parameters.Add(New SqlParameter("@expLimitID", System.Data.SqlDbType.Int, 4, "expLimitID"))
				InsertCommand.Parameters.Add(New SqlParameter("@expIsDetailRow", System.Data.SqlDbType.Int, 4, "expIsDetailRow"))
				InsertCommand.Parameters.Add(New SqlParameter("@expLimitType", System.Data.SqlDbType.Int, 4, "expLimitType"))
				InsertCommand.Parameters.Add(New SqlParameter("@expLimitCharacteristic", System.Data.SqlDbType.Int, 4, "expLimitCharacteristic"))
				InsertCommand.Parameters.Add(New SqlParameter("@expLimitInstrument", System.Data.SqlDbType.Int, 4, "expLimitInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@expLimitSector", System.Data.SqlDbType.Int, 4, "expLimitSector"))
        InsertCommand.Parameters.Add(New SqlParameter("@expLimitSectorName", System.Data.SqlDbType.NVarChar, 100, "expLimitSectorName"))
        InsertCommand.Parameters.Add(New SqlParameter("@expLimitDealingPeriod", System.Data.SqlDbType.Int, 4, "expLimitDealingPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@expLimitGreaterOrLessThan", System.Data.SqlDbType.Int, 4, "expLimitGreaterOrLessThan"))
        InsertCommand.Parameters.Add(New SqlParameter("@expLimit", System.Data.SqlDbType.Float, 8, "expLimit"))
				InsertCommand.Parameters.Add(New SqlParameter("@expDetailInstrument", System.Data.SqlDbType.Int, 4, "expDetailInstrument"))
				InsertCommand.Parameters.Add(New SqlParameter("@expExposure", System.Data.SqlDbType.Float, 8, "expExposure"))
        InsertCommand.Parameters.Add(New SqlParameter("@expUsage", System.Data.SqlDbType.Float, 8, "expUsage"))
        InsertCommand.Parameters.Add(New SqlParameter("@expStatus", System.Data.SqlDbType.NVarChar, 50, "expStatus"))
        InsertCommand.Parameters.Add(New SqlParameter("@expDateCalculated", System.Data.SqlDbType.DateTime, 8, "expDateCalculated"))
        InsertCommand.Parameters.Add(New SqlParameter("@expKnowledgeDate", System.Data.SqlDbType.DateTime, 8, "expKnowledgeDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@expKnowledgeDateWholeday", System.Data.SqlDbType.Int, 4, "expKnowledgeDateWholeday"))
        InsertCommand.Parameters.Add(New SqlParameter("@expSystemKnowledgeDate", System.Data.SqlDbType.DateTime, 8, "expSystemKnowledgeDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@expSystemKnowledgeDateWholeday", System.Data.SqlDbType.Int, 4, "expSystemKnowledgeDateWholeday"))
        InsertCommand.Parameters.Add(New SqlParameter("@expFlags", System.Data.SqlDbType.Int, 4, "expFlags"))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblExposure_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expID", System.Data.SqlDbType.Int, 4, "expID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expDate", System.Data.SqlDbType.DateTime, 8, "expDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expFund", System.Data.SqlDbType.Int, 4, "expFund"))
				UpdateCommand.Parameters.Add(New SqlParameter("@expLimitID", System.Data.SqlDbType.Int, 4, "expLimitID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@expIsDetailRow", System.Data.SqlDbType.Int, 4, "expIsDetailRow"))
				UpdateCommand.Parameters.Add(New SqlParameter("@expLimitType", System.Data.SqlDbType.Int, 4, "expLimitType"))
				UpdateCommand.Parameters.Add(New SqlParameter("@expLimitCharacteristic", System.Data.SqlDbType.Int, 4, "expLimitCharacteristic"))
				UpdateCommand.Parameters.Add(New SqlParameter("@expLimitInstrument", System.Data.SqlDbType.Int, 4, "expLimitInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expLimitSector", System.Data.SqlDbType.Int, 4, "expLimitSector"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expLimitSectorName", System.Data.SqlDbType.NVarChar, 100, "expLimitSectorName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expLimitDealingPeriod", System.Data.SqlDbType.Int, 4, "expLimitDealingPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expLimitGreaterOrLessThan", System.Data.SqlDbType.Int, 4, "expLimitGreaterOrLessThan"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expLimit", System.Data.SqlDbType.Float, 8, "expLimit"))
				UpdateCommand.Parameters.Add(New SqlParameter("@expDetailInstrument", System.Data.SqlDbType.Int, 4, "expDetailInstrument"))
				UpdateCommand.Parameters.Add(New SqlParameter("@expExposure", System.Data.SqlDbType.Float, 8, "expExposure"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expUsage", System.Data.SqlDbType.Float, 8, "expUsage"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expStatus", System.Data.SqlDbType.NVarChar, 50, "expStatus"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expDateCalculated", System.Data.SqlDbType.DateTime, 8, "expDateCalculated"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expKnowledgeDate", System.Data.SqlDbType.DateTime, 8, "expKnowledgeDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expKnowledgeDateWholeday", System.Data.SqlDbType.Int, 4, "expKnowledgeDateWholeday"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expSystemKnowledgeDate", System.Data.SqlDbType.DateTime, 8, "expSystemKnowledgeDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expSystemKnowledgeDateWholeday", System.Data.SqlDbType.Int, 4, "expSystemKnowledgeDateWholeday"))
        UpdateCommand.Parameters.Add(New SqlParameter("@expFlags", System.Data.SqlDbType.Int, 4, "expFlags"))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblExposure_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))


      Case "TBLFEEDEFINITIONS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFeeDefinitions", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FeeID", "FeeID"), _
        New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
        New System.Data.Common.DataColumnMapping("SubFundID", "SubFundID"), _
        New System.Data.Common.DataColumnMapping("ShareClassID", "ShareClassID"), _
        New System.Data.Common.DataColumnMapping("Description", "Description"), _
        New System.Data.Common.DataColumnMapping("FeeInstrumentID", "FeeInstrumentID"), _
        New System.Data.Common.DataColumnMapping("FeeCalculationPeriod", "FeeCalculationPeriod"), _
        New System.Data.Common.DataColumnMapping("FeePaymentPeriod", "FeePaymentPeriod"), _
        New System.Data.Common.DataColumnMapping("FeeSettlementPeriod", "FeeSettlementPeriod"), _
        New System.Data.Common.DataColumnMapping("FeeSettlementTPlusDays", "FeeSettlementTPlusDays"), _
        New System.Data.Common.DataColumnMapping("FeeCalculationMethod", "FeeCalculationMethod"), _
        New System.Data.Common.DataColumnMapping("FeeDaysPerPeriod", "FeeDaysPerPeriod"), _
        New System.Data.Common.DataColumnMapping("FeeUseFundYearEnd", "FeeUseFundYearEnd"), _
        New System.Data.Common.DataColumnMapping("FeeRate", "FeeRate"), _
        New System.Data.Common.DataColumnMapping("FeeRateType", "FeeRateType"), _
        New System.Data.Common.DataColumnMapping("FeeStartDate", "FeeStartDate"), _
        New System.Data.Common.DataColumnMapping("FeeEndDate", "FeeEndDate"), _
        New System.Data.Common.DataColumnMapping("FeePaymentDay", "FeePaymentDay"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblFeeDefinitions_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFeeDefinitions_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeID", System.Data.SqlDbType.Int, 4, "FeeID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@SubFundID", System.Data.SqlDbType.Int, 4, "SubFundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ShareClassID", System.Data.SqlDbType.Int, 4, "ShareClassID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 100, "Description"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeInstrumentID", System.Data.SqlDbType.Int, 4, "FeeInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeCalculationPeriod", System.Data.SqlDbType.Int, 4, "FeeCalculationPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeePaymentPeriod", System.Data.SqlDbType.Int, 4, "FeePaymentPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeSettlementPeriod", System.Data.SqlDbType.Int, 4, "FeeSettlementPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeSettlementTPlusDays", System.Data.SqlDbType.Int, 4, "FeeSettlementTPlusDays"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeCalculationMethod", System.Data.SqlDbType.Int, 4, "FeeCalculationMethod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeDaysPerPeriod", System.Data.SqlDbType.Int, 4, "FeeDaysPerPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeUseFundYearEnd", System.Data.SqlDbType.Int, 4, "FeeUseFundYearEnd"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeRate", System.Data.SqlDbType.Float, 8, "FeeRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeRateType", System.Data.SqlDbType.Int, 4, "FeeRateType"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeStartDate", System.Data.SqlDbType.Date, 4, "FeeStartDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeEndDate", System.Data.SqlDbType.Date, 4, "FeeEndDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeePaymentDay", System.Data.SqlDbType.Int, 4, "FeePaymentDay"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblFeeDefinitions_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeID", System.Data.SqlDbType.Int, 4, "FeeID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SubFundID", System.Data.SqlDbType.Int, 4, "SubFundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ShareClassID", System.Data.SqlDbType.Int, 4, "ShareClassID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 100, "Description"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeInstrumentID", System.Data.SqlDbType.Int, 4, "FeeInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeCalculationPeriod", System.Data.SqlDbType.Int, 4, "FeeCalculationPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeePaymentPeriod", System.Data.SqlDbType.Int, 4, "FeePaymentPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeSettlementPeriod", System.Data.SqlDbType.Int, 4, "FeeSettlementPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeSettlementTPlusDays", System.Data.SqlDbType.Int, 4, "FeeSettlementTPlusDays"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeCalculationMethod", System.Data.SqlDbType.Int, 4, "FeeCalculationMethod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeDaysPerPeriod", System.Data.SqlDbType.Int, 4, "FeeDaysPerPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeUseFundYearEnd", System.Data.SqlDbType.Int, 4, "FeeUseFundYearEnd"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeRate", System.Data.SqlDbType.Float, 8, "FeeRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeRateType", System.Data.SqlDbType.Int, 4, "FeeRateType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeStartDate", System.Data.SqlDbType.Date, 4, "FeeStartDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeEndDate", System.Data.SqlDbType.Date, 4, "FeeEndDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeePaymentDay", System.Data.SqlDbType.Int, 4, "FeePaymentDay"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFeeDefinitions_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeID", System.Data.SqlDbType.Int, 4, "FeeID"))


      Case "TBLFLORENCEDATAVALUES"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFlorenceDataValues", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("DataValueID", "DataValueID"), _
        New System.Data.Common.DataColumnMapping("DataEntityID", "DataEntityID"), _
        New System.Data.Common.DataColumnMapping("DataItemID", "DataItemID"), _
        New System.Data.Common.DataColumnMapping("DataValueDate", "DataValueDate"), _
        New System.Data.Common.DataColumnMapping("DataValueType", "DataValueType"), _
        New System.Data.Common.DataColumnMapping("TextData", "TextData"), _
        New System.Data.Common.DataColumnMapping("NumericData", "NumericData"), _
        New System.Data.Common.DataColumnMapping("PercentageData", "PercentageData"), _
        New System.Data.Common.DataColumnMapping("DateData", "DateData"), _
        New System.Data.Common.DataColumnMapping("BooleanData", "BooleanData"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFlorenceDataValues_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFlorenceDataValues_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@DataValueID", System.Data.SqlDbType.Int, 4, "DataValueID"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataEntityID", System.Data.SqlDbType.Int, 4, "DataEntityID"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataItemID", System.Data.SqlDbType.Int, 4, "DataItemID"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataValueDate", System.Data.SqlDbType.DateTime, 8, "DataValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataValueType", System.Data.SqlDbType.Int, 4, "DataValueType"))
        InsertCommand.Parameters.Add(New SqlParameter("@TextData", System.Data.SqlDbType.Text, Int32.MaxValue, "TextData"))
        InsertCommand.Parameters.Add(New SqlParameter("@NumericData", System.Data.SqlDbType.Float, 8, "NumericData"))
        InsertCommand.Parameters.Add(New SqlParameter("@PercentageData", System.Data.SqlDbType.Float, 8, "PercentageData"))
        InsertCommand.Parameters.Add(New SqlParameter("@DateData", System.Data.SqlDbType.DateTime, 8, "DateData"))
        InsertCommand.Parameters.Add(New SqlParameter("@BooleanData", System.Data.SqlDbType.Bit, 1, "BooleanData"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFlorenceDataValues_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataValueID", System.Data.SqlDbType.Int, 4, "DataValueID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataEntityID", System.Data.SqlDbType.Int, 4, "DataEntityID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataItemID", System.Data.SqlDbType.Int, 4, "DataItemID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataValueDate", System.Data.SqlDbType.DateTime, 8, "DataValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataValueType", System.Data.SqlDbType.Int, 4, "DataValueType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TextData", System.Data.SqlDbType.Text, Int32.MaxValue, "TextData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@NumericData", System.Data.SqlDbType.Float, 8, "NumericData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PercentageData", System.Data.SqlDbType.Float, 8, "PercentageData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DateData", System.Data.SqlDbType.DateTime, 8, "DateData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BooleanData", System.Data.SqlDbType.Bit, 1, "BooleanData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFlorenceDataValues_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@DataValueID", System.Data.SqlDbType.Int, 4, "DataValueID"))


      Case "TBLFLORENCEENTITY"
        ' Table Mappings
        ' 
        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblFlorenceEntity", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("EntityID", "EntityID"), _
				New System.Data.Common.DataColumnMapping("EntityName", "EntityName"), _
				New System.Data.Common.DataColumnMapping("EntityIsInactive", "EntityIsInactive"), _
				New System.Data.Common.DataColumnMapping("ItemGroupID", "ItemGroupID"), _
				New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
				New System.Data.Common.DataColumnMapping("AdminContact", "AdminContact"), _
				New System.Data.Common.DataColumnMapping("AdminEmailSalutation", "AdminEmailSalutation"), _
				New System.Data.Common.DataColumnMapping("AdminPhone", "AdminPhone"), _
				New System.Data.Common.DataColumnMapping("AdminEmail", "AdminEmail"), _
				New System.Data.Common.DataColumnMapping("AdminFax", "AdminFax"), _
				New System.Data.Common.DataColumnMapping("DDStatus", "DDStatus"), _
				New System.Data.Common.DataColumnMapping("DDStatusComment", "DDStatusComment"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFlorenceEntity_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFlorenceEntity_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@EntityID", System.Data.SqlDbType.Int, 4, "EntityID"))
        InsertCommand.Parameters.Add(New SqlParameter("@EntityName", System.Data.SqlDbType.NVarChar, 100, "EntityName"))
        InsertCommand.Parameters.Add(New SqlParameter("@EntityIsInactive", System.Data.SqlDbType.Bit, 1, "EntityIsInactive"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemGroupID", System.Data.SqlDbType.Int, 4, "ItemGroupID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminContact", System.Data.SqlDbType.NVarChar, 100, "AdminContact"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminEmailSalutation", System.Data.SqlDbType.NVarChar, 50, "AdminEmailSalutation"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminPhone", System.Data.SqlDbType.NVarChar, 50, "AdminPhone"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminEmail", System.Data.SqlDbType.NVarChar, 50, "AdminEmail"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminFax", System.Data.SqlDbType.NVarChar, 50, "AdminFax"))
				InsertCommand.Parameters.Add(New SqlParameter("@DDStatus", System.Data.SqlDbType.Int, 4, "DDStatus"))
				InsertCommand.Parameters.Add(New SqlParameter("@DDStatusComment", System.Data.SqlDbType.NVarChar, 100, "DDStatusComment"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFlorenceEntity_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@EntityID", System.Data.SqlDbType.Int, 4, "EntityID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EntityName", System.Data.SqlDbType.NVarChar, 100, "EntityName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EntityIsInactive", System.Data.SqlDbType.Bit, 1, "EntityIsInactive"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemGroupID", System.Data.SqlDbType.Int, 4, "ItemGroupID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminContact", System.Data.SqlDbType.NVarChar, 100, "AdminContact"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminEmailSalutation", System.Data.SqlDbType.NVarChar, 50, "AdminEmailSalutation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminPhone", System.Data.SqlDbType.NVarChar, 50, "AdminPhone"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminEmail", System.Data.SqlDbType.NVarChar, 50, "AdminEmail"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminFax", System.Data.SqlDbType.NVarChar, 50, "AdminFax"))
				UpdateCommand.Parameters.Add(New SqlParameter("@DDStatus", System.Data.SqlDbType.Int, 4, "DDStatus"))
				UpdateCommand.Parameters.Add(New SqlParameter("@DDStatusComment", System.Data.SqlDbType.NVarChar, 100, "DDStatusComment"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFlorenceEntity_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@EntityID", System.Data.SqlDbType.Int, 4, "EntityID"))


      Case "TBLFLORENCEGROUP"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblFlorenceGroup", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("ItemGroup", "ItemGroup"), _
				New System.Data.Common.DataColumnMapping("ItemGroupDescription", "ItemGroupDescription"), _
				New System.Data.Common.DataColumnMapping("LegacyGroup", "LegacyGroup"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFlorenceGroup_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFlorenceGroup_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemGroup", System.Data.SqlDbType.Int, 4, "ItemGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemGroupDescription", System.Data.SqlDbType.NVarChar, 100, "ItemGroupDescription"))
				InsertCommand.Parameters.Add(New SqlParameter("@LegacyGroup", System.Data.SqlDbType.Int, 4, "LegacyGroup"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFlorenceGroup_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemGroup", System.Data.SqlDbType.Int, 4, "ItemGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemGroupDescription", System.Data.SqlDbType.NVarChar, 100, "ItemGroupDescription"))
				UpdateCommand.Parameters.Add(New SqlParameter("@LegacyGroup", System.Data.SqlDbType.Int, 4, "LegacyGroup"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFlorenceGroup_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ItemGroup", System.Data.SqlDbType.Int, 4, "ItemGroup"))


      Case "TBLFLORENCEITEMS"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFlorenceItems", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ItemID", "ItemID"), _
        New System.Data.Common.DataColumnMapping("Category", "Category"), _
        New System.Data.Common.DataColumnMapping("ItemDescription", "ItemDescription"), _
        New System.Data.Common.DataColumnMapping("UpdatePeriod", "UpdatePeriod"), _
        New System.Data.Common.DataColumnMapping("DataUpdatePeriod", "DataUpdatePeriod"), _
        New System.Data.Common.DataColumnMapping("ItemMessage", "ItemMessage"), _
        New System.Data.Common.DataColumnMapping("ItemGroup", "ItemGroup"), _
        New System.Data.Common.DataColumnMapping("ItemDiscontinued", "ItemDiscontinued"), _
        New System.Data.Common.DataColumnMapping("KeyDetail", "KeyDetail"), _
        New System.Data.Common.DataColumnMapping("IsDataItem", "IsDataItem"), _
        New System.Data.Common.DataColumnMapping("DataItemType", "DataItemType"), _
        New System.Data.Common.DataColumnMapping("UpdateStatusOnDataUpdate", "UpdateStatusOnDataUpdate"), _
        New System.Data.Common.DataColumnMapping("IsLimitItem", "IsLimitItem"), _
        New System.Data.Common.DataColumnMapping("LimitType", "LimitType"), _
        New System.Data.Common.DataColumnMapping("LimitIsPercent", "LimitIsPercent"), _
        New System.Data.Common.DataColumnMapping("FromLimit", "FromLimit"), _
        New System.Data.Common.DataColumnMapping("ToLimit", "ToLimit"), _
        New System.Data.Common.DataColumnMapping("LimitThreshold", "LimitThreshold"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFlorenceItems_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFlorenceItems_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemID", System.Data.SqlDbType.Int, 4, "ItemID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Category", System.Data.SqlDbType.NVarChar, 50, "Category"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemDescription", System.Data.SqlDbType.NVarChar, 100, "ItemDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@UpdatePeriod", System.Data.SqlDbType.Int, 4, "UpdatePeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataUpdatePeriod", System.Data.SqlDbType.Int, 4, "DataUpdatePeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemMessage", System.Data.SqlDbType.NVarChar, 500, "ItemMessage"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemGroup", System.Data.SqlDbType.Int, 4, "ItemGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemDiscontinued", System.Data.SqlDbType.Int, 4, "ItemDiscontinued"))
        InsertCommand.Parameters.Add(New SqlParameter("@KeyDetail", System.Data.SqlDbType.Int, 4, "KeyDetail"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsDataItem", System.Data.SqlDbType.Int, 4, "IsDataItem"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataItemType", System.Data.SqlDbType.Int, 4, "DataItemType"))
        InsertCommand.Parameters.Add(New SqlParameter("@UpdateStatusOnDataUpdate", System.Data.SqlDbType.Bit, 1, "UpdateStatusOnDataUpdate"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsLimitItem", System.Data.SqlDbType.Int, 4, "IsLimitItem"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitType", System.Data.SqlDbType.Int, 4, "LimitType"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitIsPercent", System.Data.SqlDbType.Bit, 1, "LimitIsPercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@FromLimit", System.Data.SqlDbType.Float, 8, "FromLimit"))
        InsertCommand.Parameters.Add(New SqlParameter("@ToLimit", System.Data.SqlDbType.Float, 8, "ToLimit"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitThreshold", System.Data.SqlDbType.Float, 8, "LimitThreshold"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFlorenceItems_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemID", System.Data.SqlDbType.Int, 4, "ItemID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Category", System.Data.SqlDbType.NVarChar, 50, "Category"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemDescription", System.Data.SqlDbType.NVarChar, 100, "ItemDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@UpdatePeriod", System.Data.SqlDbType.Int, 4, "UpdatePeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataUpdatePeriod", System.Data.SqlDbType.Int, 4, "DataUpdatePeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemMessage", System.Data.SqlDbType.NVarChar, 500, "ItemMessage"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemGroup", System.Data.SqlDbType.Int, 4, "ItemGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemDiscontinued", System.Data.SqlDbType.Int, 4, "ItemDiscontinued"))
        UpdateCommand.Parameters.Add(New SqlParameter("@KeyDetail", System.Data.SqlDbType.Int, 4, "KeyDetail"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsDataItem", System.Data.SqlDbType.Int, 4, "IsDataItem"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataItemType", System.Data.SqlDbType.Int, 4, "DataItemType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@UpdateStatusOnDataUpdate", System.Data.SqlDbType.Bit, 1, "UpdateStatusOnDataUpdate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsLimitItem", System.Data.SqlDbType.Int, 4, "IsLimitItem"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitType", System.Data.SqlDbType.Int, 4, "LimitType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitIsPercent", System.Data.SqlDbType.Bit)).SourceColumn = "LimitIsPercent"
        UpdateCommand.Parameters.Add(New SqlParameter("@FromLimit", System.Data.SqlDbType.Float, 8, "FromLimit"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ToLimit", System.Data.SqlDbType.Float, 8, "ToLimit"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitThreshold", System.Data.SqlDbType.Float, 8, "LimitThreshold"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFlorenceItems_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ItemID", System.Data.SqlDbType.Int, 4, "ItemID"))


      Case "TBLFLORENCESTATUS"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFlorenceStatus", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("StatusID", "StatusID"), _
        New System.Data.Common.DataColumnMapping("EntityID", "EntityID"), _
        New System.Data.Common.DataColumnMapping("ItemID", "ItemID"), _
        New System.Data.Common.DataColumnMapping("ItemStatusID", "ItemStatusID"), _
        New System.Data.Common.DataColumnMapping("DateCompleted", "DateCompleted"), _
        New System.Data.Common.DataColumnMapping("Comment", "Comment"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFlorenceStatus_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFlorenceStatus_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@StatusID", System.Data.SqlDbType.Int, 4, "StatusID"))
        InsertCommand.Parameters.Add(New SqlParameter("@EntityID", System.Data.SqlDbType.Int, 4, "EntityID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemID", System.Data.SqlDbType.Int, 4, "ItemID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemStatusID", System.Data.SqlDbType.Int, 4, "ItemStatusID"))
        InsertCommand.Parameters.Add(New SqlParameter("@DateCompleted", System.Data.SqlDbType.DateTime, 8, "DateCompleted"))
        InsertCommand.Parameters.Add(New SqlParameter("@Comment", System.Data.SqlDbType.Text, Int32.MaxValue, "Comment"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFlorenceStatus_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@StatusID", System.Data.SqlDbType.Int, 4, "StatusID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EntityID", System.Data.SqlDbType.Int, 4, "EntityID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemID", System.Data.SqlDbType.Int, 4, "ItemID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemStatusID", System.Data.SqlDbType.Int, 4, "ItemStatusID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DateCompleted", System.Data.SqlDbType.DateTime, 8, "DateCompleted"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Comment", System.Data.SqlDbType.Text, Int32.MaxValue, "Comment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFlorenceStatus_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@StatusID", System.Data.SqlDbType.Int, 4, "StatusID"))


      Case "TBLFLORENCESTATUSIDS"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFlorenceStatusIDs", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ItemStatusID", "ItemStatusID"), _
        New System.Data.Common.DataColumnMapping("ItemStatusDescription", "ItemStatusDescription"), _
        New System.Data.Common.DataColumnMapping("ItemIsComplete", "ItemIsComplete"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFlorenceStatusIDs_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFlorenceStatusIDs_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemStatusID", System.Data.SqlDbType.Int, 4, "ItemStatusID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemStatusDescription", System.Data.SqlDbType.NVarChar, 50, "ItemStatusDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemIsComplete", System.Data.SqlDbType.Int, 4, "ItemIsComplete"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFlorenceStatusIDs_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemStatusID", System.Data.SqlDbType.Int, 4, "ItemStatusID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemStatusDescription", System.Data.SqlDbType.NVarChar, 50, "ItemStatusDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemIsComplete", System.Data.SqlDbType.Int, 4, "ItemIsComplete"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFlorenceStatusIDs_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ItemStatusID", System.Data.SqlDbType.Int, 4, "ItemStatusID"))


      Case "TBLFLORENCEWORKITEMS"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFlorenceWorkItems", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("EntityID", "EntityID"), _
        New System.Data.Common.DataColumnMapping("EntityName", "EntityName"), _
        New System.Data.Common.DataColumnMapping("EntityIsInactive", "EntityIsInactive"), _
        New System.Data.Common.DataColumnMapping("ItemGroupID", "ItemGroupID"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("ItemID", "ItemID"), _
        New System.Data.Common.DataColumnMapping("Category", "Category"), _
        New System.Data.Common.DataColumnMapping("ItemDescription", "ItemDescription"), _
        New System.Data.Common.DataColumnMapping("UpdatePeriod", "UpdatePeriod"), _
        New System.Data.Common.DataColumnMapping("ItemDiscontinued", "ItemDiscontinued"), _
        New System.Data.Common.DataColumnMapping("IsDataItem", "IsDataItem"), _
        New System.Data.Common.DataColumnMapping("IsLimitItem", "IsLimitItem"), _
        New System.Data.Common.DataColumnMapping("StatusID", "StatusID"), _
        New System.Data.Common.DataColumnMapping("ItemStatusID", "ItemStatusID"), _
        New System.Data.Common.DataColumnMapping("DateCompleted", "DateCompleted"), _
        New System.Data.Common.DataColumnMapping("IsDateOverdue", "IsDateOverdue") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFlorenceWorkItems_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


      Case "TBLFLORENCEWORKITEMSDATA"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFlorenceWorkItemsData", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("EntityID", "EntityID"), _
        New System.Data.Common.DataColumnMapping("EntityName", "EntityName"), _
        New System.Data.Common.DataColumnMapping("EntityIsInactive", "EntityIsInactive"), _
        New System.Data.Common.DataColumnMapping("ItemGroupID", "ItemGroupID"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("ItemID", "ItemID"), _
        New System.Data.Common.DataColumnMapping("Category", "Category"), _
        New System.Data.Common.DataColumnMapping("ItemDescription", "ItemDescription"), _
        New System.Data.Common.DataColumnMapping("DataUpdatePeriod", "DataUpdatePeriod"), _
        New System.Data.Common.DataColumnMapping("ItemDiscontinued", "ItemDiscontinued"), _
        New System.Data.Common.DataColumnMapping("IsDataItem", "IsDataItem"), _
        New System.Data.Common.DataColumnMapping("IsLimitItem", "IsLimitItem"), _
        New System.Data.Common.DataColumnMapping("DataValueDate", "DataValueDate"), _
        New System.Data.Common.DataColumnMapping("IsDateOverdue", "IsDateOverdue") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFlorenceWorkItemsData_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


      Case "TBLFUND"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFund", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
        New System.Data.Common.DataColumnMapping("FundCode", "FundCode"), _
        New System.Data.Common.DataColumnMapping("FundName", "FundName"), _
        New System.Data.Common.DataColumnMapping("FundLegalEntity", "FundLegalEntity"), _
        New System.Data.Common.DataColumnMapping("FundBaseCurrency", "FundBaseCurrency"), _
        New System.Data.Common.DataColumnMapping("FundPricingPeriod", "FundPricingPeriod"), _
        New System.Data.Common.DataColumnMapping("FundUnit", "FundUnit"), _
        New System.Data.Common.DataColumnMapping("FundParentFund", "FundParentFund"), _
        New System.Data.Common.DataColumnMapping("FundYearEnd", "FundYearEnd"), _
        New System.Data.Common.DataColumnMapping("FundNAVReconciliationPlusDays", "FundNAVReconciliationPlusDays"), _
        New System.Data.Common.DataColumnMapping("FundDefaultTradeFXToFundCurrency", "FundDefaultTradeFXToFundCurrency"), _
        New System.Data.Common.DataColumnMapping("FundHasMultipleShareClasses", "FundHasMultipleShareClasses"), _
        New System.Data.Common.DataColumnMapping("FundUser", "FundUser"), _
        New System.Data.Common.DataColumnMapping("FundManagementFeeInstrument", "FundManagementFeeInstrument"), _
        New System.Data.Common.DataColumnMapping("FundPerformanceFeeInstrument", "FundPerformanceFeeInstrument"), _
        New System.Data.Common.DataColumnMapping("FundPerformanceFeeCrystalisedInstrument", "FundPerformanceFeeCrystalisedInstrument"), _
        New System.Data.Common.DataColumnMapping("FundMovementComission", "FundMovementComission"), _
        New System.Data.Common.DataColumnMapping("FundManagementFeesBeforeSubscriptions", "FundManagementFeesBeforeSubscriptions"), _
        New System.Data.Common.DataColumnMapping("FundUsesEqualisation", "FundUsesEqualisation"), _
        New System.Data.Common.DataColumnMapping("FundManagementFeesAccruePeriod", "FundManagementFeesAccruePeriod"), _
        New System.Data.Common.DataColumnMapping("FundManagementFeesPaymentPeriod", "FundManagementFeesPaymentPeriod"), _
        New System.Data.Common.DataColumnMapping("FundPerformanceFeesAccruePeriod", "FundPerformanceFeesAccruePeriod"), _
        New System.Data.Common.DataColumnMapping("FundPerformanceFeesPaymentPeriod", "FundPerformanceFeesPaymentPeriod"), _
        New System.Data.Common.DataColumnMapping("FundSubscriptionNAVDatePeriod", "FundSubscriptionNAVDatePeriod"), _
        New System.Data.Common.DataColumnMapping("FundSubscriptionSettlementDatePeriod", "FundSubscriptionSettlementDatePeriod"), _
        New System.Data.Common.DataColumnMapping("FundRedemptionNAVDatePeriod", "FundRedemptionNAVDatePeriod"), _
        New System.Data.Common.DataColumnMapping("FundRedemptionSettlementDatePeriod", "FundRedemptionSettlementDatePeriod"), _
        New System.Data.Common.DataColumnMapping("FundReportGroup", "FundReportGroup"), _
        New System.Data.Common.DataColumnMapping("FundInitialSubscriptionFee", "FundInitialSubscriptionFee"), _
        New System.Data.Common.DataColumnMapping("FundManagementFeeText", "FundManagementFeeText"), _
        New System.Data.Common.DataColumnMapping("FundPerformanceFeeText", "FundPerformanceFeeText"), _
        New System.Data.Common.DataColumnMapping("FundManagementCompany", "FundManagementCompany"), _
        New System.Data.Common.DataColumnMapping("FundManagers", "FundManagers"), _
        New System.Data.Common.DataColumnMapping("FundContactAddress", "FundContactAddress"), _
        New System.Data.Common.DataColumnMapping("FundContactPhone", "FundContactPhone"), _
        New System.Data.Common.DataColumnMapping("FundContactEmail", "FundContactEmail"), _
        New System.Data.Common.DataColumnMapping("FundMinimumInvestment", "FundMinimumInvestment"), _
        New System.Data.Common.DataColumnMapping("FundSubscriptionPeriod", "FundSubscriptionPeriod"), _
        New System.Data.Common.DataColumnMapping("FundSubscriptionNotice", "FundSubscriptionNotice"), _
        New System.Data.Common.DataColumnMapping("FundRedemptionPeriod", "FundRedemptionPeriod"), _
        New System.Data.Common.DataColumnMapping("FundRedemptionNotice", "FundRedemptionNotice"), _
        New System.Data.Common.DataColumnMapping("FundJurisdiction", "FundJurisdiction"), _
        New System.Data.Common.DataColumnMapping("FundAdministrator", "FundAdministrator"), _
        New System.Data.Common.DataColumnMapping("FundAdministratorTraderID", "FundAdministratorTraderID"), _
        New System.Data.Common.DataColumnMapping("FundCustodian", "FundCustodian"), _
        New System.Data.Common.DataColumnMapping("FundListings", "FundListings"), _
        New System.Data.Common.DataColumnMapping("FundLinkedListedInstrument", "FundLinkedListedInstrument"), _
        New System.Data.Common.DataColumnMapping("FundReviewGroup", "FundReviewGroup"), _
        New System.Data.Common.DataColumnMapping("FundISIN", "FundISIN"), _
        New System.Data.Common.DataColumnMapping("FundSEDOL", "FundSEDOL"), _
        New System.Data.Common.DataColumnMapping("FundBloomberg", "FundBloomberg"), _
        New System.Data.Common.DataColumnMapping("FundReutersCode", "FundReutersCode"), _
        New System.Data.Common.DataColumnMapping("FundInvestmentStartDate", "FundInvestmentStartDate"), _
        New System.Data.Common.DataColumnMapping("FundDisclaimer", "FundDisclaimer"), _
        New System.Data.Common.DataColumnMapping("FundObjective", "FundObjective"), _
        New System.Data.Common.DataColumnMapping("FundAdministratorCode", "FundAdministratorCode"), _
        New System.Data.Common.DataColumnMapping("FundForceMultiShareClassReconciliation", "FundForceMultiShareClassReconciliation"), _
        New System.Data.Common.DataColumnMapping("FundClosed", "FundClosed"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFund_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFund_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundCode", System.Data.SqlDbType.NVarChar, 100, "FundCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundName", System.Data.SqlDbType.NVarChar, 100, "FundName"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundLegalEntity", System.Data.SqlDbType.NVarChar, 100, "FundLegalEntity"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundBaseCurrency", System.Data.SqlDbType.Int, 4, "FundBaseCurrency"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundPricingPeriod", System.Data.SqlDbType.Int, 4, "FundPricingPeriod"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundUnit", System.Data.SqlDbType.Int, 4, "FundUnit"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundParentFund", System.Data.SqlDbType.Int, 4, "FundParentFund"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundYearEnd", System.Data.SqlDbType.Date, 3, "FundYearEnd"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundNAVReconciliationPlusDays", System.Data.SqlDbType.Int, 4, "FundNAVReconciliationPlusDays"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundDefaultTradeFXToFundCurrency", System.Data.SqlDbType.Bit, 1, "FundDefaultTradeFXToFundCurrency"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundHasMultipleShareClasses", System.Data.SqlDbType.Bit, 1, "FundHasMultipleShareClasses"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundManagementFeeInstrument", System.Data.SqlDbType.Int, 4, "FundManagementFeeInstrument"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeeInstrument", System.Data.SqlDbType.Int, 4, "FundPerformanceFeeInstrument"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeeCrystalisedInstrument", System.Data.SqlDbType.Int, 4, "FundPerformanceFeeCrystalisedInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundMovementComission", System.Data.SqlDbType.Float, 8, "FundMovementComission"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundManagementFeesBeforeSubscriptions", System.Data.SqlDbType.Bit, 1, "FundManagementFeesBeforeSubscriptions"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundUsesEqualisation", System.Data.SqlDbType.Int, 4, "FundUsesEqualisation"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundManagementFeesAccruePeriod", System.Data.SqlDbType.Int, 4, "FundManagementFeesAccruePeriod"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundManagementFeesPaymentPeriod", System.Data.SqlDbType.Int, 4, "FundManagementFeesPaymentPeriod"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeesAccruePeriod", System.Data.SqlDbType.Int, 4, "FundPerformanceFeesAccruePeriod"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeesPaymentPeriod", System.Data.SqlDbType.Int, 4, "FundPerformanceFeesPaymentPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundSubscriptionNAVDatePeriod", System.Data.SqlDbType.Int, 4, "FundSubscriptionNAVDatePeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundSubscriptionSettlementDatePeriod", System.Data.SqlDbType.Int, 4, "FundSubscriptionSettlementDatePeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundRedemptionNAVDatePeriod", System.Data.SqlDbType.Int, 4, "FundRedemptionNAVDatePeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundRedemptionSettlementDatePeriod", System.Data.SqlDbType.Int, 4, "FundRedemptionSettlementDatePeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundReportGroup", System.Data.SqlDbType.NVarChar, 50, "FundReportGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundInitialSubscriptionFee", System.Data.SqlDbType.NVarChar, 50, "FundInitialSubscriptionFee"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundManagementFeeText", System.Data.SqlDbType.NVarChar, 50, "FundManagementFeeText"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeeText", System.Data.SqlDbType.NVarChar, 50, "FundPerformanceFeeText"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundManagementCompany", System.Data.SqlDbType.NVarChar, 50, "FundManagementCompany"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundManagers", System.Data.SqlDbType.NVarChar, 100, "FundManagers"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundContactAddress", System.Data.SqlDbType.NVarChar, 100, "FundContactAddress"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundContactPhone", System.Data.SqlDbType.NVarChar, 50, "FundContactPhone"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundContactEmail", System.Data.SqlDbType.NVarChar, 50, "FundContactEmail"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundMinimumInvestment", System.Data.SqlDbType.NVarChar, 50, "FundMinimumInvestment"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundSubscriptionPeriod", System.Data.SqlDbType.NVarChar, 50, "FundSubscriptionPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundSubscriptionNotice", System.Data.SqlDbType.NVarChar, 50, "FundSubscriptionNotice"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundRedemptionPeriod", System.Data.SqlDbType.NVarChar, 50, "FundRedemptionPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundRedemptionNotice", System.Data.SqlDbType.NVarChar, 50, "FundRedemptionNotice"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundJurisdiction", System.Data.SqlDbType.NVarChar, 50, "FundJurisdiction"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundAdministrator", System.Data.SqlDbType.NVarChar, 50, "FundAdministrator"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundCustodian", System.Data.SqlDbType.NVarChar, 50, "FundCustodian"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundListings", System.Data.SqlDbType.NVarChar, 50, "FundListings"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundLinkedListedInstrument", System.Data.SqlDbType.Int, 4, "FundLinkedListedInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundReviewGroup", System.Data.SqlDbType.Int, 4, "FundReviewGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundISIN", System.Data.SqlDbType.NVarChar, 20, "FundISIN"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundSEDOL", System.Data.SqlDbType.NVarChar, 20, "FundSEDOL"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundBloomberg", System.Data.SqlDbType.NVarChar, 30, "FundBloomberg"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundReutersCode", System.Data.SqlDbType.NVarChar, 20, "FundReutersCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundInvestmentStartDate", System.Data.SqlDbType.DateTime, 8, "FundInvestmentStartDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundDisclaimer", System.Data.SqlDbType.Text, Int32.MaxValue, "FundDisclaimer"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundObjective", System.Data.SqlDbType.Text, Int32.MaxValue, "FundObjective"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundAdministratorCode", System.Data.SqlDbType.NVarChar, 50, "FundAdministratorCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundForceMultiShareClassReconciliation", System.Data.SqlDbType.Bit, 1, "FundForceMultiShareClassReconciliation"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundClosed", System.Data.SqlDbType.Int, 4, "FundClosed"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        ' 
        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFund_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundCode", System.Data.SqlDbType.NVarChar, 100, "FundCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundName", System.Data.SqlDbType.NVarChar, 100, "FundName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundLegalEntity", System.Data.SqlDbType.NVarChar, 100, "FundLegalEntity"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundBaseCurrency", System.Data.SqlDbType.Int, 4, "FundBaseCurrency"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundPricingPeriod", System.Data.SqlDbType.Int, 4, "FundPricingPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundUnit", System.Data.SqlDbType.Int, 4, "FundUnit"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundParentFund", System.Data.SqlDbType.Int, 4, "FundParentFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundYearEnd", System.Data.SqlDbType.Date, 3, "FundYearEnd"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundNAVReconciliationPlusDays", System.Data.SqlDbType.Int, 4, "FundNAVReconciliationPlusDays"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundDefaultTradeFXToFundCurrency", System.Data.SqlDbType.Bit, 1, "FundDefaultTradeFXToFundCurrency"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundHasMultipleShareClasses", System.Data.SqlDbType.Bit, 1, "FundHasMultipleShareClasses"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundManagementFeeInstrument", System.Data.SqlDbType.Int, 4, "FundManagementFeeInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeeInstrument", System.Data.SqlDbType.Int, 4, "FundPerformanceFeeInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeeCrystalisedInstrument", System.Data.SqlDbType.Int, 4, "FundPerformanceFeeCrystalisedInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundMovementComission", System.Data.SqlDbType.Float, 8, "FundMovementComission"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundManagementFeesBeforeSubscriptions", System.Data.SqlDbType.Bit, 1, "FundManagementFeesBeforeSubscriptions"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundUsesEqualisation", System.Data.SqlDbType.Int, 4, "FundUsesEqualisation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundManagementFeesAccruePeriod", System.Data.SqlDbType.Int, 4, "FundManagementFeesAccruePeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundManagementFeesPaymentPeriod", System.Data.SqlDbType.Int, 4, "FundManagementFeesPaymentPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeesAccruePeriod", System.Data.SqlDbType.Int, 4, "FundPerformanceFeesAccruePeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeesPaymentPeriod", System.Data.SqlDbType.Int, 4, "FundPerformanceFeesPaymentPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundSubscriptionNAVDatePeriod", System.Data.SqlDbType.Int, 4, "FundSubscriptionNAVDatePeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundSubscriptionSettlementDatePeriod", System.Data.SqlDbType.Int, 4, "FundSubscriptionSettlementDatePeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundRedemptionNAVDatePeriod", System.Data.SqlDbType.Int, 4, "FundRedemptionNAVDatePeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundRedemptionSettlementDatePeriod", System.Data.SqlDbType.Int, 4, "FundRedemptionSettlementDatePeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundReportGroup", System.Data.SqlDbType.NVarChar, 50, "FundReportGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundInitialSubscriptionFee", System.Data.SqlDbType.NVarChar, 50, "FundInitialSubscriptionFee"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundManagementFeeText", System.Data.SqlDbType.NVarChar, 50, "FundManagementFeeText"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundPerformanceFeeText", System.Data.SqlDbType.NVarChar, 50, "FundPerformanceFeeText"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundManagementCompany", System.Data.SqlDbType.NVarChar, 50, "FundManagementCompany"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundManagers", System.Data.SqlDbType.NVarChar, 100, "FundManagers"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundContactAddress", System.Data.SqlDbType.NVarChar, 100, "FundContactAddress"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundContactPhone", System.Data.SqlDbType.NVarChar, 50, "FundContactPhone"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundContactEmail", System.Data.SqlDbType.NVarChar, 50, "FundContactEmail"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundMinimumInvestment", System.Data.SqlDbType.NVarChar, 50, "FundMinimumInvestment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundSubscriptionPeriod", System.Data.SqlDbType.NVarChar, 50, "FundSubscriptionPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundSubscriptionNotice", System.Data.SqlDbType.NVarChar, 50, "FundSubscriptionNotice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundRedemptionPeriod", System.Data.SqlDbType.NVarChar, 50, "FundRedemptionPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundRedemptionNotice", System.Data.SqlDbType.NVarChar, 50, "FundRedemptionNotice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundJurisdiction", System.Data.SqlDbType.NVarChar, 50, "FundJurisdiction"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundAdministrator", System.Data.SqlDbType.NVarChar, 50, "FundAdministrator"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundCustodian", System.Data.SqlDbType.NVarChar, 50, "FundCustodian"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundListings", System.Data.SqlDbType.NVarChar, 50, "FundListings"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundLinkedListedInstrument", System.Data.SqlDbType.Int, 4, "FundLinkedListedInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundReviewGroup", System.Data.SqlDbType.Int, 4, "FundReviewGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundISIN", System.Data.SqlDbType.NVarChar, 20, "FundISIN"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundSEDOL", System.Data.SqlDbType.NVarChar, 20, "FundSEDOL"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundBloomberg", System.Data.SqlDbType.NVarChar, 30, "FundBloomberg"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundReutersCode", System.Data.SqlDbType.NVarChar, 20, "FundReutersCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundInvestmentStartDate", System.Data.SqlDbType.DateTime, 8, "FundInvestmentStartDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundDisclaimer", System.Data.SqlDbType.Text, Int32.MaxValue, "FundDisclaimer"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundObjective", System.Data.SqlDbType.Text, Int32.MaxValue, "FundObjective"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundAdministratorCode", System.Data.SqlDbType.NVarChar, 50, "FundAdministratorCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundForceMultiShareClassReconciliation", System.Data.SqlDbType.Bit, 1, "FundForceMultiShareClassReconciliation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundClosed", System.Data.SqlDbType.Int, 4, "FundClosed"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DateEntered", System.Data.SqlDbType.DateTime, 8, "DateEntered"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DateDeleted", System.Data.SqlDbType.DateTime, 8, "DateDeleted"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFund_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))


      Case "TBLFUNDCONTACTDETAILS"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFundContactDetails", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
        New System.Data.Common.DataColumnMapping("AdminContact", "AdminContact"), _
        New System.Data.Common.DataColumnMapping("AdminPhone", "AdminPhone"), _
        New System.Data.Common.DataColumnMapping("AdminFax", "AdminFax"), _
        New System.Data.Common.DataColumnMapping("AdminMainEmail", "AdminMainEmail"), _
        New System.Data.Common.DataColumnMapping("AdminSecondaryEMail", "AdminSecondaryEMail"), _
        New System.Data.Common.DataColumnMapping("EMailSalutation", "EMailSalutation"), _
        New System.Data.Common.DataColumnMapping("InvestorRelationsContact", "InvestorRelationsContact"), _
        New System.Data.Common.DataColumnMapping("InvestorRelationsPhone", "InvestorRelationsPhone"), _
        New System.Data.Common.DataColumnMapping("InvestorRelationsEmail", "InvestorRelationsEmail"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFundContactDetails_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFundContactDetails_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminContact", System.Data.SqlDbType.NVarChar, 100, "AdminContact"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminPhone", System.Data.SqlDbType.NVarChar, 50, "AdminPhone"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminFax", System.Data.SqlDbType.NVarChar, 50, "AdminFax"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminMainEmail", System.Data.SqlDbType.NVarChar, 100, "AdminMainEmail"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdminSecondaryEMail", System.Data.SqlDbType.NVarChar, 100, "AdminSecondaryEMail"))
        InsertCommand.Parameters.Add(New SqlParameter("@EMailSalutation", System.Data.SqlDbType.NVarChar, 50, "EMailSalutation"))
        InsertCommand.Parameters.Add(New SqlParameter("@InvestorRelationsContact", System.Data.SqlDbType.NVarChar, 100, "InvestorRelationsContact"))
        InsertCommand.Parameters.Add(New SqlParameter("@InvestorRelationsPhone", System.Data.SqlDbType.NVarChar, 50, "InvestorRelationsPhone"))
        InsertCommand.Parameters.Add(New SqlParameter("@InvestorRelationsEmail", System.Data.SqlDbType.NVarChar, 100, "InvestorRelationsEmail"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFundContactDetails_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminContact", System.Data.SqlDbType.NVarChar, 100, "AdminContact"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminPhone", System.Data.SqlDbType.NVarChar, 50, "AdminPhone"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminFax", System.Data.SqlDbType.NVarChar, 50, "AdminFax"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminMainEmail", System.Data.SqlDbType.NVarChar, 100, "AdminMainEmail"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdminSecondaryEMail", System.Data.SqlDbType.NVarChar, 100, "AdminSecondaryEMail"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EMailSalutation", System.Data.SqlDbType.NVarChar, 50, "EMailSalutation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InvestorRelationsContact", System.Data.SqlDbType.NVarChar, 100, "InvestorRelationsContact"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InvestorRelationsPhone", System.Data.SqlDbType.NVarChar, 50, "InvestorRelationsPhone"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InvestorRelationsEmail", System.Data.SqlDbType.NVarChar, 100, "InvestorRelationsEmail"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFundContactDetails_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))


      Case "TBLFUNDTYPE"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFundType", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FundTypeID", "FundTypeID"), _
        New System.Data.Common.DataColumnMapping("FundTypeDescription", "FundTypeDescription"), _
        New System.Data.Common.DataColumnMapping("FundTypeGroup", "FundTypeGroup"), _
        New System.Data.Common.DataColumnMapping("FundTypePertracIndex", "FundTypePertracIndex"), _
        New System.Data.Common.DataColumnMapping("FundTypeShortTrackRecordVolatility", "FundTypeShortTrackRecordVolatility"), _
        New System.Data.Common.DataColumnMapping("FundTypeSortOrder", "FundTypeSortOrder"), _
        New System.Data.Common.DataColumnMapping("FundTypeAttributionGrouping", "FundTypeAttributionGrouping"), _
        New System.Data.Common.DataColumnMapping("FundTypeIsAdministrativeOnly", "FundTypeIsAdministrativeOnly"), _
        New System.Data.Common.DataColumnMapping("FundTypeReporterGrouping", "FundTypeReporterGrouping"), _
        New System.Data.Common.DataColumnMapping("FundTypeShowInReporter", "FundTypeShowInReporter"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFundType_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFundType_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeID", System.Data.SqlDbType.Int, 4, "FundTypeID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeDescription", System.Data.SqlDbType.NVarChar, 100, "FundTypeDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeGroup", System.Data.SqlDbType.NVarChar, 100, "FundTypeGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypePertracIndex", System.Data.SqlDbType.Int, 4, "FundTypePertracIndex"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeShortTrackRecordVolatility", System.Data.SqlDbType.Float, 8, "FundTypeShortTrackRecordVolatility"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeSortOrder", System.Data.SqlDbType.Int, 4, "FundTypeSortOrder"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeAttributionGrouping", System.Data.SqlDbType.NVarChar, 100, "FundTypeAttributionGrouping"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeIsAdministrativeOnly", System.Data.SqlDbType.Int, 4, "FundTypeIsAdministrativeOnly"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeReporterGrouping", System.Data.SqlDbType.NVarChar, 100, "FundTypeReporterGrouping"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundTypeShowInReporter", System.Data.SqlDbType.Bit, 1, "FundTypeShowInReporter"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFundType_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeID", System.Data.SqlDbType.Int, 4, "FundTypeID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeDescription", System.Data.SqlDbType.NVarChar, 100, "FundTypeDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeGroup", System.Data.SqlDbType.NVarChar, 100, "FundTypeGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypePertracIndex", System.Data.SqlDbType.Int, 4, "FundTypePertracIndex"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeShortTrackRecordVolatility", System.Data.SqlDbType.Float, 8, "FundTypeShortTrackRecordVolatility"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeSortOrder", System.Data.SqlDbType.Int, 4, "FundTypeSortOrder"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeAttributionGrouping", System.Data.SqlDbType.NVarChar, 100, "FundTypeAttributionGrouping"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeIsAdministrativeOnly", System.Data.SqlDbType.Int, 4, "FundTypeIsAdministrativeOnly"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeReporterGrouping", System.Data.SqlDbType.NVarChar, 100, "FundTypeReporterGrouping"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundTypeShowInReporter", System.Data.SqlDbType.Bit, 1, "FundTypeShowInReporter"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFundType_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FundTypeID", System.Data.SqlDbType.Int, 4, "FundTypeID"))


      Case "TBLFUNDMILESTONES"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblFundMilestones", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("MileStoneID", "MileStoneID"), _
				New System.Data.Common.DataColumnMapping("MilestoneFundID", "MilestoneFundID"), _
				New System.Data.Common.DataColumnMapping("MilestoneDate", "MilestoneDate"), _
				New System.Data.Common.DataColumnMapping("MilestoneKnowledgeDate", "MilestoneKnowledgeDate"), _
				New System.Data.Common.DataColumnMapping("MilestoneLastDate", "MilestoneLastDate"), _
				New System.Data.Common.DataColumnMapping("Fund_NAV", "Fund_NAV"), _
				New System.Data.Common.DataColumnMapping("Fund_GNAV", "Fund_GNAV"), _
				New System.Data.Common.DataColumnMapping("Fund_BNAV", "Fund_BNAV"), _
				New System.Data.Common.DataColumnMapping("Fund_GAV", "Fund_GAV"), _
				New System.Data.Common.DataColumnMapping("Fund_UnitsSubscribed", "Fund_UnitsSubscribed"), _
				New System.Data.Common.DataColumnMapping("Fund_UnitsRedeemed", "Fund_UnitsRedeemed"), _
				New System.Data.Common.DataColumnMapping("Administrator_NAV", "Administrator_NAV"), _
				New System.Data.Common.DataColumnMapping("Administrator_GNAV", "Administrator_GNAV"), _
				New System.Data.Common.DataColumnMapping("Administrator_BNAV", "Administrator_BNAV"), _
				New System.Data.Common.DataColumnMapping("Administrator_GAV", "Administrator_GAV"), _
				New System.Data.Common.DataColumnMapping("Administrator_UnitsSubscribed", "Administrator_UnitsSubscribed"), _
				New System.Data.Common.DataColumnMapping("Administrator_UnitsRedeemed", "Administrator_UnitsRedeemed"), _
				New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFundMilestones_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFundMilestones_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@MileStoneID", System.Data.SqlDbType.Int, 4, "MileStoneID"))
        InsertCommand.Parameters.Add(New SqlParameter("@MilestoneFundID", System.Data.SqlDbType.Int, 4, "MilestoneFundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@MilestoneDate", System.Data.SqlDbType.DateTime, 8, "MilestoneDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@MilestoneKnowledgeDate", System.Data.SqlDbType.DateTime, 8, "MilestoneKnowledgeDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@MilestoneLastDate", System.Data.SqlDbType.DateTime, 8, "MilestoneLastDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@Fund_NAV", System.Data.SqlDbType.Float, 8, "Fund_NAV"))
				InsertCommand.Parameters.Add(New SqlParameter("@Fund_GNAV", System.Data.SqlDbType.Float, 8, "Fund_GNAV"))
				InsertCommand.Parameters.Add(New SqlParameter("@Fund_BNAV", System.Data.SqlDbType.Float, 8, "Fund_BNAV"))
				InsertCommand.Parameters.Add(New SqlParameter("@Fund_GAV", System.Data.SqlDbType.Float, 8, "Fund_GAV"))
				InsertCommand.Parameters.Add(New SqlParameter("@Fund_UnitsSubscribed", System.Data.SqlDbType.Float, 8, "Fund_UnitsSubscribed"))
				InsertCommand.Parameters.Add(New SqlParameter("@Fund_UnitsRedeemed", System.Data.SqlDbType.Float, 8, "Fund_UnitsRedeemed"))
				InsertCommand.Parameters.Add(New SqlParameter("@Administrator_NAV", System.Data.SqlDbType.Float, 8, "Administrator_NAV"))
				InsertCommand.Parameters.Add(New SqlParameter("@Administrator_GNAV", System.Data.SqlDbType.Float, 8, "Administrator_GNAV"))
				InsertCommand.Parameters.Add(New SqlParameter("@Administrator_BNAV", System.Data.SqlDbType.Float, 8, "Administrator_BNAV"))
				InsertCommand.Parameters.Add(New SqlParameter("@Administrator_GAV", System.Data.SqlDbType.Float, 8, "Administrator_GAV"))
				InsertCommand.Parameters.Add(New SqlParameter("@Administrator_UnitsSubscribed", System.Data.SqlDbType.Float, 8, "Administrator_UnitsSubscribed"))
				InsertCommand.Parameters.Add(New SqlParameter("@Administrator_UnitsRedeemed", System.Data.SqlDbType.Float, 8, "Administrator_UnitsRedeemed"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFundMilestones_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@MileStoneID", System.Data.SqlDbType.Int, 4, "MileStoneID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MilestoneFundID", System.Data.SqlDbType.Int, 4, "MilestoneFundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MilestoneDate", System.Data.SqlDbType.DateTime, 8, "MilestoneDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MilestoneKnowledgeDate", System.Data.SqlDbType.DateTime, 8, "MilestoneKnowledgeDate"))
				UpdateCommand.Parameters.Add(New SqlParameter("@MilestoneLastDate", System.Data.SqlDbType.DateTime, 8, "MilestoneLastDate"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Fund_NAV", System.Data.SqlDbType.Float, 8, "Fund_NAV"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Fund_GNAV", System.Data.SqlDbType.Float, 8, "Fund_GNAV"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Fund_BNAV", System.Data.SqlDbType.Float, 8, "Fund_BNAV"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Fund_GAV", System.Data.SqlDbType.Float, 8, "Fund_GAV"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Fund_UnitsSubscribed", System.Data.SqlDbType.Float, 8, "Fund_UnitsSubscribed"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Fund_UnitsRedeemed", System.Data.SqlDbType.Float, 8, "Fund_UnitsRedeemed"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Administrator_NAV", System.Data.SqlDbType.Float, 8, "Administrator_NAV"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Administrator_GNAV", System.Data.SqlDbType.Float, 8, "Administrator_GNAV"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Administrator_BNAV", System.Data.SqlDbType.Float, 8, "Administrator_BNAV"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Administrator_GAV", System.Data.SqlDbType.Float, 8, "Administrator_GAV"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Administrator_UnitsSubscribed", System.Data.SqlDbType.Float, 8, "Administrator_UnitsSubscribed"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Administrator_UnitsRedeemed", System.Data.SqlDbType.Float, 8, "Administrator_UnitsRedeemed"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFundMilestones_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@MileStoneID", System.Data.SqlDbType.Int, 4, "MileStoneID"))




      Case "TBLFUNDPERFORMANCEFEEPOINTS"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFundPerformanceFeePoints", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FeeID", "FeeID"), _
        New System.Data.Common.DataColumnMapping("FeeFund", "FeeFund"), _
        New System.Data.Common.DataColumnMapping("FeeDate", "FeeDate"), _
        New System.Data.Common.DataColumnMapping("FeeFundPrice", "FeeFundPrice"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblFundPerformanceFeePoints_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        ' Insert, Update and Delete to do.



      Case "TBLFX"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFX", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FXID", "FXID"), _
        New System.Data.Common.DataColumnMapping("FXCurrencyCode", "FXCurrencyCode"), _
        New System.Data.Common.DataColumnMapping("FXDate", "FXDate"), _
        New System.Data.Common.DataColumnMapping("FXRate", "FXRate"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblFX_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFX_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FXID", System.Data.SqlDbType.Int, 4, "FXID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXCurrencyCode", System.Data.SqlDbType.Int, 4, "FXCurrencyCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXDate", System.Data.SqlDbType.DateTime, 8, "FXDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXRate", System.Data.SqlDbType.Float, 8, "FXRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFX_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXID", System.Data.SqlDbType.Int, 4, "FXID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXCurrencyCode", System.Data.SqlDbType.Int, 4, "FXCurrencyCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXDate", System.Data.SqlDbType.DateTime, 8, "FXDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXRate", System.Data.SqlDbType.Float, 8, "FXRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFX_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FXID", System.Data.SqlDbType.Int, 4, "FXID"))


      Case "TBLFXONDATE"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblFX", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FXID", "FXID"), _
        New System.Data.Common.DataColumnMapping("FXCurrencyCode", "FXCurrencyCode"), _
        New System.Data.Common.DataColumnMapping("FXDate", "FXDate"), _
        New System.Data.Common.DataColumnMapping("FXRate", "FXRate"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblFX_OnDate]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@FXDate", System.Data.SqlDbType.Date)).Value = Now.Date
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblFX_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FXID", System.Data.SqlDbType.Int, 4, "FXID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXCurrencyCode", System.Data.SqlDbType.Int, 4, "FXCurrencyCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXDate", System.Data.SqlDbType.DateTime, 8, "FXDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXRate", System.Data.SqlDbType.Float, 8, "FXRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblFX_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXID", System.Data.SqlDbType.Int, 4, "FXID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXCurrencyCode", System.Data.SqlDbType.Int, 4, "FXCurrencyCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXDate", System.Data.SqlDbType.DateTime, 8, "FXDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXRate", System.Data.SqlDbType.Float, 8, "FXRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblFX_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FXID", System.Data.SqlDbType.Int, 4, "FXID"))


      Case "TBLGENOACONSTRAINTLIST"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblGenoaConstraintList", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ConstraintGroupID", "ConstraintGroupID"), _
        New System.Data.Common.DataColumnMapping("ConstraintGroupName", "ConstraintGroupName"), _
         New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblGenoaConstraintList_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblGenoaConstraintList_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ConstraintGroupID", System.Data.SqlDbType.Int, 4, "ConstraintGroupID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ConstraintGroupName", System.Data.SqlDbType.NVarChar, 100, "ConstraintGroupName"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblGenoaConstraintList_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ConstraintGroupID", System.Data.SqlDbType.Int, 4, "ConstraintGroupID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ConstraintGroupName", System.Data.SqlDbType.NVarChar, 100, "ConstraintGroupName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblGenoaConstraintList_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ConstraintGroupID", System.Data.SqlDbType.Int, 4, "ConstraintGroupID"))

      Case "TBLGENOACONSTRAINTITEMS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblGenoaConstraintItems", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ConstraintGroupID", "ConstraintGroupID"), _
        New System.Data.Common.DataColumnMapping("ConstraintID", "ConstraintID"), _
        New System.Data.Common.DataColumnMapping("CustomFieldID", "CustomFieldID"), _
        New System.Data.Common.DataColumnMapping("TestFieldLessThan", "TestFieldLessThan"), _
        New System.Data.Common.DataColumnMapping("TestFieldEqualTo", "TestFieldEqualTo"), _
        New System.Data.Common.DataColumnMapping("TestFieldGreaterThan", "TestFieldGreaterThan"), _
        New System.Data.Common.DataColumnMapping("TestValueString", "TestValueString"), _
        New System.Data.Common.DataColumnMapping("TestValueNumeric", "TestValueNumeric"), _
        New System.Data.Common.DataColumnMapping("TestValueDate", "TestValueDate"), _
        New System.Data.Common.DataColumnMapping("LowerLimit", "LowerLimit"), _
        New System.Data.Common.DataColumnMapping("UpperLimit", "UpperLimit"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblGenoaConstraintItems_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblGenoaConstraintItems_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ConstraintGroupID", System.Data.SqlDbType.Int, 4, "ConstraintGroupID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ConstraintID", System.Data.SqlDbType.Int, 4, "ConstraintID"))
        InsertCommand.Parameters.Add(New SqlParameter("@CustomFieldID", System.Data.SqlDbType.Int, 4, "CustomFieldID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TestFieldLessThan", System.Data.SqlDbType.Bit, 1, "TestFieldLessThan"))
        InsertCommand.Parameters.Add(New SqlParameter("@TestFieldEqualTo", System.Data.SqlDbType.Bit, 1, "TestFieldEqualTo"))
        InsertCommand.Parameters.Add(New SqlParameter("@TestFieldGreaterThan", System.Data.SqlDbType.Bit, 1, "TestFieldGreaterThan"))
        InsertCommand.Parameters.Add(New SqlParameter("@TestValueString", System.Data.SqlDbType.NVarChar, 50, "TestValueString"))
        InsertCommand.Parameters.Add(New SqlParameter("@TestValueNumeric", System.Data.SqlDbType.Float, 8, "TestValueNumeric"))
        InsertCommand.Parameters.Add(New SqlParameter("@TestValueDate", System.Data.SqlDbType.DateTime, 8, "TestValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@LowerLimit", System.Data.SqlDbType.Float, 8, "LowerLimit"))
        InsertCommand.Parameters.Add(New SqlParameter("@UpperLimit", System.Data.SqlDbType.Float, 8, "UpperLimit"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblGenoaConstraintItems_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ConstraintGroupID", System.Data.SqlDbType.Int, 4, "ConstraintGroupID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ConstraintID", System.Data.SqlDbType.Int, 4, "ConstraintID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CustomFieldID", System.Data.SqlDbType.Int, 4, "CustomFieldID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TestFieldLessThan", System.Data.SqlDbType.Bit, 1, "TestFieldLessThan"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TestFieldEqualTo", System.Data.SqlDbType.Bit, 1, "TestFieldEqualTo"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TestFieldGreaterThan", System.Data.SqlDbType.Bit, 1, "TestFieldGreaterThan"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TestValueString", System.Data.SqlDbType.NVarChar, 50, "TestValueString"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TestValueNumeric", System.Data.SqlDbType.Float, 8, "TestValueNumeric"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TestValueDate", System.Data.SqlDbType.DateTime, 8, "TestValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LowerLimit", System.Data.SqlDbType.Float, 8, "LowerLimit"))
        UpdateCommand.Parameters.Add(New SqlParameter("@UpperLimit", System.Data.SqlDbType.Float, 8, "UpperLimit"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblGenoaConstraintItems_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ConstraintID", System.Data.SqlDbType.Int, 4, "ConstraintID"))

      Case "TBLGENOASAVEDSEARCHLIST"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblGenoaSavedSearchList", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("SearchID", "SearchID"), _
        New System.Data.Common.DataColumnMapping("SearchName", "SearchName"), _
        New System.Data.Common.DataColumnMapping("VisibleToAllUsers", "VisibleToAllUsers"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblGenoaSavedSearchList_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblGenoaSavedSearchList_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@SearchID", System.Data.SqlDbType.Int, 4, "SearchID"))
        InsertCommand.Parameters.Add(New SqlParameter("@SearchName", System.Data.SqlDbType.NVarChar, 100, "SearchName"))
        InsertCommand.Parameters.Add(New SqlParameter("@VisibleToAllUsers", System.Data.SqlDbType.Bit, 1, "VisibleToAllUsers"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblGenoaSavedSearchList_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@SearchID", System.Data.SqlDbType.Int, 4, "SearchID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SearchName", System.Data.SqlDbType.NVarChar, 100, "SearchName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@VisibleToAllUsers", System.Data.SqlDbType.Bit, 1, "VisibleToAllUsers"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblGenoaSavedSearchList_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@SearchID", System.Data.SqlDbType.Int, 4, "SearchID"))

      Case "TBLGENOASAVEDSEARCHITEMS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblGenoaSavedSearchItems", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("SearchID", "SearchID"), _
        New System.Data.Common.DataColumnMapping("SearchItemID", "SearchItemID"), _
        New System.Data.Common.DataColumnMapping("FieldIsStatic", "FieldIsStatic"), _
        New System.Data.Common.DataColumnMapping("FieldIsCustom", "FieldIsCustom"), _
        New System.Data.Common.DataColumnMapping("IsAND", "IsAND"), _
        New System.Data.Common.DataColumnMapping("IsOR", "IsOR"), _
        New System.Data.Common.DataColumnMapping("SelectFieldID", "SelectFieldID"), _
        New System.Data.Common.DataColumnMapping("ConditionOperand", "ConditionOperand"), _
        New System.Data.Common.DataColumnMapping("SelectValue1", "SelectValue1"), _
        New System.Data.Common.DataColumnMapping("SelectValue2", "SelectValue2"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblGenoaSavedSearchItems_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblGenoaSavedSearchItems_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@SearchID", System.Data.SqlDbType.Int, 4, "SearchID"))
        InsertCommand.Parameters.Add(New SqlParameter("@SearchItemID", System.Data.SqlDbType.Int, 4, "SearchItemID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldIsStatic", System.Data.SqlDbType.Bit, 1, "FieldIsStatic"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldIsCustom", System.Data.SqlDbType.Bit, 1, "FieldIsCustom"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsAND", System.Data.SqlDbType.Bit, 1, "IsAND"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsOR", System.Data.SqlDbType.Bit, 1, "IsOR"))
        InsertCommand.Parameters.Add(New SqlParameter("@SelectFieldID", System.Data.SqlDbType.NVarChar, 50, "SelectFieldID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ConditionOperand", System.Data.SqlDbType.NVarChar, 10, "ConditionOperand"))
        InsertCommand.Parameters.Add(New SqlParameter("@SelectValue1", System.Data.SqlDbType.NVarChar, 100, "SelectValue1"))
        InsertCommand.Parameters.Add(New SqlParameter("@SelectValue2", System.Data.SqlDbType.NVarChar, 100, "SelectValue2"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblGenoaSavedSearchItems_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@SearchID", System.Data.SqlDbType.Int, 4, "SearchID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SearchItemID", System.Data.SqlDbType.Int, 4, "SearchItemID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldIsStatic", System.Data.SqlDbType.Bit, 1, "FieldIsStatic"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldIsCustom", System.Data.SqlDbType.Bit, 1, "FieldIsCustom"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsAND", System.Data.SqlDbType.Bit, 1, "IsAND"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsOR", System.Data.SqlDbType.Bit, 1, "IsOR"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SelectFieldID", System.Data.SqlDbType.NVarChar, 50, "SelectFieldID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ConditionOperand", System.Data.SqlDbType.NVarChar, 10, "ConditionOperand"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SelectValue1", System.Data.SqlDbType.NVarChar, 100, "SelectValue1"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SelectValue2", System.Data.SqlDbType.NVarChar, 100, "SelectValue2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblGenoaSavedSearchItems_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@SearchItemID", System.Data.SqlDbType.Int, 4, "SearchItemID"))

			Case "TBLGREEKS"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblGreeks", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("GreekID", "GreekID"), _
				New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
				New System.Data.Common.DataColumnMapping("GreekDate", "GreekDate"), _
				New System.Data.Common.DataColumnMapping("Delta", "Delta"), _
				New System.Data.Common.DataColumnMapping("Gamma", "Gamma"), _
				New System.Data.Common.DataColumnMapping("Rho", "Rho"), _
				New System.Data.Common.DataColumnMapping("Vega", "Vega"), _
				New System.Data.Common.DataColumnMapping("Theta", "Theta"), _
				New System.Data.Common.DataColumnMapping("Volatility", "Volatility"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblGreeks_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblGreeks_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@GreekID", System.Data.SqlDbType.Int, 4, "GreekID"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				InsertCommand.Parameters.Add(New SqlParameter("@GreekDate", System.Data.SqlDbType.DateTime, 8, "GreekDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@Delta", System.Data.SqlDbType.Float, 8, "Delta"))
				InsertCommand.Parameters.Add(New SqlParameter("@Gamma", System.Data.SqlDbType.Float, 8, "Gamma"))
				InsertCommand.Parameters.Add(New SqlParameter("@Rho", System.Data.SqlDbType.Float, 8, "Rho"))
				InsertCommand.Parameters.Add(New SqlParameter("@Vega", System.Data.SqlDbType.Float, 8, "Vega"))
				InsertCommand.Parameters.Add(New SqlParameter("@Theta", System.Data.SqlDbType.Float, 8, "Theta"))
				InsertCommand.Parameters.Add(New SqlParameter("@Volatility", System.Data.SqlDbType.Float, 8, "Volatility"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblGreeks_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@GreekID", System.Data.SqlDbType.Int, 4, "GreekID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@GreekDate", System.Data.SqlDbType.DateTime, 8, "GreekDate"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Delta", System.Data.SqlDbType.Float, 8, "Delta"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Gamma", System.Data.SqlDbType.Float, 8, "Gamma"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Rho", System.Data.SqlDbType.Float, 8, "Rho"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Vega", System.Data.SqlDbType.Float, 8, "Vega"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Theta", System.Data.SqlDbType.Float, 8, "Theta"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Volatility", System.Data.SqlDbType.Float, 8, "Volatility"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblGreeks_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@GreekID", System.Data.SqlDbType.Int, 4, "GreekID"))

			Case "TBLGREEKS_SELECTBYINSTRUMENT"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblGreeks", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("GreekID", "GreekID"), _
				New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
				New System.Data.Common.DataColumnMapping("GreekDate", "GreekDate"), _
				New System.Data.Common.DataColumnMapping("Delta", "Delta"), _
				New System.Data.Common.DataColumnMapping("Gamma", "Gamma"), _
				New System.Data.Common.DataColumnMapping("Rho", "Rho"), _
				New System.Data.Common.DataColumnMapping("Vega", "Vega"), _
				New System.Data.Common.DataColumnMapping("Theta", "Theta"), _
				New System.Data.Common.DataColumnMapping("Volatility", "Volatility"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblGreeks_SelectByInstrument]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int))
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

			Case "TBLGREEKS_SELECTBYDATE"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblGreeks", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("GreekID", "GreekID"), _
				New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
				New System.Data.Common.DataColumnMapping("GreekDate", "GreekDate"), _
				New System.Data.Common.DataColumnMapping("Delta", "Delta"), _
				New System.Data.Common.DataColumnMapping("Gamma", "Gamma"), _
				New System.Data.Common.DataColumnMapping("Rho", "Rho"), _
				New System.Data.Common.DataColumnMapping("Vega", "Vega"), _
				New System.Data.Common.DataColumnMapping("Theta", "Theta"), _
				New System.Data.Common.DataColumnMapping("Volatility", "Volatility"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblGreeks_SelectByDate]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@GreekDate", System.Data.SqlDbType.DateTime))
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


			Case "TBLGROUPLIST"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblGroupList", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("GroupListID", "GroupListID"), _
				New System.Data.Common.DataColumnMapping("GroupListName", "GroupListName"), _
				New System.Data.Common.DataColumnMapping("GroupGroup", "GroupGroup"), _
				New System.Data.Common.DataColumnMapping("GroupDateFrom", "GroupDateFrom"), _
				New System.Data.Common.DataColumnMapping("GroupDateTo", "GroupDateTo"), _
				New System.Data.Common.DataColumnMapping("DefaultConstraintGroup", "DefaultConstraintGroup"), _
				New System.Data.Common.DataColumnMapping("DefaultCovarianceMatrix", "DefaultCovarianceMatrix"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblGroupList_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblGroupList_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@GroupListID", System.Data.SqlDbType.Int, 4, "GroupListID"))
				InsertCommand.Parameters.Add(New SqlParameter("@GroupListName", System.Data.SqlDbType.NVarChar, 100, "GroupListName"))
				InsertCommand.Parameters.Add(New SqlParameter("@GroupGroup", System.Data.SqlDbType.NVarChar, 50, "GroupGroup"))
				InsertCommand.Parameters.Add(New SqlParameter("@GroupDateFrom", System.Data.SqlDbType.DateTime, 8, "GroupDateFrom"))
				InsertCommand.Parameters.Add(New SqlParameter("@GroupDateTo", System.Data.SqlDbType.DateTime, 8, "GroupDateTo"))
				InsertCommand.Parameters.Add(New SqlParameter("@DefaultConstraintGroup", System.Data.SqlDbType.Int, 4, "DefaultConstraintGroup"))
				InsertCommand.Parameters.Add(New SqlParameter("@DefaultCovarianceMatrix", System.Data.SqlDbType.Int, 4, "DefaultCovarianceMatrix"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblGroupList_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@GroupListID", System.Data.SqlDbType.Int, 4, "GroupListID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@GroupListName", System.Data.SqlDbType.NVarChar, 100, "GroupListName"))
				UpdateCommand.Parameters.Add(New SqlParameter("@GroupGroup", System.Data.SqlDbType.NVarChar, 50, "GroupGroup"))
				UpdateCommand.Parameters.Add(New SqlParameter("@GroupDateFrom", System.Data.SqlDbType.DateTime, 8, "GroupDateFrom"))
				UpdateCommand.Parameters.Add(New SqlParameter("@GroupDateTo", System.Data.SqlDbType.DateTime, 8, "GroupDateTo"))
				UpdateCommand.Parameters.Add(New SqlParameter("@DefaultConstraintGroup", System.Data.SqlDbType.Int, 4, "DefaultConstraintGroup"))
				UpdateCommand.Parameters.Add(New SqlParameter("@DefaultCovarianceMatrix", System.Data.SqlDbType.Int, 4, "DefaultCovarianceMatrix"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblGroupList_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@GroupListID", System.Data.SqlDbType.Int, 4, "GroupListID"))


      Case "TBLGROUPITEMS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblGroupItems", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("GroupID", "GroupID"), _
        New System.Data.Common.DataColumnMapping("GroupItemID", "GroupItemID"), _
        New System.Data.Common.DataColumnMapping("GroupPertracCode", "GroupPertracCode"), _
        New System.Data.Common.DataColumnMapping("GroupIndexCode", "GroupIndexCode"), _
        New System.Data.Common.DataColumnMapping("GroupSector", "GroupSector"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblGroupItems_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblGroupItems_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupItemID", System.Data.SqlDbType.Int, 4, "GroupItemID"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupPertracCode", System.Data.SqlDbType.Int, 4, "GroupPertracCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupIndexCode", System.Data.SqlDbType.Int, 4, "GroupIndexCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupSector", System.Data.SqlDbType.NVarChar, 100, "GroupSector"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblGroupItems_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupItemID", System.Data.SqlDbType.Int, 4, "GroupItemID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupPertracCode", System.Data.SqlDbType.Int, 4, "GroupPertracCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupIndexCode", System.Data.SqlDbType.Int, 4, "GroupIndexCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupSector", System.Data.SqlDbType.NVarChar, 100, "GroupSector"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblGroupItems_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@GroupItemID", System.Data.SqlDbType.Int, 4, "GroupItemID"))


      Case "TBLGROUPITEMDATA"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblGroupItemData", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("GroupItemID", "GroupItemID"), _
        New System.Data.Common.DataColumnMapping("GroupPertracCode", "GroupPertracCode"), _
        New System.Data.Common.DataColumnMapping("GroupLiquidity", "GroupLiquidity"), _
        New System.Data.Common.DataColumnMapping("GroupHolding", "GroupHolding"), _
        New System.Data.Common.DataColumnMapping("GroupNewHolding", "GroupNewHolding"), _
        New System.Data.Common.DataColumnMapping("GroupPercent", "GroupPercent"), _
        New System.Data.Common.DataColumnMapping("GroupExpectedReturn", "GroupExpectedReturn"), _
        New System.Data.Common.DataColumnMapping("GroupUpperBound", "GroupUpperBound"), _
        New System.Data.Common.DataColumnMapping("GroupLowerBound", "GroupLowerBound"), _
        New System.Data.Common.DataColumnMapping("GroupFloor", "GroupFloor"), _
        New System.Data.Common.DataColumnMapping("GroupCap", "GroupCap"), _
        New System.Data.Common.DataColumnMapping("GroupTradeSize", "GroupTradeSize"), _
        New System.Data.Common.DataColumnMapping("GroupBeta", "GroupBeta"), _
        New System.Data.Common.DataColumnMapping("GroupAlpha", "GroupAlpha"), _
        New System.Data.Common.DataColumnMapping("GroupIndexE", "GroupIndexE"), _
        New System.Data.Common.DataColumnMapping("GroupBetaE", "GroupBetaE"), _
        New System.Data.Common.DataColumnMapping("GroupAlphaE", "GroupAlphaE"), _
        New System.Data.Common.DataColumnMapping("GroupStdErr", "GroupStdErr"), _
        New System.Data.Common.DataColumnMapping("GroupScalingFactor", "GroupScalingFactor"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblGroupItemData_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblGroupItemData_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupItemID", System.Data.SqlDbType.Int, 4, "GroupItemID"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupPertracCode", System.Data.SqlDbType.Int, 4, "GroupPertracCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupLiquidity", System.Data.SqlDbType.Int, 4, "GroupLiquidity"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupHolding", System.Data.SqlDbType.Float, 8, "GroupHolding"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupNewHolding", System.Data.SqlDbType.Float, 8, "GroupNewHolding"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupPercent", System.Data.SqlDbType.Float, 8, "GroupPercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupExpectedReturn", System.Data.SqlDbType.Float, 8, "GroupExpectedReturn"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupUpperBound", System.Data.SqlDbType.Float, 8, "GroupUpperBound"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupLowerBound", System.Data.SqlDbType.Float, 8, "GroupLowerBound"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupFloor", System.Data.SqlDbType.Bit, 1, "GroupFloor"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupCap", System.Data.SqlDbType.Bit, 1, "GroupCap"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupTradeSize", System.Data.SqlDbType.Float, 8, "GroupTradeSize"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupBeta", System.Data.SqlDbType.Float, 8, "GroupBeta"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupAlpha", System.Data.SqlDbType.Float, 8, "GroupAlpha"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupIndexE", System.Data.SqlDbType.Float, 8, "GroupIndexE"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupBetaE", System.Data.SqlDbType.Float, 8, "GroupBetaE"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupAlphaE", System.Data.SqlDbType.Float, 8, "GroupAlphaE"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupStdErr", System.Data.SqlDbType.Float, 8, "GroupStdErr"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupScalingFactor", System.Data.SqlDbType.Float, 8, "GroupScalingFactor"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblGroupItemData_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupItemID", System.Data.SqlDbType.Int, 4, "GroupItemID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupPertracCode", System.Data.SqlDbType.Int, 4, "GroupPertracCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupLiquidity", System.Data.SqlDbType.Int, 4, "GroupLiquidity"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupHolding", System.Data.SqlDbType.Float, 8, "GroupHolding"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupNewHolding", System.Data.SqlDbType.Float, 8, "GroupNewHolding"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupPercent", System.Data.SqlDbType.Float, 8, "GroupPercent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupExpectedReturn", System.Data.SqlDbType.Float, 8, "GroupExpectedReturn"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupUpperBound", System.Data.SqlDbType.Float, 8, "GroupUpperBound"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupLowerBound", System.Data.SqlDbType.Float, 8, "GroupLowerBound"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupFloor", System.Data.SqlDbType.Bit, 1, "GroupFloor"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupCap", System.Data.SqlDbType.Bit, 1, "GroupCap"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupTradeSize", System.Data.SqlDbType.Float, 8, "GroupTradeSize"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupBeta", System.Data.SqlDbType.Float, 8, "GroupBeta"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupAlpha", System.Data.SqlDbType.Float, 8, "GroupAlpha"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupIndexE", System.Data.SqlDbType.Float, 8, "GroupIndexE"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupBetaE", System.Data.SqlDbType.Float, 8, "GroupBetaE"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupAlphaE", System.Data.SqlDbType.Float, 8, "GroupAlphaE"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupStdErr", System.Data.SqlDbType.Float, 8, "GroupStdErr"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupScalingFactor", System.Data.SqlDbType.Float, 8, "GroupScalingFactor"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblGroupItemData_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@GroupItemID", System.Data.SqlDbType.Int, 4, "GroupItemID"))


      Case "TBLGROUPSAGGREGATED"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblGroupsAggregated", _
        New System.Data.Common.DataColumnMapping() { _
        New System.Data.Common.DataColumnMapping("GroupListID", "GroupListID"), _
        New System.Data.Common.DataColumnMapping("GroupListName", "GroupListName"), _
        New System.Data.Common.DataColumnMapping("GroupGroup", "GroupGroup"), _
        New System.Data.Common.DataColumnMapping("GroupDateFrom", "GroupDateFrom"), _
        New System.Data.Common.DataColumnMapping("GroupDateTo", "GroupDateTo"), _
        New System.Data.Common.DataColumnMapping("GroupItemID", "GroupItemID"), _
        New System.Data.Common.DataColumnMapping("GroupPertracCode", "GroupPertracCode"), _
        New System.Data.Common.DataColumnMapping("PertracName", "PertracName"), _
        New System.Data.Common.DataColumnMapping("GroupIndexCode", "GroupIndexCode"), _
        New System.Data.Common.DataColumnMapping("IndexName", "IndexName"), _
        New System.Data.Common.DataColumnMapping("GroupLiquidity", "GroupLiquidity"), _
        New System.Data.Common.DataColumnMapping("GroupHolding", "GroupHolding"), _
        New System.Data.Common.DataColumnMapping("GroupTrade", "GroupTrade"), _
        New System.Data.Common.DataColumnMapping("GroupNewHolding", "GroupNewHolding"), _
        New System.Data.Common.DataColumnMapping("GroupPercent", "GroupPercent"), _
        New System.Data.Common.DataColumnMapping("GroupExpectedReturn", "GroupExpectedReturn"), _
        New System.Data.Common.DataColumnMapping("GroupUpperBound", "GroupUpperBound"), _
        New System.Data.Common.DataColumnMapping("GroupLowerBound", "GroupLowerBound"), _
        New System.Data.Common.DataColumnMapping("GroupFloor", "GroupFloor"), _
        New System.Data.Common.DataColumnMapping("GroupCap", "GroupCap"), _
        New System.Data.Common.DataColumnMapping("GroupTradeSize", "GroupTradeSize"), _
        New System.Data.Common.DataColumnMapping("GroupBeta", "GroupBeta"), _
        New System.Data.Common.DataColumnMapping("GroupAlpha", "GroupAlpha"), _
        New System.Data.Common.DataColumnMapping("GroupIndexE", "GroupIndexE"), _
        New System.Data.Common.DataColumnMapping("GroupBetaE", "GroupBetaE"), _
        New System.Data.Common.DataColumnMapping("GroupAlphaE", "GroupAlphaE"), _
        New System.Data.Common.DataColumnMapping("GroupStdErr", "GroupStdErr"), _
        New System.Data.Common.DataColumnMapping("GroupScalingFactor", "GroupScalingFactor"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblGroupsAggregated_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@GroupListID", System.Data.SqlDbType.Int)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))



      Case "TBLINDEXMAPPING"
        ' tblIndexMapping
        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblIndexMapping", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("MappingID", "MappingID"), _
        New System.Data.Common.DataColumnMapping("IndexTicker", "IndexTicker"), _
        New System.Data.Common.DataColumnMapping("IndexInstrumentID", "IndexInstrumentID"), _
        New System.Data.Common.DataColumnMapping("MappedFundTypeID", "MappedFundTypeID"), _
        New System.Data.Common.DataColumnMapping("MappingStartDate", "MappingStartDate"), _
        New System.Data.Common.DataColumnMapping("MappingEndDate", "MappingEndDate"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Select Command
        SelectCommand.CommandText = "[adp_tblIndexMapping_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@IndexTicker", System.Data.SqlDbType.NVarChar, 50)).Value = DBNull.Value
        SelectCommand.Parameters.Add(New SqlParameter("@IndexInstrumentID", System.Data.SqlDbType.Int)).Value = DBNull.Value
        SelectCommand.Parameters.Add(New SqlParameter("@MappedFundTypeID", System.Data.SqlDbType.Int)).Value = DBNull.Value
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblIndexMapping_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@MappingID", System.Data.SqlDbType.Int, 4, "MappingID"))
        InsertCommand.Parameters.Add(New SqlParameter("@IndexTicker", System.Data.SqlDbType.NVarChar, 50, "IndexTicker"))
        InsertCommand.Parameters.Add(New SqlParameter("@IndexInstrumentID", System.Data.SqlDbType.Int, 4, "IndexInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@MappedFundTypeID", System.Data.SqlDbType.Int, 4, "MappedFundTypeID"))
        InsertCommand.Parameters.Add(New SqlParameter("@MappingStartDate", System.Data.SqlDbType.DateTime, 8, "MappingStartDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@MappingEndDate", System.Data.SqlDbType.DateTime, 8, "MappingEndDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblIndexMapping_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@MappingID", System.Data.SqlDbType.Int, 4, "MappingID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IndexTicker", System.Data.SqlDbType.NVarChar, 50, "IndexTicker"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IndexInstrumentID", System.Data.SqlDbType.Int, 4, "IndexInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MappedFundTypeID", System.Data.SqlDbType.Int, 4, "MappedFundTypeID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MappingStartDate", System.Data.SqlDbType.DateTime, 8, "MappingStartDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MappingEndDate", System.Data.SqlDbType.DateTime, 8, "MappingEndDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblIndexMapping_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@MappingID", System.Data.SqlDbType.Int, 4, "MappingID"))


      Case "TBLINSTRUMENT"

        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblInstrument", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("InstrumentCode", "InstrumentCode"), _
        New System.Data.Common.DataColumnMapping("InstrumentPFPCID", "InstrumentPFPCID"), _
        New System.Data.Common.DataColumnMapping("InstrumentPertracCode", "InstrumentPertracCode"), _
        New System.Data.Common.DataColumnMapping("InstrumentParent", "InstrumentParent"), _
        New System.Data.Common.DataColumnMapping("InstrumentParentEquivalentRatio", "InstrumentParentEquivalentRatio"), _
        New System.Data.Common.DataColumnMapping("InstrumentDescription", "InstrumentDescription"), _
        New System.Data.Common.DataColumnMapping("InstrumentClass", "InstrumentClass"), _
        New System.Data.Common.DataColumnMapping("InstrumentType", "InstrumentType"), _
        New System.Data.Common.DataColumnMapping("InstrumentFundID", "InstrumentFundID"), _
        New System.Data.Common.DataColumnMapping("InstrumentFundType", "InstrumentFundType"), _
        New System.Data.Common.DataColumnMapping("InstrumentCurrency", "InstrumentCurrency"), _
        New System.Data.Common.DataColumnMapping("InstrumentCurrencyID", "InstrumentCurrencyID"), _
        New System.Data.Common.DataColumnMapping("InstrumentUnderlying", "InstrumentUnderlying"), _
        New System.Data.Common.DataColumnMapping("InstrumentIsFXFuture", "InstrumentIsFXFuture"), _
        New System.Data.Common.DataColumnMapping("InstrumentFXFutureConstantCurrency", "InstrumentFXFutureConstantCurrency"), _
        New System.Data.Common.DataColumnMapping("InstrumentBenchmark", "InstrumentBenchmark"), _
        New System.Data.Common.DataColumnMapping("InstrumentUnitPerformanceBenchmark", "InstrumentUnitPerformanceBenchmark"), _
        New System.Data.Common.DataColumnMapping("InstrumentUnitManagementFees", "InstrumentUnitManagementFees"), _
        New System.Data.Common.DataColumnMapping("InstrumentUnitPerformanceFees", "InstrumentUnitPerformanceFees"), _
        New System.Data.Common.DataColumnMapping("InstrumentUnitPerformanceHurdle", "InstrumentUnitPerformanceHurdle"), _
        New System.Data.Common.DataColumnMapping("InstrumentContractSize", "InstrumentContractSize"), _
        New System.Data.Common.DataColumnMapping("InstrumentMultiplier", "InstrumentMultiplier"), _
        New System.Data.Common.DataColumnMapping("InstrumentSharesPerOption", "InstrumentSharesPerOption"), _
        New System.Data.Common.DataColumnMapping("InstrumentExpectedReturn", "InstrumentExpectedReturn"), _
        New System.Data.Common.DataColumnMapping("InstrumentWorstCase", "InstrumentWorstCase"), _
        New System.Data.Common.DataColumnMapping("InstrumentLimitDisplay", "InstrumentLimitDisplay"), _
        New System.Data.Common.DataColumnMapping("InstrumentNoticeDays", "InstrumentNoticeDays"), _
        New System.Data.Common.DataColumnMapping("InstrumentDefaultSettlementDays_Buy", "InstrumentDefaultSettlementDays_Buy"), _
        New System.Data.Common.DataColumnMapping("InstrumentDefaultSettlementDays_Sell", "InstrumentDefaultSettlementDays_Sell"), _
        New System.Data.Common.DataColumnMapping("InstrumentDealingCutOffTime", "InstrumentDealingCutOffTime"), _
        New System.Data.Common.DataColumnMapping("InstrumentCalendar", "InstrumentCalendar"), _
        New System.Data.Common.DataColumnMapping("InstrumentDealingPeriod", "InstrumentDealingPeriod"), _
        New System.Data.Common.DataColumnMapping("InstrumentDealingBaseDate", "InstrumentDealingBaseDate"), _
        New System.Data.Common.DataColumnMapping("InstrumentLockupPeriod", "InstrumentLockupPeriod"), _
        New System.Data.Common.DataColumnMapping("InstrumentLastValidTradeDate", "InstrumentLastValidTradeDate"), _
        New System.Data.Common.DataColumnMapping("InstrumentExpiryDate", "InstrumentExpiryDate"), _
        New System.Data.Common.DataColumnMapping("InstrumentOrderPrecision", "InstrumentOrderPrecision"), _
        New System.Data.Common.DataColumnMapping("InstrumentOrderOnlyByValue", "InstrumentOrderOnlyByValue"), _
        New System.Data.Common.DataColumnMapping("InstrumentPenalty", "InstrumentPenalty"), _
        New System.Data.Common.DataColumnMapping("InstrumentPenaltyComment", "InstrumentPenaltyComment"), _
        New System.Data.Common.DataColumnMapping("[InstrumentUpfrontfees]", "[InstrumentUpfrontfees]"), _
        New System.Data.Common.DataColumnMapping("InstrumentLiquidityComment", "InstrumentLiquidityComment"), _
        New System.Data.Common.DataColumnMapping("InstrumentExcludeGAV", "InstrumentExcludeGAV"), _
        New System.Data.Common.DataColumnMapping("InstrumentExcludeAUM", "InstrumentExcludeAUM"), _
        New System.Data.Common.DataColumnMapping("InstrumentIsManagementFee", "InstrumentIsManagementFee"), _
        New System.Data.Common.DataColumnMapping("InstrumentIsIncentiveFee", "InstrumentIsIncentiveFee"), _
        New System.Data.Common.DataColumnMapping("InstrumentIsCrystalisedIncentive", "InstrumentIsCrystalisedIncentive"), _
        New System.Data.Common.DataColumnMapping("InstrumentIsMovementCommission", "InstrumentIsMovementCommission"), _
        New System.Data.Common.DataColumnMapping("InstrumentIsEqualisation", "InstrumentIsEqualisation"), _
        New System.Data.Common.DataColumnMapping("InstrumentIsLeverageInstrument", "InstrumentIsLeverageInstrument"), _
        New System.Data.Common.DataColumnMapping("InstrumentLeverageCounterparty", "InstrumentLeverageCounterparty"), _
        New System.Data.Common.DataColumnMapping("InstrumentExchangeCode", "InstrumentExchangeCode"), _
        New System.Data.Common.DataColumnMapping("InstrumentISIN", "InstrumentISIN"), _
        New System.Data.Common.DataColumnMapping("InstrumentSEDOL", "InstrumentSEDOL"), _
        New System.Data.Common.DataColumnMapping("InstrumentMorningstar", "InstrumentMorningstar"), _
        New System.Data.Common.DataColumnMapping("InstrumentPortfolioTicker", "InstrumentPortfolioTicker"), _
        New System.Data.Common.DataColumnMapping("InstrumentCountryISO2", "InstrumentCountryISO2"), _
        New System.Data.Common.DataColumnMapping("AttributionMarket", "AttributionMarket"), _
        New System.Data.Common.DataColumnMapping("AttributionSector", "AttributionSector"), _
        New System.Data.Common.DataColumnMapping("InstrumentCapturePrice", "InstrumentCapturePrice"), _
        New System.Data.Common.DataColumnMapping("InstrumentReviewGroup", "InstrumentReviewGroup"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Select Command
        SelectCommand.CommandText = "[adp_tblInstrument_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        ' 
        InsertCommand.CommandText = "[adp_tblInstrument_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentCode", System.Data.SqlDbType.NVarChar, 100, "InstrumentCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentPFPCID", System.Data.SqlDbType.NVarChar, 100, "InstrumentPFPCID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentPertracCode", System.Data.SqlDbType.Int, 4, "InstrumentPertracCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentParent", System.Data.SqlDbType.Int, 4, "InstrumentParent"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentParentEquivalentRatio", System.Data.SqlDbType.Float, 8, "InstrumentParentEquivalentRatio"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentDescription", System.Data.SqlDbType.NVarChar, 100, "InstrumentDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentClass", System.Data.SqlDbType.NVarChar, 100, "InstrumentClass"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentType", System.Data.SqlDbType.Int, 4, "InstrumentType"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentFundID", System.Data.SqlDbType.Int, 4, "InstrumentFundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentFundType", System.Data.SqlDbType.Int, 4, "InstrumentFundType"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentCurrency", System.Data.SqlDbType.NVarChar, 50, "InstrumentCurrency"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentCurrencyID", System.Data.SqlDbType.Int, 4, "InstrumentCurrencyID"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentUnderlying", System.Data.SqlDbType.Int, 4, "InstrumentUnderlying"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentIsFXFuture", System.Data.SqlDbType.Bit, 1, "InstrumentIsFXFuture"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentFXFutureConstantCurrency", System.Data.SqlDbType.Float, 8, "InstrumentFXFutureConstantCurrency"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentBenchmark", System.Data.SqlDbType.Int, 4, "InstrumentBenchmark"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentUnitPerformanceBenchmark", System.Data.SqlDbType.Int, 4, "InstrumentUnitPerformanceBenchmark"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentUnitManagementFees", System.Data.SqlDbType.Float, 8, "InstrumentUnitManagementFees"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentUnitPerformanceFees", System.Data.SqlDbType.Float, 8, "InstrumentUnitPerformanceFees"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentUnitPerformanceHurdle", System.Data.SqlDbType.Float, 8, "InstrumentUnitPerformanceHurdle"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentContractSize", System.Data.SqlDbType.Float, 8, "InstrumentContractSize"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentMultiplier", System.Data.SqlDbType.Float, 8, "InstrumentMultiplier"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentSharesPerOption", System.Data.SqlDbType.Float, 8, "InstrumentSharesPerOption"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentExpectedReturn", System.Data.SqlDbType.Float, 8, "InstrumentExpectedReturn"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentWorstCase", System.Data.SqlDbType.Float, 8, "InstrumentWorstCase"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentLimitDisplay", System.Data.SqlDbType.Bit, 1, "InstrumentLimitDisplay"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentNoticeDays", System.Data.SqlDbType.Int, 4, "InstrumentNoticeDays"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentDefaultSettlementDays_Buy", System.Data.SqlDbType.Int, 4, "InstrumentDefaultSettlementDays_Buy"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentDefaultSettlementDays_Sell", System.Data.SqlDbType.Int, 4, "InstrumentDefaultSettlementDays_Sell"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentDealingCutOffTime", System.Data.SqlDbType.Int, 4, "InstrumentDealingCutOffTime"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentCalendar", System.Data.SqlDbType.Int, 4, "InstrumentCalendar"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentDealingPeriod", System.Data.SqlDbType.Int, 4, "InstrumentDealingPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentDealingBaseDate", System.Data.SqlDbType.SmallDateTime, 4, "InstrumentDealingBaseDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentLockupPeriod", System.Data.SqlDbType.Int, 4, "InstrumentLockupPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentLastValidTradeDate", System.Data.SqlDbType.DateTime, 8, "InstrumentLastValidTradeDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentExpiryDate", System.Data.SqlDbType.DateTime, 8, "InstrumentExpiryDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentOrderPrecision", System.Data.SqlDbType.Int, 4, "InstrumentOrderPrecision"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentOrderOnlyByValue", System.Data.SqlDbType.Bit, 1, "InstrumentOrderOnlyByValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentPenalty", System.Data.SqlDbType.Float, 8, "InstrumentPenalty"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentPenaltyComment", System.Data.SqlDbType.NVarChar, 100, "InstrumentPenaltyComment"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentUpfrontfees", System.Data.SqlDbType.Int, 4, "InstrumentUpfrontfees"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentLiquidityComment", System.Data.SqlDbType.NVarChar, 100, "InstrumentLiquidityComment"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentExcludeGAV", System.Data.SqlDbType.Int, 4, "InstrumentExcludeGAV"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentExcludeAUM", System.Data.SqlDbType.Bit, 1, "InstrumentExcludeAUM"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentIsManagementFee", System.Data.SqlDbType.Int, 4, "InstrumentIsManagementFee"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentIsIncentiveFee", System.Data.SqlDbType.Int, 4, "InstrumentIsIncentiveFee"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentIsCrystalisedIncentive", System.Data.SqlDbType.Bit, 1, "InstrumentIsCrystalisedIncentive"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentIsMovementCommission", System.Data.SqlDbType.Bit, 1, "InstrumentIsMovementCommission"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentIsEqualisation", System.Data.SqlDbType.Int, 4, "InstrumentIsEqualisation"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentIsLeverageInstrument", System.Data.SqlDbType.Int, 4, "InstrumentIsLeverageInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentLeverageCounterparty", System.Data.SqlDbType.Int, 4, "InstrumentLeverageCounterparty"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentExchangeCode", System.Data.SqlDbType.NVarChar, 50, "InstrumentExchangeCode"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentISIN", System.Data.SqlDbType.NVarChar, 25, "InstrumentISIN"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentSEDOL", System.Data.SqlDbType.NVarChar, 25, "InstrumentSEDOL"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentMorningstar", System.Data.SqlDbType.NVarChar, 25, "InstrumentMorningstar"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentPortfolioTicker", System.Data.SqlDbType.NVarChar, 50, "InstrumentPortfolioTicker"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentCountryISO2", System.Data.SqlDbType.NVarChar, 2, "InstrumentCountryISO2"))
        InsertCommand.Parameters.Add(New SqlParameter("@AttributionMarket", System.Data.SqlDbType.NVarChar, 50, "AttributionMarket"))
        InsertCommand.Parameters.Add(New SqlParameter("@AttributionSector", System.Data.SqlDbType.NVarChar, 50, "AttributionSector"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentCapturePrice", System.Data.SqlDbType.Bit, 1, "InstrumentCapturePrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentReviewGroup", System.Data.SqlDbType.Int, 4, "InstrumentReviewGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblInstrument_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentCode", System.Data.SqlDbType.NVarChar, 100, "InstrumentCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentPFPCID", System.Data.SqlDbType.NVarChar, 100, "InstrumentPFPCID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentPertracCode", System.Data.SqlDbType.Int, 4, "InstrumentPertracCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentParent", System.Data.SqlDbType.Int, 4, "InstrumentParent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentParentEquivalentRatio", System.Data.SqlDbType.Float, 8, "InstrumentParentEquivalentRatio"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentDescription", System.Data.SqlDbType.NVarChar, 100, "InstrumentDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentClass", System.Data.SqlDbType.NVarChar, 100, "InstrumentClass"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentType", System.Data.SqlDbType.Int, 4, "InstrumentType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentFundID", System.Data.SqlDbType.Int, 4, "InstrumentFundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentFundType", System.Data.SqlDbType.Int, 4, "InstrumentFundType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentCurrency", System.Data.SqlDbType.NVarChar, 50, "InstrumentCurrency"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentCurrencyID", System.Data.SqlDbType.Int, 4, "InstrumentCurrencyID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentUnderlying", System.Data.SqlDbType.Int, 4, "InstrumentUnderlying"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentIsFXFuture", System.Data.SqlDbType.Bit, 1, "InstrumentIsFXFuture"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentFXFutureConstantCurrency", System.Data.SqlDbType.Float, 8, "InstrumentFXFutureConstantCurrency"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentBenchmark", System.Data.SqlDbType.Int, 4, "InstrumentBenchmark"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentUnitPerformanceBenchmark", System.Data.SqlDbType.Int, 4, "InstrumentUnitPerformanceBenchmark"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentUnitManagementFees", System.Data.SqlDbType.Float, 8, "InstrumentUnitManagementFees"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentUnitPerformanceFees", System.Data.SqlDbType.Float, 8, "InstrumentUnitPerformanceFees"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentUnitPerformanceHurdle", System.Data.SqlDbType.Float, 8, "InstrumentUnitPerformanceHurdle"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentContractSize", System.Data.SqlDbType.Float, 8, "InstrumentContractSize"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentMultiplier", System.Data.SqlDbType.Float, 8, "InstrumentMultiplier"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentSharesPerOption", System.Data.SqlDbType.Float, 8, "InstrumentSharesPerOption"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentExpectedReturn", System.Data.SqlDbType.Float, 8, "InstrumentExpectedReturn"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentWorstCase", System.Data.SqlDbType.Float, 8, "InstrumentWorstCase"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentLimitDisplay", System.Data.SqlDbType.Bit, 1, "InstrumentLimitDisplay"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentNoticeDays", System.Data.SqlDbType.Int, 4, "InstrumentNoticeDays"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentDefaultSettlementDays_Buy", System.Data.SqlDbType.Int, 4, "InstrumentDefaultSettlementDays_Buy"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentDefaultSettlementDays_Sell", System.Data.SqlDbType.Int, 4, "InstrumentDefaultSettlementDays_Sell"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentDealingCutOffTime", System.Data.SqlDbType.Int, 4, "InstrumentDealingCutOffTime"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentCalendar", System.Data.SqlDbType.Int, 4, "InstrumentCalendar"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentDealingPeriod", System.Data.SqlDbType.Int, 4, "InstrumentDealingPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentDealingBaseDate", System.Data.SqlDbType.SmallDateTime, 4, "InstrumentDealingBaseDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentLockupPeriod", System.Data.SqlDbType.Int, 4, "InstrumentLockupPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentLastValidTradeDate", System.Data.SqlDbType.DateTime, 8, "InstrumentLastValidTradeDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentExpiryDate", System.Data.SqlDbType.DateTime, 8, "InstrumentExpiryDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentOrderPrecision", System.Data.SqlDbType.Int, 4, "InstrumentOrderPrecision"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentOrderOnlyByValue", System.Data.SqlDbType.Bit, 1, "InstrumentOrderOnlyByValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentPenalty", System.Data.SqlDbType.Float, 8, "InstrumentPenalty"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentPenaltyComment", System.Data.SqlDbType.NVarChar, 100, "InstrumentPenaltyComment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentUpfrontfees", System.Data.SqlDbType.Int, 4, "InstrumentUpfrontfees"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentLiquidityComment", System.Data.SqlDbType.NVarChar, 100, "InstrumentLiquidityComment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentExcludeGAV", System.Data.SqlDbType.Int, 4, "InstrumentExcludeGAV"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentExcludeAUM", System.Data.SqlDbType.Bit, 1, "InstrumentExcludeAUM"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentIsManagementFee", System.Data.SqlDbType.Int, 4, "InstrumentIsManagementFee"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentIsIncentiveFee", System.Data.SqlDbType.Int, 4, "InstrumentIsIncentiveFee"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentIsCrystalisedIncentive", System.Data.SqlDbType.Bit, 1, "InstrumentIsCrystalisedIncentive"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentIsMovementCommission", System.Data.SqlDbType.Bit, 1, "InstrumentIsMovementCommission"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentIsEqualisation", System.Data.SqlDbType.Int, 4, "InstrumentIsEqualisation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentIsLeverageInstrument", System.Data.SqlDbType.Int, 4, "InstrumentIsLeverageInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentLeverageCounterparty", System.Data.SqlDbType.Int, 4, "InstrumentLeverageCounterparty"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentExchangeCode", System.Data.SqlDbType.NVarChar, 50, "InstrumentExchangeCode"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentISIN", System.Data.SqlDbType.NVarChar, 25, "InstrumentISIN"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentSEDOL", System.Data.SqlDbType.NVarChar, 25, "InstrumentSEDOL"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentMorningstar", System.Data.SqlDbType.NVarChar, 25, "InstrumentMorningstar"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentPortfolioTicker", System.Data.SqlDbType.NVarChar, 50, "InstrumentPortfolioTicker"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentCountryISO2", System.Data.SqlDbType.NVarChar, 2, "InstrumentCountryISO2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AttributionMarket", System.Data.SqlDbType.NVarChar, 50, "AttributionMarket"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AttributionSector", System.Data.SqlDbType.NVarChar, 50, "AttributionSector"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentCapturePrice", System.Data.SqlDbType.Bit, 1, "InstrumentCapturePrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentReviewGroup", System.Data.SqlDbType.Int, 4, "InstrumentReviewGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand

        DeleteCommand.CommandText = "[adp_tblInstrument_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))

      Case "TBLINSTRUMENTNOTES"

        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblInstrumentNotes", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("InstrumentManagerNote", "InstrumentManagerNote"), _
        New System.Data.Common.DataColumnMapping("InstrumentUnitNote", "InstrumentUnitNote"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Select Command
        SelectCommand.CommandText = "[adp_tblInstrumentNotes_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        ' 
        InsertCommand.CommandText = "[adp_tblInstrumentNotes_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentManagerNote", System.Data.SqlDbType.NText, Int32.MaxValue, "InstrumentManagerNote"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentUnitNote", System.Data.SqlDbType.NText, Int32.MaxValue, "InstrumentUnitNote"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblInstrumentNotes_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentManagerNote", System.Data.SqlDbType.NText, Int32.MaxValue, "InstrumentManagerNote"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentUnitNote", System.Data.SqlDbType.NText, Int32.MaxValue, "InstrumentUnitNote"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand

        DeleteCommand.CommandText = "[adp_tblInstrumentNotes_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))

      Case "TBLINSTRUMENTFLORENCEENTITYLINK"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblInstrumentFlorenceEntityLink", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("LinkID", "LinkID"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("EntityID", "EntityID"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblInstrumentFlorenceEntityLink_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblInstrumentFlorenceEntityLink_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@LinkID", System.Data.SqlDbType.Int, 4, "LinkID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@EntityID", System.Data.SqlDbType.Int, 4, "EntityID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        ' 
        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblInstrumentFlorenceEntityLink_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@LinkID", System.Data.SqlDbType.Int, 4, "LinkID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EntityID", System.Data.SqlDbType.Int, 4, "EntityID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblInstrumentFlorenceEntityLink_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        DeleteCommand.Parameters.Add(New SqlParameter("@EntityID", System.Data.SqlDbType.Int, 4, "EntityID"))



      Case "TBLINSTRUMENTNAMESGROSSUNITS" ' tblInstrumentNamesGrossUnits

        ' Table Mappings

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblInstrumentNamesGrossUnits", _
        New System.Data.Common.DataColumnMapping() { _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("InstrumentDescription", "InstrumentDescription") _
        })})

        ' Select Command
        SelectCommand.CommandText = "[adp_tblInstrumentNamesGrossUnits_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


      Case "TBLINSTRUMENTTYPE"

        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblInstrumentType", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("InstrumentTypeID", "InstrumentTypeID"), _
				New System.Data.Common.DataColumnMapping("InstrumentTypeDescription", "InstrumentTypeDescription"), _
				New System.Data.Common.DataColumnMapping("InstrumentTypeTreatAs", "InstrumentTypeTreatAs"), _
				New System.Data.Common.DataColumnMapping("InstrumentTypeFuturesStylePricing", "InstrumentTypeFuturesStylePricing"), _
				New System.Data.Common.DataColumnMapping("InstrumentTypeHasDelta", "InstrumentTypeHasDelta"), _
				New System.Data.Common.DataColumnMapping("InstrumentTypeIsExpense", "InstrumentTypeIsExpense"), _
				New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblInstrumentType_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblInstrumentType_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentTypeID", System.Data.SqlDbType.Int, 4, "InstrumentTypeID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentTypeDescription", System.Data.SqlDbType.NVarChar, 100, "InstrumentTypeDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentTypeTreatAs", System.Data.SqlDbType.Int, 4, "InstrumentTypeTreatAs"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentTypeFuturesStylePricing", System.Data.SqlDbType.Bit, 1, "InstrumentTypeFuturesStylePricing"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentTypeHasDelta", System.Data.SqlDbType.Bit, 1, "InstrumentTypeHasDelta"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentTypeIsExpense", System.Data.SqlDbType.Bit, 1, "InstrumentTypeIsExpense"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblInstrumentType_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentTypeID", System.Data.SqlDbType.Int, 4, "InstrumentTypeID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentTypeDescription", System.Data.SqlDbType.NVarChar, 100, "InstrumentTypeDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentTypeTreatAs", System.Data.SqlDbType.Int, 4, "InstrumentTypeTreatAs"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentTypeFuturesStylePricing", System.Data.SqlDbType.Bit, 1, "InstrumentTypeFuturesStylePricing"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentTypeHasDelta", System.Data.SqlDbType.Bit, 1, "InstrumentTypeHasDelta"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentTypeIsExpense", System.Data.SqlDbType.Bit, 1, "InstrumentTypeIsExpense"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblInstrumentType_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@InstrumentTypeID", System.Data.SqlDbType.Int, 4, "InstrumentTypeID"))


      Case "TBLINVESTORGROUP"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblInvestorGroup", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("IGID", "IGID"), _
        New System.Data.Common.DataColumnMapping("IGName", "IGName"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblInvestorGroup_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblInvestorGroup_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@IGID", System.Data.SqlDbType.Int, 4, "IGID"))
        InsertCommand.Parameters.Add(New SqlParameter("@IGName", System.Data.SqlDbType.NVarChar, 50, "IGName"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblInvestorGroup_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@IGID", System.Data.SqlDbType.Int, 4, "IGID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IGName", System.Data.SqlDbType.NVarChar, 50, "IGName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblInvestorGroup_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@IGID", System.Data.SqlDbType.Int, 4, "IGID"))


      Case "TBLJOB"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblJob", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("JobID", "JobID"), _
        New System.Data.Common.DataColumnMapping("JobPerson", "JobPerson"), _
        New System.Data.Common.DataColumnMapping("JobFirm", "JobFirm"), _
        New System.Data.Common.DataColumnMapping("JobLocation", "JobLocation"), _
        New System.Data.Common.DataColumnMapping("JobFrom", "JobFrom"), _
        New System.Data.Common.DataColumnMapping("JobTo", "JobTo"), _
        New System.Data.Common.DataColumnMapping("JobTitle", "JobTitle"), _
        New System.Data.Common.DataColumnMapping("JobCurrent", "JobCurrent"), _
        New System.Data.Common.DataColumnMapping("JobDateEntered", "JobDateEntered"), _
        New System.Data.Common.DataColumnMapping("JobDateDeleted", "JobDateDeleted") _
        })})

        ' Set Select Command
        '
        SelectCommand.CommandText = "[adp_tblJob_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblJob_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@JobID", System.Data.SqlDbType.Int, 4, "JobID"))
        InsertCommand.Parameters.Add(New SqlParameter("@JobPerson", System.Data.SqlDbType.Int, 4, "JobPerson"))
        InsertCommand.Parameters.Add(New SqlParameter("@JobFirm", System.Data.SqlDbType.Int, 4, "JobFirm"))
        InsertCommand.Parameters.Add(New SqlParameter("@JobLocation", System.Data.SqlDbType.Int, 4, "JobLocation"))
        InsertCommand.Parameters.Add(New SqlParameter("@JobFrom", System.Data.SqlDbType.DateTime, 8, "JobFrom"))
        InsertCommand.Parameters.Add(New SqlParameter("@JobTo", System.Data.SqlDbType.DateTime, 8, "JobTo"))
        InsertCommand.Parameters.Add(New SqlParameter("@JobLocation", System.Data.SqlDbType.Int, 4, "JobLocation"))
        InsertCommand.Parameters.Add(New SqlParameter("@JobTitle", System.Data.SqlDbType.NVarChar, 255, "JobTitle"))
        InsertCommand.Parameters.Add(New SqlParameter("@JobCurrent", System.Data.SqlDbType.Bit, 1, "JobCurrent"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblJob_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobID", System.Data.SqlDbType.Int, 4, "JobID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobPerson", System.Data.SqlDbType.Int, 4, "JobPerson"))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobFirm", System.Data.SqlDbType.Int, 4, "JobFirm"))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobLocation", System.Data.SqlDbType.Int, 4, "JobLocation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobFrom", System.Data.SqlDbType.DateTime, 8, "JobFrom"))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobTo", System.Data.SqlDbType.DateTime, 8, "JobTo"))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobLocation", System.Data.SqlDbType.Int, 4, "JobLocation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobTitle", System.Data.SqlDbType.NVarChar, 255, "JobTitle"))
        UpdateCommand.Parameters.Add(New SqlParameter("@JobCurrent", System.Data.SqlDbType.Bit, 1, "JobCurrent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblJob_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@JobID", System.Data.SqlDbType.Int, 4, "JobID"))


      Case "TBLLIMITS"    ' To do : Update, Insert & Delete commands.

        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblLimits", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("limID", "limID"), _
				New System.Data.Common.DataColumnMapping("limDescription", "limDescription"), _
				New System.Data.Common.DataColumnMapping("limFund", "limFund"), _
				New System.Data.Common.DataColumnMapping("limOnPremium", "limOnPremium"), _
				New System.Data.Common.DataColumnMapping("limType", "limType"), _
				New System.Data.Common.DataColumnMapping("limInstrument", "limInstrument"), _
				New System.Data.Common.DataColumnMapping("limCharacteristic", "limCharacteristic"), _
				New System.Data.Common.DataColumnMapping("limSector", "limSector"), _
				New System.Data.Common.DataColumnMapping("limSectorGroup", "limSectorGroup"), _
				New System.Data.Common.DataColumnMapping("limDealingPeriod", "limDealingPeriod"), _
				New System.Data.Common.DataColumnMapping("limGreaterOrLessThan", "limGreaterOrLessThan"), _
				New System.Data.Common.DataColumnMapping("limLimit", "limLimit"), _
				New System.Data.Common.DataColumnMapping("limIsPercent", "limIsPercent"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblLimits_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblLimits_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitID", System.Data.SqlDbType.Int, 4, "limID"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitDescription", System.Data.SqlDbType.NVarChar, 200, "limDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitFund", System.Data.SqlDbType.Int, 4, "limFund"))
				InsertCommand.Parameters.Add(New SqlParameter("@LimitOnPremium", System.Data.SqlDbType.Int, 4, "limOnPremium"))
				InsertCommand.Parameters.Add(New SqlParameter("@LimitType", System.Data.SqlDbType.Int, 4, "limType"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitInstrument", System.Data.SqlDbType.Int, 4, "limInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitCharacteristic", System.Data.SqlDbType.Int, 4, "limCharacteristic"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitSector", System.Data.SqlDbType.Int, 4, "limSector"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitSectorGroup", System.Data.SqlDbType.NVarChar, 100, "limSectorGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitDealingPeriod", System.Data.SqlDbType.Int, 4, "limDealingPeriod"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitGreaterOrLessThan", System.Data.SqlDbType.Int, 4, "limGreaterOrLessThan"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitLimit", System.Data.SqlDbType.Float, 8, "limLimit"))
        InsertCommand.Parameters.Add(New SqlParameter("@LimitIsPercent", System.Data.SqlDbType.Int, 4, "limIsPercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblLimits_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitID", System.Data.SqlDbType.Int, 4, "limID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitDescription", System.Data.SqlDbType.NVarChar, 200, "limDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitFund", System.Data.SqlDbType.Int, 4, "limFund"))
				UpdateCommand.Parameters.Add(New SqlParameter("@LimitOnPremium", System.Data.SqlDbType.Int, 4, "limOnPremium"))
				UpdateCommand.Parameters.Add(New SqlParameter("@LimitType", System.Data.SqlDbType.Int, 4, "limType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitInstrument", System.Data.SqlDbType.Int, 4, "limInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitCharacteristic", System.Data.SqlDbType.Int, 4, "limCharacteristic"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitSector", System.Data.SqlDbType.Int, 4, "limSector"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitSectorGroup", System.Data.SqlDbType.NVarChar, 100, "limSectorGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitDealingPeriod", System.Data.SqlDbType.Int, 4, "limDealingPeriod"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitGreaterOrLessThan", System.Data.SqlDbType.Int, 4, "limGreaterOrLessThan"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitLimit", System.Data.SqlDbType.Float, 8, "limLimit"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LimitIsPercent", System.Data.SqlDbType.Int, 4, "limIsPercent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblLimits_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@LimitID", System.Data.SqlDbType.Int, 4, "limID"))



      Case "TBLLOCATIONS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblLocations", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("CityID", "CityID"), _
        New System.Data.Common.DataColumnMapping("Country", "Country"), _
        New System.Data.Common.DataColumnMapping("CountryName", "CountryName"), _
        New System.Data.Common.DataColumnMapping("City", "City"), _
        New System.Data.Common.DataColumnMapping("Region", "Region"), _
        New System.Data.Common.DataColumnMapping("RegionName", "RegionName"), _
        New System.Data.Common.DataColumnMapping("Population", "Population"), _
        New System.Data.Common.DataColumnMapping("Lattitude", "Lattitude"), _
        New System.Data.Common.DataColumnMapping("Longitude", "Longitude"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblLocations_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '

        'SqlUpdateCommand


        'SqlDeleteCommand
        '


      Case "TBLMANAGEMENTCOMPANY"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblManagementCompany", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FirmID", "FirmID"), _
        New System.Data.Common.DataColumnMapping("FirmName", "FirmName"), _
        New System.Data.Common.DataColumnMapping("FirmDescription", "FirmDescription"), _
        New System.Data.Common.DataColumnMapping("FirmPrimaryLocation", "FirmPrimaryLocation"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblManagementCompany_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblManagementCompany_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FirmID", System.Data.SqlDbType.Int, 4, "FirmID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FirmName", System.Data.SqlDbType.NVarChar, 100, "FirmName"))
        InsertCommand.Parameters.Add(New SqlParameter("@FirmDescription", System.Data.SqlDbType.Text, 2147483647, "FirmDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@FirmPrimaryLocation", System.Data.SqlDbType.Int, 4, "FirmPrimaryLocation"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblManagementCompany_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FirmID", System.Data.SqlDbType.Int, 4, "FirmID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FirmName", System.Data.SqlDbType.NVarChar, 100, "FirmName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FirmDescription", System.Data.SqlDbType.Text, 2147483647, "FirmDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FirmPrimaryLocation", System.Data.SqlDbType.Int, 4, "FirmPrimaryLocation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblManagementCompany_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FirmID", System.Data.SqlDbType.Int, 4, "FirmID"))


      Case "TBLMARKETINSTRUMENTS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblMarketInstruments", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("MarketInstrumentID", "MarketInstrumentID"), _
        New System.Data.Common.DataColumnMapping("Ticker", "Ticker"), _
        New System.Data.Common.DataColumnMapping("Description", "Description"), _
        New System.Data.Common.DataColumnMapping("InstrumentType", "InstrumentType"), _
        New System.Data.Common.DataColumnMapping("InstrumentCurrencyID", "InstrumentCurrencyID"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblMarketInstruments_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblMarketInstruments_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@MarketInstrumentID", System.Data.SqlDbType.Int, 4, "MarketInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Ticker", System.Data.SqlDbType.NVarChar, 35, "Ticker"))
        InsertCommand.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 100, "Description"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentType", System.Data.SqlDbType.Int, 4, "InstrumentType"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentCurrencyID", System.Data.SqlDbType.Int, 4, "InstrumentCurrencyID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblMarketInstruments_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@MarketInstrumentID", System.Data.SqlDbType.Int, 4, "MarketInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Ticker", System.Data.SqlDbType.NVarChar, 35, "Ticker"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 100, "Description"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentType", System.Data.SqlDbType.Int, 4, "InstrumentType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentCurrencyID", System.Data.SqlDbType.Int, 4, "InstrumentCurrencyID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblMarketInstruments_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@MarketInstrumentID", System.Data.SqlDbType.Int, 4, "MarketInstrumentID"))



      Case "TBLMARKETPRICES"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblMarketPrices", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("MarketInstrumentID", "MarketInstrumentID"), _
        New System.Data.Common.DataColumnMapping("PriceDate", "PriceDate"), _
        New System.Data.Common.DataColumnMapping("PriceDataType", "PriceDataType"), _
        New System.Data.Common.DataColumnMapping("PriceValue", "PriceValue"), _
        New System.Data.Common.DataColumnMapping("TextValue", "TextValue"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblMarketPrices_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblMarketPrices_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@MarketInstrumentID", System.Data.SqlDbType.Int, 4, "MarketInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceDate", System.Data.SqlDbType.DateTime, 8, "PriceDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceDataType", System.Data.SqlDbType.Int, 4, "PriceDataType"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceValue", System.Data.SqlDbType.Float, 8, "PriceValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@TextValue", System.Data.SqlDbType.NVarChar, 20, "TextValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblMarketPrices_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@MarketInstrumentID", System.Data.SqlDbType.Int, 4, "MarketInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceDate", System.Data.SqlDbType.DateTime, 8, "PriceDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceDataType", System.Data.SqlDbType.Int, 4, "PriceDataType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceValue", System.Data.SqlDbType.Float, 8, "PriceValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TextValue", System.Data.SqlDbType.NVarChar, 20, "TextValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblMarketPrices_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@MarketInstrumentID", System.Data.SqlDbType.Int, 4, "MarketInstrumentID"))
        DeleteCommand.Parameters.Add(New SqlParameter("@PriceDate", System.Data.SqlDbType.DateTime, 8, "PriceDate"))
        DeleteCommand.Parameters.Add(New SqlParameter("@PriceDataType", System.Data.SqlDbType.Int, 4, "PriceDataType"))



      Case "TBLMEETING"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblMeeting", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("MeetingID", "MeetingID"), _
        New System.Data.Common.DataColumnMapping("MeetingSelect", "MeetingSelect"), _
        New System.Data.Common.DataColumnMapping("MeetingTitle", "MeetingTitle"), _
        New System.Data.Common.DataColumnMapping("MeetingContact", "MeetingContact"), _
        New System.Data.Common.DataColumnMapping("MeetingDate", "MeetingDate"), _
        New System.Data.Common.DataColumnMapping("MeetingPlace", "MeetingPlace"), _
        New System.Data.Common.DataColumnMapping("MeetingNote", "MeetingNote"), _
        New System.Data.Common.DataColumnMapping("MeetingGood", "MeetingGood"), _
        New System.Data.Common.DataColumnMapping("MeetingBad", "MeetingBad"), _
        New System.Data.Common.DataColumnMapping("MeetingWisdom", "MeetingWisdom"), _
        New System.Data.Common.DataColumnMapping("MeetingManagementCompanyID", "MeetingManagementCompanyID"), _
        New System.Data.Common.DataColumnMapping("MeetingLocation", "MeetingLocation"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID1", "MeetingFundID1"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID2", "MeetingFundID2"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID3", "MeetingFundID3"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID4", "MeetingFundID4"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID5", "MeetingFundID5"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson1", "MeetingFCPerson1"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson2", "MeetingFCPerson2"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson3", "MeetingFCPerson3"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson4", "MeetingFCPerson4"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson5", "MeetingFCPerson5"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson1", "MeetingFundPerson1"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson2", "MeetingFundPerson2"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson3", "MeetingFundPerson3"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson4", "MeetingFundPerson4"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson5", "MeetingFundPerson5"), _
        New System.Data.Common.DataColumnMapping("MeetingStatus", "MeetingStatus"), _
        New System.Data.Common.DataColumnMapping("MeetingExistingFund", "MeetingExistingFund"), _
        New System.Data.Common.DataColumnMapping("MeetingFundType", "MeetingFundType"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblMeeting_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblMeeting_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingID", System.Data.SqlDbType.Int, 4, "MeetingID"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingTitle", System.Data.SqlDbType.NVarChar, 255, "MeetingTitle"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingContact", System.Data.SqlDbType.NVarChar, 255, "MeetingContact"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingDate", System.Data.SqlDbType.DateTime, 8, "MeetingDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingPlace", System.Data.SqlDbType.NVarChar, 255, "MeetingPlace"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingNote", System.Data.SqlDbType.Text, 2147483647, "MeetingNote"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingGood", System.Data.SqlDbType.NVarChar, 500, "MeetingGood"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingBad", System.Data.SqlDbType.NVarChar, 500, "MeetingBad"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingWisdom", System.Data.SqlDbType.NVarChar, 255, "MeetingWisdom"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingManagementCompanyID", System.Data.SqlDbType.Int, 4, "MeetingManagementCompanyID"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingLocation", System.Data.SqlDbType.Int, 4, "MeetingLocation"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundID1", System.Data.SqlDbType.Int, 4, "MeetingFundID1"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundID2", System.Data.SqlDbType.Int, 4, "MeetingFundID2"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundID3", System.Data.SqlDbType.Int, 4, "MeetingFundID3"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundID4", System.Data.SqlDbType.Int, 4, "MeetingFundID4"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundID5", System.Data.SqlDbType.Int, 4, "MeetingFundID5"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson1", System.Data.SqlDbType.Int, 4, "MeetingFCPerson1"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson2", System.Data.SqlDbType.Int, 4, "MeetingFCPerson2"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson3", System.Data.SqlDbType.Int, 4, "MeetingFCPerson3"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson4", System.Data.SqlDbType.Int, 4, "MeetingFCPerson4"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson5", System.Data.SqlDbType.Int, 4, "MeetingFCPerson5"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson1", System.Data.SqlDbType.Int, 4, "MeetingFundPerson1"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson2", System.Data.SqlDbType.Int, 4, "MeetingFundPerson2"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson3", System.Data.SqlDbType.Int, 4, "MeetingFundPerson3"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson4", System.Data.SqlDbType.Int, 4, "MeetingFundPerson4"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson5", System.Data.SqlDbType.Int, 4, "MeetingFundPerson5"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingStatus", System.Data.SqlDbType.Int, 4, "MeetingStatus"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingFundType", System.Data.SqlDbType.Int, 4, "MeetingFundType"))
        InsertCommand.Parameters.Add(New SqlParameter("@MeetingExistingFund", System.Data.SqlDbType.Bit, 1, "MeetingExistingFund"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblMeeting_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingID", System.Data.SqlDbType.Int, 4, "MeetingID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingTitle", System.Data.SqlDbType.NVarChar, 255, "MeetingTitle"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingContact", System.Data.SqlDbType.NVarChar, 255, "MeetingContact"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingDate", System.Data.SqlDbType.DateTime, 8, "MeetingDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingPlace", System.Data.SqlDbType.NVarChar, 255, "MeetingPlace"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingNote", System.Data.SqlDbType.Text, 2147483647, "MeetingNote"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingGood", System.Data.SqlDbType.NVarChar, 500, "MeetingGood"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingBad", System.Data.SqlDbType.NVarChar, 500, "MeetingBad"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingWisdom", System.Data.SqlDbType.NVarChar, 255, "MeetingWisdom"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingManagementCompanyID", System.Data.SqlDbType.Int, 4, "MeetingManagementCompanyID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingLocation", System.Data.SqlDbType.Int, 4, "MeetingLocation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundID1", System.Data.SqlDbType.Int, 4, "MeetingFundID1"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundID2", System.Data.SqlDbType.Int, 4, "MeetingFundID2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundID3", System.Data.SqlDbType.Int, 4, "MeetingFundID3"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundID4", System.Data.SqlDbType.Int, 4, "MeetingFundID4"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundID5", System.Data.SqlDbType.Int, 4, "MeetingFundID5"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson1", System.Data.SqlDbType.Int, 4, "MeetingFCPerson1"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson2", System.Data.SqlDbType.Int, 4, "MeetingFCPerson2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson3", System.Data.SqlDbType.Int, 4, "MeetingFCPerson3"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson4", System.Data.SqlDbType.Int, 4, "MeetingFCPerson4"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFCPerson5", System.Data.SqlDbType.Int, 4, "MeetingFCPerson5"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson1", System.Data.SqlDbType.Int, 4, "MeetingFundPerson1"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson2", System.Data.SqlDbType.Int, 4, "MeetingFundPerson2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson3", System.Data.SqlDbType.Int, 4, "MeetingFundPerson3"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson4", System.Data.SqlDbType.Int, 4, "MeetingFundPerson4"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundPerson5", System.Data.SqlDbType.Int, 4, "MeetingFundPerson5"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingStatus", System.Data.SqlDbType.Int, 4, "MeetingStatus"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingFundType", System.Data.SqlDbType.Int, 4, "MeetingFundType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MeetingExistingFund", System.Data.SqlDbType.Bit, 1, "MeetingExistingFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblMeeting_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@MeetingID", System.Data.SqlDbType.Int, 4, "MeetingID"))


      Case "TBLMEETINGPEOPLE"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblMeetingPeople", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("PeopleID", "PeopleID"), _
        New System.Data.Common.DataColumnMapping("PeoplePerson", "PeoplePerson"), _
        New System.Data.Common.DataColumnMapping("PeopleDescription", "PeopleDescription"), _
        New System.Data.Common.DataColumnMapping("PeopleFirm", "PeopleFirm"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblMeetingPeople_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblMeetingPeople_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@PeopleID", System.Data.SqlDbType.Int, 4, "PeopleID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PeoplePerson", System.Data.SqlDbType.NVarChar, 50, "PeoplePerson"))
        InsertCommand.Parameters.Add(New SqlParameter("@PeopleDescription", System.Data.SqlDbType.Text, 2147483647, "PeopleDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@PeopleFirm", System.Data.SqlDbType.Int, 4, "PeopleFirm"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblMeetingPeople_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeopleID", System.Data.SqlDbType.Int, 4, "PeopleID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeoplePerson", System.Data.SqlDbType.NVarChar, 50, "PeoplePerson"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeopleDescription", System.Data.SqlDbType.Text, 2147483647, "PeopleDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeopleFirm", System.Data.SqlDbType.Int, 4, "PeopleFirm"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblMeetingPeople_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@PeopleID", System.Data.SqlDbType.Int, 4, "PeopleID"))


      Case "TBLMEETINGSTATUS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblMeetingStatus", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("StatusID", "StatusID"), _
        New System.Data.Common.DataColumnMapping("StatusDescription", "StatusDescription"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblMeetingStatus_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblMeetingStatus_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@StatusID", System.Data.SqlDbType.Int, 4, "StatusID"))
        InsertCommand.Parameters.Add(New SqlParameter("@StatusDescription", System.Data.SqlDbType.NVarChar, 50, "StatusDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblMeetingStatus_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@StatusID", System.Data.SqlDbType.Int, 4, "StatusID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@StatusDescription", System.Data.SqlDbType.NVarChar, 50, "StatusDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblMeetingStatus_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@StatusID", System.Data.SqlDbType.Int, 4, "StatusID"))

			Case "TBLTRADEFILEEMSXSTATUS"

				pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblTradeFileEmsxStatus", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
        New System.Data.Common.DataColumnMapping("SequenceNumber", "SequenceNumber"), _
        New System.Data.Common.DataColumnMapping("OrderStatus", "OrderStatus"), _
        New System.Data.Common.DataColumnMapping("CorrelationID", "CorrelationID"), _
        New System.Data.Common.DataColumnMapping("StatusString", "StatusString"), _
        New System.Data.Common.DataColumnMapping("BrokerID", "BrokerID"), _
        New System.Data.Common.DataColumnMapping("EMSX_BrokerID", "EMSX_BrokerID"), _
        New System.Data.Common.DataColumnMapping("VeniceAccountID", "VeniceAccountID"), _
        New System.Data.Common.DataColumnMapping("EMSX_Ticker", "EMSX_Ticker"), _
        New System.Data.Common.DataColumnMapping("RouteIDs", "RouteIDs"), _
        New System.Data.Common.DataColumnMapping("OrderAmount", "OrderAmount"), _
        New System.Data.Common.DataColumnMapping("FilledAmount", "FilledAmount"), _
        New System.Data.Common.DataColumnMapping("AveragePrice", "AveragePrice"), _
        New System.Data.Common.DataColumnMapping("Cancelled", "Cancelled"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})

				' Select Command
				SelectCommand.CommandText = "[adp_tblTradeFileEmsxStatus_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				' 
				InsertCommand.CommandText = "[adp_tblTradeFileEmsxStatus_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
				InsertCommand.Parameters.Add(New SqlParameter("@SequenceNumber", System.Data.SqlDbType.BigInt, 8, "SequenceNumber"))
				InsertCommand.Parameters.Add(New SqlParameter("@OrderStatus", System.Data.SqlDbType.Int, 4, "OrderStatus"))
				InsertCommand.Parameters.Add(New SqlParameter("@CorrelationID", System.Data.SqlDbType.Int, 4, "CorrelationID"))
				InsertCommand.Parameters.Add(New SqlParameter("@StatusString", System.Data.SqlDbType.NVarChar, 500, "StatusString"))
        InsertCommand.Parameters.Add(New SqlParameter("@BrokerID", System.Data.SqlDbType.Int, 4, "BrokerID"))
        InsertCommand.Parameters.Add(New SqlParameter("@EMSX_BrokerID", System.Data.SqlDbType.NVarChar, 50, "EMSX_BrokerID"))
        InsertCommand.Parameters.Add(New SqlParameter("@VeniceAccountID", System.Data.SqlDbType.Int, 4, "VeniceAccountID"))
        InsertCommand.Parameters.Add(New SqlParameter("@EMSX_Ticker", System.Data.SqlDbType.NVarChar, 50, "EMSX_Ticker"))
				InsertCommand.Parameters.Add(New SqlParameter("@RouteIDs", System.Data.SqlDbType.VarChar, 500, "RouteIDs"))
        InsertCommand.Parameters.Add(New SqlParameter("@OrderAmount", System.Data.SqlDbType.Float, 8, "OrderAmount"))
        InsertCommand.Parameters.Add(New SqlParameter("@FilledAmount", System.Data.SqlDbType.Float, 8, "FilledAmount"))
        InsertCommand.Parameters.Add(New SqlParameter("@AveragePrice", System.Data.SqlDbType.Float, 8, "AveragePrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@Cancelled", System.Data.SqlDbType.Bit, 1, "Cancelled"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand
				' 
				UpdateCommand.CommandText = "[adp_tblTradeFileEmsxStatus_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@SequenceNumber", System.Data.SqlDbType.BigInt, 8, "SequenceNumber"))
				UpdateCommand.Parameters.Add(New SqlParameter("@OrderStatus", System.Data.SqlDbType.Int, 4, "OrderStatus"))
				UpdateCommand.Parameters.Add(New SqlParameter("@CorrelationID", System.Data.SqlDbType.Int, 4, "CorrelationID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@StatusString", System.Data.SqlDbType.NVarChar, 500, "StatusString"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BrokerID", System.Data.SqlDbType.Int, 4, "BrokerID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EMSX_BrokerID", System.Data.SqlDbType.NVarChar, 50, "EMSX_BrokerID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@VeniceAccountID", System.Data.SqlDbType.Int, 4, "VeniceAccountID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EMSX_Ticker", System.Data.SqlDbType.NVarChar, 50, "EMSX_Ticker"))
				UpdateCommand.Parameters.Add(New SqlParameter("@RouteIDs", System.Data.SqlDbType.VarChar, 500, "RouteIDs"))
				UpdateCommand.Parameters.Add(New SqlParameter("@OrderAmount", System.Data.SqlDbType.Float, 8, "OrderAmount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FilledAmount", System.Data.SqlDbType.Float, 8, "FilledAmount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AveragePrice", System.Data.SqlDbType.Float, 8, "Averageprice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Cancelled", System.Data.SqlDbType.Bit, 1, "Cancelled"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand

				DeleteCommand.CommandText = "[adp_tblTradeFileEmsxStatus_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))


      Case "TBLTRADEFILEEXECUTIONREPORTING"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblTradeFileExecutionReporting", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("TransactionID", "TransactionID"), _
        New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
        New System.Data.Common.DataColumnMapping("AdministratorID", "AdministratorID"), _
        New System.Data.Common.DataColumnMapping("TradeReportString", "TradeReportString"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblTradeFileExecutionReporting_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        ' 
        InsertCommand.CommandText = "[adp_tblTradeFileExecutionReporting_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionID", System.Data.SqlDbType.Int, 4, "TransactionID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@AdministratorID", System.Data.SqlDbType.Int, 4, "AdministratorID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TradeReportString", System.Data.SqlDbType.NVarChar, 2000, "TradeReportString"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        ' 
        UpdateCommand.CommandText = "[adp_tblTradeFileExecutionReporting_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionID", System.Data.SqlDbType.Int, 4, "TransactionID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AdministratorID", System.Data.SqlDbType.Int, 4, "AdministratorID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TradeReportString", System.Data.SqlDbType.NVarChar, 2000, "TradeReportString"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand

        DeleteCommand.CommandText = "[adp_tblTradeFileExecutionReporting_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))


      Case "TBLTRADEFILEPENDING"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblTradeFilePending", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("BrokerID", "BrokerID"), _
        New System.Data.Common.DataColumnMapping("VeniceAccountID", "VeniceAccountID"), _
        New System.Data.Common.DataColumnMapping("BrokerSpecific", "BrokerSpecific"), _
        New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
        New System.Data.Common.DataColumnMapping("WorkflowID", "WorkflowID"), _
        New System.Data.Common.DataColumnMapping("TriggerDate", "TriggerDate"), _
        New System.Data.Common.DataColumnMapping("TradefileText", "TradefileText"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblTradeFilePending_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblTradeFilePending_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@BrokerID", System.Data.SqlDbType.Int, 4, "BrokerID"))
        InsertCommand.Parameters.Add(New SqlParameter("@VeniceAccountID", System.Data.SqlDbType.Int, 4, "VeniceAccountID"))
        InsertCommand.Parameters.Add(New SqlParameter("@BrokerSpecific", System.Data.SqlDbType.NVarChar, 50, "BrokerSpecific"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@WorkflowID", System.Data.SqlDbType.Int, 4, "WorkflowID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TriggerDate", System.Data.SqlDbType.DateTime, 8, "TriggerDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TradefileText", System.Data.SqlDbType.VarChar, 1000, "TradefileText"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        ' Can't Update this table

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblTradeFilePending_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))


			Case "TBLPENDINGTRANSACTIONS"	'
				' Note : This retrieves only the First Leg of the Transaction.
				' Second (Cash) legs are not included.

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblPendingTransactions", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("TransactionID", "TransactionID"), _
				New System.Data.Common.DataColumnMapping("TransactionTicket", "TransactionTicket"), _
				New System.Data.Common.DataColumnMapping("TransactionFund", "TransactionFund"), _
				New System.Data.Common.DataColumnMapping("TransactionSubFund", "TransactionSubFund"), _
				New System.Data.Common.DataColumnMapping("TransactionInstrument", "TransactionInstrument"), _
				New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
				New System.Data.Common.DataColumnMapping("TransactionValueorAmount", "TransactionValueorAmount"), _
				New System.Data.Common.DataColumnMapping("TransactionUnits", "TransactionUnits"), _
				New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
				New System.Data.Common.DataColumnMapping("TransactionCosts", "TransactionCosts"), _
				New System.Data.Common.DataColumnMapping("TransactionCostPercent", "TransactionCostPercent"), _
				New System.Data.Common.DataColumnMapping("TransactionFXRate", "TransactionFXRate"), _
				New System.Data.Common.DataColumnMapping("TransactionEffectivePrice", "TransactionEffectivePrice"), _
				New System.Data.Common.DataColumnMapping("TransactionEffectiveWatermark", "TransactionEffectiveWatermark"), _
				New System.Data.Common.DataColumnMapping("TransactionSpecificInitialEqualisationValue", "TransactionSpecificInitialEqualisationValue"), _
				New System.Data.Common.DataColumnMapping("TransactionCounterparty", "TransactionCounterparty"), _
				New System.Data.Common.DataColumnMapping("TransactionInvestment", "TransactionInvestment"), _
				New System.Data.Common.DataColumnMapping("TransactionExecution", "TransactionExecution"), _
				New System.Data.Common.DataColumnMapping("TransactionDataEntry", "TransactionDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionDecisionDate", "TransactionDecisionDate"), _
				New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
				New System.Data.Common.DataColumnMapping("TransactionFIFOValueDate", "TransactionFIFOValueDate"), _
				New System.Data.Common.DataColumnMapping("TransactionEffectiveValueDate", "TransactionEffectiveValueDate"), _
				New System.Data.Common.DataColumnMapping("TransactionSettlementDate", "TransactionSettlementDate"), _
				New System.Data.Common.DataColumnMapping("TransactionConfirmationDate", "TransactionConfirmationDate"), _
				New System.Data.Common.DataColumnMapping("TransactionCostIsPercent", "TransactionCostIsPercent"), _
				New System.Data.Common.DataColumnMapping("TransactionExemptFromUpdate", "TransactionExemptFromUpdate"), _
				New System.Data.Common.DataColumnMapping("TransactionIsTransfer", "TransactionIsTransfer"), _
				New System.Data.Common.DataColumnMapping("TransactionSpecificInitialEqualisationFlag", "TransactionSpecificInitialEqualisationFlag"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalAmount", "TransactionFinalAmount"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalPrice", "TransactionFinalPrice"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalCosts", "TransactionFinalCosts"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalSettlement", "TransactionFinalSettlement"), _
				New System.Data.Common.DataColumnMapping("TransactionInstructionFlag", "TransactionInstructionFlag"), _
				New System.Data.Common.DataColumnMapping("TransactionBankConfirmation", "TransactionBankConfirmation"), _
				New System.Data.Common.DataColumnMapping("TransactionInitialDataEntry", "TransactionInitialDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionAmountConfirmed", "TransactionAmountConfirmed"), _
				New System.Data.Common.DataColumnMapping("TransactionSettlementConfirmed", "TransactionSettlementConfirmed"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalDataEntry", "TransactionFinalDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionTradeStatusID", "TransactionTradeStatusID"), _
				New System.Data.Common.DataColumnMapping("TransactionComment", "TransactionComment"), _
				New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
				New System.Data.Common.DataColumnMapping("TransactionUser", "TransactionUser"), _
				New System.Data.Common.DataColumnMapping("TransactionGroup", "TransactionGroup"), _
				New System.Data.Common.DataColumnMapping("TransactionAdminStatus", "TransactionAdminStatus"), _
				New System.Data.Common.DataColumnMapping("TransactionCTFLAGS", "TransactionCTFLAGS"), _
				New System.Data.Common.DataColumnMapping("TransactionCompoundRN", "TransactionCompoundRN") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblPendingTransactions_FirstLeg_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblPendingTransactions_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionTicket", System.Data.SqlDbType.NVarChar, 50, "TransactionTicket"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionFund", System.Data.SqlDbType.Int, 4, "TransactionFund"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionSubFund", System.Data.SqlDbType.Int, 4, "TransactionSubFund"))
				'InsertCommand.Parameters.Add(New SqlParameter("@TransactionSubFundValueFlag", System.Data.SqlDbType.SmallInt, 2, "TransactionSubFundValueFlag"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionInstrument", System.Data.SqlDbType.Int, 4, "TransactionInstrument"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionValueorAmount", System.Data.SqlDbType.NVarChar, 10, "TransactionValueorAmount"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionUnits", System.Data.SqlDbType.Float, 8, "TransactionUnits"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionCosts", System.Data.SqlDbType.Float, 8, "TransactionCosts"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionCostPercent", System.Data.SqlDbType.Float, 8, "TransactionCostPercent"))
				'InsertCommand.Parameters.Add(New SqlParameter("@TransactionMainFundPrice", System.Data.SqlDbType.Float, 8, "TransactionMainFundPrice"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionFXRate", System.Data.SqlDbType.Float, 8, "TransactionFXRate"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectivePrice", System.Data.SqlDbType.Float, 8, "TransactionEffectivePrice"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveWatermark", System.Data.SqlDbType.Float, 8, "TransactionEffectiveWatermark"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationValue", System.Data.SqlDbType.Float, 8, "TransactionSpecificInitialEqualisationValue"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionCounterparty", System.Data.SqlDbType.Int, 4, "TransactionCounterparty"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionInvestment", System.Data.SqlDbType.NVarChar, 50, "TransactionInvestment"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionExecution", System.Data.SqlDbType.NVarChar, 50, "TransactionExecution"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionDataEntry", System.Data.SqlDbType.NVarChar, 50, "TransactionDataEntry"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionDecisionDate", System.Data.SqlDbType.DateTime, 8, "TransactionDecisionDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionFIFOValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionFIFOValueDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionEffectiveValueDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionSettlementDate", System.Data.SqlDbType.DateTime, 8, "TransactionSettlementDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionConfirmationDate", System.Data.SqlDbType.DateTime, 8, "TransactionConfirmationDate"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionExemptFromUpdate", System.Data.SqlDbType.Bit, 1, "TransactionExemptFromUpdate"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionIsTransfer", System.Data.SqlDbType.Bit, 1, "TransactionIsTransfer"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationFlag", System.Data.SqlDbType.Bit, 1, "TransactionSpecificInitialEqualisationFlag"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalAmount", System.Data.SqlDbType.Int, 4, "TransactionFinalAmount"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalPrice", System.Data.SqlDbType.Int, 4, "TransactionFinalPrice"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalCosts", System.Data.SqlDbType.Int, 4, "TransactionFinalCosts"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalSettlement", System.Data.SqlDbType.Int, 4, "TransactionFinalSettlement"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionInstructionFlag", System.Data.SqlDbType.Int, 4, "TransactionInstructionFlag"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionBankConfirmation", System.Data.SqlDbType.Int, 4, "TransactionBankConfirmation"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionInitialDataEntry", System.Data.SqlDbType.Int, 4, "TransactionInitialDataEntry"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionAmountConfirmed", System.Data.SqlDbType.Int, 4, "TransactionAmountConfirmed"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionSettlementConfirmed", System.Data.SqlDbType.Int, 4, "TransactionSettlementConfirmed"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalDataEntry", System.Data.SqlDbType.Int, 4, "TransactionFinalDataEntry"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionTradeStatusID", System.Data.SqlDbType.Int, 4, "TransactionTradeStatusID"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionComment", System.Data.SqlDbType.NVarChar, 500, "TransactionComment"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionGroup", System.Data.SqlDbType.NVarChar, 50, "TransactionGroup"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionAdminStatus", System.Data.SqlDbType.Int, 4, "TransactionAdminStatus"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionCTFLAGS", System.Data.SqlDbType.Int, 4, "TransactionCTFLAGS"))
				InsertCommand.Parameters.Add(New SqlParameter("@TransactionCompoundRN", System.Data.SqlDbType.Int, 4, "TransactionCompoundRN"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = ""
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))


				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblPendingTransactions_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@TransactionID", System.Data.SqlDbType.Int, 4, "TransactionID"))




      Case "TBLPERSON"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPerson", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("PersonID", "PersonID"), _
        New System.Data.Common.DataColumnMapping("Person", "Person"), _
        New System.Data.Common.DataColumnMapping("PersonUserName", "PersonUserName"), _
        New System.Data.Common.DataColumnMapping("PersonEmail", "PersonEmail"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPerson_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPerson_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@PersonID", System.Data.SqlDbType.Int, 4, "PersonID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Person", System.Data.SqlDbType.NVarChar, 100, "Person"))
        InsertCommand.Parameters.Add(New SqlParameter("@PersonUserName", System.Data.SqlDbType.NVarChar, 50, "PersonUserName"))
        InsertCommand.Parameters.Add(New SqlParameter("@PersonEmail", System.Data.SqlDbType.NVarChar, 100, "PersonEmail"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPerson_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@PersonID", System.Data.SqlDbType.Int, 4, "PersonID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Person", System.Data.SqlDbType.NVarChar, 100, "Person"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PersonUserName", System.Data.SqlDbType.NVarChar, 50, "PersonUserName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PersonEmail", System.Data.SqlDbType.NVarChar, 100, "PersonEmail"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPerson_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@PersonID", System.Data.SqlDbType.Int, 4, "PersonID"))


      Case "TBLPERIOD"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPeriod", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("PeriodID", "PeriodID"), _
        New System.Data.Common.DataColumnMapping("PeriodTitle", "PeriodTitle"), _
        New System.Data.Common.DataColumnMapping("PeriodDescription", "PeriodDescription"), _
        New System.Data.Common.DataColumnMapping("PeriodDateFrom", "PeriodDateFrom"), _
        New System.Data.Common.DataColumnMapping("PeriodDateTo", "PeriodDateTo"), _
        New System.Data.Common.DataColumnMapping("AppName", "AppName"), _
        New System.Data.Common.DataColumnMapping("IsGlobal", "IsGlobal"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPeriod_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPeriod_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@PeriodID", System.Data.SqlDbType.Int, 4, "PeriodID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PeriodTitle", System.Data.SqlDbType.NVarChar, 50, "PeriodTitle"))
        InsertCommand.Parameters.Add(New SqlParameter("@PeriodDescription", System.Data.SqlDbType.NVarChar, 50, "PeriodDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@PeriodDateFrom", System.Data.SqlDbType.DateTime, 8, "PeriodDateFrom"))
        InsertCommand.Parameters.Add(New SqlParameter("@PeriodDateTo", System.Data.SqlDbType.DateTime, 8, "PeriodDateTo"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsGlobal", System.Data.SqlDbType.Bit, 1, "IsGlobal"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPeriod_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeriodID", System.Data.SqlDbType.Int, 4, "PeriodID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeriodTitle", System.Data.SqlDbType.NVarChar, 50, "PeriodTitle"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeriodDescription", System.Data.SqlDbType.NVarChar, 50, "PeriodDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeriodDateFrom", System.Data.SqlDbType.DateTime, 8, "PeriodDateFrom"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PeriodDateTo", System.Data.SqlDbType.DateTime, 8, "PeriodDateTo"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsGlobal", System.Data.SqlDbType.Bit, 1, "IsGlobal"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPeriod_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@PeriodID", System.Data.SqlDbType.Int, 4, "PeriodID"))


      Case "TBLPERTRACCUSTOMFIELDS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPertracCustomFields", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FieldID", "FieldID"), _
        New System.Data.Common.DataColumnMapping("FieldName", "FieldName"), _
        New System.Data.Common.DataColumnMapping("CustomFieldType", "CustomFieldType"), _
        New System.Data.Common.DataColumnMapping("StartDate", "StartDate"), _
        New System.Data.Common.DataColumnMapping("EndDate", "EndDate"), _
        New System.Data.Common.DataColumnMapping("IsMaxValue", "IsMaxValue"), _
        New System.Data.Common.DataColumnMapping("IsMinValue", "IsMinValue"), _
        New System.Data.Common.DataColumnMapping("IsFirstValue", "IsFirstValue"), _
        New System.Data.Common.DataColumnMapping("IsLastValue", "IsLastValue"), _
        New System.Data.Common.DataColumnMapping("IsAverageValue", "IsAverageValue"), _
        New System.Data.Common.DataColumnMapping("IsIRR", "IsIRR"), _
        New System.Data.Common.DataColumnMapping("FieldPeriodCount", "FieldPeriodCount"), _
        New System.Data.Common.DataColumnMapping("FieldIsVolatile", "FieldIsVolatile"), _
        New System.Data.Common.DataColumnMapping("FieldIsCalculated", "FieldIsCalculated"), _
        New System.Data.Common.DataColumnMapping("FieldIsSearchable", "FieldIsSearchable"), _
        New System.Data.Common.DataColumnMapping("FieldIsOptimisable", "FieldIsOptimisable"), _
        New System.Data.Common.DataColumnMapping("FieldDataType", "FieldDataType"), _
        New System.Data.Common.DataColumnMapping("FieldDetails", "FieldDetails"), _
        New System.Data.Common.DataColumnMapping("DefaultValue", "DefaultValue"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPertracCustomFields_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPertracCustomFields_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldID", System.Data.SqlDbType.Int, 4, "FieldID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldName", System.Data.SqlDbType.NVarChar, 100, "FieldName"))
        InsertCommand.Parameters.Add(New SqlParameter("@CustomFieldType", System.Data.SqlDbType.Int, 4, "CustomFieldType"))
        InsertCommand.Parameters.Add(New SqlParameter("@StartDate", System.Data.SqlDbType.DateTime, 8, "StartDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@EndDate", System.Data.SqlDbType.DateTime, 8, "EndDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsMaxValue", System.Data.SqlDbType.Bit, 1, "IsMaxValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsMinValue", System.Data.SqlDbType.Bit, 1, "IsMinValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsFirstValue", System.Data.SqlDbType.Bit, 1, "IsFirstValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsLastValue", System.Data.SqlDbType.Bit, 1, "IsLastValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsAverageValue", System.Data.SqlDbType.Bit, 1, "IsAverageValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsIRR", System.Data.SqlDbType.Bit, 1, "IsIRR"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldPeriodCount", System.Data.SqlDbType.Int, 4, "FieldPeriodCount"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldIsVolatile", System.Data.SqlDbType.Bit, 1, "FieldIsVolatile"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldIsCalculated", System.Data.SqlDbType.Bit, 1, "FieldIsCalculated"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldIsSearchable", System.Data.SqlDbType.Bit, 1, "FieldIsSearchable"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldIsOptimisable", System.Data.SqlDbType.Bit, 1, "FieldIsOptimisable"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldDataType", System.Data.SqlDbType.Int, 4, "FieldDataType"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldDetails", System.Data.SqlDbType.NVarChar, 500, "FieldDetails"))
        InsertCommand.Parameters.Add(New SqlParameter("@DefaultValue", System.Data.SqlDbType.NVarChar, 100, "DefaultValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPertracCustomFields_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldID", System.Data.SqlDbType.Int, 4, "FieldID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldName", System.Data.SqlDbType.NVarChar, 100, "FieldName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CustomFieldType", System.Data.SqlDbType.Int, 4, "CustomFieldType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@StartDate", System.Data.SqlDbType.DateTime, 8, "StartDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@EndDate", System.Data.SqlDbType.DateTime, 8, "EndDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsMaxValue", System.Data.SqlDbType.Bit, 1, "IsMaxValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsMinValue", System.Data.SqlDbType.Bit, 1, "IsMinValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsFirstValue", System.Data.SqlDbType.Bit, 1, "IsFirstValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsLastValue", System.Data.SqlDbType.Bit, 1, "IsLastValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsAverageValue", System.Data.SqlDbType.Bit, 1, "IsAverageValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsIRR", System.Data.SqlDbType.Bit, 1, "IsIRR"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldPeriodCount", System.Data.SqlDbType.Int, 4, "FieldPeriodCount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldIsVolatile", System.Data.SqlDbType.Bit, 1, "FieldIsVolatile"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldIsCalculated", System.Data.SqlDbType.Bit, 1, "FieldIsCalculated"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldIsSearchable", System.Data.SqlDbType.Bit, 1, "FieldIsSearchable"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldIsOptimisable", System.Data.SqlDbType.Bit, 1, "FieldIsOptimisable"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldDataType", System.Data.SqlDbType.Int, 4, "FieldDataType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldDetails", System.Data.SqlDbType.NVarChar, 500, "FieldDetails"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DefaultValue", System.Data.SqlDbType.NVarChar, 100, "DefaultValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPertracCustomFields_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FieldID", System.Data.SqlDbType.Int, 4, "FieldID"))


      Case "TBLPERTRACCUSTOMFIELDDATA"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPertracCustomFieldData", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FieldDataID", "FieldDataID"), _
        New System.Data.Common.DataColumnMapping("CustomFieldID", "CustomFieldID"), _
        New System.Data.Common.DataColumnMapping("PertracID", "PertracID"), _
        New System.Data.Common.DataColumnMapping("GroupID", "GroupID"), _
        New System.Data.Common.DataColumnMapping("FieldNumericData", "FieldNumericData"), _
        New System.Data.Common.DataColumnMapping("FieldTextData", "FieldTextData"), _
        New System.Data.Common.DataColumnMapping("FieldDateData", "FieldDateData"), _
        New System.Data.Common.DataColumnMapping("FieldBooleanData", "FieldBooleanData"), _
        New System.Data.Common.DataColumnMapping("UpdateRequired", "UpdateRequired"), _
        New System.Data.Common.DataColumnMapping("LatestReturnDate", "LatestReturnDate"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})


        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPertracCustomFieldData_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPertracCustomFieldData_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldDataID", System.Data.SqlDbType.Int, 4, "FieldDataID"))
        InsertCommand.Parameters.Add(New SqlParameter("@CustomFieldID", System.Data.SqlDbType.Int, 4, "CustomFieldID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PertracID", System.Data.SqlDbType.Int, 4, "PertracID"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldNumericData", System.Data.SqlDbType.Float, 8, "FieldNumericData"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldTextData", System.Data.SqlDbType.NVarChar, 50, "FieldTextData"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldDateData", System.Data.SqlDbType.DateTime, 8, "FieldDateData"))
        InsertCommand.Parameters.Add(New SqlParameter("@FieldBooleanData", System.Data.SqlDbType.Bit, 1, "FieldBooleanData"))
        InsertCommand.Parameters.Add(New SqlParameter("@UpdateRequired", System.Data.SqlDbType.Bit, 1, "UpdateRequired"))
        InsertCommand.Parameters.Add(New SqlParameter("@LatestReturnDate", System.Data.SqlDbType.DateTime, 8, "LatestReturnDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPertracCustomFieldData_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldDataID", System.Data.SqlDbType.Int, 4, "FieldDataID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CustomFieldID", System.Data.SqlDbType.Int, 4, "CustomFieldID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PertracID", System.Data.SqlDbType.Int, 4, "PertracID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldNumericData", System.Data.SqlDbType.Float, 8, "FieldNumericData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldTextData", System.Data.SqlDbType.NVarChar, 50, "FieldTextData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldDateData", System.Data.SqlDbType.DateTime, 8, "FieldDateData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FieldBooleanData", System.Data.SqlDbType.Bit, 1, "FieldBooleanData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@UpdateRequired", System.Data.SqlDbType.Bit, 1, "UpdateRequired"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LatestReturnDate", System.Data.SqlDbType.DateTime, 8, "LatestReturnDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPertracCustomFieldData_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"))
        DeleteCommand.Parameters.Add(New SqlParameter("@CustomFieldID", System.Data.SqlDbType.Int, 4, "CustomFieldID"))
        DeleteCommand.Parameters.Add(New SqlParameter("@PertracID", System.Data.SqlDbType.Int, 4, "PertracID"))


      Case "TBLPERTRACFIELDMAPPING"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPertracFieldMapping", _
        New System.Data.Common.DataColumnMapping() { _
        New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("DataProvider", "DataProvider"), _
        New System.Data.Common.DataColumnMapping("FCP_FieldName", "FCP_FieldName"), _
        New System.Data.Common.DataColumnMapping("PertracField", "PertracField"), _
        New System.Data.Common.DataColumnMapping("ProviderFieldName", "ProviderFieldName"), _
        New System.Data.Common.DataColumnMapping("FieldIsNumeric", "FieldIsNumeric"), _
        New System.Data.Common.DataColumnMapping("Multiplier", "Multiplier") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPertracFieldMapping_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


      Case "TBLPFPCPORTFOLIORECONCILLIATION"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPFPCPortfolioReconcilliation", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("ValuationDate", "ValuationDate"), _
        New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
        New System.Data.Common.DataColumnMapping("FCPInstrumentID", "FCPInstrumentID"), _
        New System.Data.Common.DataColumnMapping("PFPCInstrumentID", "PFPCInstrumentID"), _
        New System.Data.Common.DataColumnMapping("PFPCInstrumentDescription", "PFPCInstrumentDescription"), _
        New System.Data.Common.DataColumnMapping("FCP_Holding", "FCP_Holding"), _
        New System.Data.Common.DataColumnMapping("FCP_HoldingCompleteness", "FCP_HoldingCompleteness"), _
        New System.Data.Common.DataColumnMapping("PFPC_Holding", "PFPC_Holding"), _
        New System.Data.Common.DataColumnMapping("Holding_Difference", "Holding_Difference"), _
        New System.Data.Common.DataColumnMapping("FCP_Price", "FCP_Price"), _
        New System.Data.Common.DataColumnMapping("FCP_PriceFinal", "FCP_PriceFinal"), _
        New System.Data.Common.DataColumnMapping("FCP_PriceAdministrator", "FCP_PriceAdministrator"), _
        New System.Data.Common.DataColumnMapping("FCP_PriceDate", "FCP_PriceDate"), _
        New System.Data.Common.DataColumnMapping("PFPC_Price", "PFPC_Price"), _
        New System.Data.Common.DataColumnMapping("Price_Difference", "Price_Difference"), _
        New System.Data.Common.DataColumnMapping("FCP_FXRate", "FCP_FXRate"), _
        New System.Data.Common.DataColumnMapping("PFPC_FXRate", "PFPC_FXRate"), _
        New System.Data.Common.DataColumnMapping("FXRate_Difference", "FXRate_Difference"), _
        New System.Data.Common.DataColumnMapping("FCP_LocalCcyValue", "FCP_LocalCcyValue"), _
        New System.Data.Common.DataColumnMapping("PFPC_LocalCcyValue", "PFPC_LocalCcyValue"), _
        New System.Data.Common.DataColumnMapping("LocalCcyValue_Difference", "LocalCcyValue_Difference"), _
        New System.Data.Common.DataColumnMapping("FCP_BookCcyValue", "FCP_BookCcyValue"), _
        New System.Data.Common.DataColumnMapping("PFPC_BookCcyValue", "PFPC_BookCcyValue"), _
        New System.Data.Common.DataColumnMapping("BookCcyValue_Difference", "BookCcyValue_Difference"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPfpcPortfolioReconcilliation_Selectcommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPfpcPortfolioReconcilliation_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ValuationDate", System.Data.SqlDbType.DateTime, 8, "ValuationDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCPInstrumentID", System.Data.SqlDbType.Int, 4, "FCPInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PFPCInstrumentID", System.Data.SqlDbType.NVarChar, 50, "PFPCInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PFPCInstrumentDescription", System.Data.SqlDbType.NVarChar, 50, "PFPCInstrumentDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_Holding", System.Data.SqlDbType.Float, 8, "FCP_Holding"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_HoldingCompleteness", System.Data.SqlDbType.Float, 8, "FCP_HoldingCompleteness"))
        InsertCommand.Parameters.Add(New SqlParameter("@PFPC_Holding", System.Data.SqlDbType.Float, 8, "PFPC_Holding"))
        InsertCommand.Parameters.Add(New SqlParameter("@Holding_Difference", System.Data.SqlDbType.Float, 8, "Holding_Difference"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_Price", System.Data.SqlDbType.Float, 8, "FCP_Price"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_PriceFinal", System.Data.SqlDbType.Int, 4, "FCP_PriceFinal"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_PriceAdministrator", System.Data.SqlDbType.Int, 4, "FCP_PriceAdministrator"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_PriceDate", System.Data.SqlDbType.DateTime, 8, "FCP_PriceDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PFPC_Price", System.Data.SqlDbType.Float, 8, "PFPC_Price"))
        InsertCommand.Parameters.Add(New SqlParameter("@Price_Difference", System.Data.SqlDbType.Float, 8, "Price_Difference"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_FXRate", System.Data.SqlDbType.Float, 8, "FCP_FXRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PFPC_FXRate", System.Data.SqlDbType.Float, 8, "PFPC_FXRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXRate_Difference", System.Data.SqlDbType.Float, 8, "FXRate_Difference"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_LocalCcyValue", System.Data.SqlDbType.Float, 8, "FCP_LocalCcyValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@PFPC_LocalCcyValue", System.Data.SqlDbType.Float, 8, "PFPC_LocalCcyValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@LocalCcyValue_Difference", System.Data.SqlDbType.Float, 8, "LocalCcyValue_Difference"))
        InsertCommand.Parameters.Add(New SqlParameter("@FCP_BookCcyValue", System.Data.SqlDbType.Float, 8, "FCP_BookCcyValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@PFPC_BookCcyValue", System.Data.SqlDbType.Float, 8, "PFPC_BookCcyValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@BookCcyValue_Difference", System.Data.SqlDbType.Float, 8, "BookCcyValue_Difference"))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPfpcPortfolioReconcilliation_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ValuationDate", System.Data.SqlDbType.DateTime, 8, "ValuationDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCPInstrumentID", System.Data.SqlDbType.Int, 4, "FCPInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PFPCInstrumentID", System.Data.SqlDbType.NVarChar, 50, "PFPCInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PFPCInstrumentDescription", System.Data.SqlDbType.NVarChar, 50, "PFPCInstrumentDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_Holding", System.Data.SqlDbType.Float, 8, "FCP_Holding"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_HoldingCompleteness", System.Data.SqlDbType.Float, 8, "FCP_HoldingCompleteness"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PFPC_Holding", System.Data.SqlDbType.Float, 8, "PFPC_Holding"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Holding_Difference", System.Data.SqlDbType.Float, 8, "Holding_Difference"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_Price", System.Data.SqlDbType.Float, 8, "FCP_Price"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_PriceFinal", System.Data.SqlDbType.Int, 4, "FCP_PriceFinal"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_PriceAdministrator", System.Data.SqlDbType.Int, 4, "FCP_PriceAdministrator"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_PriceDate", System.Data.SqlDbType.DateTime, 8, "FCP_PriceDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PFPC_Price", System.Data.SqlDbType.Float, 8, "PFPC_Price"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Price_Difference", System.Data.SqlDbType.Float, 8, "Price_Difference"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_FXRate", System.Data.SqlDbType.Float, 8, "FCP_FXRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PFPC_FXRate", System.Data.SqlDbType.Float, 8, "PFPC_FXRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXRate_Difference", System.Data.SqlDbType.Float, 8, "FXRate_Difference"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_LocalCcyValue", System.Data.SqlDbType.Float, 8, "FCP_LocalCcyValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PFPC_LocalCcyValue", System.Data.SqlDbType.Float, 8, "PFPC_LocalCcyValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LocalCcyValue_Difference", System.Data.SqlDbType.Float, 8, "LocalCcyValue_Difference"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FCP_BookCcyValue", System.Data.SqlDbType.Float, 8, "FCP_BookCcyValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PFPC_BookCcyValue", System.Data.SqlDbType.Float, 8, "PFPC_BookCcyValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BookCcyValue_Difference", System.Data.SqlDbType.Float, 8, "BookCcyValue_Difference"))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPfpcPortfolioReconcilliation_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))


      Case "TBLPFPCPORTFOLIOVALUATION"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblpfpcPortfolioValuation", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("ValuationDate", "ValuationDate"), _
        New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
        New System.Data.Common.DataColumnMapping("FundName", "FundName"), _
        New System.Data.Common.DataColumnMapping("FundCurrency", "FundCurrency"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("InstrumentDescription", "InstrumentDescription"), _
        New System.Data.Common.DataColumnMapping("InstrumentCurrency", "InstrumentCurrency"), _
        New System.Data.Common.DataColumnMapping("Amount", "Amount"), _
        New System.Data.Common.DataColumnMapping("LocalCcyPrice", "LocalCcyPrice"), _
        New System.Data.Common.DataColumnMapping("FXRate", "FXRate"), _
        New System.Data.Common.DataColumnMapping("LocalCcyValue", "LocalCcyValue"), _
        New System.Data.Common.DataColumnMapping("BookCcyValue", "BookCcyValue"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblpfpcPortfolioValuation_selectcommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblpfpcPortfolioValuation_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ValuationDate", System.Data.SqlDbType.DateTime, 8, "ValuationDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundName", System.Data.SqlDbType.NVarChar, 200, "FundName"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundCurrency", System.Data.SqlDbType.NVarChar, 10, "FundCurrency"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.NVarChar, 50, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentDescription", System.Data.SqlDbType.NVarChar, 100, "InstrumentDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentCurrency", System.Data.SqlDbType.NVarChar, 10, "InstrumentCurrency"))
        InsertCommand.Parameters.Add(New SqlParameter("@Amount", System.Data.SqlDbType.Float, 8, "Amount"))
        InsertCommand.Parameters.Add(New SqlParameter("@LocalCcyPrice", System.Data.SqlDbType.Float, 8, "LocalCcyPrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXRate", System.Data.SqlDbType.Float, 8, "FXRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@LocalCcyValue", System.Data.SqlDbType.Float, 8, "LocalCcyValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@BookCcyValue", System.Data.SqlDbType.Float, 8, "BookCcyValue"))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblpfpcPortfolioValuation_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ValuationDate", System.Data.SqlDbType.DateTime, 8, "ValuationDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundName", System.Data.SqlDbType.NVarChar, 200, "FundName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundCurrency", System.Data.SqlDbType.NVarChar, 10, "FundCurrency"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.NVarChar, 50, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentDescription", System.Data.SqlDbType.NVarChar, 100, "InstrumentDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentCurrency", System.Data.SqlDbType.NVarChar, 10, "InstrumentCurrency"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Amount", System.Data.SqlDbType.Float, 8, "Amount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LocalCcyPrice", System.Data.SqlDbType.Float, 8, "LocalCcyPrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXRate", System.Data.SqlDbType.Float, 8, "FXRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@LocalCcyValue", System.Data.SqlDbType.Float, 8, "LocalCcyValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@BookCcyValue", System.Data.SqlDbType.Float, 8, "BookCcyValue"))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblpfpcPortfolioValuation_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))


      Case "TBLPFPCSHAREALLOCATION"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPFPCShareAllocation", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
        New System.Data.Common.DataColumnMapping("ValuationDate", "ValuationDate"), _
        New System.Data.Common.DataColumnMapping("TradeDate", "TradeDate"), _
        New System.Data.Common.DataColumnMapping("AccountID", "AccountID"), _
        New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
        New System.Data.Common.DataColumnMapping("IssuedShares", "IssuedShares"), _
        New System.Data.Common.DataColumnMapping("CashInvested", "CashInvested") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPfpcShareAllocation_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPfpcShareAllocation_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ValuationDate", System.Data.SqlDbType.DateTime, 8, "ValuationDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TradeDate", System.Data.SqlDbType.DateTime, 8, "TradeDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@AccountID", System.Data.SqlDbType.NVarChar, 50, "AccountID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.NVarChar, 50, "TransactionType"))
        InsertCommand.Parameters.Add(New SqlParameter("@IssuedShares", System.Data.SqlDbType.Float, 8, "IssuedShares"))
        InsertCommand.Parameters.Add(New SqlParameter("@CashInvested", System.Data.SqlDbType.Float, 8, "CashInvested"))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPfpcShareAllocation_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ValuationDate", System.Data.SqlDbType.DateTime, 8, "ValuationDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TradeDate", System.Data.SqlDbType.DateTime, 8, "TradeDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@AccountID", System.Data.SqlDbType.NVarChar, 50, "AccountID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.NVarChar, 50, "TransactionType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IssuedShares", System.Data.SqlDbType.Float, 8, "IssuedShares"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CashInvested", System.Data.SqlDbType.Float, 8, "CashInvested"))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPfpcShareAllocation_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))


      Case "TBLPORTFOLIODATA"
        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPortfolioData", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("PortfolioDataID", "PortfolioDataID"), _
        New System.Data.Common.DataColumnMapping("PortfolioItemType", "PortfolioItemType"), _
        New System.Data.Common.DataColumnMapping("Ticker", "Ticker"), _
        New System.Data.Common.DataColumnMapping("FilingDate", "FilingDate"), _
        New System.Data.Common.DataColumnMapping("MarketInstrumentID", "MarketInstrumentID"), _
        New System.Data.Common.DataColumnMapping("Holding", "Holding"), _
        New System.Data.Common.DataColumnMapping("Weight", "Weight"), _
        New System.Data.Common.DataColumnMapping("MonitorItem", "MonitorItem"), _
        New System.Data.Common.DataColumnMapping("XMLData", "XMLData"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPortfolioData_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        ' Note this command is used directly in Naples:DataBaseClient:ProcessPortfolioUpdate()
        ' Additional parameters may need to be added to this code also.
        '
        InsertCommand.CommandText = "[adp_tblPortfolioData_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioDataID", System.Data.SqlDbType.Int, 4, "PortfolioDataID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioItemID", System.Data.SqlDbType.Int, 4, "PortfolioItemID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioItemType", System.Data.SqlDbType.Int, 4, "PortfolioItemType"))
        InsertCommand.Parameters.Add(New SqlParameter("@Ticker", System.Data.SqlDbType.NVarChar, 35, "Ticker"))
        InsertCommand.Parameters.Add(New SqlParameter("@FilingDate", System.Data.SqlDbType.DateTime, 8, "FilingDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@MarketInstrumentID", System.Data.SqlDbType.Int, 4, "MarketInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Holding", System.Data.SqlDbType.Float, 8, "Holding"))
        InsertCommand.Parameters.Add(New SqlParameter("@Weight", System.Data.SqlDbType.Float, 8, "Weight"))
        InsertCommand.Parameters.Add(New SqlParameter("@MonitorItem", System.Data.SqlDbType.Bit)).SourceColumn = "MonitorItem"
        InsertCommand.Parameters.Add(New SqlParameter("@XMLData", System.Data.SqlDbType.Text, Int32.MaxValue, "XMLData"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPortfolioData_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@RN", System.Data.SqlDbType.Int, 4, "RN"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioDataID", System.Data.SqlDbType.Int, 4, "PortfolioDataID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioItemID", System.Data.SqlDbType.Int, 4, "PortfolioItemID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioItemType", System.Data.SqlDbType.Int, 4, "PortfolioItemType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Ticker", System.Data.SqlDbType.NVarChar, 35, "Ticker"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FilingDate", System.Data.SqlDbType.DateTime, 8, "FilingDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MarketInstrumentID", System.Data.SqlDbType.Int, 4, "MarketInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Holding", System.Data.SqlDbType.Float, 8, "Holding"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Weight", System.Data.SqlDbType.Float, 8, "Weight"))
        UpdateCommand.Parameters.Add(New SqlParameter("@MonitorItem", System.Data.SqlDbType.Bit)).SourceColumn = "MonitorItem"
        UpdateCommand.Parameters.Add(New SqlParameter("@XMLData", System.Data.SqlDbType.Text, Int32.MaxValue, "XMLData"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPortfolioData_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@PortfolioItemID", System.Data.SqlDbType.Int, 4, "PortfolioItemID"))


      Case "TBLPORTFOLIOINDEX"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPortfolioIndex", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("PortfolioID", "PortfolioID"), _
        New System.Data.Common.DataColumnMapping("PortfolioTicker", "PortfolioTicker"), _
        New System.Data.Common.DataColumnMapping("PortfolioDescription", "PortfolioDescription"), _
        New System.Data.Common.DataColumnMapping("PortfolioType", "PortfolioType"), _
        New System.Data.Common.DataColumnMapping("PortfolioFilingDate", "PortfolioFilingDate"), _
        New System.Data.Common.DataColumnMapping("PortfolioDataID", "PortfolioDataID"), _
        New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPortfolioIndex_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPortfolioIndex_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioID", System.Data.SqlDbType.Int, 4, "PortfolioID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioTicker", System.Data.SqlDbType.NVarChar, 35, "PortfolioTicker"))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioDescription", System.Data.SqlDbType.NVarChar, 200, "PortfolioDescription"))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioType", System.Data.SqlDbType.Int, 4, "PortfolioType"))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioFilingDate", System.Data.SqlDbType.DateTime, 8, "PortfolioFilingDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PortfolioDataID", System.Data.SqlDbType.Int, 4, "PortfolioDataID"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPortfolioIndex_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioID", System.Data.SqlDbType.Int, 4, "PortfolioID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioTicker", System.Data.SqlDbType.NVarChar, 35, "PortfolioTicker"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioDescription", System.Data.SqlDbType.NVarChar, 200, "PortfolioDescription"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioType", System.Data.SqlDbType.Int, 4, "PortfolioType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioFilingDate", System.Data.SqlDbType.DateTime, 8, "PortfolioFilingDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PortfolioDataID", System.Data.SqlDbType.Int, 4, "PortfolioDataID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPortfolioIndex_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@PortfolioID", System.Data.SqlDbType.Int, 4, "PortfolioID"))


      Case "TBLPORTFOLIOITEMTYPES"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPortfolioItemTypes", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("PortfolioItemType", "PortfolioItemType"), _
        New System.Data.Common.DataColumnMapping("PortfolioItemTypeDescription", "PortfolioItemTypeDescription") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPortfolioItemTypes_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


      Case "TBLPRICE", "TBLPRICEBYINSTRUMENT"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblPrice", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("PriceID", "PriceID"), _
        New System.Data.Common.DataColumnMapping("PriceInstrument", "PriceInstrument"), _
        New System.Data.Common.DataColumnMapping("PriceDate", "PriceDate"), _
        New System.Data.Common.DataColumnMapping("PricePercent", "PricePercent"), _
        New System.Data.Common.DataColumnMapping("PriceMultiplier", "PriceMultiplier"), _
        New System.Data.Common.DataColumnMapping("PriceLevel", "PriceLevel"), _
        New System.Data.Common.DataColumnMapping("PriceNAV", "PriceNAV"), _
        New System.Data.Common.DataColumnMapping("PriceBasicNAV", "PriceBasicNAV"), _
        New System.Data.Common.DataColumnMapping("PriceGNAV", "PriceGNAV"), _
        New System.Data.Common.DataColumnMapping("PriceGAV", "PriceGAV"), _
        New System.Data.Common.DataColumnMapping("PriceBaseDate", "PriceBaseDate"), _
        New System.Data.Common.DataColumnMapping("PriceFinal", "PriceFinal"), _
        New System.Data.Common.DataColumnMapping("PriceIsPercent", "PriceIsPercent"), _
        New System.Data.Common.DataColumnMapping("PriceIsAdministrator", "PriceIsAdministrator"), _
        New System.Data.Common.DataColumnMapping("PriceIsMilestone", "PriceIsMilestone"), _
        New System.Data.Common.DataColumnMapping("PriceComment", "PriceComment"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command

        Select Case pTableName.ToUpper

          Case "TBLPRICEBYINSTRUMENT"

            SelectCommand.CommandText = "[adp_tblPrice_SelectByInstrument]"

            SelectCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int)).Value = 0

          Case Else

            ' "TblPrice"

            SelectCommand.CommandText = "[adp_tblPrice_SelectCommand]"

        End Select

        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblPrice_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceID", System.Data.SqlDbType.Int, 4, "PriceID"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceInstrument", System.Data.SqlDbType.Int, 4, "PriceInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceDate", System.Data.SqlDbType.DateTime, 8, "PriceDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PricePercent", System.Data.SqlDbType.Float, 8, "PricePercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceMultiplier", System.Data.SqlDbType.Float, 8, "PriceMultiplier"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceLevel", System.Data.SqlDbType.Float, 8, "PriceLevel"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceBasicNAV", System.Data.SqlDbType.Float, 8, "PriceBasicNAV"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceGNAV", System.Data.SqlDbType.Float, 8, "PriceGNAV"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceGAV", System.Data.SqlDbType.Float, 8, "PriceGAV"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceBaseDate", System.Data.SqlDbType.DateTime, 8, "PriceBaseDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceFinal", System.Data.SqlDbType.Int, 4, "PriceFinal"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceIsPercent", System.Data.SqlDbType.Int, 4, "PriceIsPercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceIsAdministrator", System.Data.SqlDbType.Int, 4, "PriceIsAdministrator"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceIsMilestone", System.Data.SqlDbType.Int, 4, "PriceIsMilestone"))
        InsertCommand.Parameters.Add(New SqlParameter("@PriceComment", System.Data.SqlDbType.NVarChar, 100, "PriceComment"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblPrice_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceID", System.Data.SqlDbType.Int, 4, "PriceID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceInstrument", System.Data.SqlDbType.Int, 4, "PriceInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceDate", System.Data.SqlDbType.DateTime, 8, "PriceDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PricePercent", System.Data.SqlDbType.Float, 8, "PricePercent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceMultiplier", System.Data.SqlDbType.Float, 8, "PriceMultiplier"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceLevel", System.Data.SqlDbType.Float, 8, "PriceLevel"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceBasicNAV", System.Data.SqlDbType.Float, 8, "PriceBasicNAV"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceGNAV", System.Data.SqlDbType.Float, 8, "PriceGNAV"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceGAV", System.Data.SqlDbType.Float, 8, "PriceGAV"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceBaseDate", System.Data.SqlDbType.DateTime, 8, "PriceBaseDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceFinal", System.Data.SqlDbType.Int, 4, "PriceFinal"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceIsPercent", System.Data.SqlDbType.Int, 4, "PriceIsPercent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceIsAdministrator", System.Data.SqlDbType.Int, 4, "PriceIsAdministrator"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceIsMilestone", System.Data.SqlDbType.Int, 4, "PriceIsMilestone"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PriceComment", System.Data.SqlDbType.NVarChar, 100, "PriceComment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblPrice_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@PriceID", System.Data.SqlDbType.Int, 4, "PriceID"))
        DeleteCommand.Parameters.Add(New SqlParameter("@PriceInstrument", System.Data.SqlDbType.Int, 4, "PriceInstrument"))
        DeleteCommand.Parameters.Add(New SqlParameter("@PriceDate", System.Data.SqlDbType.DateTime, 8, "PriceDate"))

      Case "TBLRECAVAILABILITITES"

        ' Use default Column mapping.

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblRecAvailabilities", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("captureID", "captureID"), _
        New System.Data.Common.DataColumnMapping("NAVDate", "NAVDate"), _
        New System.Data.Common.DataColumnMapping("FundCode", "FundCode"), _
        New System.Data.Common.DataColumnMapping("Description", "Description"), _
        New System.Data.Common.DataColumnMapping("PercentAssets_Value", "PercentAssets_Value"), _
        New System.Data.Common.DataColumnMapping("PercentAvailabilities_Value", "PercentAvailabilities_Value"), _
        New System.Data.Common.DataColumnMapping("Currency", "Currency"), _
        New System.Data.Common.DataColumnMapping("Balance_Value", "Balance_Value"), _
        New System.Data.Common.DataColumnMapping("CurrentBalance_Value", "CurrentBalance_Value"), _
        New System.Data.Common.DataColumnMapping("PreviousBalance_Value", "PreviousBalance_Value"), _
        New System.Data.Common.DataColumnMapping("PreviousCurrentBalance_Value", "PreviousCurrentBalance_Value"), _
        New System.Data.Common.DataColumnMapping("BalanceVariation_Value", "BalanceVariation_Value"), _
        New System.Data.Common.DataColumnMapping("BalanceVariationPercent_Value", "BalanceVariationPercent_Value") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblRecAvailabilitites_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@MaxCaptureID", System.Data.SqlDbType.Int, 4)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@NavDate", System.Data.SqlDbType.Date, 3))
        SelectCommand.Parameters.Add(New SqlParameter("@FundCode", System.Data.SqlDbType.NVarChar, 50)).Value = ""
        SelectCommand.Parameters.Add(New SqlParameter("@Currency", System.Data.SqlDbType.NVarChar, 5)).Value = ""
        SelectCommand.Parameters.Add(New SqlParameter("@Description", System.Data.SqlDbType.NVarChar, 255)).Value = ""
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

			Case "TBLREPORTCUSTOMISATION"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblReportCustomisation", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("ApplicationName", "ApplicationName"), _
				New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
				New System.Data.Common.DataColumnMapping("ReportName", "ReportName"), _
				New System.Data.Common.DataColumnMapping("FieldName", "FieldName"), _
				New System.Data.Common.DataColumnMapping("PropertyName", "PropertyName"), _
				New System.Data.Common.DataColumnMapping("PropertyValue", "PropertyValue"), _
				New System.Data.Common.DataColumnMapping("IsNumeric", "IsNumeric") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblReportCustomisation_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@ApplicationName", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@ReportName", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblReportCustomisation_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@ApplicationName", System.Data.SqlDbType.NVarChar, 50, "ApplicationName"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
				InsertCommand.Parameters.Add(New SqlParameter("@ReportName", System.Data.SqlDbType.NVarChar, 50, "ReportName"))
				InsertCommand.Parameters.Add(New SqlParameter("@FieldName", System.Data.SqlDbType.NVarChar, 50, "FieldName"))
				InsertCommand.Parameters.Add(New SqlParameter("@PropertyName", System.Data.SqlDbType.NVarChar, 20, "PropertyName"))
				InsertCommand.Parameters.Add(New SqlParameter("@PropertyValue", System.Data.SqlDbType.NVarChar, 1000, "PropertyValue"))
				InsertCommand.Parameters.Add(New SqlParameter("@IsNumeric", System.Data.SqlDbType.Bit, 1, "IsNumeric"))

				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblReportCustomisation_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@ApplicationName", System.Data.SqlDbType.NVarChar, 50, "ApplicationName"))
				UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@ReportName", System.Data.SqlDbType.NVarChar, 50, "ReportName"))
				UpdateCommand.Parameters.Add(New SqlParameter("@FieldName", System.Data.SqlDbType.NVarChar, 50, "FieldName"))
				UpdateCommand.Parameters.Add(New SqlParameter("@PropertyName", System.Data.SqlDbType.NVarChar, 20, "PropertyName"))
				UpdateCommand.Parameters.Add(New SqlParameter("@PropertyValue", System.Data.SqlDbType.NVarChar, 1000, "PropertyValue"))
				UpdateCommand.Parameters.Add(New SqlParameter("@IsNumeric", System.Data.SqlDbType.Bit, 1, "IsNumeric"))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblReportCustomisation_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@ApplicationName", System.Data.SqlDbType.NVarChar, 50, "ApplicationName"))
				DeleteCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
				DeleteCommand.Parameters.Add(New SqlParameter("@ReportName", System.Data.SqlDbType.NVarChar, 50, "ReportName"))
				DeleteCommand.Parameters.Add(New SqlParameter("@FieldName", System.Data.SqlDbType.NVarChar, 50, "FieldName"))
				DeleteCommand.Parameters.Add(New SqlParameter("@PropertyName", System.Data.SqlDbType.NVarChar, 20, "PropertyName"))


      Case "TBLREPORTDETAILLOG"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblReportDetailLog", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("AppName", "AppName"), _
        New System.Data.Common.DataColumnMapping("ReportName", "ReportName"), _
        New System.Data.Common.DataColumnMapping("KeyField", "KeyField"), _
        New System.Data.Common.DataColumnMapping("ValueField", "ValueField"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblReportDetailLog_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblReportDetailLog_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ReportName", System.Data.SqlDbType.NVarChar, 30, "ReportName"))
        InsertCommand.Parameters.Add(New SqlParameter("@KeyField", System.Data.SqlDbType.NVarChar, 50, "KeyField"))
        InsertCommand.Parameters.Add(New SqlParameter("@ValueField", System.Data.SqlDbType.Int, 4, "ValueField"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


			Case "TBLRISKDATA"
				' Table Mappings

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblRiskData", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("RiskDataID", "RiskDataID"), _
				New System.Data.Common.DataColumnMapping("DataCharacteristicId", "DataCharacteristicId"), _
				New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
				New System.Data.Common.DataColumnMapping("DataWeighting", "DataWeighting"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


				' Select Command
				SelectCommand.CommandText = "[adp_tblRiskData_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblRiskData_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				InsertCommand.Parameters.Add(New SqlParameter("@DataWeighting", System.Data.SqlDbType.Float, 8, "DataWeighting"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				' 
				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblRiskData_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@DataWeighting", System.Data.SqlDbType.Float, 8, "DataWeighting"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblRiskData_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))
				DeleteCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))


			Case "TBLRISKDATACATEGORY"
				' Table Mappings

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblRiskDataCategory", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("DataCategoryId", "DataCategoryId"), _
				New System.Data.Common.DataColumnMapping("DataCategory", "DataCategory"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


				' Select Command
				SelectCommand.CommandText = "[adp_tblRiskDataCategory_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblRiskDataCategory_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@DataCategoryId", System.Data.SqlDbType.Int, 4, "DataCategoryId"))
				InsertCommand.Parameters.Add(New SqlParameter("@DataCategory", System.Data.SqlDbType.NVarChar, 100, "DataCategory"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				' 
				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblRiskDataCategory_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@DataCategoryId", System.Data.SqlDbType.Int, 4, "DataCategoryId"))
				UpdateCommand.Parameters.Add(New SqlParameter("@DataCategory", System.Data.SqlDbType.NVarChar, 100, "DataCategory"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblRiskDataCategory_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@DataCategoryId", System.Data.SqlDbType.Int, 4, "DataCategoryId"))


      Case "TBLRISKDATACHARACTERISTIC"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblRiskDataCharacteristic", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("DataCategoryId", "DataCategoryId"), _
				New System.Data.Common.DataColumnMapping("DataCharacteristicId", "DataCharacteristicId"), _
				New System.Data.Common.DataColumnMapping("DataCharacteristic", "DataCharacteristic"), _
				New System.Data.Common.DataColumnMapping("DataCharacteristicTypeID", "DataCharacteristicTypeID"), _
				New System.Data.Common.DataColumnMapping("FixedValue", "FixedValue"), _
				New System.Data.Common.DataColumnMapping("DefaultValue", "DefaultValue"), _
				New System.Data.Common.DataColumnMapping("BooleanValue", "BooleanValue"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblRiskDataCharacteristic_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblRiskDataCharacteristic_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@DataCategoryId", System.Data.SqlDbType.Int, 4, "DataCategoryId"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))
				InsertCommand.Parameters.Add(New SqlParameter("@DataCharacteristic", System.Data.SqlDbType.NVarChar, 200, "DataCharacteristic"))
				InsertCommand.Parameters.Add(New SqlParameter("@DataCharacteristicTypeID", System.Data.SqlDbType.Int, 4, "DataCharacteristicTypeID"))
				InsertCommand.Parameters.Add(New SqlParameter("@FixedValue", System.Data.SqlDbType.Float, 8, "FixedValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@DefaultValue", System.Data.SqlDbType.Float, 8, "DefaultValue"))
				InsertCommand.Parameters.Add(New SqlParameter("@BooleanValue", System.Data.SqlDbType.Float, 8, "BooleanValue"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        ' 
        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblRiskDataCharacteristic_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataCategoryId", System.Data.SqlDbType.Int, 4, "DataCategoryId"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))
				UpdateCommand.Parameters.Add(New SqlParameter("@DataCharacteristic", System.Data.SqlDbType.NVarChar, 200, "DataCharacteristic"))
				UpdateCommand.Parameters.Add(New SqlParameter("@DataCharacteristicTypeID", System.Data.SqlDbType.Int, 4, "DataCharacteristicTypeID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@FixedValue", System.Data.SqlDbType.Float, 8, "FixedValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DefaultValue", System.Data.SqlDbType.Float, 8, "DefaultValue"))
				UpdateCommand.Parameters.Add(New SqlParameter("@BooleanValue", System.Data.SqlDbType.Float, 8, "BooleanValue"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblRiskDataCharacteristic_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))


			Case "TBLRISKDATACOMPOUNDCHARACTERISTIC"
				' Table Mappings

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblRiskDataCompoundCharacteristic", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("CompoundID", "CompoundID"), _
				New System.Data.Common.DataColumnMapping("ParentCharacteristicID", "ParentCharacteristicID"), _
				New System.Data.Common.DataColumnMapping("ChildCharacteristicID", "ChildCharacteristicID"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


				' Select Command
				SelectCommand.CommandText = "[adp_tblRiskDataCompoundCharacteristic_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblRiskDataCompoundCharacteristic_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@CompoundID", System.Data.SqlDbType.Int, 4, "CompoundID"))
				InsertCommand.Parameters.Add(New SqlParameter("@ParentCharacteristicID", System.Data.SqlDbType.Int, 4, "ParentCharacteristicID"))
				InsertCommand.Parameters.Add(New SqlParameter("@ChildCharacteristicID", System.Data.SqlDbType.Int, 4, "ChildCharacteristicID"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				' 
				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblRiskDataCompoundCharacteristic_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@CompoundID", System.Data.SqlDbType.Int, 4, "CompoundID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@ParentCharacteristicID", System.Data.SqlDbType.Int, 4, "ParentCharacteristicID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@ChildCharacteristicID", System.Data.SqlDbType.Int, 4, "ChildCharacteristicID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblRiskDataCompoundCharacteristic_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@ParentCharacteristicID", System.Data.SqlDbType.Int, 4, "ParentCharacteristicID"))
				DeleteCommand.Parameters.Add(New SqlParameter("@ChildCharacteristicID", System.Data.SqlDbType.Int, 4, "ChildCharacteristicID"))


			Case "TBLRISKDATALIMITS"
				' Table Mappings

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblRiskDataLimits", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("RiskDataLimitID", "RiskDataLimitID"), _
				New System.Data.Common.DataColumnMapping("LimitDescription", "LimitDescription"), _
				New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
				New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
				New System.Data.Common.DataColumnMapping("CharacteristicID", "CharacteristicID"), _
				New System.Data.Common.DataColumnMapping("Granularity", "Granularity"), _
				New System.Data.Common.DataColumnMapping("GreaterOrLessThan", "GreaterOrLessThan"), _
				New System.Data.Common.DataColumnMapping("LimitLevel", "LimitLevel"), _
				New System.Data.Common.DataColumnMapping("isPercent", "isPercent"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


				' Select Command
				SelectCommand.CommandText = "[adp_tblRiskDataLimits_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblRiskDataLimits_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@RiskDataLimitID", System.Data.SqlDbType.Int, 4, "RiskDataLimitID"))
				InsertCommand.Parameters.Add(New SqlParameter("@LimitDescription", System.Data.SqlDbType.NVarChar, 200, "LimitDescription"))
				InsertCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				InsertCommand.Parameters.Add(New SqlParameter("@CharacteristicID", System.Data.SqlDbType.Int, 4, "CharacteristicID"))
				InsertCommand.Parameters.Add(New SqlParameter("@Granularity", System.Data.SqlDbType.Int, 4, "Granularity"))
				InsertCommand.Parameters.Add(New SqlParameter("@GreaterOrLessThan", System.Data.SqlDbType.Float, 8, "GreaterOrLessThan"))
				InsertCommand.Parameters.Add(New SqlParameter("@LimitLevel", System.Data.SqlDbType.Float, 8, "LimitLevel"))
				InsertCommand.Parameters.Add(New SqlParameter("@isPercent", System.Data.SqlDbType.Int, 4, "isPercent"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				' 
				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblRiskDataLimits_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@RiskDataLimitID", System.Data.SqlDbType.Int, 4, "RiskDataLimitID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@LimitDescription", System.Data.SqlDbType.NVarChar, 200, "LimitDescription"))
				UpdateCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4, "FundID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@CharacteristicID", System.Data.SqlDbType.Int, 4, "CharacteristicID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Granularity", System.Data.SqlDbType.Int, 4, "Granularity"))
				UpdateCommand.Parameters.Add(New SqlParameter("@GreaterOrLessThan", System.Data.SqlDbType.Float, 8, "GreaterOrLessThan"))
				UpdateCommand.Parameters.Add(New SqlParameter("@LimitLevel", System.Data.SqlDbType.Float, 8, "LimitLevel"))
				UpdateCommand.Parameters.Add(New SqlParameter("@isPercent", System.Data.SqlDbType.Int, 4, "isPercent"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblRiskDataLimits_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@RiskDataLimitID", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))


      Case "TBLSELECTCHARACTERISTICS"
        ' Table Mappings

        pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblSelectCharacteristics", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("RiskDataID", "RiskDataID"), _
				New System.Data.Common.DataColumnMapping("DataCategoryId", "DataCategoryId"), _
				New System.Data.Common.DataColumnMapping("DataCharacteristicId", "DataCharacteristicId"), _
				New System.Data.Common.DataColumnMapping("DataCharacteristicTypeID", "DataCharacteristicTypeID"), _
				New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
				New System.Data.Common.DataColumnMapping("DataWeighting", "DataWeighting"), _
				New System.Data.Common.DataColumnMapping("DataCategory", "DataCategory"), _
				New System.Data.Common.DataColumnMapping("DataCharacteristic", "DataCharacteristic"), _
				New System.Data.Common.DataColumnMapping("InstrumentDescription", "InstrumentDescription"), _
				New System.Data.Common.DataColumnMapping("InstrumentType", "InstrumentType"), _
				New System.Data.Common.DataColumnMapping("isFixed", "isFixed"), _
				New System.Data.Common.DataColumnMapping("isDefault", "isDefault"), _
				New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})


        ' Select Command
        SelectCommand.CommandText = "[adp_tblSelectCharacteristics_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@DataCategoryId", System.Data.SqlDbType.Int))
        SelectCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int))
        SelectCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int))
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblSelectCharacteristics_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@DataCategoryId", System.Data.SqlDbType.Int, 4, "DataCategoryId"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))
        InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@DataWeighting", System.Data.SqlDbType.Float, 8, "DataWeighting"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        ' 
        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblSelectCharacteristics_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataCategoryId", System.Data.SqlDbType.Int, 4, "DataCategoryId"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataCharacteristicId", System.Data.SqlDbType.Int, 4, "DataCharacteristicId"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@DataWeighting", System.Data.SqlDbType.Float, 8, "DataWeighting"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblSelectCharacteristics_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@RiskDataID", System.Data.SqlDbType.Int, 4, "RiskDataID"))


      Case "TBLSELECTMEETING" ' No Update, Insert & Delete commands.

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblMeeting", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("MeetingID", "MeetingID"), _
        New System.Data.Common.DataColumnMapping("MeetingTitle", "MeetingTitle"), _
        New System.Data.Common.DataColumnMapping("MeetingContact", "MeetingContact"), _
        New System.Data.Common.DataColumnMapping("MeetingDate", "MeetingDate"), _
        New System.Data.Common.DataColumnMapping("MeetingPlace", "MeetingPlace"), _
        New System.Data.Common.DataColumnMapping("MeetingNote", "MeetingNote"), _
        New System.Data.Common.DataColumnMapping("MeetingGood", "MeetingGood"), _
        New System.Data.Common.DataColumnMapping("MeetingBad", "MeetingBad"), _
        New System.Data.Common.DataColumnMapping("MeetingWisdom", "MeetingWisdom"), _
        New System.Data.Common.DataColumnMapping("MeetingManagementCompanyID", "MeetingManagementCompanyID"), _
        New System.Data.Common.DataColumnMapping("ManagementCompanyName", "ManagementCompanyName"), _
        New System.Data.Common.DataColumnMapping("MeetingLocation", "MeetingLocation"), _
        New System.Data.Common.DataColumnMapping("LocationName", "LocationName"), _
        New System.Data.Common.DataColumnMapping("RegionName", "RegionName"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID1", "MeetingFundID1"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID2", "MeetingFundID2"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID3", "MeetingFundID3"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID4", "MeetingFundID4"), _
        New System.Data.Common.DataColumnMapping("MeetingFundID5", "MeetingFundID5"), _
        New System.Data.Common.DataColumnMapping("FundName1", "FundName1"), _
        New System.Data.Common.DataColumnMapping("FundName2", "FundName2"), _
        New System.Data.Common.DataColumnMapping("FundName3", "FundName3"), _
        New System.Data.Common.DataColumnMapping("FundName4", "FundName4"), _
        New System.Data.Common.DataColumnMapping("FundName5", "FundName5"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson1", "MeetingFCPerson1"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson2", "MeetingFCPerson2"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson3", "MeetingFCPerson3"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson4", "MeetingFCPerson4"), _
        New System.Data.Common.DataColumnMapping("MeetingFCPerson5", "MeetingFCPerson5"), _
        New System.Data.Common.DataColumnMapping("FCPersonName1", "FCPersonName1"), _
        New System.Data.Common.DataColumnMapping("FCPersonName2", "FCPersonName2"), _
        New System.Data.Common.DataColumnMapping("FCPersonName3", "FCPersonName3"), _
        New System.Data.Common.DataColumnMapping("FCPersonName4", "FCPersonName4"), _
        New System.Data.Common.DataColumnMapping("FCPersonName5", "FCPersonName5"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson1", "MeetingFundPerson1"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson2", "MeetingFundPerson2"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson3", "MeetingFundPerson3"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson4", "MeetingFundPerson4"), _
        New System.Data.Common.DataColumnMapping("MeetingFundPerson5", "MeetingFundPerson5"), _
        New System.Data.Common.DataColumnMapping("FundPersonName1", "FundPersonName1"), _
        New System.Data.Common.DataColumnMapping("FundPersonName2", "FundPersonName2"), _
        New System.Data.Common.DataColumnMapping("FundPersonName3", "FundPersonName3"), _
        New System.Data.Common.DataColumnMapping("FundPersonName4", "FundPersonName4"), _
        New System.Data.Common.DataColumnMapping("FundPersonName5", "FundPersonName5"), _
        New System.Data.Common.DataColumnMapping("MeetingStatus", "MeetingStatus"), _
        New System.Data.Common.DataColumnMapping("StatusDescription", "StatusDescription"), _
        New System.Data.Common.DataColumnMapping("MeetingExistingFund", "MeetingExistingFund"), _
        New System.Data.Common.DataColumnMapping("MeetingFundType", "MeetingFundType"), _
        New System.Data.Common.DataColumnMapping("FundTypeDescription", "FundTypeDescription"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Sienna_GetMeetingsData]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


			Case "TBLSELECTFUTURESNOTIONAL"	' tblSelectFuturesNotional

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblSelectFuturesNotional", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("FundID", "FundID"), _
				New System.Data.Common.DataColumnMapping("FundCode", "FundCode"), _
				New System.Data.Common.DataColumnMapping("FundName", "FundName"), _
				New System.Data.Common.DataColumnMapping("FundCurrencyID", "FundCurrencyID"), _
				New System.Data.Common.DataColumnMapping("FundCurrencyDescription", "FundCurrencyDescription"), _
				New System.Data.Common.DataColumnMapping("FuturesInstrumentID", "FuturesInstrumentID"), _
				New System.Data.Common.DataColumnMapping("FuturesInstrumentDescription", "FuturesInstrumentDescription"), _
				New System.Data.Common.DataColumnMapping("NotionalInstrumentID", "NotionalInstrumentID"), _
				New System.Data.Common.DataColumnMapping("NotionalInstrumentDescription", "NotionalInstrumentDescription"), _
				New System.Data.Common.DataColumnMapping("OpenPosition", "OpenPosition"), _
				New System.Data.Common.DataColumnMapping("CalculatedNotional", "CalculatedNotional"), _
				New System.Data.Common.DataColumnMapping("BookedNotional", "BookedNotional"), _
				New System.Data.Common.DataColumnMapping("FuturesContractSize", "FuturesContractSize"), _
				New System.Data.Common.DataColumnMapping("FuturesCurrencyID", "FuturesCurrencyID"), _
				New System.Data.Common.DataColumnMapping("FuturesCurrencyDescription", "FuturesCurrencyDescription"), _
				New System.Data.Common.DataColumnMapping("NotionalCurrencyID", "NotionalCurrencyID"), _
				New System.Data.Common.DataColumnMapping("NotionalCurrencyDescription", "NotionalCurrencyDescription"), _
				New System.Data.Common.DataColumnMapping("FundTypeID", "FundTypeID"), _
				New System.Data.Common.DataColumnMapping("FundTypeSortOrder", "FundTypeSortOrder"), _
				New System.Data.Common.DataColumnMapping("FundTypeDescription", "FundTypeDescription"), _
				New System.Data.Common.DataColumnMapping("Value", "Value"), _
				New System.Data.Common.DataColumnMapping("USD_Value", "USD_Value"), _
				New System.Data.Common.DataColumnMapping("Local_Value", "Local_Value"), _
				New System.Data.Common.DataColumnMapping("UnrealisedProfit", "UnrealisedProfit"), _
				New System.Data.Common.DataColumnMapping("USD_UnrealisedProfit", "USD_UnrealisedProfit"), _
				New System.Data.Common.DataColumnMapping("Local_UnrealisedProfit", "Local_UnrealisedProfit"), _
				New System.Data.Common.DataColumnMapping("PriceDate", "PriceDate"), _
				New System.Data.Common.DataColumnMapping("PriceLevel", "PriceLevel"), _
				New System.Data.Common.DataColumnMapping("Multiplier", "Multiplier"), _
				New System.Data.Common.DataColumnMapping("FundFXRate", "FundFXRate"), _
				New System.Data.Common.DataColumnMapping("FuturesInstrumentFXRate", "FuturesInstrumentFXRate"), _
				New System.Data.Common.DataColumnMapping("CompoundFXRate", "CompoundFXRate"), _
				New System.Data.Common.DataColumnMapping("NotionalTradeToZeroRealisedPnL", "NotionalTradeToZeroRealisedPnL"), _
				New System.Data.Common.DataColumnMapping("NotionalTradeToZeroUnRealisedPnL", "NotionalTradeToZeroUnRealisedPnL") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[rpt_Venice_NotionalAllocationReport]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int))
				SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int))
				SelectCommand.Parameters.Add(New SqlParameter("@UnRealisedCalculationType", System.Data.SqlDbType.Int))
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				SelectCommand.Parameters("@FundID").Value = 0
				SelectCommand.Parameters("@ValueDate").Value = Date.Now.Date
				SelectCommand.Parameters("@StatusGroupFilter").Value = ""
				SelectCommand.Parameters("@AdministratorDatesFilter").Value = 0
				SelectCommand.Parameters("@UnRealisedCalculationType").Value = 0


			Case "TBLSELECTTRANSACTION"	' No Update, Insert & Delete commands.
				' Note : This retrieves only the First Leg of the Transaction.
				' Second (Cash) legs are not included.

				pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblSelectTransaction", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("TransactionID", "TransactionID"), _
        New System.Data.Common.DataColumnMapping("TransactionTicket", "TransactionTicket"), _
        New System.Data.Common.DataColumnMapping("TransactionFund", "TransactionFund"), _
        New System.Data.Common.DataColumnMapping("TransactionSubFund", "TransactionSubFund"), _
        New System.Data.Common.DataColumnMapping("TransactionInstrument", "TransactionInstrument"), _
        New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
        New System.Data.Common.DataColumnMapping("TransactionValueorAmount", "TransactionValueorAmount"), _
        New System.Data.Common.DataColumnMapping("TransactionUnits", "TransactionUnits"), _
        New System.Data.Common.DataColumnMapping("TransactionSignedUnits", "TransactionSignedUnits"), _
        New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionCosts", "TransactionCosts"), _
        New System.Data.Common.DataColumnMapping("TransactionCostPercent", "TransactionCostPercent"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlement", "TransactionSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionSignedSettlement", "TransactionSignedSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionFXRate", "TransactionFXRate"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementCurrencyID", "TransactionSettlementCurrencyID"), _
        New System.Data.Common.DataColumnMapping("TransactionCounterparty", "TransactionCounterparty"), _
        New System.Data.Common.DataColumnMapping("TransactionBroker", "TransactionBroker"), _
        New System.Data.Common.DataColumnMapping("TransactionBrokerStrategy", "TransactionBrokerStrategy"), _
        New System.Data.Common.DataColumnMapping("TransactionInvestment", "TransactionInvestment"), _
        New System.Data.Common.DataColumnMapping("TransactionExecution", "TransactionExecution"), _
        New System.Data.Common.DataColumnMapping("TransactionDataEntry", "TransactionDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionDecisionDate", "TransactionDecisionDate"), _
        New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementDate", "TransactionSettlementDate"), _
        New System.Data.Common.DataColumnMapping("TransactionConfirmationDate", "TransactionConfirmationDate"), _
        New System.Data.Common.DataColumnMapping("TransactionEntryDate", "TransactionEntryDate"), _
        New System.Data.Common.DataColumnMapping("TransactionCostIsPercent", "TransactionCostIsPercent"), _
        New System.Data.Common.DataColumnMapping("TransactionExemptFromUpdate", "TransactionExemptFromUpdate"), _
        New System.Data.Common.DataColumnMapping("TransactionRedeemWholeHoldingFlag", "TransactionRedeemWholeHoldingFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionIsTransfer", "TransactionIsTransfer"), _
        New System.Data.Common.DataColumnMapping("TransactionSpecificInitialEqualisationFlag", "TransactionSpecificInitialEqualisationFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalAmount", "TransactionFinalAmount"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalPrice", "TransactionFinalPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalCosts", "TransactionFinalCosts"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalSettlement", "TransactionFinalSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionInstructionFlag", "TransactionInstructionFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionBankConfirmation", "TransactionBankConfirmation"), _
        New System.Data.Common.DataColumnMapping("TransactionInitialDataEntry", "TransactionInitialDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionAmountConfirmed", "TransactionAmountConfirmed"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementConfirmed", "TransactionSettlementConfirmed"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalDataEntry", "TransactionFinalDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionComment", "TransactionComment"), _
        New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
        New System.Data.Common.DataColumnMapping("TransactionRedemptionID", "TransactionRedemptionID"), _
        New System.Data.Common.DataColumnMapping("TransactionFeeID", "TransactionFeeID"), _
        New System.Data.Common.DataColumnMapping("TransactionSpecificToFundUnitID", "TransactionSpecificToFundUnitID"), _
        New System.Data.Common.DataColumnMapping("TransactionUser", "TransactionUser"), _
        New System.Data.Common.DataColumnMapping("TransactionGroup", "TransactionGroup"), _
        New System.Data.Common.DataColumnMapping("TransactionAdminStatus", "TransactionAdminStatus"), _
        New System.Data.Common.DataColumnMapping("TransactionTradeStatusID", "TransactionTradeStatusID"), _
        New System.Data.Common.DataColumnMapping("TransactionCTFLAGS", "TransactionCTFLAGS"), _
        New System.Data.Common.DataColumnMapping("TransactionCompoundRN", "TransactionCompoundRN"), _
        New System.Data.Common.DataColumnMapping("TransactionDateEntered", "TransactionDateEntered"), _
        New System.Data.Common.DataColumnMapping("FundName", "FundName"), _
        New System.Data.Common.DataColumnMapping("FundLegalEntity", "FundLegalEntity"), _
        New System.Data.Common.DataColumnMapping("InstrumentDescription", "InstrumentDescription"), _
        New System.Data.Common.DataColumnMapping("VersusInstrumentDescription", "VersusInstrumentDescription"), _
        New System.Data.Common.DataColumnMapping("SpecificFundUnitName", "SpecificFundUnitName"), _
        New System.Data.Common.DataColumnMapping("InstrumentCurrencyID", "InstrumentCurrencyID"), _
        New System.Data.Common.DataColumnMapping("InstrumentCurrency", "InstrumentCurrency"), _
        New System.Data.Common.DataColumnMapping("InstrumentPenaltyComment", "InstrumentPenaltyComment"), _
        New System.Data.Common.DataColumnMapping("InstrumentClass", "InstrumentClass"), _
        New System.Data.Common.DataColumnMapping("InstrumentISIN", "InstrumentISIN"), _
        New System.Data.Common.DataColumnMapping("InstrumentType", "InstrumentType"), _
        New System.Data.Common.DataColumnMapping("InstrumentNoticeDays", "InstrumentNoticeDays"), _
        New System.Data.Common.DataColumnMapping("InstrumentPenalty", "InstrumentPenalty"), _
        New System.Data.Common.DataColumnMapping("InstrumentUpfrontfees", "InstrumentUpfrontfees"), _
        New System.Data.Common.DataColumnMapping("TransactionTypeName", "TransactionTypeName"), _
        New System.Data.Common.DataColumnMapping("TradeStatusName", "TradeStatusName"), _
        New System.Data.Common.DataColumnMapping("CounterpartyName", "CounterpartyName"), _
        New System.Data.Common.DataColumnMapping("AmendedSettlement", "AmendedSettlement") _
        })})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblSelectTransaction_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@BothLegs", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				SelectCommand.Parameters("@BothLegs").Value = 0

			Case "TBLSELECTTRANSACTIONBYGROUP", "TBLSELECTTRANSACTIONFILEPENDING"	' No Update, Insert & Delete commands.
				' Note : This retrieves only the First Leg of the Transaction.
				' Second (Cash) legs are not normally included.
				' Note, this section handles the two select tables for the pending transactions form.

				' Note table name below is 'tblSelectTransaction'.
				' This is not (yet) configured as a normal table, just a shadow of the 'tblSelectTransaction'
				' BEWARE :- This Must be changed if it is to be used as a 'Normal' Venice table.

				pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblSelectTransaction", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("TransactionID", "TransactionID"), _
        New System.Data.Common.DataColumnMapping("TransactionTicket", "TransactionTicket"), _
        New System.Data.Common.DataColumnMapping("TransactionFund", "TransactionFund"), _
        New System.Data.Common.DataColumnMapping("TransactionSubFund", "TransactionSubFund"), _
        New System.Data.Common.DataColumnMapping("TransactionInstrument", "TransactionInstrument"), _
        New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
        New System.Data.Common.DataColumnMapping("TransactionValueorAmount", "TransactionValueorAmount"), _
        New System.Data.Common.DataColumnMapping("TransactionUnits", "TransactionUnits"), _
        New System.Data.Common.DataColumnMapping("TransactionSignedUnits", "TransactionSignedUnits"), _
        New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionCosts", "TransactionCosts"), _
        New System.Data.Common.DataColumnMapping("TransactionCostPercent", "TransactionCostPercent"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlement", "TransactionSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionSignedSettlement", "TransactionSignedSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionFXRate", "TransactionFXRate"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementCurrencyID", "TransactionSettlementCurrencyID"), _
        New System.Data.Common.DataColumnMapping("TransactionCounterparty", "TransactionCounterparty"), _
        New System.Data.Common.DataColumnMapping("TransactionBroker", "TransactionBroker"), _
        New System.Data.Common.DataColumnMapping("TransactionBrokerStrategy", "TransactionBrokerStrategy"), _
        New System.Data.Common.DataColumnMapping("TransactionInvestment", "TransactionInvestment"), _
        New System.Data.Common.DataColumnMapping("TransactionExecution", "TransactionExecution"), _
        New System.Data.Common.DataColumnMapping("TransactionDataEntry", "TransactionDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionDecisionDate", "TransactionDecisionDate"), _
        New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementDate", "TransactionSettlementDate"), _
        New System.Data.Common.DataColumnMapping("TransactionConfirmationDate", "TransactionConfirmationDate"), _
        New System.Data.Common.DataColumnMapping("TransactionEntryDate", "TransactionEntryDate"), _
        New System.Data.Common.DataColumnMapping("TransactionCostIsPercent", "TransactionCostIsPercent"), _
        New System.Data.Common.DataColumnMapping("TransactionExemptFromUpdate", "TransactionExemptFromUpdate"), _
        New System.Data.Common.DataColumnMapping("TransactionRedeemWholeHoldingFlag", "TransactionRedeemWholeHoldingFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionIsTransfer", "TransactionIsTransfer"), _
        New System.Data.Common.DataColumnMapping("TransactionSpecificInitialEqualisationFlag", "TransactionSpecificInitialEqualisationFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalAmount", "TransactionFinalAmount"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalPrice", "TransactionFinalPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalCosts", "TransactionFinalCosts"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalSettlement", "TransactionFinalSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionInstructionFlag", "TransactionInstructionFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionBankConfirmation", "TransactionBankConfirmation"), _
        New System.Data.Common.DataColumnMapping("TransactionInitialDataEntry", "TransactionInitialDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionAmountConfirmed", "TransactionAmountConfirmed"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementConfirmed", "TransactionSettlementConfirmed"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalDataEntry", "TransactionFinalDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionComment", "TransactionComment"), _
        New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
        New System.Data.Common.DataColumnMapping("TransactionRedemptionID", "TransactionRedemptionID"), _
        New System.Data.Common.DataColumnMapping("TransactionVersusInstrument", "TransactionVersusInstrument"), _
        New System.Data.Common.DataColumnMapping("TransactionUser", "TransactionUser"), _
        New System.Data.Common.DataColumnMapping("TransactionGroup", "TransactionGroup"), _
        New System.Data.Common.DataColumnMapping("TransactionAdminStatus", "TransactionAdminStatus"), _
        New System.Data.Common.DataColumnMapping("TransactionTradeStatusID", "TransactionTradeStatusID"), _
        New System.Data.Common.DataColumnMapping("TransactionCTFLAGS", "TransactionCTFLAGS"), _
        New System.Data.Common.DataColumnMapping("TransactionCompoundRN", "TransactionCompoundRN"), _
        New System.Data.Common.DataColumnMapping("TransactionDateEntered", "TransactionDateEntered"), _
        New System.Data.Common.DataColumnMapping("FundName", "FundName"), _
        New System.Data.Common.DataColumnMapping("FundLegalEntity", "FundLegalEntity"), _
        New System.Data.Common.DataColumnMapping("InstrumentDescription", "InstrumentDescription"), _
        New System.Data.Common.DataColumnMapping("VersusInstrumentDescription", "VersusInstrumentDescription"), _
        New System.Data.Common.DataColumnMapping("InstrumentCurrencyID", "InstrumentCurrencyID"), _
        New System.Data.Common.DataColumnMapping("InstrumentCurrency", "InstrumentCurrency"), _
        New System.Data.Common.DataColumnMapping("InstrumentPenaltyComment", "InstrumentPenaltyComment"), _
        New System.Data.Common.DataColumnMapping("InstrumentClass", "InstrumentClass"), _
        New System.Data.Common.DataColumnMapping("InstrumentType", "InstrumentType"), _
        New System.Data.Common.DataColumnMapping("InstrumentNoticeDays", "InstrumentNoticeDays"), _
        New System.Data.Common.DataColumnMapping("InstrumentPenalty", "InstrumentPenalty"), _
        New System.Data.Common.DataColumnMapping("InstrumentUpfrontfees", "InstrumentUpfrontfees"), _
        New System.Data.Common.DataColumnMapping("TransactionTypeName", "TransactionTypeName"), _
        New System.Data.Common.DataColumnMapping("TradeStatusName", "TradeStatusName"), _
        New System.Data.Common.DataColumnMapping("CounterpartyName", "CounterpartyName"), _
        New System.Data.Common.DataColumnMapping("AmendedSettlement", "AmendedSettlement"), _
        New System.Data.Common.DataColumnMapping("TradeFileRN", "TradeFileRN"), _
        New System.Data.Common.DataColumnMapping("BrokerID", "BrokerID"), _
        New System.Data.Common.DataColumnMapping("WorkflowID", "WorkflowID"), _
        New System.Data.Common.DataColumnMapping("BrokerDescription", "BrokerDescription") _
        })})

				' Set Select Command
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@BothLegs", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				If (pTableName.ToUpper = "TBLSELECTTRANSACTIONFILEPENDING") Then
					SelectCommand.CommandText = "[adp_tblSelectTransactionFilePending]"
				Else ' 
					SelectCommand.CommandText = "[adp_tblSelectTransactionByStatusGroup]"
					SelectCommand.Parameters.Add(New SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4))
					SelectCommand.Parameters("@GroupID").Value = (-1)
				End If

				SelectCommand.Parameters("@BothLegs").Value = 0


			Case "TBLSELECTCOMPOUNDTRANSACTION"	' No Update, Insert & Delete commands.
				' Note : This retrieves only the First Leg of the Transaction.
				' Second (Cash) legs are not included.

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblSelectCompoundTransaction", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("TransactionCompoundID", "TransactionCompoundID"), _
				New System.Data.Common.DataColumnMapping("TransactionTicket", "TransactionTicket"), _
				New System.Data.Common.DataColumnMapping("TransactionFund", "TransactionFund"), _
				New System.Data.Common.DataColumnMapping("TransactionInstrument", "TransactionInstrument"), _
				New System.Data.Common.DataColumnMapping("TransactionTemplateID", "TransactionTemplateID"), _
				New System.Data.Common.DataColumnMapping("TransactionValueorAmount", "TransactionValueorAmount"), _
				New System.Data.Common.DataColumnMapping("TransactionUnits", "TransactionUnits"), _
				New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
				New System.Data.Common.DataColumnMapping("TransactionCosts", "TransactionCosts"), _
				New System.Data.Common.DataColumnMapping("TransactionCounterparty", "TransactionCounterparty"), _
				New System.Data.Common.DataColumnMapping("TransactionInvestment", "TransactionInvestment"), _
				New System.Data.Common.DataColumnMapping("TransactionExecution", "TransactionExecution"), _
				New System.Data.Common.DataColumnMapping("TransactionDataEntry", "TransactionDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionDecisionDate", "TransactionDecisionDate"), _
				New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
				New System.Data.Common.DataColumnMapping("TransactionFIFOValueDate", "TransactionFIFOValueDate"), _
				New System.Data.Common.DataColumnMapping("TransactionSettlementDate", "TransactionSettlementDate"), _
				New System.Data.Common.DataColumnMapping("TransactionConfirmationDate", "TransactionConfirmationDate"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalAmount", "TransactionFinalAmount"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalPrice", "TransactionFinalPrice"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalCosts", "TransactionFinalCosts"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalSettlement", "TransactionFinalSettlement"), _
				New System.Data.Common.DataColumnMapping("TransactionInstructionFlag", "TransactionInstructionFlag"), _
				New System.Data.Common.DataColumnMapping("TransactionBankConfirmation", "TransactionBankConfirmation"), _
				New System.Data.Common.DataColumnMapping("TransactionInitialDataEntry", "TransactionInitialDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionAmountConfirmed", "TransactionAmountConfirmed"), _
				New System.Data.Common.DataColumnMapping("TransactionSettlementConfirmed", "TransactionSettlementConfirmed"), _
				New System.Data.Common.DataColumnMapping("TransactionFinalDataEntry", "TransactionFinalDataEntry"), _
				New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
				New System.Data.Common.DataColumnMapping("CompoundTransactionGroup", "CompoundTransactionGroup"), _
				New System.Data.Common.DataColumnMapping("CompoundTransactionComment", "CompoundTransactionComment"), _
				New System.Data.Common.DataColumnMapping("CompoundTransactionFLAGS", "CompoundTransactionFLAGS"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("FundName", "FundName"), _
				New System.Data.Common.DataColumnMapping("InstrumentDescription", "InstrumentDescription"), _
				New System.Data.Common.DataColumnMapping("TransactionTypeName", "TransactionTypeName") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblSelectCompoundTransaction_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

      Case "TBLSUBFUND"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblSubFund", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("SubFundID", "SubFundID"), _
        New System.Data.Common.DataColumnMapping("EquivalentFundID", "EquivalentFundID"), _
        New System.Data.Common.DataColumnMapping("ParentFundID", "ParentFundID"), _
        New System.Data.Common.DataColumnMapping("IsCashPool", "IsCashPool"), _
        New System.Data.Common.DataColumnMapping("IsCashSweepFund", "IsCashSweepFund"), _
        New System.Data.Common.DataColumnMapping("IsSubscriptionFund", "IsSubscriptionFund"), _
        New System.Data.Common.DataColumnMapping("ReferenceBasket", "ReferenceBasket"), _
        New System.Data.Common.DataColumnMapping("SubFundName", "SubFundName") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblSubFund_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblSubFund_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@SubFundID", System.Data.SqlDbType.Int, 4, "SubFundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@ParentFundID", System.Data.SqlDbType.Int, 4, "ParentFundID"))
        InsertCommand.Parameters.Add(New SqlParameter("@SubFundName", System.Data.SqlDbType.NVarChar, 50, "SubFundName"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsCashPool", System.Data.SqlDbType.Bit, 1, "IsCashPool"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsCashSweepFund", System.Data.SqlDbType.Bit, 1, "IsCashSweepFund"))
        InsertCommand.Parameters.Add(New SqlParameter("@IsSubscriptionsFund", System.Data.SqlDbType.Bit, 1, "IsSubscriptionsFund"))
        InsertCommand.Parameters.Add(New SqlParameter("@ReferenceBasket", System.Data.SqlDbType.Int, 4, "ReferenceBasket"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblSubFund_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@SubFundID", System.Data.SqlDbType.Int, 4, "SubFundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ParentFundID", System.Data.SqlDbType.Int, 4, "ParentFundID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@SubFundName", System.Data.SqlDbType.NVarChar, 50, "SubFundName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsCashPool", System.Data.SqlDbType.Bit, 1, "IsCashPool"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsCashSweepFund", System.Data.SqlDbType.Bit, 1, "IsCashSweepFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@IsSubscriptionsFund", System.Data.SqlDbType.Bit, 1, "IsSubscriptionsFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ReferenceBasket", System.Data.SqlDbType.Int, 4, "ReferenceBasket"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblSubFund_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@SubFundID", System.Data.SqlDbType.Int, 4, "SubFundID"))


      Case "TBLSUBFUNDHEIRARCHY"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblSubFund", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("SubFundID", "SubFundID"), _
        New System.Data.Common.DataColumnMapping("EquivalentFundID", "EquivalentFundID"), _
        New System.Data.Common.DataColumnMapping("ParentFundID", "ParentFundID"), _
        New System.Data.Common.DataColumnMapping("SubFundName", "SubFundName"), _
        New System.Data.Common.DataColumnMapping("SubFundCurrency", "SubFundCurrency"), _
        New System.Data.Common.DataColumnMapping("ReferenceBasket", "ReferenceBasket"), _
        New System.Data.Common.DataColumnMapping("IsClosed", "IsClosed"), _
        New System.Data.Common.DataColumnMapping("IsCashPool", "IsCashPool"), _
        New System.Data.Common.DataColumnMapping("IsCashSweepFund", "IsCashSweepFund"), _
        New System.Data.Common.DataColumnMapping("IsSubscriptionFund", "IsSubscriptionFund"), _
        New System.Data.Common.DataColumnMapping("Level", "Level"), _
        New System.Data.Common.DataColumnMapping("TreeOrder", "TreeOrder"), _
        New System.Data.Common.DataColumnMapping("FundPath", "FundPath"), _
        New System.Data.Common.DataColumnMapping("TopFundID", "TopFundID") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblSubFund_Heirarchy]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand

        'SqlUpdateCommand

        'SqlDeleteCommand


      Case "TBLSOPHISSTATUSGROUPS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblSophisStatusGroups", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("RECORD_TYPE", "RECORD_TYPE"), _
        New System.Data.Common.DataColumnMapping("GROUP_NAME", "GROUP_NAME"), _
        New System.Data.Common.DataColumnMapping("KERNEL_STATUS_GROUP_ID", "KERNEL_STATUS_GROUP_ID"), _
        New System.Data.Common.DataColumnMapping("PRIORITY", "PRIORITY"), _
        New System.Data.Common.DataColumnMapping("STATUS_ID", "STATUS_ID"), _
        New System.Data.Common.DataColumnMapping("STATUS_NAME", "STATUS_NAME"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblSophisStatusGroups_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))


			Case "TBLSYSTEMSTRINGS"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblSystemStrings", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("StringDescription", "StringDescription"), _
				New System.Data.Common.DataColumnMapping("StringValue", "StringValue") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblSystemStrings_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@StringDescription", System.Data.SqlDbType.NVarChar, 100))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblSystemStrings_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@StringDescription", System.Data.SqlDbType.NVarChar, 100, "StringDescription"))
				InsertCommand.Parameters.Add(New SqlParameter("@StringValue", System.Data.SqlDbType.NVarChar, 250, "StringValue"))

				'SqlUpdateCommand
				'
				UpdateCommand.CommandText = "[adp_tblSystemStrings_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@StringDescription", System.Data.SqlDbType.NVarChar, 100, "StringDescription"))
				UpdateCommand.Parameters.Add(New SqlParameter("@StringValue", System.Data.SqlDbType.NVarChar, 250, "StringValue"))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblSystemStrings_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@StringDescription", System.Data.SqlDbType.NVarChar, 100, "StringDescription"))


			Case "TBLTRADESTATUS"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblTradeStatus", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("TradeStatusID", "TradeStatusID"), _
				New System.Data.Common.DataColumnMapping("TradeStatusName", "TradeStatusName"), _
				New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
				New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblTradeStatus_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblTradeStatus_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@TradeStatusID", System.Data.SqlDbType.Int, 4, "TradeStatusID"))
				InsertCommand.Parameters.Add(New SqlParameter("@TradeStatusName", System.Data.SqlDbType.NVarChar, 200, "TradeStatusName"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand

				UpdateCommand.CommandText = "[adp_tblTradeStatus_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@TradeStatusID", System.Data.SqlDbType.Int, 4, "TradeStatusID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@TradeStatusName", System.Data.SqlDbType.NVarChar, 200, "TradeStatusName"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblTradeStatus_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@TradeStatusID", System.Data.SqlDbType.Int, 4, "TradeStatusID"))



      Case "TBLTRADESTATUSGROUP"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblTradeStatusGroup", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ItemID", "ItemID"), _
        New System.Data.Common.DataColumnMapping("TradeStatusGroupID", "TradeStatusGroupID"), _
        New System.Data.Common.DataColumnMapping("TradeStatusGroupName", "TradeStatusGroupName"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblTradeStatusGroup_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '

        'SqlUpdateCommand


        'SqlDeleteCommand
        '


      Case "TBLTRADESTATUSGROUPITEMS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblTradeStatusGroupItems", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ItemID", "ItemID"), _
        New System.Data.Common.DataColumnMapping("GroupID", "GroupID"), _
        New System.Data.Common.DataColumnMapping("StatusID", "StatusID"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblTradeStatusGroupItems_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblTradeStatusGroupItems_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@ItemID", System.Data.SqlDbType.Int, 4, "ItemID"))
        InsertCommand.Parameters.Add(New SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"))
        InsertCommand.Parameters.Add(New SqlParameter("@StatusID", System.Data.SqlDbType.Int, 4, "StatusID"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand

        UpdateCommand.CommandText = "[adp_tblTradeStatusGroupItems_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@ItemID", System.Data.SqlDbType.Int, 4, "ItemID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@StatusID", System.Data.SqlDbType.Int, 4, "StatusID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblTradeStatusGroupItems_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@ItemID", System.Data.SqlDbType.Int, 4, "ItemID"))


      Case "TBLTRANSACTION" ' 
        ' Note : This retrieves only the First Leg of the Transaction.
        ' Second (Cash) legs are not included.

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblTransaction", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("TransactionID", "TransactionID"), _
        New System.Data.Common.DataColumnMapping("TransactionTicket", "TransactionTicket"), _
        New System.Data.Common.DataColumnMapping("TransactionFund", "TransactionFund"), _
        New System.Data.Common.DataColumnMapping("TransactionSubFund", "TransactionSubFund"), _
        New System.Data.Common.DataColumnMapping("TransactionInstrument", "TransactionInstrument"), _
        New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
        New System.Data.Common.DataColumnMapping("TransactionType_Contra", "TransactionType_Contra"), _
        New System.Data.Common.DataColumnMapping("TransactionValueorAmount", "TransactionValueorAmount"), _
        New System.Data.Common.DataColumnMapping("TransactionUnits", "TransactionUnits"), _
        New System.Data.Common.DataColumnMapping("TransactionSignedUnits", "TransactionSignedUnits"), _
        New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionCleanPrice", "TransactionCleanPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionAccruedInterest", "TransactionAccruedInterest"), _
        New System.Data.Common.DataColumnMapping("TransactionBrokerFees", "TransactionBrokerFees"), _
        New System.Data.Common.DataColumnMapping("TransactionCosts", "TransactionCosts"), _
        New System.Data.Common.DataColumnMapping("TransactionCostPercent", "TransactionCostPercent"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlement", "TransactionSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionSignedSettlement", "TransactionSignedSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionFXRate", "TransactionFXRate"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementCurrencyID", "TransactionSettlementCurrencyID"), _
        New System.Data.Common.DataColumnMapping("TransactionEffectivePrice", "TransactionEffectivePrice"), _
        New System.Data.Common.DataColumnMapping("TransactionEffectiveWatermark", "TransactionEffectiveWatermark"), _
        New System.Data.Common.DataColumnMapping("TransactionSpecificInitialEqualisationValue", "TransactionSpecificInitialEqualisationValue"), _
        New System.Data.Common.DataColumnMapping("TransactionCounterparty", "TransactionCounterparty"), _
        New System.Data.Common.DataColumnMapping("TransactionBroker", "TransactionBroker"), _
        New System.Data.Common.DataColumnMapping("TransactionBrokerStrategy", "TransactionBrokerStrategy"), _
        New System.Data.Common.DataColumnMapping("TransactionInvestment", "TransactionInvestment"), _
        New System.Data.Common.DataColumnMapping("TransactionExecution", "TransactionExecution"), _
        New System.Data.Common.DataColumnMapping("TransactionDataEntry", "TransactionDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionDecisionDate", "TransactionDecisionDate"), _
        New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
        New System.Data.Common.DataColumnMapping("TransactionFIFOValueDate", "TransactionFIFOValueDate"), _
        New System.Data.Common.DataColumnMapping("TransactionEffectiveValueDate", "TransactionEffectiveValueDate"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementDate", "TransactionSettlementDate"), _
        New System.Data.Common.DataColumnMapping("TransactionConfirmationDate", "TransactionConfirmationDate"), _
        New System.Data.Common.DataColumnMapping("TransactionEntryDate", "TransactionEntryDate"), _
        New System.Data.Common.DataColumnMapping("TransactionCostIsPercent", "TransactionCostIsPercent"), _
        New System.Data.Common.DataColumnMapping("TransactionExemptFromUpdate", "TransactionExemptFromUpdate"), _
        New System.Data.Common.DataColumnMapping("TransactionRedeemWholeHoldingFlag", "TransactionRedeemWholeHoldingFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionIsTransfer", "TransactionIsTransfer"), _
        New System.Data.Common.DataColumnMapping("TransactionSpecificInitialEqualisationFlag", "TransactionSpecificInitialEqualisationFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionFuturesStylePricing", "TransactionFuturesStylePricing"), _
        New System.Data.Common.DataColumnMapping("TransactionClosingTrade", "TransactionClosingTrade"), _
        New System.Data.Common.DataColumnMapping("TransactionIsForwardPayment", "TransactionIsForwardPayment"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalAmount", "TransactionFinalAmount"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalPrice", "TransactionFinalPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalCosts", "TransactionFinalCosts"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalSettlement", "TransactionFinalSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionInstructionFlag", "TransactionInstructionFlag"), _
        New System.Data.Common.DataColumnMapping("TransactionBankConfirmation", "TransactionBankConfirmation"), _
        New System.Data.Common.DataColumnMapping("TransactionInitialDataEntry", "TransactionInitialDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionAmountConfirmed", "TransactionAmountConfirmed"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementConfirmed", "TransactionSettlementConfirmed"), _
        New System.Data.Common.DataColumnMapping("TransactionFinalDataEntry", "TransactionFinalDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionComment", "TransactionComment"), _
        New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
        New System.Data.Common.DataColumnMapping("TransactionRedemptionID", "TransactionRedemptionID"), _
        New System.Data.Common.DataColumnMapping("TransactionVersusInstrument", "TransactionVersusInstrument"), _
        New System.Data.Common.DataColumnMapping("TransactionFeeID", "TransactionFeeID"), _
        New System.Data.Common.DataColumnMapping("TransactionSpecificToFundUnitID", "TransactionSpecificToFundUnitID"), _
        New System.Data.Common.DataColumnMapping("TransactionUser", "TransactionUser"), _
        New System.Data.Common.DataColumnMapping("TransactionGroup", "TransactionGroup"), _
        New System.Data.Common.DataColumnMapping("TransactionLeg", "TransactionLeg"), _
        New System.Data.Common.DataColumnMapping("TransactionTradeStatusID", "TransactionTradeStatusID"), _
        New System.Data.Common.DataColumnMapping("TransactionAdminStatus", "TransactionAdminStatus"), _
        New System.Data.Common.DataColumnMapping("TransactionCTFLAGS", "TransactionCTFLAGS"), _
        New System.Data.Common.DataColumnMapping("TransactionCompoundRN", "TransactionCompoundRN"), _
        New System.Data.Common.DataColumnMapping("TransactionDateEntered", "TransactionDateEntered"), _
        New System.Data.Common.DataColumnMapping("TransactionDateDeleted", "TransactionDateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblTransaction_FirstLeg_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        '
        'SqlInsertCommand
        '

        InsertCommand.CommandText = "[adp_tblTransaction_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionTicket", System.Data.SqlDbType.NVarChar, 50, "TransactionTicket"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFund", System.Data.SqlDbType.Int, 4, "TransactionFund"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSubFund", System.Data.SqlDbType.Int, 4, "TransactionSubFund"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionInstrument", System.Data.SqlDbType.Int, 4, "TransactionInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionValueorAmount", System.Data.SqlDbType.NVarChar, 10, "TransactionValueorAmount"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionUnits", System.Data.SqlDbType.Float, 8, "TransactionUnits"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCleanPrice", System.Data.SqlDbType.Float, 8, "TransactionCleanPrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionAccruedInterest", System.Data.SqlDbType.Float, 8, "TransactionAccruedInterest"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionBrokerFees", System.Data.SqlDbType.Float, 8, "TransactionBrokerFees"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCosts", System.Data.SqlDbType.Float, 8, "TransactionCosts"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCostPercent", System.Data.SqlDbType.Float, 8, "TransactionCostPercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFXRate", System.Data.SqlDbType.Float, 8, "TransactionFXRate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSettlementCurrencyID", System.Data.SqlDbType.Int, 4, "TransactionSettlementCurrencyID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectivePrice", System.Data.SqlDbType.Float, 8, "TransactionEffectivePrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveWatermark", System.Data.SqlDbType.Float, 8, "TransactionEffectiveWatermark"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationValue", System.Data.SqlDbType.Float, 8, "TransactionSpecificInitialEqualisationValue"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCounterparty", System.Data.SqlDbType.Int, 4, "TransactionCounterparty"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionBroker", System.Data.SqlDbType.Int, 4, "TransactionBroker"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionBrokerStrategy", System.Data.SqlDbType.VarChar, 50, "TransactionBrokerStrategy"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionInvestment", System.Data.SqlDbType.NVarChar, 50, "TransactionInvestment"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionExecution", System.Data.SqlDbType.NVarChar, 50, "TransactionExecution"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionDataEntry", System.Data.SqlDbType.NVarChar, 50, "TransactionDataEntry"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionDecisionDate", System.Data.SqlDbType.DateTime, 8, "TransactionDecisionDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFIFOValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionFIFOValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionEffectiveValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSettlementDate", System.Data.SqlDbType.DateTime, 8, "TransactionSettlementDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionConfirmationDate", System.Data.SqlDbType.DateTime, 8, "TransactionConfirmationDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCostIsPercent", System.Data.SqlDbType.Bit, 1, "TransactionCostIsPercent"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionExemptFromUpdate", System.Data.SqlDbType.Bit, 1, "TransactionExemptFromUpdate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionRedeemWholeHoldingFlag", System.Data.SqlDbType.Bit, 1, "TransactionRedeemWholeHoldingFlag"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionIsTransfer", System.Data.SqlDbType.Bit, 1, "TransactionIsTransfer"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationFlag", System.Data.SqlDbType.Bit, 1, "TransactionSpecificInitialEqualisationFlag"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionClosingTrade", System.Data.SqlDbType.Bit, 1, "TransactionClosingTrade"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionIsForwardPayment", System.Data.SqlDbType.Bit, 1, "TransactionIsForwardPayment"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalAmount", System.Data.SqlDbType.Int, 4, "TransactionFinalAmount"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalPrice", System.Data.SqlDbType.Int, 4, "TransactionFinalPrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalCosts", System.Data.SqlDbType.Int, 4, "TransactionFinalCosts"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalSettlement", System.Data.SqlDbType.Int, 4, "TransactionFinalSettlement"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionInstructionFlag", System.Data.SqlDbType.Int, 4, "TransactionInstructionFlag"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionBankConfirmation", System.Data.SqlDbType.Int, 4, "TransactionBankConfirmation"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionInitialDataEntry", System.Data.SqlDbType.Int, 4, "TransactionInitialDataEntry"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionAmountConfirmed", System.Data.SqlDbType.Int, 4, "TransactionAmountConfirmed"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSettlementConfirmed", System.Data.SqlDbType.Int, 4, "TransactionSettlementConfirmed"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFinalDataEntry", System.Data.SqlDbType.Int, 4, "TransactionFinalDataEntry"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionComment", System.Data.SqlDbType.NVarChar, 500, "TransactionComment"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionRedemptionID", System.Data.SqlDbType.Int, 4, "TransactionRedemptionID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionVersusInstrument", System.Data.SqlDbType.Int, 4, "TransactionVersusInstrument"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionFeeID", System.Data.SqlDbType.Int, 4, "TransactionFeeID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionSpecificToFundUnitID", System.Data.SqlDbType.Int, 4, "TransactionSpecificToFundUnitID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionGroup", System.Data.SqlDbType.NVarChar, 50, "TransactionGroup"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionTradeStatusID", System.Data.SqlDbType.Int, 4, "TransactionTradeStatusID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionAdminStatus", System.Data.SqlDbType.Int, 4, "TransactionAdminStatus"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCTFLAGS", System.Data.SqlDbType.Int, 4, "TransactionCTFLAGS"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionCompoundRN", System.Data.SqlDbType.Int, 4, "TransactionCompoundRN2"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))
        InsertCommand.Parameters.Add(New SqlParameter("@ValidationOnly", System.Data.SqlDbType.Int, 4))
        InsertCommand.Parameters.Add(New SqlParameter("@rvStatusString", System.Data.SqlDbType.NVarChar, 200, System.Data.ParameterDirection.Output, True, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))

        ' 
        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblTransaction_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionTicket", System.Data.SqlDbType.NVarChar, 50, "TransactionTicket"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFund", System.Data.SqlDbType.Int, 4, "TransactionFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSubFund", System.Data.SqlDbType.Int, 4, "TransactionSubFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionInstrument", System.Data.SqlDbType.Int, 4, "TransactionInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionValueorAmount", System.Data.SqlDbType.NVarChar, 10, "TransactionValueorAmount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionUnits", System.Data.SqlDbType.Float, 8, "TransactionUnits"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCleanPrice", System.Data.SqlDbType.Float, 8, "TransactionCleanPrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionAccruedInterest", System.Data.SqlDbType.Float, 8, "TransactionAccruedInterest"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionBrokerFees", System.Data.SqlDbType.Float, 8, "TransactionBrokerFees"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCosts", System.Data.SqlDbType.Float, 8, "TransactionCosts"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCostPercent", System.Data.SqlDbType.Float, 8, "TransactionCostPercent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFXRate", System.Data.SqlDbType.Float, 8, "TransactionFXRate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSettlementCurrencyID", System.Data.SqlDbType.Int, 4, "TransactionSettlementCurrencyID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionEffectivePrice", System.Data.SqlDbType.Float, 8, "TransactionEffectivePrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveWatermark", System.Data.SqlDbType.Float, 8, "TransactionEffectiveWatermark"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationValue", System.Data.SqlDbType.Float, 8, "TransactionSpecificInitialEqualisationValue"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCounterparty", System.Data.SqlDbType.Int, 4, "TransactionCounterparty"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionBroker", System.Data.SqlDbType.Int, 4, "TransactionBroker"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionBrokerStrategy", System.Data.SqlDbType.VarChar, 50, "TransactionBrokerStrategy"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionInvestment", System.Data.SqlDbType.NVarChar, 50, "TransactionInvestment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionExecution", System.Data.SqlDbType.NVarChar, 50, "TransactionExecution"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionDataEntry", System.Data.SqlDbType.NVarChar, 50, "TransactionDataEntry"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionDecisionDate", System.Data.SqlDbType.DateTime, 8, "TransactionDecisionDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFIFOValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionFIFOValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionEffectiveValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionEffectiveValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSettlementDate", System.Data.SqlDbType.DateTime, 8, "TransactionSettlementDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionConfirmationDate", System.Data.SqlDbType.DateTime, 8, "TransactionConfirmationDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCostIsPercent", System.Data.SqlDbType.Bit, 1, "TransactionCostIsPercent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionExemptFromUpdate", System.Data.SqlDbType.Bit, 1, "TransactionExemptFromUpdate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionRedeemWholeHoldingFlag", System.Data.SqlDbType.Bit, 1, "TransactionRedeemWholeHoldingFlag"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionIsTransfer", System.Data.SqlDbType.Bit, 1, "TransactionIsTransfer"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSpecificInitialEqualisationFlag", System.Data.SqlDbType.Bit, 1, "TransactionSpecificInitialEqualisationFlag"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionClosingTrade", System.Data.SqlDbType.Bit, 1, "TransactionClosingTrade"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionIsForwardPayment", System.Data.SqlDbType.Bit, 1, "TransactionIsForwardPayment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalAmount", System.Data.SqlDbType.Int, 4, "TransactionFinalAmount"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalPrice", System.Data.SqlDbType.Int, 4, "TransactionFinalPrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalCosts", System.Data.SqlDbType.Int, 4, "TransactionFinalCosts"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalSettlement", System.Data.SqlDbType.Int, 4, "TransactionFinalSettlement"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionInstructionFlag", System.Data.SqlDbType.Int, 4, "TransactionInstructionFlag"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionBankConfirmation", System.Data.SqlDbType.Int, 4, "TransactionBankConfirmation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionInitialDataEntry", System.Data.SqlDbType.Int, 4, "TransactionInitialDataEntry"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionAmountConfirmed", System.Data.SqlDbType.Int, 4, "TransactionAmountConfirmed"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSettlementConfirmed", System.Data.SqlDbType.Int, 4, "TransactionSettlementConfirmed"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFinalDataEntry", System.Data.SqlDbType.Int, 4, "TransactionFinalDataEntry"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionComment", System.Data.SqlDbType.NVarChar, 500, "TransactionComment"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionRedemptionID", System.Data.SqlDbType.Int, 4, "TransactionRedemptionID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionVersusInstrument", System.Data.SqlDbType.Int, 4, "TransactionVersusInstrument"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionFeeID", System.Data.SqlDbType.Int, 4, "TransactionFeeID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionSpecificToFundUnitID", System.Data.SqlDbType.Int, 4, "TransactionSpecificToFundUnitID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionGroup", System.Data.SqlDbType.NVarChar, 50, "TransactionGroup"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionTradeStatusID", System.Data.SqlDbType.Int, 4, "TransactionTradeStatusID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionAdminStatus", System.Data.SqlDbType.Int, 4, "TransactionAdminStatus"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCTFLAGS", System.Data.SqlDbType.Int, 4, "TransactionCTFLAGS"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionCompoundRN", System.Data.SqlDbType.Int, 4, "TransactionCompoundRN2"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))
        UpdateCommand.Parameters.Add(New SqlParameter("@ValidationOnly", System.Data.SqlDbType.Int, 4))
        UpdateCommand.Parameters.Add(New SqlParameter("@rvStatusString", System.Data.SqlDbType.NVarChar, 200, System.Data.ParameterDirection.Output, True, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))


        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblTransaction_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@TransactionParentID", System.Data.SqlDbType.Int, 4, "TransactionParentID"))


      Case "TBLTRANSACTIONCONTRA" ' 
        ' Note : This retrieves only the First Leg of the Transaction.
        ' Second (Cash) legs are not included.

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblTransactionContra", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("TransactionID", "TransactionID"), _
        New System.Data.Common.DataColumnMapping("TransactionFund", "TransactionFund"), _
        New System.Data.Common.DataColumnMapping("TransactionSubFund", "TransactionSubFund"), _
        New System.Data.Common.DataColumnMapping("TransactionInstrument", "TransactionInstrument"), _
        New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
        New System.Data.Common.DataColumnMapping("TransactionType_Contra", "TransactionType_Contra"), _
        New System.Data.Common.DataColumnMapping("TransactionUnits", "TransactionUnits"), _
        New System.Data.Common.DataColumnMapping("TransactionSignedUnits", "TransactionSignedUnits"), _
        New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlement", "TransactionSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionSignedSettlement", "TransactionSignedSettlement"), _
        New System.Data.Common.DataColumnMapping("TransactionDataEntry", "TransactionDataEntry"), _
        New System.Data.Common.DataColumnMapping("TransactionDecisionDate", "TransactionDecisionDate"), _
        New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
        New System.Data.Common.DataColumnMapping("TransactionSettlementDate", "TransactionSettlementDate"), _
        New System.Data.Common.DataColumnMapping("TransactionConfirmationDate", "TransactionConfirmationDate"), _
        New System.Data.Common.DataColumnMapping("TransactionIsForwardPayment", "TransactionIsForwardPayment"), _
        New System.Data.Common.DataColumnMapping("TransactionParentID", "TransactionParentID"), _
        New System.Data.Common.DataColumnMapping("TransactionSpecificToFundUnitID", "TransactionSpecificToFundUnitID"), _
        New System.Data.Common.DataColumnMapping("TransactionLeg", "TransactionLeg"), _
        New System.Data.Common.DataColumnMapping("TransactionTradeStatusID", "TransactionTradeStatusID"), _
        New System.Data.Common.DataColumnMapping("TransactionAdminStatus", "TransactionAdminStatus"), _
        New System.Data.Common.DataColumnMapping("TransactionCTFLAGS", "TransactionCTFLAGS"), _
        New System.Data.Common.DataColumnMapping("TransactionCompoundRN", "TransactionCompoundRN") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblTransaction_ContraLeg_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@AuditID", System.Data.SqlDbType.Int, 4)).Value = 0
        SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

 
      Case "TBLTRANSACTIONTYPE"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblTransactionType", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("TransactionTypeID", "TransactionTypeID"), _
        New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
        New System.Data.Common.DataColumnMapping("TransactionTypeCashMove", "TransactionTypeCashMove"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblTransactionType_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblTransactionType_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionTypeID", System.Data.SqlDbType.Int, 4, "TransactionTypeID"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.NVarChar, 50, "TransactionType"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionTypeCashMove", System.Data.SqlDbType.Int, 4, "TransactionTypeCashMove"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblTransactionType_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionTypeID", System.Data.SqlDbType.Int, 4, "TransactionTypeID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.NVarChar, 50, "TransactionType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionTypeCashMove", System.Data.SqlDbType.Int, 4, "TransactionTypeCashMove"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblTransactionType_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@TransactionTypeID", System.Data.SqlDbType.Int, 4, "TransactionTypeID"))


      Case "TBLREFERENTIALINTEGRITY"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblReferentialIntegrity", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ReferentialID", "ReferentialID"), _
        New System.Data.Common.DataColumnMapping("TableName", "TableName"), _
        New System.Data.Common.DataColumnMapping("TableField", "TableField"), _
        New System.Data.Common.DataColumnMapping("FeedsTable", "FeedsTable"), _
        New System.Data.Common.DataColumnMapping("FeedsField", "FeedsField"), _
        New System.Data.Common.DataColumnMapping("DescriptionField", "DescriptionField"), _
        New System.Data.Common.DataColumnMapping("IsDescriptiveReferenceOnly", "IsDescriptiveReferenceOnly"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblReferentialIntegrity_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        ' Leave Update, Insert & Delete commands.

      Case "TBLUNITHOLDERFEES"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblUnitHolderFees", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FeeTransactionID", "FeeTransactionID"), _
        New System.Data.Common.DataColumnMapping("FeeTransactionParentID", "FeeTransactionParentID"), _
        New System.Data.Common.DataColumnMapping("FeeFund", "FeeFund"), _
        New System.Data.Common.DataColumnMapping("FundBaseCurrency", "FundBaseCurrency"), _
        New System.Data.Common.DataColumnMapping("FeeCounterparty", "FeeCounterparty"), _
        New System.Data.Common.DataColumnMapping("FeeInstrumentID", "FeeInstrumentID"), _
        New System.Data.Common.DataColumnMapping("FeeDate", "FeeDate"), _
        New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
        New System.Data.Common.DataColumnMapping("TransactionHolding", "TransactionHolding"), _
        New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
        New System.Data.Common.DataColumnMapping("InitialEqualisation", "InitialEqualisation"), _
        New System.Data.Common.DataColumnMapping("CurrentEqualisation", "CurrentEqualisation"), _
        New System.Data.Common.DataColumnMapping("ContingentRedemption", "ContingentRedemption"), _
        New System.Data.Common.DataColumnMapping("RedemptionRatio", "RedemptionRatio"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_Gross", "ManagementFees_Gross"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_Net", "ManagementFees_Net"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_DebtRebate", "ManagementFees_DebtRebate"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_EquityRebate", "ManagementFees_EquityRebate"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_MarketingFCAM", "ManagementFees_MarketingFCAM"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_MarketingOthers", "ManagementFees_MarketingOthers"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_AccruedGross", "PerformanceFees_AccruedGross"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_Gross", "PerformanceFees_Gross"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_Net", "PerformanceFees_Net"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_DebtRebate", "PerformanceFees_DebtRebate"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_EquityRebate", "PerformanceFees_EquityRebate"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_MarketingFCAM", "PerformanceFees_MarketingFCAM"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_MarketingOthers", "PerformanceFees_MarketingOthers"), _
        New System.Data.Common.DataColumnMapping("FXtoUSD", "FXtoUSD"), _
        New System.Data.Common.DataColumnMapping("GrossManagementFee_Percent", "GrossManagementFee_Percent"), _
        New System.Data.Common.DataColumnMapping("GrossPerformanceFee_Percent", "GrossPerformanceFee_Percent"), _
        New System.Data.Common.DataColumnMapping("PerformanceHurdle", "PerformanceHurdle"), _
        New System.Data.Common.DataColumnMapping("FeeTransactionWatermarkDate", "FeeTransactionWatermarkDate"), _
        New System.Data.Common.DataColumnMapping("FeeKnowledgedateUsed", "FeeKnowledgedateUsed"), _
        New System.Data.Common.DataColumnMapping("IsMilestone", "IsMilestone"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblUnitHolderFees_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblUnitHolderFees_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeTransactionID", System.Data.SqlDbType.Int, 4, "FeeTransactionID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeTransactionParentID", System.Data.SqlDbType.Int, 4, "FeeTransactionParentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeFund", System.Data.SqlDbType.Int, 4, "FeeFund"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundBaseCurrency", System.Data.SqlDbType.Int, 4, "FundBaseCurrency"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeCounterparty", System.Data.SqlDbType.Int, 4, "FeeCounterparty"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeInstrumentID", System.Data.SqlDbType.Int, 4, "FeeInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeDate", System.Data.SqlDbType.DateTime, 8, "FeeDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionHolding", System.Data.SqlDbType.Float, 8, "TransactionHolding"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@InitialEqualisation", System.Data.SqlDbType.Float, 8, "InitialEqualisation"))
        InsertCommand.Parameters.Add(New SqlParameter("@CurrentEqualisation", System.Data.SqlDbType.Float, 8, "CurrentEqualisation"))
        InsertCommand.Parameters.Add(New SqlParameter("@ContingentRedemption", System.Data.SqlDbType.Float, 8, "ContingentRedemption"))
        InsertCommand.Parameters.Add(New SqlParameter("@RedemptionRatio", System.Data.SqlDbType.Float, 8, "RedemptionRatio"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_Gross", System.Data.SqlDbType.Float, 8, "ManagementFees_Gross"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_Net", System.Data.SqlDbType.Float, 8, "ManagementFees_Net"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_DebtRebate", System.Data.SqlDbType.Float, 8, "ManagementFees_DebtRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_EquityRebate", System.Data.SqlDbType.Float, 8, "ManagementFees_EquityRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_MarketingFCAM", System.Data.SqlDbType.Float, 8, "ManagementFees_MarketingFCAM"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_MarketingOthers", System.Data.SqlDbType.Float, 8, "ManagementFees_MarketingOthers"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_AccruedGross", System.Data.SqlDbType.Float, 8, "PerformanceFees_AccruedGross"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_Gross", System.Data.SqlDbType.Float, 8, "PerformanceFees_Gross"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_Net", System.Data.SqlDbType.Float, 8, "PerformanceFees_Net"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_DebtRebate", System.Data.SqlDbType.Float, 8, "PerformanceFees_DebtRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_EquityRebate", System.Data.SqlDbType.Float, 8, "PerformanceFees_EquityRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_MarketingFCAM", System.Data.SqlDbType.Float, 8, "PerformanceFees_MarketingFCAM"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_MarketingOthers", System.Data.SqlDbType.Float, 8, "PerformanceFees_MarketingOthers"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXtoUSD", System.Data.SqlDbType.Float, 8, "FXtoUSD"))
        InsertCommand.Parameters.Add(New SqlParameter("@GrossManagementFee_Percent", System.Data.SqlDbType.Float, 8, "GrossManagementFee_Percent"))
        InsertCommand.Parameters.Add(New SqlParameter("@GrossPerformanceFee_Percent", System.Data.SqlDbType.Float, 8, "GrossPerformanceFee_Percent"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceHurdle", System.Data.SqlDbType.Float, 8, "PerformanceHurdle"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeTransactionWatermarkDate", System.Data.SqlDbType.DateTime, 8, "FeeTransactionWatermarkDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeKnowledgedateUsed", System.Data.SqlDbType.DateTime, 8, "FeeKnowledgedateUsed"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblUnitHolderFees_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeTransactionID", System.Data.SqlDbType.Int, 4, "FeeTransactionID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeTransactionParentID", System.Data.SqlDbType.Int, 4, "FeeTransactionParentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeFund", System.Data.SqlDbType.Int, 4, "FeeFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundBaseCurrency", System.Data.SqlDbType.Int, 4, "FundBaseCurrency"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeCounterparty", System.Data.SqlDbType.Int, 4, "FeeCounterparty"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeInstrumentID", System.Data.SqlDbType.Int, 4, "FeeInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeDate", System.Data.SqlDbType.DateTime, 8, "FeeDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionHolding", System.Data.SqlDbType.Float, 8, "TransactionHolding"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InitialEqualisation", System.Data.SqlDbType.Float, 8, "InitialEqualisation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CurrentEqualisation", System.Data.SqlDbType.Float, 8, "CurrentEqualisation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ContingentRedemption", System.Data.SqlDbType.Float, 8, "ContingentRedemption"))
        UpdateCommand.Parameters.Add(New SqlParameter("@RedemptionRatio", System.Data.SqlDbType.Float, 8, "RedemptionRatio"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_Gross", System.Data.SqlDbType.Float, 8, "ManagementFees_Gross"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_Net", System.Data.SqlDbType.Float, 8, "ManagementFees_Net"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_DebtRebate", System.Data.SqlDbType.Float, 8, "ManagementFees_DebtRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_EquityRebate", System.Data.SqlDbType.Float, 8, "ManagementFees_EquityRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_MarketingFCAM", System.Data.SqlDbType.Float, 8, "ManagementFees_MarketingFCAM"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_MarketingOthers", System.Data.SqlDbType.Float, 8, "ManagementFees_MarketingOthers"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_AccruedGross", System.Data.SqlDbType.Float, 8, "PerformanceFees_AccruedGross"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_Gross", System.Data.SqlDbType.Float, 8, "PerformanceFees_Gross"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_Net", System.Data.SqlDbType.Float, 8, "PerformanceFees_Net"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_DebtRebate", System.Data.SqlDbType.Float, 8, "PerformanceFees_DebtRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_EquityRebate", System.Data.SqlDbType.Float, 8, "PerformanceFees_EquityRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_MarketingFCAM", System.Data.SqlDbType.Float, 8, "PerformanceFees_MarketingFCAM"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_MarketingOthers", System.Data.SqlDbType.Float, 8, "PerformanceFees_MarketingOthers"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXtoUSD", System.Data.SqlDbType.Float, 8, "FXtoUSD"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GrossManagementFee_Percent", System.Data.SqlDbType.Float, 8, "GrossManagementFee_Percent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GrossPerformanceFee_Percent", System.Data.SqlDbType.Float, 8, "GrossPerformanceFee_Percent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceHurdle", System.Data.SqlDbType.Float, 8, "PerformanceHurdle"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeTransactionWatermarkDate", System.Data.SqlDbType.DateTime, 8, "FeeTransactionWatermarkDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeKnowledgedateUsed", System.Data.SqlDbType.DateTime, 8, "FeeKnowledgedateUsed"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblUnitHolderFees_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeFund", System.Data.SqlDbType.Int, 4, "FeeFund"))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeCounterparty", System.Data.SqlDbType.Int, 4, "FeeCounterparty"))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeInstrumentID", System.Data.SqlDbType.Int, 4, "FeeInstrumentID"))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeDate", System.Data.SqlDbType.DateTime, 8, "FeeDate"))


      Case "TBLUNITHOLDERFEES_NONMILESTONE" ' "TBLUNITHOLDERFEES_NOTMILESTONED"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblUnitHolderFees", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("FeeTransactionID", "FeeTransactionID"), _
        New System.Data.Common.DataColumnMapping("FeeTransactionParentID", "FeeTransactionParentID"), _
        New System.Data.Common.DataColumnMapping("FeeFund", "FeeFund"), _
        New System.Data.Common.DataColumnMapping("FundBaseCurrency", "FundBaseCurrency"), _
        New System.Data.Common.DataColumnMapping("FeeCounterparty", "FeeCounterparty"), _
        New System.Data.Common.DataColumnMapping("FeeInstrumentID", "FeeInstrumentID"), _
        New System.Data.Common.DataColumnMapping("FeeDate", "FeeDate"), _
        New System.Data.Common.DataColumnMapping("TransactionType", "TransactionType"), _
        New System.Data.Common.DataColumnMapping("TransactionHolding", "TransactionHolding"), _
        New System.Data.Common.DataColumnMapping("TransactionPrice", "TransactionPrice"), _
        New System.Data.Common.DataColumnMapping("TransactionValueDate", "TransactionValueDate"), _
        New System.Data.Common.DataColumnMapping("InitialEqualisation", "InitialEqualisation"), _
        New System.Data.Common.DataColumnMapping("CurrentEqualisation", "CurrentEqualisation"), _
        New System.Data.Common.DataColumnMapping("ContingentRedemption", "ContingentRedemption"), _
        New System.Data.Common.DataColumnMapping("RedemptionRatio", "RedemptionRatio"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_Gross", "ManagementFees_Gross"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_Net", "ManagementFees_Net"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_DebtRebate", "ManagementFees_DebtRebate"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_EquityRebate", "ManagementFees_EquityRebate"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_MarketingFCAM", "ManagementFees_MarketingFCAM"), _
        New System.Data.Common.DataColumnMapping("ManagementFees_MarketingOthers", "ManagementFees_MarketingOthers"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_AccruedGross", "PerformanceFees_AccruedGross"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_Gross", "PerformanceFees_Gross"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_Net", "PerformanceFees_Net"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_DebtRebate", "PerformanceFees_DebtRebate"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_EquityRebate", "PerformanceFees_EquityRebate"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_MarketingFCAM", "PerformanceFees_MarketingFCAM"), _
        New System.Data.Common.DataColumnMapping("PerformanceFees_MarketingOthers", "PerformanceFees_MarketingOthers"), _
        New System.Data.Common.DataColumnMapping("FXtoUSD", "FXtoUSD"), _
        New System.Data.Common.DataColumnMapping("GrossManagementFee_Percent", "GrossManagementFee_Percent"), _
        New System.Data.Common.DataColumnMapping("GrossPerformanceFee_Percent", "GrossPerformanceFee_Percent"), _
        New System.Data.Common.DataColumnMapping("PerformanceHurdle", "PerformanceHurdle"), _
        New System.Data.Common.DataColumnMapping("FeeTransactionWatermarkDate", "FeeTransactionWatermarkDate"), _
        New System.Data.Common.DataColumnMapping("FeeKnowledgedateUsed", "FeeKnowledgedateUsed"), _
        New System.Data.Common.DataColumnMapping("IsMilestone", "IsMilestone"), _
        New System.Data.Common.DataColumnMapping("UserEntered", "UserEntered"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblUnitHolderFees_NonMilestone_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblUnitHolderFees_NonMilestone_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeTransactionID", System.Data.SqlDbType.Int, 4, "FeeTransactionID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeTransactionParentID", System.Data.SqlDbType.Int, 4, "FeeTransactionParentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeFund", System.Data.SqlDbType.Int, 4, "FeeFund"))
        InsertCommand.Parameters.Add(New SqlParameter("@FundBaseCurrency", System.Data.SqlDbType.Int, 4, "FundBaseCurrency"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeCounterparty", System.Data.SqlDbType.Int, 4, "FeeCounterparty"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeInstrumentID", System.Data.SqlDbType.Int, 4, "FeeInstrumentID"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeDate", System.Data.SqlDbType.DateTime, 8, "FeeDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionHolding", System.Data.SqlDbType.Float, 8, "TransactionHolding"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
        InsertCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@InitialEqualisation", System.Data.SqlDbType.Float, 8, "InitialEqualisation"))
        InsertCommand.Parameters.Add(New SqlParameter("@CurrentEqualisation", System.Data.SqlDbType.Float, 8, "CurrentEqualisation"))
        InsertCommand.Parameters.Add(New SqlParameter("@ContingentRedemption", System.Data.SqlDbType.Float, 8, "ContingentRedemption"))
        InsertCommand.Parameters.Add(New SqlParameter("@RedemptionRatio", System.Data.SqlDbType.Float, 8, "RedemptionRatio"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_Gross", System.Data.SqlDbType.Float, 8, "ManagementFees_Gross"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_Net", System.Data.SqlDbType.Float, 8, "ManagementFees_Net"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_DebtRebate", System.Data.SqlDbType.Float, 8, "ManagementFees_DebtRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_EquityRebate", System.Data.SqlDbType.Float, 8, "ManagementFees_EquityRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_MarketingFCAM", System.Data.SqlDbType.Float, 8, "ManagementFees_MarketingFCAM"))
        InsertCommand.Parameters.Add(New SqlParameter("@ManagementFees_MarketingOthers", System.Data.SqlDbType.Float, 8, "ManagementFees_MarketingOthers"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_AccruedGross", System.Data.SqlDbType.Float, 8, "PerformanceFees_AccruedGross"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_Gross", System.Data.SqlDbType.Float, 8, "PerformanceFees_Gross"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_Net", System.Data.SqlDbType.Float, 8, "PerformanceFees_Net"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_DebtRebate", System.Data.SqlDbType.Float, 8, "PerformanceFees_DebtRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_EquityRebate", System.Data.SqlDbType.Float, 8, "PerformanceFees_EquityRebate"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_MarketingFCAM", System.Data.SqlDbType.Float, 8, "PerformanceFees_MarketingFCAM"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceFees_MarketingOthers", System.Data.SqlDbType.Float, 8, "PerformanceFees_MarketingOthers"))
        InsertCommand.Parameters.Add(New SqlParameter("@FXtoUSD", System.Data.SqlDbType.Float, 8, "FXtoUSD"))
        InsertCommand.Parameters.Add(New SqlParameter("@GrossManagementFee_Percent", System.Data.SqlDbType.Float, 8, "GrossManagementFee_Percent"))
        InsertCommand.Parameters.Add(New SqlParameter("@GrossPerformanceFee_Percent", System.Data.SqlDbType.Float, 8, "GrossPerformanceFee_Percent"))
        InsertCommand.Parameters.Add(New SqlParameter("@PerformanceHurdle", System.Data.SqlDbType.Float, 8, "PerformanceHurdle"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeTransactionWatermarkDate", System.Data.SqlDbType.DateTime, 8, "FeeTransactionWatermarkDate"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeeKnowledgedateUsed", System.Data.SqlDbType.DateTime, 8, "FeeKnowledgedateUsed"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblUnitHolderFees_NonMilestone_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeTransactionID", System.Data.SqlDbType.Int, 4, "FeeTransactionID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeTransactionParentID", System.Data.SqlDbType.Int, 4, "FeeTransactionParentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeFund", System.Data.SqlDbType.Int, 4, "FeeFund"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FundBaseCurrency", System.Data.SqlDbType.Int, 4, "FundBaseCurrency"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeCounterparty", System.Data.SqlDbType.Int, 4, "FeeCounterparty"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeInstrumentID", System.Data.SqlDbType.Int, 4, "FeeInstrumentID"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeDate", System.Data.SqlDbType.DateTime, 8, "FeeDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionType", System.Data.SqlDbType.Int, 4, "TransactionType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionHolding", System.Data.SqlDbType.Float, 8, "TransactionHolding"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionPrice", System.Data.SqlDbType.Float, 8, "TransactionPrice"))
        UpdateCommand.Parameters.Add(New SqlParameter("@TransactionValueDate", System.Data.SqlDbType.DateTime, 8, "TransactionValueDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@InitialEqualisation", System.Data.SqlDbType.Float, 8, "InitialEqualisation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@CurrentEqualisation", System.Data.SqlDbType.Float, 8, "CurrentEqualisation"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ContingentRedemption", System.Data.SqlDbType.Float, 8, "ContingentRedemption"))
        UpdateCommand.Parameters.Add(New SqlParameter("@RedemptionRatio", System.Data.SqlDbType.Float, 8, "RedemptionRatio"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_Gross", System.Data.SqlDbType.Float, 8, "ManagementFees_Gross"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_Net", System.Data.SqlDbType.Float, 8, "ManagementFees_Net"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_DebtRebate", System.Data.SqlDbType.Float, 8, "ManagementFees_DebtRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_EquityRebate", System.Data.SqlDbType.Float, 8, "ManagementFees_EquityRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_MarketingFCAM", System.Data.SqlDbType.Float, 8, "ManagementFees_MarketingFCAM"))
        UpdateCommand.Parameters.Add(New SqlParameter("@ManagementFees_MarketingOthers", System.Data.SqlDbType.Float, 8, "ManagementFees_MarketingOthers"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_AccruedGross", System.Data.SqlDbType.Float, 8, "PerformanceFees_AccruedGross"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_Gross", System.Data.SqlDbType.Float, 8, "PerformanceFees_Gross"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_Net", System.Data.SqlDbType.Float, 8, "PerformanceFees_Net"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_DebtRebate", System.Data.SqlDbType.Float, 8, "PerformanceFees_DebtRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_EquityRebate", System.Data.SqlDbType.Float, 8, "PerformanceFees_EquityRebate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_MarketingFCAM", System.Data.SqlDbType.Float, 8, "PerformanceFees_MarketingFCAM"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceFees_MarketingOthers", System.Data.SqlDbType.Float, 8, "PerformanceFees_MarketingOthers"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FXtoUSD", System.Data.SqlDbType.Float, 8, "FXtoUSD"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GrossManagementFee_Percent", System.Data.SqlDbType.Float, 8, "GrossManagementFee_Percent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@GrossPerformanceFee_Percent", System.Data.SqlDbType.Float, 8, "GrossPerformanceFee_Percent"))
        UpdateCommand.Parameters.Add(New SqlParameter("@PerformanceHurdle", System.Data.SqlDbType.Float, 8, "PerformanceHurdle"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeTransactionWatermarkDate", System.Data.SqlDbType.DateTime, 8, "FeeTransactionWatermarkDate"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeeKnowledgedateUsed", System.Data.SqlDbType.DateTime, 8, "FeeKnowledgedateUsed"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblUnitHolderFees_NonMilestone_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeFund", System.Data.SqlDbType.Int, 4, "FeeFund"))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeCounterparty", System.Data.SqlDbType.Int, 4, "FeeCounterparty"))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeInstrumentID", System.Data.SqlDbType.Int, 4, "FeeInstrumentID"))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeeDate", System.Data.SqlDbType.DateTime, 8, "FeeDate"))


      Case "TBLUSERPERMISSIONS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblUserPermissions", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ID", "ID"), _
        New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
        New System.Data.Common.DataColumnMapping("UserType", "UserType"), _
        New System.Data.Common.DataColumnMapping("Feature", "Feature"), _
        New System.Data.Common.DataColumnMapping("FeatureType", "FeatureType"), _
        New System.Data.Common.DataColumnMapping("Perm_Read", "Perm_Read"), _
        New System.Data.Common.DataColumnMapping("Perm_Insert", "Perm_Insert"), _
        New System.Data.Common.DataColumnMapping("Perm_Update", "Perm_Update"), _
        New System.Data.Common.DataColumnMapping("Perm_Delete", "Perm_Delete"), _
        New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered"), _
        New System.Data.Common.DataColumnMapping("DateDeleted", "DateDeleted") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblUserPermissions_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

        'SqlInsertCommand
        '
        InsertCommand.CommandText = "[adp_tblUserPermissions_InsertCommand]"
        InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
        InsertCommand.Connection = pConnection
        InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        InsertCommand.Parameters.Add(New SqlParameter("@UserName", System.Data.SqlDbType.NVarChar, 50, "UserName"))
        InsertCommand.Parameters.Add(New SqlParameter("@UserType", System.Data.SqlDbType.Int, 4, "UserType"))
        InsertCommand.Parameters.Add(New SqlParameter("@Feature", System.Data.SqlDbType.NVarChar, 50, "Feature"))
        InsertCommand.Parameters.Add(New SqlParameter("@FeatureType", System.Data.SqlDbType.Int, 4, "FeatureType"))
        InsertCommand.Parameters.Add(New SqlParameter("@Perm_Read", System.Data.SqlDbType.Int, 4, "Perm_Read"))
        InsertCommand.Parameters.Add(New SqlParameter("@Perm_Insert", System.Data.SqlDbType.Int, 4, "Perm_Insert"))
        InsertCommand.Parameters.Add(New SqlParameter("@Perm_Update", System.Data.SqlDbType.Int, 4, "Perm_Update"))
        InsertCommand.Parameters.Add(New SqlParameter("@Perm_Delete", System.Data.SqlDbType.Int, 4, "Perm_Delete"))
        InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlUpdateCommand
        '
        UpdateCommand.CommandText = "[adp_tblUserPermissions_UpdateCommand]"
        UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
        UpdateCommand.Connection = pConnection
        UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        UpdateCommand.Parameters.Add(New SqlParameter("@UserName", System.Data.SqlDbType.NVarChar, 50, "UserName"))
        UpdateCommand.Parameters.Add(New SqlParameter("@UserType", System.Data.SqlDbType.Int, 4, "UserType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Feature", System.Data.SqlDbType.NVarChar, 50, "Feature"))
        UpdateCommand.Parameters.Add(New SqlParameter("@FeatureType", System.Data.SqlDbType.Int, 4, "FeatureType"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Perm_Read", System.Data.SqlDbType.Int, 4, "Perm_Read"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Perm_Insert", System.Data.SqlDbType.Int, 4, "Perm_Insert"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Perm_Update", System.Data.SqlDbType.Int, 4, "Perm_Update"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Perm_Delete", System.Data.SqlDbType.Int, 4, "Perm_Delete"))
        UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

        'SqlDeleteCommand
        '
        DeleteCommand.CommandText = "[adp_tblUserPermissions_DeleteCommand]"
        DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
        DeleteCommand.Connection = pConnection
        DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        DeleteCommand.Parameters.Add(New SqlParameter("@UserName", System.Data.SqlDbType.NVarChar, 50, "UserName"))
        DeleteCommand.Parameters.Add(New SqlParameter("@UserType", System.Data.SqlDbType.Int, 4, "UserType"))
        DeleteCommand.Parameters.Add(New SqlParameter("@Feature", System.Data.SqlDbType.NVarChar, 50, "Feature"))
        DeleteCommand.Parameters.Add(New SqlParameter("@FeatureType", System.Data.SqlDbType.Int, 4, "FeatureType"))
        DeleteCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))


      Case "TBLVENICEFORMTOOLTIPS"

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.AddRange( _
        New System.Data.Common.DataTableMapping() { _
        New System.Data.Common.DataTableMapping("Table", "tblVeniceFormTooltips", _
        New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
        New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
        New System.Data.Common.DataColumnMapping("ReferentialID", "ReferentialID"), _
        New System.Data.Common.DataColumnMapping("FormName", "FormName"), _
        New System.Data.Common.DataColumnMapping("ControlName", "ControlName"), _
        New System.Data.Common.DataColumnMapping("ToolTip", "ToolTip") _
        })})

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblVeniceFormTooltips_SelectCommand]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime)).Value = #1/1/1900# ' This table is not KnowledgeDated, The parameter is only here for consistency.

        ' Leave Update, Insert & Delete commands.


			Case "TBLWORKFLOWRULES"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblWorkflowRules", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("RuleID", "RuleID"), _
				New System.Data.Common.DataColumnMapping("Fund", "Fund"), _
				New System.Data.Common.DataColumnMapping("InstrumentType", "InstrumentType"), _
				New System.Data.Common.DataColumnMapping("Exchange", "Exchange"), _
				New System.Data.Common.DataColumnMapping("Currency", "Currency"), _
				New System.Data.Common.DataColumnMapping("InstrumentID", "InstrumentID"), _
				New System.Data.Common.DataColumnMapping("CurrentStatus", "CurrentStatus"), _
				New System.Data.Common.DataColumnMapping("RequiresApproval", "RequiresApproval"), _
				New System.Data.Common.DataColumnMapping("IsApprovalRule", "IsApprovalRule"), _
				New System.Data.Common.DataColumnMapping("IsNewTrade", "IsNewTrade"), _
				New System.Data.Common.DataColumnMapping("ResultingWorkflow", "ResultingWorkflow"), _
				New System.Data.Common.DataColumnMapping("WorkflowPriority", "WorkflowPriority"), _
				New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblWorkflowRules_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblWorkflowRules_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@RuleID", System.Data.SqlDbType.Int, 4, "RuleID"))
				InsertCommand.Parameters.Add(New SqlParameter("@Fund", System.Data.SqlDbType.Int, 4, "Fund"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentType", System.Data.SqlDbType.Int, 4, "InstrumentType"))
				InsertCommand.Parameters.Add(New SqlParameter("@Exchange", System.Data.SqlDbType.Int, 4, "Exchange"))
				InsertCommand.Parameters.Add(New SqlParameter("@Currency", System.Data.SqlDbType.Int, 4, "Currency"))
				InsertCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				InsertCommand.Parameters.Add(New SqlParameter("@CurrentStatus", System.Data.SqlDbType.Int, 4, "CurrentStatus"))
				InsertCommand.Parameters.Add(New SqlParameter("@RequiresApproval", System.Data.SqlDbType.Int, 4, "RequiresApproval"))
				InsertCommand.Parameters.Add(New SqlParameter("@IsApprovalRule", System.Data.SqlDbType.Int, 4, "IsApprovalRule"))
				InsertCommand.Parameters.Add(New SqlParameter("@IsNewTrade", System.Data.SqlDbType.Int, 4, "IsNewTrade"))
				InsertCommand.Parameters.Add(New SqlParameter("@ResultingWorkflow", System.Data.SqlDbType.Int, 4, "ResultingWorkflow"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand

				UpdateCommand.CommandText = "[adp_tblWorkflowRules_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@RuleID", System.Data.SqlDbType.Int, 4, "RuleID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Fund", System.Data.SqlDbType.Int, 4, "Fund"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentType", System.Data.SqlDbType.Int, 4, "InstrumentType"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Exchange", System.Data.SqlDbType.Int, 4, "Exchange"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Currency", System.Data.SqlDbType.Int, 4, "Currency"))
				UpdateCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4, "InstrumentID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@CurrentStatus", System.Data.SqlDbType.Int, 4, "CurrentStatus"))
				UpdateCommand.Parameters.Add(New SqlParameter("@RequiresApproval", System.Data.SqlDbType.Int, 4, "RequiresApproval"))
				UpdateCommand.Parameters.Add(New SqlParameter("@IsApprovalRule", System.Data.SqlDbType.Int, 4, "IsApprovalRule"))
				UpdateCommand.Parameters.Add(New SqlParameter("@IsNewTrade", System.Data.SqlDbType.Int, 4, "IsNewTrade"))
				UpdateCommand.Parameters.Add(New SqlParameter("@ResultingWorkflow", System.Data.SqlDbType.Int, 4, "ResultingWorkflow"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblWorkflowRules_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@RuleID", System.Data.SqlDbType.Int, 4, "RuleID"))


			Case "TBLWORKFLOWS"

				pAdaptor.TableMappings.Clear()
				pAdaptor.TableMappings.AddRange( _
				New System.Data.Common.DataTableMapping() { _
				New System.Data.Common.DataTableMapping("Table", "tblWorkflows", _
				New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("RN", "RN"), _
				New System.Data.Common.DataColumnMapping("AuditID", "AuditID"), _
				New System.Data.Common.DataColumnMapping("WorkflowID", "WorkflowID"), _
				New System.Data.Common.DataColumnMapping("WorkflowName", "WorkflowName"), _
				New System.Data.Common.DataColumnMapping("WorkflowDescription", "WorkflowDescription"), _
				New System.Data.Common.DataColumnMapping("WorkflowDetails", "WorkflowDetails"), _
				New System.Data.Common.DataColumnMapping("CreateTradeFile", "CreateTradeFile"), _
				New System.Data.Common.DataColumnMapping("ResultingTradeStatus", "ResultingTradeStatus"), _
				New System.Data.Common.DataColumnMapping("UserName", "UserName"), _
				New System.Data.Common.DataColumnMapping("DateEntered", "DateEntered") _
				})})

				' Set Select Command
				SelectCommand.CommandText = "[adp_tblWorkflows_SelectCommand]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add( _
				New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

				'SqlInsertCommand
				'
				InsertCommand.CommandText = "[adp_tblWorkflows_InsertCommand]"
				InsertCommand.CommandType = System.Data.CommandType.StoredProcedure
				InsertCommand.Connection = pConnection
				InsertCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				InsertCommand.Parameters.Add(New SqlParameter("@WorkflowID", System.Data.SqlDbType.Int, 4, "WorkflowID"))
				InsertCommand.Parameters.Add(New SqlParameter("@WorkflowName", System.Data.SqlDbType.NVarChar, 50, "WorkflowName"))
				InsertCommand.Parameters.Add(New SqlParameter("@WorkflowDescription", System.Data.SqlDbType.NVarChar, 255, "WorkflowDescription"))
				InsertCommand.Parameters.Add(New SqlParameter("@WorkflowDetails", System.Data.SqlDbType.Int, 4, "WorkflowDetails"))
				InsertCommand.Parameters.Add(New SqlParameter("@CreateTradeFile", System.Data.SqlDbType.NVarChar, 50, "CreateTradeFile"))
				InsertCommand.Parameters.Add(New SqlParameter("@ResultingTradeStatus", System.Data.SqlDbType.Int, 4, "ResultingTradeStatus"))
				InsertCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlUpdateCommand

				UpdateCommand.CommandText = "[adp_tblWorkflows_UpdateCommand]"
				UpdateCommand.CommandType = System.Data.CommandType.StoredProcedure
				UpdateCommand.Connection = pConnection
				UpdateCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				UpdateCommand.Parameters.Add(New SqlParameter("@WorkflowID", System.Data.SqlDbType.Int, 4, "WorkflowID"))
				UpdateCommand.Parameters.Add(New SqlParameter("@WorkflowName", System.Data.SqlDbType.NVarChar, 50, "WorkflowName"))
				UpdateCommand.Parameters.Add(New SqlParameter("@WorkflowDescription", System.Data.SqlDbType.NVarChar, 255, "WorkflowDescription"))
				UpdateCommand.Parameters.Add(New SqlParameter("@WorkflowDetails", System.Data.SqlDbType.Int, 4, "WorkflowDetails"))
				UpdateCommand.Parameters.Add(New SqlParameter("@CreateTradeFile", System.Data.SqlDbType.NVarChar, 50, "CreateTradeFile"))
				UpdateCommand.Parameters.Add(New SqlParameter("@ResultingTradeStatus", System.Data.SqlDbType.Int, 4, "ResultingTradeStatus"))
				UpdateCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime, 8))

				'SqlDeleteCommand
				'
				DeleteCommand.CommandText = "[adp_tblWorkflows_DeleteCommand]"
				DeleteCommand.CommandType = System.Data.CommandType.StoredProcedure
				DeleteCommand.Connection = pConnection
				DeleteCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				DeleteCommand.Parameters.Add(New SqlParameter("@WorkflowID", System.Data.SqlDbType.Int, 4, "WorkflowID"))



				' *************************************************************************************
				' *************************************************************************************
				'
				' Adaptors used for Report Stored Procedures
				'
				' *************************************************************************************
				' *************************************************************************************

      Case "RPTATTRIBUTION", "RPTSECTORBIPS", "RPTATTRIBUTIONMONEY", "RPTATTRIBUTIONBESTWORST", "RPTPNLDETAILREPORT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_ViewAttributions]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@AttribPeriod", System.Data.SqlDbType.Int, 4)).Value = 4 ' Default to Monthly
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 200))
        SelectCommand.Parameters.Add(New SqlParameter("@AttributionYear", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@MaxAttributionDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@RecordPreference", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LookthroughAttributions", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeAttributionTotals", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParentID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "RPTFUNDPERFORMANCEREPORT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_ViewAttributions_AggregatedInfo]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@AttribPeriod", System.Data.SqlDbType.Int, 4)).Value = 4 ' Default to Monthly
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 200))
        SelectCommand.Parameters.Add(New SqlParameter("@AttributionYear", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@MaxAttributionDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@RecordPreference", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LookthroughAttributions", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeAttributionTotals", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParentID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "RPT_VENICE_ASSETSUNDERMANAGEMENT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Venice_AssetsUnderManagement]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeBorrowing", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeInternalSubscriptions", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@PriceVariant", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@UseMilestoneFundValues", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPT_VENICE_ASSETSUNDERMANAGEMENTDETAIL"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Venice_AssetsUnderManagementDetail]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeBorrowing", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeInternalSubscriptions", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@PriceVariant", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@UseMilestoneFundValues", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPT_VENICE_ASSETSUNDERMANAGEMENTACCOUNTINGDETAIL"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Venice_AssetsUnderManagement_Accounting_Detail]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeBorrowing", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeInternalSubscriptions", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@PriceVariant", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@UseMilestoneFundValues", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@ReferenceCurrency", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPT_VENICE_ASSETSUNDERMANAGEMENT_NETFLOWS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Venice_AssetsUnderManagement_NetFlows]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueYear", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeBorrowing", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPT_VENICE_FUNDGEARING"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Venice_FundGearing]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "RPT_VENICE_TRANSACTIONSHAVINGSTATUS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Venice_TransactionsHavingStatus]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@StartDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@EndDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@TradeStatus", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTAVAILABLECASH"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptAvailableCash]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 200))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@SettlementDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@DetailDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@InstrumentType", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@FundType", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "RPTAVAILABLECASHPLUS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptAvailableCashPlus]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 100))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@SettlementDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@DetailDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@PriceAndFXDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@NowDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@InstrumentType", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@FundType", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTBASICEQUALISATION"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_BasicEqualisationCalcs]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@CounterpartyID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@InvestorGroup", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTBENCHMARKATTRIBUTION"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_rptBenchmarkAttribution]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IndexTicker", System.Data.SqlDbType.NVarChar, 50))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@AttribPeriod", System.Data.SqlDbType.Int, 4)).Value = 4 ' Default to Monthly
        SelectCommand.Parameters.Add(New SqlParameter("@LookthroughAttributions", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "TBLBESTCURRENTINDEXMAPPING"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblIndexMapping_BestCurrentMapping]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@IndexTicker", System.Data.SqlDbType.NVarChar, 50))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "TBLCURRENTPORTFOLIODATA"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[adp_tblPortfolioData_CurrentPortfolio]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@IndexTicker", System.Data.SqlDbType.NVarChar, 50))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTCASHMOVE"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_CashMove]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDateTo", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateTo", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTEXPENSEREPORT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptExpenseReport]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTMANAGEMENTFEESREPORT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Venice_ManagementFeesByMonth]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int))
        SelectCommand.Parameters.Add(New SqlParameter("@Year", System.Data.SqlDbType.Int))
        SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
        SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int))
        SelectCommand.Parameters.Add(New SqlParameter("@ReportingCurrencyID", System.Data.SqlDbType.Int))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime))

        ' Leave Update, Insert & Delete commands.


      Case "RPTFUNDFEERECORDS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rpt_Venice_FundFeeRecords]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@CounterpartyID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@InvestorGroup", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@FeeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ReferenceCurrency", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTFUNDVALUATION"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptFundValuation]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 100))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParentID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTLIQUIDITYREPORT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptLiquidityReport]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTMASTERFUNDALLOCATION"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_MasterFundAllocation]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@MasterFundID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@ReportDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateTo", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "RPTPFPCPORTFOLIORECONCILLIATION"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_PFPCPortfolioReconcilliation]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 100))
        SelectCommand.Parameters.Add(New SqlParameter("@ReportDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "RPTPRICES"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptPrices]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@InstrumentID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@DateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@DateTo", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTFXS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptFXs]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@CurrencyID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@DateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@DateTo", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTPROFITANDLOSS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptProfitAndLoss]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDateTo", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@UnitPriceVariant", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@DisregardWatermarkPoints", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IsLookthrough", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@MaintainConsistentFundPnLs", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateTo", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParent", System.Data.SqlDbType.Int, 4))

        ' Leave Update, Insert & Delete commands.


      Case "RPTRISKEXPOSURESREPORT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptRiskExposuresReport]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@IsDetailRow", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@ExposureDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureLimitCategory", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureLimitInstrument", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureLimitMinUsage", System.Data.SqlDbType.Float, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureFlags", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureKnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        'SelectCommand.Parameters.Add(New SqlParameter("@SystemKnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "RPTRISKEXPOSURESCHANGEREPORT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptRiskExposuresChangeReport]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@FromExposureDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@FromExposureFlags", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@FromExposureKnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@FromSystemKnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ToExposureDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ToExposureFlags", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ToExposureKnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ToSystemKnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureLimitCategory", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureLimitInstrument", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureLimitMinUsage", System.Data.SqlDbType.Float, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTRISKPOLICY"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptRiskPolicy]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTSECTORRETURNS", "RPTSECTORWEIGHTS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_ViewAttributionsBySectorReturns]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@AttribPeriod", System.Data.SqlDbType.Int, 4)).Value = 4 ' Default to monthly
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 200))
        SelectCommand.Parameters.Add(New SqlParameter("@AttributionYear", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@MaxAttributionDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@RecordPreference", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LookthroughAttributions", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@IncludeAttributionTotals", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParentID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "RPTTRANSACTIONPRICEDIFFERENCE"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptTransactionPriceDifference]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@OnlyFinalPrices", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTUNITHOLDERPROFIT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptUnitHolderProfit]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@CounterpartyID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@InvestorGroup", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTUNITHOLDERPOSITIONS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptUnitHolderPositions]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@CounterpartyID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@InvestorGroup", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "RPTUNITPRICE"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[rptUnitPrice]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.



			Case "RPT_VENICE_RECONCILEVALUATION"

				pAdaptor.TableMappings.Clear()

				' Set Select Command
				SelectCommand.CommandText = "[rpt_Venice_ReconcileValuation]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

				SelectCommand.Connection = pConnection
				SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 100))
				SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@UnitPriceVariant", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@DisregardWatermarkPoints", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParentID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@MaxCaptureID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@UnRealisedCalculationType", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

				' Leave Update, Insert & Delete commands.






				' *************************************************************************************
				' *************************************************************************************
				'
				' Adaptors used for Other. Stored Procedures
				'
				' *************************************************************************************
				' *************************************************************************************

      Case "SPU_CHECKRISKEXPOSURESARECURRENT"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_CheckRiskExposuresAreCurrent]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection

        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@IsDetailRow", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@ExposureDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureFlags", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ExposureKnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "SPU_CHECKINSTRUMENTPRICES"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_CheckInstrumentPrices]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection

        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 100))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "SPU_PROFITANDLOSS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_ProfitAndLoss]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDateTo", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@UnitPriceVariant", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@DisregardWatermarkPoints", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateTo", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParent", System.Data.SqlDbType.Int, 4))

        ' Leave Update, Insert & Delete commands.


      Case "SPU_RISKINSTRUMENTEXPOSURES"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_RiskInstrumentExposures]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection

        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDateTo", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@UnitPriceVariant", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@DisregardWatermarkPoints", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateFrom", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateTo", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLinkedTables", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDateLimitsTable", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParentID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@FundLookthrough", System.Data.SqlDbType.Int, 4))

        ' Leave Update, Insert & Delete commands.

      Case "SPU_UNITPRICE"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_UnitPrice]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection

        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@UnitPriceVariant", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@DisregardWatermarkPoints", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

      Case "SPU_VALUATION"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_Valuation]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection

        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@LegalEntity", System.Data.SqlDbType.NVarChar, 100))
        SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@UnitPriceVariant", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@DisregardWatermarkPoints", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@AggregateToInstrumentParentID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@ShowDetailedTransactions", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@DetailDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.

			Case "SPU_VALUATION_ALL"

				pAdaptor.TableMappings.Clear()

				' Set Select Command
				SelectCommand.CommandText = "[spu_Valuation_All]"
				SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
				SelectCommand.Connection = pConnection

				SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@FundID", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@ValueDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@PreviousYearEndDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@StatusGroupFilter", System.Data.SqlDbType.NVarChar, 50))
				SelectCommand.Parameters.Add(New SqlParameter("@AdministratorDatesFilter", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@ExSubscriptionsAfterDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@ExFundExpensesAfterDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@ExManagementFeesAfterDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@ExPerformanceFeesAfterDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@ExCrystalisedPerformanceFeesAfterDate", System.Data.SqlDbType.DateTime, 8))
				SelectCommand.Parameters.Add(New SqlParameter("@UseAdministratorPriceDates", System.Data.SqlDbType.Int, 4))
				SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

				' Leave Update, Insert & Delete commands.



      Case "SPU_VIEWPORTFOLIOS"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_ViewPortfolios]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection

        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@PortfolioID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@KnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case "SPU_VIEWPORTFOLIOCHANGE"

        pAdaptor.TableMappings.Clear()

        ' Set Select Command
        SelectCommand.CommandText = "[spu_ViewPortfolioChange]"
        SelectCommand.CommandType = System.Data.CommandType.StoredProcedure
        SelectCommand.Connection = pConnection

        SelectCommand.Parameters.Add(New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add(New SqlParameter("@StartingPortfolioID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@EndingPortfolioID", System.Data.SqlDbType.Int, 4))
        SelectCommand.Parameters.Add(New SqlParameter("@StartingKnowledgeDate", System.Data.SqlDbType.DateTime, 8))
        SelectCommand.Parameters.Add(New SqlParameter("@EndingKnowledgeDate", System.Data.SqlDbType.DateTime, 8))

        ' Leave Update, Insert & Delete commands.


      Case Else
        ' Basic Default Select Adaptor.
        ' not recommended

        pAdaptor.TableMappings.Clear()
        pAdaptor.TableMappings.Clear()

        SelectCommand.CommandText = "SELECT * FROM fn_" & pTableName & "_SelectKD(@Knowledgedate)"
        SelectCommand.CommandType = System.Data.CommandType.Text

        SelectCommand.Connection = pConnection
        SelectCommand.Parameters.Add( _
        New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
        SelectCommand.Parameters.Add( _
        New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime))

    End Select

    pAdaptor.SelectCommand = SelectCommand
    pAdaptor.InsertCommand = InsertCommand
    pAdaptor.UpdateCommand = UpdateCommand
    pAdaptor.DeleteCommand = DeleteCommand

    Return True

  End Function

End Class
