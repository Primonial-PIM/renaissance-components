Imports System.Data.SqlClient
Imports System.Collections.Hashtable

Partial Public Class DataHandler
	Private dhAdaptorHandler As New AdaptorHandler

	Public DBConnections As New Hashtable
	Public Adaptors As New Hashtable
	Public Datasets As New Hashtable

	Private _ErrorMessage As String

	Public ReadOnly Property ErrorMessage() As String
		Get
			Return _ErrorMessage
		End Get
	End Property


#Region " AdaptorUpdate"

	Public Function AdaptorUpdate(ByRef pAdaptor As SqlDataAdapter, ByRef pTable As DataTable) As Integer
		Dim ThisConnection As SqlConnection
    Dim RVal As Integer = 0

		If (Not (pAdaptor.SelectCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.SelectCommand.Connection
		ElseIf (Not (pAdaptor.InsertCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.InsertCommand.Connection
		ElseIf (Not (pAdaptor.UpdateCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.UpdateCommand.Connection
		ElseIf (Not (pAdaptor.DeleteCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.DeleteCommand.Connection
		Else
			Return 0
		End If

		SyncLock ThisConnection
      SyncLock pTable
        Try
          ' OK, Update and Return.

          RVal = pAdaptor.Update(pTable)
          Return RVal

        Catch ex As Exception
          ' If an error occurred then clear and re-open the connection.

          If (ThisConnection IsNot Nothing) Then
            ThisConnection.Close()
            SqlConnection.ClearPool(ThisConnection)
            ThisConnection.Open()
          End If
        End Try

        ' Try again, without catching the error a second time.
        Return pAdaptor.Update(pTable)

      End SyncLock
		End SyncLock
	End Function

	Public Function AdaptorUpdate(ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet) As Integer
		Dim ThisConnection As SqlConnection
    Dim RVal As Integer = 0

		If (Not (pAdaptor.SelectCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.SelectCommand.Connection
		ElseIf (Not (pAdaptor.InsertCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.InsertCommand.Connection
		ElseIf (Not (pAdaptor.UpdateCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.UpdateCommand.Connection
		ElseIf (Not (pAdaptor.DeleteCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.DeleteCommand.Connection
		Else
			Return 0
		End If

		SyncLock ThisConnection
      SyncLock pDataSet
        Try
          ' OK, Update and Return.

          RVal = pAdaptor.Update(pDataSet)
          Return RVal

        Catch ex As Exception
          ' If an error occurred then clear and re-open the connection.

          If (ThisConnection IsNot Nothing) Then
            ThisConnection.Close()
            SqlConnection.ClearPool(ThisConnection)
            ThisConnection.Open()
          End If
        End Try

        ' Try again, without catching the error a second time.
        Return pAdaptor.Update(pDataSet)

      End SyncLock
		End SyncLock
	End Function

	Public Function AdaptorUpdate(ByRef pAdaptor As SqlDataAdapter, ByRef pDataRows() As DataRow) As Integer
		Dim ThisConnection As SqlConnection
    Dim RVal As Integer = 0

		If (Not (pAdaptor.SelectCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.SelectCommand.Connection
		ElseIf (Not (pAdaptor.InsertCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.InsertCommand.Connection
		ElseIf (Not (pAdaptor.UpdateCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.UpdateCommand.Connection
		ElseIf (Not (pAdaptor.DeleteCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.DeleteCommand.Connection
		Else
			Return 0
		End If

    SyncLock ThisConnection
      Try
        ' OK, Update and Return.

        RVal = pAdaptor.Update(pDataRows)
        Return RVal

      Catch ex As Exception
        ' If an error occurred then clear and re-open the connection.

        If (ThisConnection IsNot Nothing) Then
          ThisConnection.Close()
          SqlConnection.ClearPool(ThisConnection)
          ThisConnection.Open()
        End If
      End Try

      ' Try again, without catching the error a second time.

      Return pAdaptor.Update(pDataRows)
    End SyncLock

	End Function

	Public Function AdaptorUpdate(ByRef pAdaptor As SqlDataAdapter, ByRef pDataSet As DataSet, ByVal pTableName As String) As Integer
		Dim ThisConnection As SqlConnection
    Dim RVal As Integer = 0

		If (Not (pAdaptor.SelectCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.SelectCommand.Connection
		ElseIf (Not (pAdaptor.InsertCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.InsertCommand.Connection
		ElseIf (Not (pAdaptor.UpdateCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.UpdateCommand.Connection
		ElseIf (Not (pAdaptor.DeleteCommand.Connection Is Nothing)) Then
			ThisConnection = pAdaptor.DeleteCommand.Connection
		Else
			Return 0
		End If

		SyncLock ThisConnection
      SyncLock pDataSet
        Try
          ' OK, Update and Return.

          RVal = pAdaptor.Update(pDataSet, pTableName)
          Return RVal

        Catch ex As Exception
          ' If an error occurred then clear and re-open the connection.

          If (ThisConnection IsNot Nothing) Then
            ThisConnection.Close()
            SqlConnection.ClearPool(ThisConnection)
            ThisConnection.Open()
          End If
        End Try

        ' Try again, without catching the error a second time.

        Return pAdaptor.Update(pDataSet, pTableName)

      End SyncLock
		End SyncLock
	End Function

#End Region

	Public Function Add_Connection(ByVal pConnectionName As String, ByVal pConnectionString As String) As SqlConnection
		' ***************************************************************
		' 
		' ***************************************************************
		Dim myConnection As SqlConnection

		_ErrorMessage = ""

		Try
			SyncLock DBConnections
				If DBConnections.Contains(pConnectionName.ToUpper) Then
					myConnection = DBConnections(pConnectionName.ToUpper)
					If (myConnection.ConnectionString <> pConnectionString) Then
						myConnection.ConnectionString = pConnectionString
						If myConnection.State = ConnectionState.Open Then
							Try
								myConnection.Close()
								myConnection.Open()
							Catch ex As Exception
							End Try
						End If
					End If

					Return myConnection
					Exit Function
				End If

				myConnection = New SqlConnection

				myConnection.ConnectionString = pConnectionString

				Try
					myConnection.Open()
				Catch ex As Exception
					_ErrorMessage = ex.Message
					Return Nothing
				End Try

				If Not DBConnections.Contains(pConnectionName.ToUpper) Then
					DBConnections.Add(pConnectionName.ToUpper, myConnection)
				End If
			End SyncLock

			Return myConnection

		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Overloads Function Get_Connection(ByVal pConnectionName As String) As SqlConnection
		' ***************************************************************
		' Return the connection relating to the given Name if it exists.
		' ***************************************************************
		_ErrorMessage = ""

		Try
			SyncLock DBConnections
				If DBConnections.Contains(pConnectionName.ToUpper) Then
					Dim RVal As SqlConnection

					RVal = DBConnections(pConnectionName.ToUpper)

					Try

						Select Case RVal.State

							Case ConnectionState.Closed
								RVal.Open()

							Case ConnectionState.Broken
								RVal.Close()
								RVal.Open()

						End Select

					Catch ex As Exception
					End Try

					Return RVal
					Exit Function
				Else
					Return Nothing
					Exit Function
				End If
			End SyncLock
		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Overloads Function Get_Connection(ByVal pConnectionName As String, ByVal pConnectionString As String) As SqlConnection
		' ***************************************************************
		' Return the connection relating to the given Name if it exists, 
		' otherwise create a new one using the given connection string.
		' ***************************************************************
		_ErrorMessage = ""

		Try
			SyncLock DBConnections
				If DBConnections.Contains(pConnectionName.ToUpper) Then
					Dim RVal As SqlConnection

					RVal = DBConnections(pConnectionName.ToUpper)

					Try

						Select Case RVal.State

							Case ConnectionState.Closed
								RVal.Open()

							Case ConnectionState.Broken
								RVal.Close()
								RVal.Open()

						End Select

					Catch ex As Exception
					End Try

					Return RVal
					Exit Function
				End If
			End SyncLock

			Return Add_Connection(pConnectionName, pConnectionString)
			Exit Function
		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Function Drop_Connection(ByVal pConnectionName As String) As Boolean
		' ***************************************************************
		' Drop a connection from the DataHandler
		' ***************************************************************
		_ErrorMessage = ""

		Try
			SyncLock DBConnections
				If DBConnections.Contains(pConnectionName.ToUpper) Then
					DBConnections.Remove(pConnectionName.ToUpper)
				End If
			End SyncLock
		Catch ex As Exception
		End Try

		Return True

	End Function

	Public Overloads Function Add_Adaptor(ByVal pAdaptorName As String, ByVal pConnectionName As String, ByVal pTableName As String) As SqlDataAdapter
		' ***************************************************************
		' Return the ADO Adaptor relating to the given name, create and return
		' a new adaptor if it does not already exist, provided the given
		' connection also exists.
		' ***************************************************************
		Dim myAdaptor As SqlDataAdapter
		Dim myConnection As SqlConnection

		_ErrorMessage = ""

		Try
			myConnection = Get_Connection(pConnectionName)
			If myConnection Is Nothing Then
				Return Nothing
				Exit Function
			End If

			SyncLock Adaptors

				If Adaptors.Contains(pAdaptorName.ToUpper) Then
					myAdaptor = Adaptors(pAdaptorName.ToUpper)
				Else
					myAdaptor = New SqlDataAdapter
				End If

				If dhAdaptorHandler.Set_AdaptorCommands(myConnection, myAdaptor, pTableName) = False Then
					Return Nothing
					Exit Function
				End If

				If Not Adaptors.Contains(pAdaptorName.ToUpper) Then
					Adaptors.Add(pAdaptorName.ToUpper, myAdaptor)
				End If

			End SyncLock

			Return myAdaptor

		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Overloads Function Add_Adaptor(ByVal pAdaptorName As String, ByVal pConnectionName As String, ByVal pConnectionString As String, ByVal pTableName As String) As SqlDataAdapter
		' ***************************************************************
		' Return the ADO Adaptor relating to the given name, create and return
		' a new adaptor if it does not already exist. Create the associated Connection
		' object if necessary.
		' ***************************************************************
		Dim myAdaptor As SqlDataAdapter
		Dim myConnection As SqlConnection

		_ErrorMessage = ""

		Try
			myConnection = Get_Connection(pConnectionName)
			If myConnection Is Nothing Then
				myConnection = Add_Connection(pConnectionName, pConnectionString)
				If myConnection Is Nothing Then
					Return Nothing
					Exit Function
				End If
			End If

			SyncLock Adaptors

				If Adaptors.Contains(pAdaptorName.ToUpper) Then
					myAdaptor = Adaptors(pAdaptorName.ToUpper)
				Else
					myAdaptor = New SqlDataAdapter
				End If

				If dhAdaptorHandler.Set_AdaptorCommands(myConnection, myAdaptor, pTableName) = False Then
					Return Nothing
					Exit Function
				End If

				If Not Adaptors.Contains(pAdaptorName.ToUpper) Then
					Adaptors.Add(pAdaptorName.ToUpper, myAdaptor)
				End If

			End SyncLock

			Return myAdaptor
		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Overloads Function Get_Adaptor(ByVal pAdaptorName As String) As SqlDataAdapter
		' ***************************************************************
		' Return the ADO Adaptor relating to the given name, if it already exists.
		' ***************************************************************

		_ErrorMessage = ""

		Try
			SyncLock Adaptors
				If Adaptors.Contains(pAdaptorName.ToUpper) Then
					Return Adaptors(pAdaptorName.ToUpper)
					Exit Function
				End If
			End SyncLock

			Return Nothing

		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Overloads Function Get_Adaptor(ByVal pAdaptorName As String, ByVal pConnectionName As String, ByVal pTableName As String) As SqlDataAdapter
		' ***************************************************************
		' Return the ADO Adaptor relating to the given name, create and return
		' a new adaptor if it does not already exist, provided the given
		' connection also exists.
		' ***************************************************************

		_ErrorMessage = ""

		Try
			SyncLock Adaptors
				If Adaptors.Contains(pAdaptorName.ToUpper) Then
					Return Adaptors(pAdaptorName.ToUpper)
					Exit Function
				End If
			End SyncLock

			Return Add_Adaptor(pAdaptorName, pConnectionName, pTableName)
		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try
	End Function

	Public Overloads Function Get_Adaptor(ByVal pAdaptorName As String, ByVal pConnectionName As String, ByVal pConnectionString As String, ByVal pTableName As String) As SqlDataAdapter
		' ***************************************************************
		' Return the ADO Adaptor relating to the given name, create and return
		' a new adaptor if it does not already exist. Create the associated Connection
		' object if necessary.
		' ***************************************************************

		_ErrorMessage = ""

		Try
			SyncLock Adaptors
				If Adaptors.Contains(pAdaptorName.ToUpper) Then
					Dim RVal As SqlDataAdapter = Adaptors(pAdaptorName.ToUpper)

					Try
						If (RVal.SelectCommand.Connection IsNot Nothing) Then
							If (RVal.SelectCommand.Connection.State = ConnectionState.Closed) Then
								RVal.SelectCommand.Connection.Open()
							End If
						End If
					Catch ex As Exception
					End Try

					Return RVal
					Exit Function
				End If
			End SyncLock

			Return Add_Adaptor(pAdaptorName, pConnectionName, pConnectionString, pTableName)
		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try
	End Function

	Public Function Drop_Adaptor(ByVal pAdaptorName As String) As Boolean
		' ***************************************************************
		' Drop the ADO Adaptor relating to the given name.
		' ***************************************************************

		_ErrorMessage = ""

		Try
			SyncLock Adaptors
				If Adaptors.Contains(pAdaptorName.ToUpper) Then
					Adaptors.Remove(pAdaptorName.ToUpper)
				End If
			End SyncLock

			Return True
		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Sub Add_Dataset(ByVal pDatasetName As String, ByVal pDataset As DataSet)

		SyncLock Datasets
			Try
				If Datasets.Contains(pDatasetName.ToUpper) Then
					Datasets.Remove(pDatasetName.ToUpper)
				End If
			Catch ex As Exception
			End Try

			Try
				Datasets.Add(pDatasetName.ToUpper, pDataset)
			Catch ex As Exception
			End Try
		End SyncLock

	End Sub

	Public Function Add_Dataset(ByVal pDatasetName As String) As DataSet
		' ***************************************************************
		' Return a Dataset of the given Type / Name and add it to the 
		' DataHandler Datasets collection.
		'
		' This code assumes that the Tablename and DatasetNames are alligned, also
		' it assumes that the dataset definitions are held in the current
		' Assembly. (VeniceDataClass at time of writing.)
		'
		' ***************************************************************

		Dim myDataset As DataSet

		_ErrorMessage = ""

		Try

			SyncLock Datasets

				' ***************************************************************
				' Return the existing dataset if it is already loaded.
				' ***************************************************************
				If Datasets.Contains(pDatasetName.ToUpper) Then
					myDataset = CType(Datasets(pDatasetName.ToUpper), DataSet)
					myDataset.Tables.Clear()

					Return myDataset
					Exit Function
				End If

				' ***************************************************************
				' Instantiate a new dataset using the given name.
				' ***************************************************************

				myDataset = Instantiate_Dataset(pDatasetName)

				'Try
				'  Dim ThisAssembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
				'  myDataset = CType(ThisAssembly.CreateInstance("VeniceDataClass." & pDatasetName, True), DataSet)

				'  If (myDataset Is Nothing) Then
				'    myDataset = New DataSet
				'  End If


				'Catch ex As Exception
				'  ' Having failed to instantiate the given Object type, default to a vanilla dataset.
				'  myDataset = New DataSet
				'End Try

				' ***************************************************************
				' Add to the DataHandler datasets hashtable.
				' ***************************************************************
				If (Not (myDataset Is Nothing)) Then
					Datasets.Add(pDatasetName.ToUpper, myDataset)
				End If

			End SyncLock

			Return myDataset

		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Function Instantiate_Dataset(ByVal pDatasetName As String) As DataSet
		' ***************************************************************
		' Return a New Dataset of the given Type.
		'
		' This code assumes that the Tablename and DatasetNames are alligned, also
		' it assumes that the dataset definitions are held in the current
    ' Assembly. (RenaissanceDataClass at time of writing.)
		'
		' ***************************************************************

		Dim myDataset As DataSet = Nothing

		_ErrorMessage = ""

		Try
			' ***************************************************************
			' Instantiate a new dataset using the given name.
			' ***************************************************************

			Dim ThisAssembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
			Dim AssemblyShortName As String
			AssemblyShortName = ThisAssembly.FullName.Split(New Char() {","})(0).Trim()

			myDataset = CType(ThisAssembly.CreateInstance(AssemblyShortName & "." & pDatasetName, True), DataSet)

		Catch ex As Exception
			_ErrorMessage = ex.Message
		End Try

		If (myDataset Is Nothing) Then
			myDataset = New DataSet
		End If

		Return myDataset

	End Function

	Public Overloads Function Get_Dataset(ByVal pDatasetName As String, Optional ByVal pCreateIfAbsent As Boolean = False) As DataSet
		' ***************************************************************
		' Return the dataset relating to the given name, create and return
		' a new Dataset if it does not already exists.
		' ***************************************************************

		_ErrorMessage = ""

		Try
			SyncLock Datasets
				If Datasets.Contains(pDatasetName.ToUpper) Then
					Return Datasets(pDatasetName.ToUpper)
					Exit Function
				End If
			End SyncLock

			If (pCreateIfAbsent = True) Then
				Return Add_Dataset(pDatasetName)
			Else
				Return Nothing
			End If
		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

	Public Function Drop_Dataset(ByVal pDatasetName As String) As Boolean
		' ***************************************************************
		' Delete the dataset relating to the given name, if it exists.
		' ***************************************************************

		_ErrorMessage = ""

		Try
			SyncLock Datasets
				If Datasets.Contains(pDatasetName.ToUpper) Then
					Datasets.Remove(pDatasetName.ToUpper)
				End If
			End SyncLock

			Return True
		Catch ex As Exception
			_ErrorMessage = ex.Message
			Return Nothing
		End Try

	End Function

End Class
