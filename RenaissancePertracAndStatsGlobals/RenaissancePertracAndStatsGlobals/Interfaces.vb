﻿Imports RenaissanceGlobals

Public Class Globals

  Public Const Ulong_31BitMask As ULong = CULng((2 ^ 31) - 1)
  Public Const Ulong_32BitMask As ULong = CULng((2 ^ 32) - 1)

End Class

Public Class NetDeltaItemClass

  Public PertracId As ULong
  Public DeltaArray() As Double

  Public Sub New()

    PertracId = 0UL
    DeltaArray = Nothing

  End Sub

  Public Sub New(ByVal pPertracId As ULong, ByVal pDeltaArray() As Double)

    Me.New()

    Me.PertracId = pPertracId
    Me.DeltaArray = pDeltaArray

  End Sub

End Class

Public Enum LookthroughEnum As Integer

  None = 0
  FullLookthrough = 1
  JustLookthroughGroups = 2
  LookthroughToSimulations = 4

End Enum

Public Interface GroupPriceSeriesProvider

  Sub GetGroupSeries(ByVal pNestingLevel As Integer, ByVal pGroupID As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByVal pStatsDatePeriod As DealingPeriod, ByRef pDatesArray() As Date, ByRef pReturnsArray() As Double, ByRef pNetDeltaArray() As NetDeltaItemClass, ByVal ParentStatsID As ULong, ByRef DependencyList As List(Of ULong), ByVal pSampleSize As Integer)
  Sub GetGroupMembers(ByVal pGroupID As Integer, ByRef pGroupMembers() As Integer, ByRef pGroupWeights() As Double)
  Sub SetDynamicGroupMembers(ByVal GroupID As Integer, ByVal GroupMembers() As Integer, ByVal GroupWeights() As Double)
  Sub GetDynamicGroupMembers(ByVal GroupID As Integer, ByRef GroupMembers() As Integer, ByRef GroupWeights() As Double, ByVal SampleSize As Integer, ByVal Lookthrough As Boolean)

  Sub GetSimulationSeries(ByVal pNestingLevel As Integer, ByVal pGroupID As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByVal pStatsDatePeriod As DealingPeriod, ByRef pDatesArray() As Date, ByRef pReturnsArray() As Double, ByRef pNAVArray() As Double, ByRef pNetDeltaItem As NetDeltaItemClass, ByVal ParentStatsID As ULong, ByRef DependencyList As List(Of ULong))

End Interface

Public Interface StatsSeriesProvider

  Function DateSeries(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date) As Date()
  Function ReturnSeries(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date) As Double()
  Function NAVSeries(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pScalingFactor As Double, ByVal pNormaliseNAVs As Boolean, ByVal InitialNormalisationValue As Double) As Double()
  Function StdDevSeries(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal pPeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date) As Double()
  Function NetDeltas(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pLookthrough As Boolean) As NetDeltaItemClass()
  Function NetDeltas(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean, ByVal PeriodCount As Integer, ByVal pLamda As Double, ByVal Annualised As Boolean, ByVal pStatsStartDate As Date, ByVal pStatsEndDate As Date, ByVal pLookthrough As LookthroughEnum) As NetDeltaItemClass()
  Function SetDependsOnMe(ByVal pParentStatsKey As ULong, ByVal pChildStatsKey As ULong) As ULong
  Function GetStatCacheKey(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracID As ULong, ByVal MatchBackfillVol As Boolean) As ULong

  Sub CalcAvgStDevSeries(ByVal thisReturnsArray() As Double, ByRef thisAverageArray() As Double, ByRef thisStDevArray() As Double, ByVal pPeriodCount As Integer, ByVal pLamda As Double)

  ReadOnly Property AnnualPeriodCount(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod) As Integer


End Interface
