﻿Public Interface IRenaissanceSearchUpdateEvent

  Event RenaissanceSearchUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer)
  Event RenaissanceGroupStatsUpdateEvent(ByVal sender As Object, ByVal GroupID As Integer)

  ReadOnly Property CompoundGroups() As Generic.SortedDictionary(Of Integer, Integer())

End Interface
