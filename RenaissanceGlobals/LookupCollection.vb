Imports System
Imports System.Collections
Imports System.Windows.Forms
Imports Telerik.WinControls.UI

Public Interface ILookup
	ReadOnly Property Key() As Object
	ReadOnly Property Value() As Object
End Interface

Public Interface ILookupCollection
	ReadOnly Property KeyArray() As Array
	Sub Sort()
End Interface

Public Interface IRadCombo
	ReadOnly Property RadComboItemArray(Optional ByVal pSelectString As String = "") As RadComboBoxItem()
	Property Item(ByVal key As Object) As Object
	Function Contains(ByVal key As Object) As Boolean
	Sub ReplaceComboItem(ByVal key As Object)
	Property ActiveRadCombo() As Object
  ReadOnly Property Count() As Integer
  ReadOnly Property IndexOfKey(ByVal pValue As Object) As Integer
  ReadOnly Property ValueMember(ByVal Index As Integer) As Object
  ReadOnly Property DisplayMember(ByVal Index As Integer) As Object
End Interface

' Collection to implement a dictionary object, sorted by Value
'
' Acknowledgements to The Code Project and Foulds.Net
'
' Adjustments made to the original code to Minimise the amount of array sorting and to 
' Speed the retrieval of individual values by adding a parallel HashTable.
'
' This does of course double the memory required, but it does improve performance significantly.
'
'
Public Class LookupCollectionCacheItemClass

	Private _ChangeID As Integer
	Private _ValueField As String
	Private _DisplayField As String
	Private _SelectString As String
	Private _SortString As String
	Private _OrderAscending As Boolean
	Private _LeadingBlank As Boolean
	Private _BlankValue As Object
	Private _ThisCollection As Object

	Private Sub New()
		' No default Constructor.
	End Sub

	Public Sub New(ByVal ChangeID As Integer, ByVal ValueField As String, ByVal DisplayField As String, ByVal SelectString As String, ByVal SortString As String, ByVal OrderAscending As Boolean, ByVal LeadingBlank As Boolean, ByVal BlankValue As Object)
		_ChangeID = ChangeID
		_ValueField = ValueField
		_DisplayField = DisplayField
		_SelectString = SelectString
		_SortString = SortString
		_OrderAscending = OrderAscending
		_LeadingBlank = LeadingBlank
		_BlankValue = BlankValue
		_ThisCollection = Nothing
	End Sub

	Public Sub New(ByVal ChangeID As Integer, ByVal ValueField As String, ByVal DisplayField As String, ByVal SelectString As String, ByVal SortString As String, ByVal OrderAscending As Boolean, ByVal LeadingBlank As Boolean, ByVal BlankValue As Object, ByVal ThisCollection As Object)
		_ChangeID = ChangeID
		_ValueField = ValueField
		_DisplayField = DisplayField
		_SelectString = SelectString
		_SortString = SortString
		_OrderAscending = OrderAscending
		_LeadingBlank = LeadingBlank
		_BlankValue = BlankValue
		_ThisCollection = ThisCollection
	End Sub

	Public ReadOnly Property Key() As String
		Get
			Return _ChangeID.ToString & "|" & _ValueField & "|" & _DisplayField & "|" & _SelectString & "|" & _SortString & "|" & _OrderAscending.ToString & "|" & _LeadingBlank.ToString & "|" & _BlankValue.ToString
		End Get
	End Property

	Public Property ThisCollection() As Object
		Get
			Return _ThisCollection
		End Get
		Set(ByVal value As Object)
			_ThisCollection = value
		End Set
	End Property

	Public ReadOnly Property ChangeID() As Integer
		Get
			Return _ChangeID
		End Get
	End Property

	Public ReadOnly Property ValueField() As String
		Get
			Return _ValueField
		End Get
	End Property

	Public ReadOnly Property DisplayField() As String
		Get
			Return _DisplayField
		End Get
	End Property

	Public ReadOnly Property SelectString() As String
		Get
			Return _SelectString
		End Get
	End Property

	Public ReadOnly Property SortString() As String
		Get
			Return _SortString
		End Get
	End Property

	Public ReadOnly Property OrderAscending() As Boolean
		Get
			Return _OrderAscending
		End Get
	End Property

	Public ReadOnly Property LeadingBlank() As Boolean
		Get
			Return _LeadingBlank
		End Get
	End Property

	Public ReadOnly Property BlankValue() As Object
		Get
			Return _BlankValue
		End Get
	End Property

End Class

Public Class LookupAscendingSorter
	Implements System.Collections.IComparer

	Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
		If (TypeOf x Is ILookup) AndAlso (TypeOf y Is ILookup) Then
			Return CType(CType(x, ILookup).Value, IComparable).CompareTo(CType(y, ILookup).Value)
		Else
			Return 0
		End If
	End Function

End Class

Public Class LookupDescendingSorter
	Implements System.Collections.IComparer

	Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
		If (TypeOf x Is ILookup) AndAlso (TypeOf y Is ILookup) Then
			Return CType(CType(y, ILookup).Value, IComparable).CompareTo(CType(x, ILookup).Value)
		Else
			Return 0
		End If
	End Function
End Class

Public Class Lookup(Of KeyType, ValueType)
	'Implements IComparable
	Implements ILookup

	Private mKey As KeyType
	Private mValue As ValueType
	Private mRadComboItem As RadComboBoxItem

	' ---------------------------------------------------------------------
	'  Overloaded Constructor
	' ---------------------------------------------------------------------
	Public Sub New()
		Me.mRadComboItem = New RadComboBoxItem("", Nothing)
	End Sub

	Public Sub New(ByVal key As KeyType, ByVal value As ValueType)
		Me.Key = key
		Me.Value = value

		SetNewComboItem(key, value, False)

	End Sub

	Public Sub New(ByVal key As KeyType, ByVal value As ValueType, ByVal pExistingInstrument As Boolean)

		Me.Key = key
		Me.Value = value
		SetNewComboItem(key, value, pExistingInstrument)

	End Sub


	' ---------------------------------------------------------------------
	'  CompareTo
	' ---------------------------------------------------------------------
	'Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo
	'	Dim result As Integer
	'	result = 0

	'	If (TypeOf obj Is Lookup(Of KeyType, ValueType)) Then
	'		result = CType(Me.Value, IComparable).CompareTo(CType(obj, Lookup(Of KeyType, ValueType)).Value)
	'	End If

	'	Return result

	'End Function

	' ---------------------------------------------------------------------
	'  ToDictionaryEntry
	' ---------------------------------------------------------------------
	Public Function ToDictionaryEntry() As DictionaryEntry
		Return New DictionaryEntry(Me.Key, Me.Value)
	End Function


	' =====================================================================
	'  PROPERTIES
	' =====================================================================

	Public Property Key() As KeyType
		Get
			Return Me.mKey
		End Get
		Set(ByVal Value As KeyType)
			Me.mKey = Value
			mRadComboItem.Value = Value
		End Set
	End Property

	Public Property Value() As ValueType
		Get
			Return Me.mValue
		End Get
		Set(ByVal Value As ValueType)
			Me.mValue = Value
			mRadComboItem.Text = Value.ToString
		End Set
	End Property

	Public Property ExistingInstrument() As Boolean
		Get
			Return mRadComboItem.Font.Bold
		End Get
		Set(ByVal value As Boolean)
			If (value <> mRadComboItem.Font.Bold) Then
				If (value) Then
					mRadComboItem.Font = New Drawing.Font(mRadComboItem.Font, Drawing.FontStyle.Bold)
				Else
					mRadComboItem.Font = New Drawing.Font(mRadComboItem.Font, Drawing.FontStyle.Regular)
				End If
			End If
		End Set
	End Property

	Public ReadOnly Property ComboItem() As RadComboBoxItem
		Get
			Return Me.mRadComboItem
		End Get
	End Property

	Public Function SetNewComboItem(ByVal pKey As KeyType, ByVal value As ValueType, ByVal pExistingInstrument As Boolean) As RadComboBoxItem

		Me.mRadComboItem = New RadComboBoxItem(value.ToString, pKey)
		If (pExistingInstrument) Then
			Me.mRadComboItem.Font = New Drawing.Font(Me.mRadComboItem.Font, Drawing.FontStyle.Bold)
		End If

		Return Me.mRadComboItem

	End Function

	Public ReadOnly Property ILookup_Key() As Object Implements ILookup.Key
		Get
			Return Me.Key
		End Get
	End Property

	Public ReadOnly Property ILookup_Value() As Object Implements ILookup.Value
		Get
			Return Me.Value
		End Get
	End Property
End Class

Public Class LookupEnumerator(Of KeyType, ValueType)
	Implements IDictionaryEnumerator

	Private index As Integer
	Private items As ArrayList

	' ---------------------------------------------------------------------
	'  Constructor
	' ---------------------------------------------------------------------

	Public Sub New(ByVal list As ArrayList)
		Me.index = (-1)
		Me.items = list
	End Sub


	' ---------------------------------------------------------------------
	'  MoveNext
	' ---------------------------------------------------------------------

	Public Function MoveNext() As Boolean Implements System.Collections.IEnumerator.MoveNext
		Me.index += 1

		If (Me.index >= Me.items.Count) Then
			Return False
		End If

		Return True
	End Function


	' =====================================================================
	'  PROPERTIES
	' =====================================================================
	' ---------------------------------------------------------------------
	'  Reset
	' ---------------------------------------------------------------------

	Public Sub Reset() Implements System.Collections.IEnumerator.Reset
		Me.index = -1

	End Sub


	' ---------------------------------------------------------------------
	'  Current
	' ---------------------------------------------------------------------

	Public ReadOnly Property Current() As Object Implements System.Collections.IEnumerator.Current
		Get
			If (Me.index < 0 Or index >= Me.items.Count) Then
				Throw New InvalidOperationException
			End If

			Return Me.items(index)

		End Get
	End Property


	' ---------------------------------------------------------------------
	'  Entry
	' ---------------------------------------------------------------------

	Public ReadOnly Property Entry() As DictionaryEntry Implements System.Collections.IDictionaryEnumerator.Entry
		Get
			Return CType(Me.Current, Lookup(Of KeyType, ValueType)).ToDictionaryEntry
		End Get
	End Property


	' ---------------------------------------------------------------------
	'  Key
	' ---------------------------------------------------------------------

	Public ReadOnly Property Key() As Object Implements System.Collections.IDictionaryEnumerator.Key
		Get
			Return Me.Entry.Key
		End Get
	End Property


	' ---------------------------------------------------------------------
	'  Value
	' ---------------------------------------------------------------------
	Public ReadOnly Property Value() As Object Implements System.Collections.IDictionaryEnumerator.Value
		Get
			Return Me.Entry.Value
		End Get
	End Property

End Class

Public Class LookupCollection(Of KeyType, ValueType)
	Implements ILookupCollection, ICollection, IDictionary, IEnumerable, IRadCombo

	Private mItems As ArrayList
	Private hItems As Hashtable
	Private ComboItemArray(-1) As RadComboBoxItem

	Private SortFlag As Boolean
	Private thisKeys As ArrayList
	Private SortDescending As Boolean
	Private SelectString As String
	Private SelectLock As System.Threading.ReaderWriterLock
	Private _ActiveRadCombo As Object

	' ---------------------------------------------------------------------
	'  Constructor
	' ---------------------------------------------------------------------

	Public Sub New()

		mItems = New ArrayList
		hItems = New Hashtable

		SortFlag = True
		thisKeys = Nothing
		SortDescending = False
		SelectLock = New System.Threading.ReaderWriterLock
		_ActiveRadCombo = Nothing

	End Sub

	Public Sub New(ByVal pSortDescending As Boolean)

		Me.New()

		SortDescending = pSortDescending

	End Sub

	' ---------------------------------------------------------------------
	'  Add
	' ---------------------------------------------------------------------

	Public Sub DAdd(ByVal key As Object, ByVal value As Object) Implements System.Collections.IDictionary.Add
		Try
			Me.Add(CType(key, KeyType), CType(value, ValueType))
		Catch ex As Exception
		End Try
	End Sub

	Public Sub Add(ByVal key As KeyType, ByVal value As ValueType)

		'  do some validation
		If (key Is Nothing) Then
			Throw New ArgumentNullException("key is a null reference")
			Exit Sub
		ElseIf (hItems.ContainsKey(key)) Then	' Me.Contains(key)) Then
			Throw New ArgumentException("An element with the same key already exists")
			Exit Sub
		End If

		'  add the new item
		Dim newItem As Lookup(Of KeyType, ValueType) = New Lookup(Of KeyType, ValueType)

		newItem.Key = key
		newItem.Value = value

		thisKeys = Nothing
		Me.mItems.Add(newItem)
		Me.hItems.Add(key, value)
		SortFlag = False

		' Me.mItems.Sort()

	End Sub

	Public Sub Sort() Implements ILookupCollection.Sort
		If SortDescending Then
			Me.mItems.Sort(New LookupDescendingSorter)
		Else
			Me.mItems.Sort(New LookupAscendingSorter)
		End If

		Try
			If (ComboItemArray.Length < mItems.Count) Then
				ReDim ComboItemArray(mItems.Count - 1)
			End If

			Dim Index As Integer

			For Index = 0 To (mItems.Count - 1)
				ComboItemArray(Index) = CType(mItems(Index), Lookup(Of KeyType, ValueType)).ComboItem
			Next
		Catch ex As Exception
		End Try

		Me.SortFlag = True
		thisKeys = Nothing
	End Sub

	' ---------------------------------------------------------------------
	'  Clear
	' ---------------------------------------------------------------------

	Public Sub Clear() Implements System.Collections.IDictionary.Clear
		Me.mItems.Clear()
		Me.hItems.Clear()
		thisKeys = Nothing
	End Sub

	' ---------------------------------------------------------------------
	'  Contains
	' ---------------------------------------------------------------------

	Public Function DContains(ByVal key As Object) As Boolean Implements System.Collections.IDictionary.Contains, IRadCombo.Contains
		Try
			Return Me.Contains(CType(key, KeyType))
		Catch ex As Exception
		End Try
	End Function

	Public Function Contains(ByVal key As KeyType) As Boolean
		Return Me.hItems.Contains(CType(key, KeyType))
	End Function

	' ---------------------------------------------------------------------
	'  CopyTo
	' ---------------------------------------------------------------------

	Public Sub CopyTo(ByVal array As System.Array, ByVal index As Integer) Implements System.Collections.ICollection.CopyTo
		If (Not SortFlag) Then
			Me.Sort()
			SortFlag = True
		End If

		Me.mItems.CopyTo(array, index)
	End Sub

	' ---------------------------------------------------------------------
	'  GetEnumerator (1)
	' ---------------------------------------------------------------------

	Public Function GetEnumerator() As System.Collections.IDictionaryEnumerator Implements System.Collections.IDictionary.GetEnumerator
		If (Not SortFlag) Then
			Me.Sort()
			SortFlag = True
		End If

		Return New LookupEnumerator(Of KeyType, ValueType)(Me.mItems)
	End Function

	' ---------------------------------------------------------------------
	'  GetEnumerator (2)
	' ---------------------------------------------------------------------

	Public Function GetEnumerator1() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
		If (Not SortFlag) Then
			Me.Sort()
			SortFlag = True
		End If

		Return New LookupEnumerator(Of KeyType, ValueType)(Me.mItems)
	End Function

	' ---------------------------------------------------------------------
	'  Remove
	' ---------------------------------------------------------------------

	Public Sub DRemove(ByVal key As Object) Implements System.Collections.IDictionary.Remove
		Me.Remove(CType(key, KeyType))
	End Sub

	Public Sub Remove(ByVal key As KeyType)

		If (key Is Nothing) Then
			Throw New ArgumentNullException("key is a null reference")
		End If

		Dim deleteItem As Lookup(Of KeyType, ValueType)
		deleteItem = Me.GetByKey(key)

		If (Not (deleteItem Is Nothing)) Then
			Me.mItems.Remove(deleteItem)
			Me.hItems.Remove(key)

			thisKeys = Nothing

			SortFlag = False ' Prompt the re-creation of the ComboItemArray

		End If

	End Sub

	' ---------------------------------------------------------------------
	' ReplaceComboItem 
	' ---------------------------------------------------------------------

	Public Sub ReplaceComboItem(ByVal pKey As Object) Implements IRadCombo.ReplaceComboItem

		Dim Key As KeyType

		Try

			If (pKey Is Nothing) Then
				Throw New ArgumentNullException("Key is a null reference")
			End If

			Key = CType(pKey, KeyType)

			Dim ReplaceItem As Lookup(Of KeyType, ValueType)
			Dim NewComboItem As RadComboBoxItem

			ReplaceItem = Me.GetByKey(Key)

			If (Not (ReplaceItem Is Nothing)) Then
				NewComboItem = ReplaceItem.SetNewComboItem(ReplaceItem.Key, ReplaceItem.Value, ReplaceItem.ComboItem.Font.Bold)

				If (ComboItemArray IsNot Nothing) AndAlso (ComboItemArray.Length > 0) Then
					Dim Index As Integer

					For Index = 0 To (ComboItemArray.Length - 1)
						If CType(ComboItemArray(Index).Value, IComparable).CompareTo(Key) = 0 Then
							ComboItemArray(Index) = NewComboItem
							Exit Sub
						End If
					Next
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

	' =====================================================================
	'  PRIVATE
	' =====================================================================

	Private Function GetByKey(ByVal Key As Object) As Lookup(Of KeyType, ValueType)
		Dim result As Lookup(Of KeyType, ValueType) = Nothing

		result = Nothing

		If (Me.hItems.Count > 0) Then
			Dim curItem As Lookup(Of KeyType, ValueType)

			For Each curItem In Me.mItems
				If curItem.Key.Equals(Key) Then
					result = curItem
					Exit For
				End If
			Next

		End If

		Return (result)
	End Function

	' =====================================================================
	'  PROPERTIES
	' =====================================================================

	Public ReadOnly Property Count() As Integer Implements System.Collections.ICollection.Count, IRadCombo.Count
		Get
			Return Me.mItems.Count
		End Get
	End Property

	Public ReadOnly Property IsSynchronized() As Boolean Implements System.Collections.ICollection.IsSynchronized
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property SyncRoot() As Object Implements System.Collections.ICollection.SyncRoot
		Get
			Return Me
		End Get
	End Property

	Public ReadOnly Property IsFixedSize() As Boolean Implements System.Collections.IDictionary.IsFixedSize
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.IDictionary.IsReadOnly
		Get
			Return False
		End Get
	End Property

	Public Property DItem(ByVal key As Object) As Object Implements System.Collections.IDictionary.Item, IRadCombo.Item
		Get
			Return Me.Item(CType(key, KeyType))
		End Get
		Set(ByVal value As Object)
			Me.Item(CType(key, KeyType)) = CType(value, ValueType)
		End Set
	End Property

	Default Public Property Item(ByVal key As KeyType) As ValueType
		Get

			Return Me.hItems.Item(key)

		End Get
		Set(ByVal Value As ValueType)

			Me.hItems.Item(key) = Value

			For Each ThisLookup As Lookup(Of KeyType, ValueType) In mItems
				If ThisLookup.Key.Equals(key) Then
					ThisLookup.Value = Value
					SortFlag = False
					Exit For
				End If
			Next

		End Set
	End Property

  Public ReadOnly Property IndexOfKey(ByVal pKey As Object) As Integer Implements IRadCombo.IndexOfKey
    Get
      Dim ThisLookup As Lookup(Of KeyType, ValueType)
      Dim CompValue As Integer

      If (Not SortFlag) Then
        Me.Sort()
        SortFlag = True
      End If

      For Index As Integer = 0 To (mItems.Count - 1)
        ThisLookup = mItems(Index)
        CompValue = CType(ThisLookup.Key, IComparable).CompareTo(CType(pKey, IComparable))
        If CompValue = 0 Then
          Return Index
        End If
      Next

      Return (-1)
    End Get
  End Property

	Public ReadOnly Property Keys() As System.Collections.ICollection Implements System.Collections.IDictionary.Keys
		Get

			If (Not (thisKeys Is Nothing)) Then
				Return thisKeys
			End If

			Dim curItem As Lookup(Of KeyType, ValueType)
			thisKeys = New ArrayList

			For Each curItem In Me.mItems
				thisKeys.Add(curItem.Key)
			Next

			Return thisKeys

		End Get
	End Property

	Public ReadOnly Property KeyArray() As Array Implements ILookupCollection.KeyArray
		Get
			Dim RVal As Array = Array.CreateInstance(GetType(KeyType), hItems.Keys.Count)
			hItems.Keys.CopyTo(RVal, 0)
			Return RVal
		End Get
	End Property

	Public ReadOnly Property Values() As System.Collections.ICollection Implements System.Collections.IDictionary.Values
		Get
			Dim result As ArrayList
			result = New ArrayList

			Dim curItem As Lookup(Of KeyType, ValueType)

			If (SortFlag = False) Then
				Me.Sort()
				SortFlag = True
			End If

			For Each curItem In Me.mItems
				result.Add(curItem.Value)
			Next

			Return result

		End Get
	End Property

  Public ReadOnly Property KeyValue(ByVal Index As Integer) As Object Implements IRadCombo.ValueMember
    Get

      If (Index > 0) AndAlso (Index < mItems.Count) Then

        If (Not SortFlag) Then
          Me.Sort()
          SortFlag = True
        End If

        Return CType(mItems(Index), Lookup(Of KeyType, ValueType)).Key

      End If

      Return Nothing

    End Get
  End Property

  Public ReadOnly Property ValueValue(ByVal Index As Integer) As Object Implements IRadCombo.DisplayMember
    Get

      If (Index > 0) AndAlso (Index < mItems.Count) Then

        If (Not SortFlag) Then
          Me.Sort()
          SortFlag = True
        End If

        Return CType(mItems(Index), Lookup(Of KeyType, ValueType)).Value

      End If

      Return Nothing

    End Get
  End Property

	Public Property ActiveRadCombo() As Object Implements IRadCombo.ActiveRadCombo
		Get
			Return _ActiveRadCombo
		End Get
		Set(ByVal value As Object)
			_ActiveRadCombo = value
		End Set
	End Property

	Public ReadOnly Property RadComboItemArray(Optional ByVal pSelectString As String = "") As RadComboBoxItem() Implements IRadCombo.RadComboItemArray
		Get
			If (pSelectString.Length <= 0) OrElse (pSelectString = "*") OrElse (pSelectString = "%") Then
				Return ComboItemArray
			Else
				Try
					SelectLock.AcquireWriterLock(0)
					SelectString = pSelectString
					Return Array.FindAll(Of RadComboBoxItem)(ComboItemArray, AddressOf MatchSelectString)
				Catch ex As Exception
				Finally
					SelectLock.ReleaseWriterLock()
				End Try
			End If

			Return New RadComboBoxItem() {}

		End Get
	End Property

	Private Function MatchSelectString(ByVal s As RadComboBoxItem) As Boolean

		Return (SelectString Like s.Text)

	End Function


End Class

