Imports System.Windows.Forms
Imports System.Net

Public Class Globals

  Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#
  Public Const Renaissance_BaseDate As Date = #1/1/1900#

  Public Const Renaissance_EndDate_Data As Date = #1/1/2100#

  Public Const LAST_SECOND As Integer = 86399

	Public Const DEFAULT_DATA_PERIOD As DealingPeriod = DealingPeriod.Daily

  ' Standard Date Formats

  Public Const DISPLAYMEMBER_DATEFORMAT As String = "dd MMM yyyy"
  Public Const DISPLAYMEMBER_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
	Public Const DISPLAYMEMBER_INTEGERFORMAT As String = "#,##0"
	Public Const DISPLAYMEMBER_DOUBLEFORMAT As String = "#,##0.00"
  Public Const DISPLAYMEMBER_LONGDOUBLEFORMAT As String = "#,##0.0000"
  Public Const DISPLAYMEMBER_PERCENTAGEFORMAT As String = "#,##0.00%"
	Public Const DISPLAYMEMBER_PERCENTAGEFORMATLong As String = "#,##0.0000%"

  Public Const REPORT_DATEFORMAT As String = "dd MMMM yyyy"
  Public Const REPORT_LONGDATEFORMAT As String = "dd MMM yyyy HH:mm:ss"
  Public Const REPORT_MonthAndYear_DATEFORMAT As String = "MMMM yyyy"

  Public Const QUERY_SHORTDATEFORMAT As String = "yyyy-MM-dd"
  Public Const QUERY_LONGDATEFORMAT As String = "yyyy-MM-dd HH:mm:ss"

  Public Const FILENAME_DATEFORMAT As String = "yyyyMMdd_HHmmss"

  Public Const TIMEFORMAT As String = "HH:mm:ss"

  Public Const SUBFUND_EFFECTIVEFUNDMODIFIER As Integer = 2 ^ 30 ' Single Bit to Convert to <Effective Fund> ID
  Public Const SUBFUND_ID_MASK As Integer = (2 ^ 30) - 1 ' Mask to Convert Back again.

	Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_SENDER_ID As String = "TROPICPFQ"
	Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_BANK_CODE As String = "30026"
	Public Const PRIMONIAL_BNP_TOPIC_DEFAULT_AGENCY_CODE As String = "01000"

End Class

Public Interface StandardRenaissanceMainForm
  ' ************************************************************************************************
  ' 'Standard' Mainform Interface
  '
  ' Basic Interface representing essential functions provided by the main Renaissance Form.
  '
  ' The intention was to allow the development of classes, such as the Pertrac Data Class, that
  ' could be shared between Renaisance applications without having reference to a specific service
  ' form. i.e. if VeniceMain and GenoaMain implement this interface then thay can both use the 
  ' Pertrac class without modification.
  '
  ' ************************************************************************************************

  Event RenaissanceAutoUpdate(ByVal sender As Object, ByVal e As RenaissanceUpdateEventArgs)

  Sub Main_RaiseEvent(ByVal pEventArgs As RenaissanceGlobals.RenaissanceUpdateEventArgs)

  Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset) As DataSet
  Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean) As DataSet
  Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet
	Function Load_Table(ByVal pDatasetDetails As RenaissanceGlobals.StandardDataset, ByVal pUpdateDetail As String, ByVal pForceRefresh As Boolean, ByVal pRaiseEvent As Boolean) As DataSet

  Function GetRenaissanceConnection() As System.Data.SqlClient.SqlConnection

  Function LogError(ByVal p_Error_Location As String, _
    ByVal p_System_Error_Number As Integer, _
    ByVal p_System_Error_Description As String, _
    Optional ByVal p_Error_Message As String = "", _
    Optional ByVal p_Error_StackTrace As String = "", _
    Optional ByVal p_Show_Error As Boolean = True, _
    Optional ByVal p_MessageHeading As String = "", _
    Optional ByVal p_MessageButtons As System.Windows.Forms.MessageBoxButtons = MessageBoxButtons.OK, _
    Optional ByVal p_MessageBoxIcon As System.Windows.Forms.MessageBoxIcon = MessageBoxIcon.Exclamation, _
    Optional ByVal p_Log_to As Integer = 0) As Integer

  Property Main_Knowledgedate() As Date
  ReadOnly Property MainDataHandler() As RenaissanceDataClass.DataHandler
  ReadOnly Property MainAdaptorHandler() As RenaissanceDataClass.AdaptorHandler
  ReadOnly Property MasternameDictionary() As LookupCollection(Of Integer, String)
  ReadOnly Property LocationsDictionary() As LookupCollection(Of Integer, String)

End Interface

' Table / Event identifier Enumeration.

Public Enum RenaissanceChangeID As Integer
  ' Table / Event identifier Enumeration.
  ' It is intended that this enumeration should increment sequentially from Zero.
  ' The Last / Largest value should be represented by the 'MaxValue' entry.
  ' The 'MaxValue' entry IS used to initialise arrays etc.
  ' 

  None = 0
  tblAttribution
  tblAutoUpdate

	tblBenchmarkIndex
	tblBenchmarkItems
  tblBestFX
  tblBestMarketPrice
	tblBookmark
	tblBroker
	tblBrokerAccounts

  tblChangeControl
  tblCompoundTransaction
  tblCompoundTransactionParameters
  tblCompoundTransactionTemplate
  tblCounterparty
  tblCovariance
  tblCovarianceList
  tblCTA_Simulation
  tblCTA_Folders
  tblCTA_SimulationGroupList
  tblCTA_SimulationGroupItems
  tblCurrency

  tblDataDictionary_TransactionFormLabels
  tblDataProviderInstrumentType
  tblDataTask
  tblDataTaskType
  tblDealingPeriod

  tblEstimate
  tblErrorLog
  tblExposure

  tblFeeDefinitions
  tblFlorenceDataValues
  tblFlorenceEntity
  tblFlorenceGroup
  tblFlorenceItems
  tblFlorenceStatus
  tblFlorenceStatusIDs
  tblFlorenceWorkItems
  tblFlorenceWorkItemsData
  tblFund
  tblFundContactDetails
  tblFundMilestones
  tblFundPerformanceFeePoints
  tblFundType
  tblFX

  tblGenoaConstraintList
  tblGenoaConstraintItems
  tblGenoaSavedSearchList
  tblGenoaSavedSearchItems
  tblGroupsAggregated
	tblGreeks
	tblGroupList
	tblGroupItems
  tblGroupItemData

  tblIndexMapping
  tblInstrument
  tblInstrumentNotes
	tblInstrumentFlorenceEntityLink
  tblInstrumentType
  tblInstrumentNamesGrossUnits
  tblInvestorGroup

  tblJob

  tblLimits
  tblLimitType
  tblLiquidityLimit
  tblLocations

  tblManagementCompany
  tblMarketInstruments
  tblMarketPrices
  tblMarketPriceTypes
  tblMeeting
  tblMeetingPeople
  tblMeetingStatus
  tblMonthlyComment

  tblNaplesErrorLog

	tblPendingTransactions
  tblPeriod
  tblPerson
  tblPertracCustomFields
  tblPertracCustomFieldData
  tblPertracFieldMapping
  tblPFPCCustodyRecord
  tblPFPCPortfolioReconcilliation
  tblPFPCPortfolioValuation
  tblPFPCShareAllocation
  tblPortfolioData
  tblPortfolioIndex
  tblPortfolioItemTypes
  tblPortfolioType
  tblPrice

  tblRecAvailablilities
  tblRecHoldings
  tblRecInventory

  tblReferentialIntegrity
  tblRelationship
  tblReport
  tblReportCustomisation
  tblReportDetailLog
  tblReportStatus
  tblReportType

  tblRiskData
  tblRiskDataCategory
	tblRiskDataCharacteristic
	tblRiskDataCompoundCharacteristic

	tblSavePortfolioDetails
	tblSectorLimit
	tblSelectCharacteristics
	tblSelectFuturesNotional
	tblSelectMeeting
	tblSelectTransaction
	tblSelectCompoundTransaction
	tblSophisStatusGroups
	tblSubFund
  tblSubFundHeirarchy
  tblSystemDates
	tblSystemStrings

	tblTradeFileAcknowledgement
  tblTradeFileEmsxStatus
  tblTradeFileExecutionReporting
	tblTradeFilePending
	tblTradeStatus
  tblTradeStatusGroup
	tblTradeStatusGroupItems
  tblTransaction
  tblTransactionContra
	tblTransactionType

  tblUnitHolderFees
  tblUserPermissions

  tblVeniceFormTooltips

  tblWorkflows
	tblWorkflowRules

  ' Entries to help track changes to Administrator Data

  Administrator_Holdings
  Administrator_Disponsbilities

  ' Known status messages

  KansasStatusMessage

  ' Pertrac Tables :-

  Mastername
  Information
  Performance
  InstrumentPerformance

  ' Non Table Related Updates :-

  KnowledgeDate
  Connection
  StatsCacheRefresh

  ' Terminate Message
  Terminate1
  Terminate2

  ' Max Value. MUST be the last member.
  MaxValue

End Enum

' Permission Usertype Enumeration. To match Legacy Venice.
Public Enum PermissionUserType As Integer
  TypeUser = 1
  TypeGroup = 2
End Enum

' Permission FeatureType Enumeration.
Public Enum PermissionFeatureType
  TypeForm = 1
  TypeReport = 2
  TypeSystem = 5
  TypeGroup = 10
  TypeUser = 20
End Enum

' Permission Bitmap Enumeration.
Public Enum PermissionBitmap
  PermNone = 0
  PermRead = 1
  PermUpdate = 2
  PermInsert = 4
  PermDelete = 8
End Enum

' EventArgument object used to report changes to tables
Public Class RenaissanceUpdateEventArgs
  ' EventArgument object used to report changes to tables
  '
  ' A single event argument object can be used to indicate that any number of Tables / Items has changed
  ' There is a strong relationship between this EventArgs object and the RenaissanceTableID enumeration.
  ' There is no particualr limit to the Size of the Enumeration not the number of 'Flags' available
  ' to this Event Object.
  ' This Evemt may well be used for update events that are not related to the underlying tables.
  '
  Inherits EventArgs

  ' Array used to record which tables have been updated.
  Private _ChangedTables(RenaissanceChangeID.MaxValue) As Boolean
  Private _UpdateDetail(RenaissanceChangeID.MaxValue) As String

  ' Basic Constructor
  Public Sub New()
    MyBase.new()
  End Sub

  ' Constructor that also sets the initial Table-Changed value
  Public Sub New(ByVal pTableID As RenaissanceChangeID)
    MyBase.new()
    _ChangedTables(pTableID) = True
  End Sub

	Public Sub New(ByVal pTableID As RenaissanceChangeID, ByVal pUpdateDetail As String)
		MyBase.new()
		_ChangedTables(pTableID) = True
		_UpdateDetail(pTableID) = pUpdateDetail
	End Sub

  ' Event Object property used to Set or Get the 'Changed' status of an underlying table.
  Public Property TableChanged(ByVal pTableID As RenaissanceChangeID) As Boolean
    ' Get the underlying status.
    ' Simply the relevant value for the _ChangedTables() Array
    Get
      If (pTableID >= 0) And (pTableID < _ChangedTables.GetUpperBound(0)) Then
        Return _ChangedTables(pTableID)
      Else
        Return False
      End If

    End Get

    ' Set the underlying status.
    ' Simply set the relevant value for the _ChangedTables() Array
    Set(ByVal pSetValue As Boolean)
      If (pTableID >= 0) And (pTableID < _ChangedTables.GetUpperBound(0)) Then
        _ChangedTables(pTableID) = pSetValue
      End If

    End Set
  End Property

  ' Event Object property used to Set or Get the 'Change Detail' of an underlying table.
	Public Property UpdateDetail(ByVal pTableID As RenaissanceChangeID) As String
		' Get the underlying status.
		' Simply the relevant value for the _ChangedTables() Array
		Get
			Dim RVal As String = ""

			Try
				If (pTableID >= 0) And (pTableID < _ChangedTables.GetUpperBound(0)) Then
					If (_UpdateDetail IsNot Nothing) AndAlso (_UpdateDetail.Length > pTableID) AndAlso (_UpdateDetail(pTableID) IsNot Nothing) Then
						RVal = _UpdateDetail(pTableID)
					End If
				End If
			Catch ex As Exception
				RVal = ""
			End Try

			Return RVal
		End Get

		' Set the underlying status.
		' Simply set the relevant value for the _ChangedTables() Array
		Set(ByVal pSetValue As String)
			If (pTableID >= 0) And (pTableID < _ChangedTables.GetUpperBound(0)) Then
				If (_UpdateDetail Is Nothing) Then
					ReDim _UpdateDetail(_ChangedTables.Length - 1)
				ElseIf (_UpdateDetail.Length <= pTableID) Then
					ReDim Preserve _UpdateDetail(_ChangedTables.Length - 1)
				End If

				If (_UpdateDetail IsNot Nothing) AndAlso (_UpdateDetail.Length > pTableID) Then
					_UpdateDetail(pTableID) = pSetValue
				End If

			End If
		End Set
	End Property

  Public ReadOnly Property TablesChanged() As RenaissanceChangeID()
    Get
      Dim ReturnArray() As RenaissanceChangeID
      Dim Counter As Integer
      Dim TableCount As Integer

      TableCount = 0
      For Counter = 1 To (_ChangedTables.GetLength(0) - 1)
        If _ChangedTables(Counter) = True Then
          TableCount += 1
        End If
      Next

      If (TableCount > 0) Then
        ReDim ReturnArray(TableCount - 1)
        TableCount = 0

        For Counter = 1 To (_ChangedTables.GetLength(0) - 1)
          If _ChangedTables(Counter) = True Then
            Try
              ReturnArray(TableCount) = CType(Counter, RenaissanceChangeID)
              TableCount += 1
            Catch ex As Exception
            End Try
          End If
        Next
      Else
        ReDim ReturnArray(0)
        ReturnArray(0) = RenaissanceChangeID.None
      End If

      Return ReturnArray
    End Get
  End Property

  Public ReadOnly Property UpdateDetails() As String()
    Get
      Dim ReturnArray() As String
      Dim Counter As Integer
      Dim TableCount As Integer

      TableCount = 0
      For Counter = 1 To (_ChangedTables.GetLength(0) - 1)
        If _ChangedTables(Counter) = True Then
          TableCount += 1
        End If
      Next

      If (TableCount > 0) Then
        ReDim ReturnArray(TableCount - 1)
        TableCount = 0

        For Counter = 1 To (_ChangedTables.GetLength(0) - 1)
          If _ChangedTables(Counter) = True Then
            Try
              If (_UpdateDetail(Counter) Is Nothing) Then
                ReturnArray(TableCount) = ""
              Else
                ReturnArray(TableCount) = _UpdateDetail(Counter)
              End If
              TableCount += 1
            Catch ex As Exception
            End Try
          End If
        Next
      Else
        ReDim ReturnArray(0)
        ReturnArray(0) = ""
      End If

      Return ReturnArray
    End Get
  End Property

End Class

' TimerUpdateClass : Used to organise to timed update of forms.
Public Class RenaissanceTimerUpdateClass
  ' ******************************************************************************************
  ' This class is designed to control the Timed-Update of Renaissance forms.
  ' For forms where the time required for the 'GetFormData()' function affects the 
  ' Select-As-You-Type functionallity, or where some Time Consuming function results from a control selection
  ' then it is possible to set up a time delayed update process for the form.
  ' There may be multiple Update classes associated with different functions of the form, or a single update class 
  ' could be used to marshal differing update processes.
  ' 
  ' See Genoa or Sienna for examples of it's use.
  ' ******************************************************************************************

  Public Delegate Function UpdateProcessDelegate() As Boolean

  Private _UpdateTimeLock As Object

  Private _FormToUpdate As Boolean
  Private _FormToUpdateCounter As Integer

  Private _UpdateRequestTime As Date
  Private _FormUpdateProcess As UpdateProcessDelegate
  Private _UpdateTelayTicks As Integer
  Private _InUpdate As Boolean
  Private _Default_UpdateDelayTicks As Integer

  Private Sub New()
    _UpdateTimeLock = New Object
    _FormToUpdate = False
    _FormToUpdateCounter = 0
    _UpdateRequestTime = Now()
    _FormUpdateProcess = Nothing
    _Default_UpdateDelayTicks = 500
    _UpdateTelayTicks = _Default_UpdateDelayTicks
    _InUpdate = False
  End Sub

  Public Property UpdateTelayTicks() As Integer
    Get
      Return _UpdateTelayTicks
    End Get
    Set(ByVal value As Integer)
      If (value > 0) Then
        _UpdateTelayTicks = value
      End If
    End Set
  End Property

  Public Sub New(ByVal pUpdateProcess As UpdateProcessDelegate)
    Me.New()

    _FormUpdateProcess = pUpdateProcess
  End Sub

  Public Sub New(ByVal pUpdateProcess As UpdateProcessDelegate, ByVal pUpdateDelayInTicks As Integer)
    Me.New()

    _UpdateTelayTicks = pUpdateDelayInTicks
    _FormUpdateProcess = pUpdateProcess
  End Sub

  Public Sub Clear()
    SyncLock _UpdateTimeLock
      _FormToUpdate = False
      _UpdateRequestTime = Now()
      _FormUpdateProcess = Nothing
      _UpdateTelayTicks = _Default_UpdateDelayTicks
      _InUpdate = False
    End SyncLock
  End Sub

  Public Property FormToUpdate() As Boolean
    Get
      SyncLock _UpdateTimeLock
        Return _FormToUpdate
      End SyncLock
    End Get
    Set(ByVal value As Boolean)
      SyncLock _UpdateTimeLock
        Try

          If (value = False) AndAlso ((Now() - _UpdateRequestTime).TotalMilliseconds < (_UpdateTelayTicks / 4.0#)) Then
            Exit Property
          End If

          _FormToUpdate = value

          If (value) Then
            _UpdateRequestTime = Now
            _FormToUpdateCounter = 1
          End If

        Catch ex As Exception
        End Try

      End SyncLock
    End Set
  End Property

  Public Function CheckUpdate() As Boolean

    If (_InUpdate) Or (Not _FormToUpdate) Then
      Return False
    End If

    SyncLock _UpdateTimeLock
      If (_FormToUpdate) AndAlso ((Now() - _UpdateRequestTime).TotalMilliseconds > _UpdateTelayTicks) Then
        Try
          _InUpdate = True

          If (_FormUpdateProcess IsNot Nothing) Then

            ' _FormToUpdateCounter is used to handle the situation where FormToUpdate() is set whilst _FormUpdateProcess() is running.

            _FormToUpdateCounter = 0

            If _FormUpdateProcess() Then

              If (_FormToUpdateCounter <= 0) Then
                FormToUpdate = False
              End If

            Else

              _FormToUpdateCounter = 1

            End If
          Else
            FormToUpdate = False
          End If

        Catch ex As Exception
        Finally
          _InUpdate = False
        End Try

        Return True
      Else
        Return False
      End If
    End SyncLock

  End Function

End Class

#Region " Standard Adaptor Name / Dataset Name / Table Name & ChangeID details."

' Data Structure used by 'RenaissanceStandardDatasets' Class
Public Class StandardDataset
  ' Facilitator class for the 'RenaissanceStandardDatasets' Class.

  Private _AdaptorName As String
  Private _DatasetName As String
  Private _TableName As String
  Private _ChangeID As RenaissanceChangeID
  Private _IsKnowledgeDated As Boolean ' Is this Dataset affected by changes to the Knowledgedate.
  Private _ChangeIDsToTriggerUpdate() As RenaissanceChangeID
	Private _ChangeIDsToPropogateDetailsFrom() As RenaissanceChangeID	' If one of these tables updates, with update details, pass those details down to this table too.
	Private _ChangeIDsToNote() As RenaissanceChangeID
  Private _NotedIDsHaveChanged As Boolean
  Private _UpdateOrder As Integer
  Private _ActivityCount As Integer

  Public Sub New(ByVal pTableName As String, ByVal pChangeID As RenaissanceChangeID)
    Call Me.New(pTableName, pChangeID, True, Nothing, 0, Nothing, Nothing)
  End Sub

  Public Sub New(ByVal pTableName As String, ByVal pChangeID As RenaissanceChangeID, ByVal pIsKnowledgeDated As Boolean)
    Call Me.New(pTableName, pChangeID, pIsKnowledgeDated, Nothing, 0, Nothing, Nothing)
  End Sub

  Public Sub New(ByVal pTableName As String, ByVal pChangeID As RenaissanceChangeID, ByVal pIsKnowledgeDated As Boolean, ByVal pChangeIDsToTriggerUpdate() As RenaissanceChangeID, ByVal pUpdateOrder As Integer, ByVal pChangeIDsToNote() As RenaissanceChangeID)
    Call Me.New(pTableName, pChangeID, pIsKnowledgeDated, pChangeIDsToTriggerUpdate, pUpdateOrder, pChangeIDsToNote, Nothing)
  End Sub

  Public Sub New(ByVal pTableName As String, ByVal pChangeID As RenaissanceChangeID, ByVal pIsKnowledgeDated As Boolean, ByVal pChangeIDsToTriggerUpdate() As RenaissanceChangeID, ByVal pUpdateOrder As Integer, ByVal pChangeIDsToNote() As RenaissanceChangeID, ByVal pChangeIDsToPropogateDetailsFrom() As RenaissanceChangeID)
    Me._AdaptorName = "Adp" & pTableName
    Me._DatasetName = "Ds" & pTableName
    Me._TableName = "tbl" & pTableName
    Me._ChangeID = pChangeID
    Me._IsKnowledgeDated = pIsKnowledgeDated
    Me._ChangeIDsToTriggerUpdate = pChangeIDsToTriggerUpdate
    Me._ChangeIDsToPropogateDetailsFrom = pChangeIDsToPropogateDetailsFrom
    Me._ChangeIDsToNote = pChangeIDsToNote
    Me._NotedIDsHaveChanged = False
    Me._UpdateOrder = pUpdateOrder
    Me._ActivityCount = 0
  End Sub

  Public ReadOnly Property Adaptorname() As String
    Get
      Return _AdaptorName
    End Get
  End Property

  Public ReadOnly Property DatasetName() As String
    Get
      Return _DatasetName
    End Get
  End Property

  Public ReadOnly Property TableName() As String
    Get
      Return _TableName
    End Get
  End Property

  Public ReadOnly Property ChangeID() As RenaissanceChangeID
    Get
      Return _ChangeID
    End Get
  End Property

  Public ReadOnly Property IsKnowledgeDated() As Boolean
    Get
      Return _IsKnowledgeDated
    End Get
  End Property

  Public ReadOnly Property ChangeIDsToTriggerUpdate() As RenaissanceChangeID()
    Get
      Return _ChangeIDsToTriggerUpdate
    End Get
  End Property

  Public ReadOnly Property ChangeIDsToNote() As RenaissanceChangeID()
    Get
      Return _ChangeIDsToNote
    End Get
  End Property

  Public ReadOnly Property IsChangeIDToTriggerUpdate(ByVal IDValue As RenaissanceChangeID) As Boolean
    Get
      Try
        If (_ChangeIDsToTriggerUpdate Is Nothing) OrElse (_ChangeIDsToTriggerUpdate.Length <= 0) Then
          Return False
        End If
      Catch ex As Exception
        Return False
      End Try

      Dim ChangeCounter As Integer
      For ChangeCounter = 0 To (_ChangeIDsToTriggerUpdate.Length - 1)
        If (_ChangeIDsToTriggerUpdate(ChangeCounter) = IDValue) Then
          Return True
        End If
      Next

      Return False
    End Get
  End Property

	Public ReadOnly Property IsChangeIdToPropogateDetailsFrom(ByVal IDValue As RenaissanceChangeID) As Boolean
		Get
			Try
				If (_ChangeIDsToPropogateDetailsFrom Is Nothing) OrElse (_ChangeIDsToPropogateDetailsFrom.Length <= 0) Then
					Return False
				End If
			Catch ex As Exception
				Return False
			End Try

			Dim ChangeCounter As Integer
			For ChangeCounter = 0 To (_ChangeIDsToPropogateDetailsFrom.Length - 1)
				If (_ChangeIDsToPropogateDetailsFrom(ChangeCounter) = IDValue) Then
					Return True
				End If
			Next

			Return False
		End Get
	End Property

  Public ReadOnly Property ActivityCount() As Integer
    Get
      Return Me._ActivityCount
    End Get
  End Property

  Public ReadOnly Property UpdateOrder() As Integer
    Get
      Return Me._UpdateOrder
    End Get
  End Property

  Public Function ActivityCount_Increment() As Integer
    If Me._ActivityCount < Integer.MaxValue Then
      Me._ActivityCount += 1
    End If

    Return Me._ActivityCount
  End Function

  Public Function ActivityCount_Decrement() As Integer
    If Me._ActivityCount > 0 Then
      Me._ActivityCount -= 1
    End If

    Return Me._ActivityCount
  End Function

  Public Function ISChangeIDToNote(ByVal IDValue As RenaissanceChangeID) As Boolean
    Try
      If (_ChangeIDsToNote Is Nothing) OrElse (_ChangeIDsToNote.Length <= 0) Then
        Return False
      End If
    Catch ex As Exception
      Return False
    End Try

    Dim ChangeCounter As Integer
    For ChangeCounter = 0 To (_ChangeIDsToNote.Length - 1)
      If (_ChangeIDsToNote(ChangeCounter) = IDValue) Then
        _NotedIDsHaveChanged = True
        Return True
      End If
    Next

    Return False
  End Function

  Public Property NotedIDsHaveChanged() As Boolean
    Get
      Return _NotedIDsHaveChanged
    End Get
    Set(ByVal Value As Boolean)
      _NotedIDsHaveChanged = Value
    End Set
  End Property
End Class

' Collection of Standard Dataset / Adaptor / TableNames / ChangeIDs

Public Class StandardDatasetsComparer
  Implements IComparer

  Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
    ' Order by UpdateOrder, ChangeID
    Dim X_Dataset As StandardDataset
    Dim Y_Dataset As StandardDataset

    Dim RVal As Integer = 0

    Try
      If (TypeOf x Is StandardDataset) Then
        X_Dataset = CType(x, StandardDataset)
      Else
        Return 0
      End If

      If (TypeOf y Is StandardDataset) Then
        Y_Dataset = CType(y, StandardDataset)
      Else
        Return 0
      End If

      RVal = X_Dataset.UpdateOrder.CompareTo(Y_Dataset.UpdateOrder)

      If RVal = 0 Then
        RVal = X_Dataset.ChangeID.CompareTo(Y_Dataset.ChangeID)
      End If

    Catch ex As Exception

      Return 0
    End Try

    Return RVal

  End Function
End Class

Public Class RenaissanceStandardDatasets
  ' A Class designed to hold, ReadOnly, the Standard Tablenames with their Standard Adaptor Name, 
  ' Dataset Name and Change ID.
  '
  ' Based on the 'StandardDataset' Class.
  '
  ' The Details for each fund are held in PRIVATE locals and are exposed via ReadOnly Properties.
  ' Once Insantiated, this gives a readonly collection of Table etc. Details.
  '
  ' Is there a better way to do this ?
  '
  Private Shared _tblAttribution As StandardDataset = New StandardDataset("Attribution", RenaissanceChangeID.tblAttribution)
  Private Shared _tblAutoUpdate As StandardDataset = New StandardDataset("AutoUpdate", RenaissanceChangeID.tblAutoUpdate, False)
	Private Shared _tblBenchmarkIndex As StandardDataset = New StandardDataset("BenchmarkIndex", RenaissanceChangeID.tblBenchmarkIndex, True)
	Private Shared _tblBenchmarkItems As StandardDataset = New StandardDataset("BenchmarkItems", RenaissanceChangeID.tblBenchmarkItems, True)
  Private Shared _tblBestFX As StandardDataset = New StandardDataset("BestFX", RenaissanceChangeID.tblBestFX, True, New RenaissanceChangeID() {RenaissanceChangeID.tblFX}, 0, Nothing)
  Private Shared _tblBestMarketPrice As StandardDataset = New StandardDataset("BestMarketPrice", RenaissanceChangeID.tblBestMarketPrice, True, New RenaissanceChangeID() {RenaissanceChangeID.tblMarketPrices}, 0, Nothing)
  Private Shared _tblBookmark As StandardDataset = New StandardDataset("Bookmark", RenaissanceChangeID.tblBookmark)
	Private Shared _tblBroker As StandardDataset = New StandardDataset("Broker", RenaissanceChangeID.tblBroker)
	Private Shared _tblBrokerAccounts As StandardDataset = New StandardDataset("BrokerAccounts", RenaissanceChangeID.tblBrokerAccounts)
	Private Shared _tblChangeControl As StandardDataset = New StandardDataset("ChangeControl", RenaissanceChangeID.tblChangeControl)
  Private Shared _tblCompoundTransaction As StandardDataset = New StandardDataset("CompoundTransaction", RenaissanceChangeID.tblCompoundTransaction)
  Private Shared _tblCompoundTransactionParameters As StandardDataset = New StandardDataset("CompoundTransactionParameters", RenaissanceChangeID.tblCompoundTransactionParameters)
  Private Shared _tblCompoundTransactionTemplate As StandardDataset = New StandardDataset("CompoundTransactionTemplate", RenaissanceChangeID.tblCompoundTransactionTemplate)
  Private Shared _tblCounterparty As StandardDataset = New StandardDataset("Counterparty", RenaissanceChangeID.tblCounterparty)
  Private Shared _tblCovariance As StandardDataset = New StandardDataset("Covariance", RenaissanceChangeID.tblCovariance)
  Private Shared _tblCovarianceList As StandardDataset = New StandardDataset("CovarianceList", RenaissanceChangeID.tblCovarianceList)
  Private Shared _tblCTA_Simulation As StandardDataset = New StandardDataset("CTA_Simulation", RenaissanceChangeID.tblCTA_Simulation)
  Private Shared _tblCTA_Folders As StandardDataset = New StandardDataset("CTA_Folders", RenaissanceChangeID.tblCTA_Folders)
  Private Shared _tblCTA_SimulationGroupList As StandardDataset = New StandardDataset("CTA_SimulationGroupList", RenaissanceChangeID.tblCTA_SimulationGroupList)
  Private Shared _tblCTA_SimulationGroupItems As StandardDataset = New StandardDataset("CTA_SimulationGroupItems", RenaissanceChangeID.tblCTA_SimulationGroupItems)
  Private Shared _tblCurrency As StandardDataset = New StandardDataset("Currency", RenaissanceChangeID.tblCurrency)
  Private Shared _tblDataDictionary_TransactionFormLabels As StandardDataset = New StandardDataset("DataDictionary_TransactionFormLabels", RenaissanceChangeID.tblDataDictionary_TransactionFormLabels)
  Private Shared _tblDataProviderInstrumentType As StandardDataset = New StandardDataset("DataProviderInstrumentType", RenaissanceChangeID.tblDataProviderInstrumentType)
  Private Shared _tblDataTask As StandardDataset = New StandardDataset("DataTask", RenaissanceChangeID.tblDataTask)
  Private Shared _tblDataTaskType As StandardDataset = New StandardDataset("DataTaskType", RenaissanceChangeID.tblDataTaskType)
  Private Shared _tblDealingPeriod As StandardDataset = New StandardDataset("DealingPeriod", RenaissanceChangeID.tblDealingPeriod)
  Private Shared _tblErrorLog As StandardDataset = New StandardDataset("INVEST_ERROR_LOG", RenaissanceChangeID.tblErrorLog, False)
  Private Shared _tblEstimate As StandardDataset = New StandardDataset("Estimate", RenaissanceChangeID.tblEstimate)
  Private Shared _tblExposure As StandardDataset = New StandardDataset("Exposure", RenaissanceChangeID.tblExposure)
  Private Shared _tblFeeDefinitions As StandardDataset = New StandardDataset("FeeDefinitions", RenaissanceChangeID.tblFeeDefinitions)
  Private Shared _tblFlorenceDataValues As StandardDataset = New StandardDataset("FlorenceDataValues", RenaissanceChangeID.tblFlorenceDataValues)
  Private Shared _tblFlorenceEntity As StandardDataset = New StandardDataset("FlorenceEntity", RenaissanceChangeID.tblFlorenceEntity)
  Private Shared _tblFlorenceGroup As StandardDataset = New StandardDataset("FlorenceGroup", RenaissanceChangeID.tblFlorenceGroup)
  Private Shared _tblFlorenceItems As StandardDataset = New StandardDataset("FlorenceItems", RenaissanceChangeID.tblFlorenceItems)
  Private Shared _tblFlorenceStatus As StandardDataset = New StandardDataset("FlorenceStatus", RenaissanceChangeID.tblFlorenceStatus)
  Private Shared _tblFlorenceStatusIDs As StandardDataset = New StandardDataset("FlorenceStatusIDs", RenaissanceChangeID.tblFlorenceStatusIDs)
  Private Shared _tblFlorenceWorkItems As StandardDataset = New StandardDataset("FlorenceWorkItems", RenaissanceChangeID.tblFlorenceWorkItems, True, New RenaissanceChangeID() {RenaissanceChangeID.tblFlorenceEntity, RenaissanceChangeID.tblFlorenceItems, RenaissanceChangeID.tblFlorenceStatus, RenaissanceChangeID.tblFlorenceStatusIDs}, 0, New RenaissanceChangeID() {})
  Private Shared _tblFlorenceWorkItemsData As StandardDataset = New StandardDataset("FlorenceWorkItemsData", RenaissanceChangeID.tblFlorenceWorkItems, True, New RenaissanceChangeID() {RenaissanceChangeID.tblFlorenceEntity, RenaissanceChangeID.tblFlorenceItems, RenaissanceChangeID.tblFlorenceDataValues, RenaissanceChangeID.tblFlorenceStatusIDs}, 1, New RenaissanceChangeID() {})
  Private Shared _tblFund As StandardDataset = New StandardDataset("Fund", RenaissanceChangeID.tblFund)
  Private Shared _tblFundContactDetails As StandardDataset = New StandardDataset("FundContactDetails", RenaissanceChangeID.tblFundContactDetails)
  Private Shared _tblFundMilestones As StandardDataset = New StandardDataset("FundMilestones", RenaissanceChangeID.tblFundMilestones)
  Private Shared _tblFundPerformanceFeePoints As StandardDataset = New StandardDataset("FundPerformanceFeePoints", RenaissanceChangeID.tblFundPerformanceFeePoints)
  Private Shared _tblFundType As StandardDataset = New StandardDataset("FundType", RenaissanceChangeID.tblFundType)
  Private Shared _tblFX As StandardDataset = New StandardDataset("FX", RenaissanceChangeID.tblFX)
  Private Shared _tblGenoaConstraintList As StandardDataset = New StandardDataset("GenoaConstraintList", RenaissanceChangeID.tblGenoaConstraintList)
  Private Shared _tblGenoaConstraintItems As StandardDataset = New StandardDataset("GenoaConstraintItems", RenaissanceChangeID.tblGenoaConstraintItems)
  Private Shared _tblGenoaSavedSearchList As StandardDataset = New StandardDataset("GenoaSavedSearchList", RenaissanceChangeID.tblGenoaSavedSearchList)
  Private Shared _tblGenoaSavedSearchItems As StandardDataset = New StandardDataset("GenoaSavedSearchItems", RenaissanceChangeID.tblGenoaSavedSearchItems)
	Private Shared _tblGreeks As StandardDataset = New StandardDataset("Greeks", RenaissanceChangeID.tblGreeks)
	Private Shared _tblGroupList As StandardDataset = New StandardDataset("GroupList", RenaissanceChangeID.tblGroupList)
  Private Shared _tblGroupItems As StandardDataset = New StandardDataset("GroupItems", RenaissanceChangeID.tblGroupItems)
  Private Shared _tblGroupItemData As StandardDataset = New StandardDataset("GroupItemData", RenaissanceChangeID.tblGroupItemData)
  Private Shared _tblGroupsAggregated As StandardDataset = New StandardDataset("GroupsAggregated", RenaissanceChangeID.tblGroupItemData, True, New RenaissanceChangeID() {RenaissanceChangeID.tblGroupList, RenaissanceChangeID.tblGroupItems, RenaissanceChangeID.tblGroupItemData}, 0, New RenaissanceChangeID() {RenaissanceChangeID.tblGroupItems, RenaissanceChangeID.tblGroupItemData}, New RenaissanceChangeID() {RenaissanceChangeID.tblGroupItems, RenaissanceChangeID.tblGroupItemData})
  Private Shared _tblIndexMapping As StandardDataset = New StandardDataset("IndexMapping", RenaissanceChangeID.tblIndexMapping)
  Private Shared _tblInstrument As StandardDataset = New StandardDataset("Instrument", RenaissanceChangeID.tblInstrument)
  Private Shared _tblInstrumentNotes As StandardDataset = New StandardDataset("InstrumentNotes", RenaissanceChangeID.tblInstrumentNotes)
  Private Shared _tblInstrumentFlorenceEntityLink As StandardDataset = New StandardDataset("InstrumentFlorenceEntityLink", RenaissanceChangeID.tblInstrumentFlorenceEntityLink)
  Private Shared _tblInstrumentNamesGrossUnits As StandardDataset = New StandardDataset("InstrumentNamesGrossUnits", RenaissanceChangeID.tblInstrumentNamesGrossUnits, True, New RenaissanceChangeID() {RenaissanceChangeID.tblInstrument}, 0, Nothing)
  Private Shared _tblInstrumentType As StandardDataset = New StandardDataset("InstrumentType", RenaissanceChangeID.tblInstrumentType)
  Private Shared _tblInvestorGroup As StandardDataset = New StandardDataset("InvestorGroup", RenaissanceChangeID.tblInvestorGroup)
  Private Shared _tblJob As StandardDataset = New StandardDataset("Job", RenaissanceChangeID.tblJob)
  Private Shared _tblLimits As StandardDataset = New StandardDataset("Limits", RenaissanceChangeID.tblLimits)
  Private Shared _tblLimitType As StandardDataset = New StandardDataset("LimitType", RenaissanceChangeID.tblLimitType)
  Private Shared _tblLiquidityLimit As StandardDataset = New StandardDataset("LiquidityLimit", RenaissanceChangeID.tblLiquidityLimit)
  Private Shared _tblLocations As StandardDataset = New StandardDataset("Locations", RenaissanceChangeID.tblLocations)
  Private Shared _tblManagementCompany As StandardDataset = New StandardDataset("ManagementCompany", RenaissanceChangeID.tblManagementCompany)
  Private Shared _tblMarketInstruments As StandardDataset = New StandardDataset("MarketInstruments", RenaissanceChangeID.tblMarketInstruments)
  Private Shared _tblMarketPrices As StandardDataset = New StandardDataset("MarketPrices", RenaissanceChangeID.tblMarketPrices)
  Private Shared _tblMeeting As StandardDataset = New StandardDataset("Meeting", RenaissanceChangeID.tblMeeting)
  Private Shared _tblMeetingStatus As StandardDataset = New StandardDataset("MeetingStatus", RenaissanceChangeID.tblMeetingStatus)
  Private Shared _tblMeetingPeople As StandardDataset = New StandardDataset("MeetingPeople", RenaissanceChangeID.tblMeetingPeople)
  Private Shared _tblMonthlyComment As StandardDataset = New StandardDataset("MonthlyComment", RenaissanceChangeID.tblMonthlyComment)
  Private Shared _tblPeriod As StandardDataset = New StandardDataset("Period", RenaissanceChangeID.tblPeriod)
	Private Shared _tblPendingTransactions As StandardDataset = New StandardDataset("PendingTransactions", RenaissanceChangeID.tblPendingTransactions)
	Private Shared _tblPerson As StandardDataset = New StandardDataset("Person", RenaissanceChangeID.tblPerson)
  Private Shared _tblPertracCustomFields As StandardDataset = New StandardDataset("PertracCustomFields", RenaissanceChangeID.tblPertracCustomFields)
  Private Shared _tblPertracCustomFieldData As StandardDataset = New StandardDataset("PertracCustomFieldData", RenaissanceChangeID.tblPertracCustomFieldData)
  Private Shared _tblPertracFieldMapping As StandardDataset = New StandardDataset("PertracFieldMapping", RenaissanceChangeID.tblPertracFieldMapping)
  Private Shared _tblPFPCCustodyRecord As StandardDataset = New StandardDataset("PFPCCustodyRecord", RenaissanceChangeID.tblPFPCCustodyRecord, False)
  Private Shared _tblPFPCPortfolioReconcilliation As StandardDataset = New StandardDataset("PFPCPortfolioReconcilliation", RenaissanceChangeID.tblPFPCPortfolioReconcilliation, False)
  Private Shared _tblPFPCPortfolioValuation As StandardDataset = New StandardDataset("PFPCPortfolioValuation", RenaissanceChangeID.tblPFPCPortfolioValuation, False)
  Private Shared _tblPFPCShareAllocation As StandardDataset = New StandardDataset("PFPCShareAllocation", RenaissanceChangeID.tblPFPCShareAllocation)
  Private Shared _tblPortfolioData As StandardDataset = New StandardDataset("PortfolioData", RenaissanceChangeID.tblPortfolioData)
  Private Shared _tblPortfolioIndex As StandardDataset = New StandardDataset("PortfolioIndex", RenaissanceChangeID.tblPortfolioIndex)
  Private Shared _tblPortfolioItemTypes As StandardDataset = New StandardDataset("PortfolioItemTypes", RenaissanceChangeID.tblPortfolioItemTypes)
  Private Shared _tblPortfolioType As StandardDataset = New StandardDataset("PortfolioType", RenaissanceChangeID.tblPortfolioType)
  Private Shared _tblPrice As StandardDataset = New StandardDataset("Price", RenaissanceChangeID.tblPrice)
  Private Shared _tblRecAvailablilities As StandardDataset = New StandardDataset("RecAvailablilities", RenaissanceChangeID.tblRecAvailablilities)
  Private Shared _tblReferentialIntegrity As StandardDataset = New StandardDataset("ReferentialIntegrity", RenaissanceChangeID.tblReferentialIntegrity)
  Private Shared _tblRelationship As StandardDataset = New StandardDataset("Relationship", RenaissanceChangeID.tblRelationship, False)
  Private Shared _tblReport As StandardDataset = New StandardDataset("Report", RenaissanceChangeID.tblReport)
  Private Shared _tblReportCustomisation As StandardDataset = New StandardDataset("ReportCustomisation", RenaissanceChangeID.tblReportCustomisation)
  Private Shared _tblReportDetailLog As StandardDataset = New StandardDataset("ReportDetailLog", RenaissanceChangeID.tblReportDetailLog)
  Private Shared _tblReportStatus As StandardDataset = New StandardDataset("ReportStatus", RenaissanceChangeID.tblReportStatus)
  Private Shared _tblReportType As StandardDataset = New StandardDataset("ReportType", RenaissanceChangeID.tblReportType)
  Private Shared _tblRiskData As StandardDataset = New StandardDataset("RiskData", RenaissanceChangeID.tblRiskData)
  Private Shared _tblRiskDataCategory As StandardDataset = New StandardDataset("RiskDataCategory", RenaissanceChangeID.tblRiskDataCategory)
	Private Shared _tblRiskDataCharacteristic As StandardDataset = New StandardDataset("RiskDataCharacteristic", RenaissanceChangeID.tblRiskDataCharacteristic)
	Private Shared _tblRiskDataCompoundCharacteristic As StandardDataset = New StandardDataset("RiskDataCompoundCharacteristic", RenaissanceChangeID.tblRiskDataCompoundCharacteristic)
  Private Shared _tblSelectCharacteristics As StandardDataset = New StandardDataset("SelectCharacteristics", RenaissanceChangeID.tblSelectCharacteristics, True, New RenaissanceChangeID() {RenaissanceChangeID.tblRiskData, RenaissanceChangeID.tblRiskDataCategory, RenaissanceChangeID.tblRiskDataCharacteristic, RenaissanceChangeID.tblRiskDataCompoundCharacteristic}, 0, New RenaissanceChangeID() {})
  Private Shared _tblSavePortfolioDetails As StandardDataset = New StandardDataset("SavePortfolioDetails", RenaissanceChangeID.tblSavePortfolioDetails)
  Private Shared _tblSectorLimit As StandardDataset = New StandardDataset("SectorLimit", RenaissanceChangeID.tblSectorLimit)
  Private Shared _tblSelectFuturesNotional As StandardDataset = New StandardDataset("SelectFuturesNotional", RenaissanceChangeID.tblSelectFuturesNotional, True, New RenaissanceChangeID() {RenaissanceChangeID.tblTransaction, RenaissanceChangeID.tblInstrumentType}, 10, New RenaissanceChangeID() {})
  Private Shared _tblSelectMeeting As StandardDataset = New StandardDataset("SelectMeeting", RenaissanceChangeID.tblSelectMeeting, True, New RenaissanceChangeID() {RenaissanceChangeID.tblMeeting}, 0, New RenaissanceChangeID() {RenaissanceChangeID.tblInstrument, RenaissanceChangeID.tblManagementCompany, RenaissanceChangeID.tblMeetingPeople, RenaissanceChangeID.tblFundType, RenaissanceChangeID.Mastername})
  Private Shared _tblSelectTransaction As StandardDataset = New StandardDataset("SelectTransaction", RenaissanceChangeID.tblSelectTransaction, True, New RenaissanceChangeID() {RenaissanceChangeID.tblTransaction}, 2, New RenaissanceChangeID() {RenaissanceChangeID.tblInstrument, RenaissanceChangeID.tblCounterparty, RenaissanceChangeID.tblFund}, New RenaissanceChangeID() {RenaissanceChangeID.tblTransaction})
  Private Shared _tblSelectCompoundTransaction As StandardDataset = New StandardDataset("SelectCompoundTransaction", RenaissanceChangeID.tblSelectCompoundTransaction, True, New RenaissanceChangeID() {RenaissanceChangeID.tblCompoundTransaction}, 3, New RenaissanceChangeID() {RenaissanceChangeID.tblInstrument, RenaissanceChangeID.tblCounterparty, RenaissanceChangeID.tblFund}, New RenaissanceChangeID() {RenaissanceChangeID.tblCompoundTransaction})
	Private Shared _tblSophisStatusGroups As StandardDataset = New StandardDataset("SophisStatusGroups", RenaissanceChangeID.tblSophisStatusGroups)
	Private Shared _tblSubFund As StandardDataset = New StandardDataset("SubFund", RenaissanceChangeID.tblSubFund)
  Private Shared _tblSubFundHeirarchy As StandardDataset = New StandardDataset("SubFundHeirarchy", RenaissanceChangeID.tblSubFundHeirarchy, True, New RenaissanceChangeID() {RenaissanceChangeID.tblSubFund}, 2, New RenaissanceChangeID() {}, New RenaissanceChangeID() {RenaissanceChangeID.tblSubFund})
  Private Shared _tblSystemDates As StandardDataset = New StandardDataset("SystemDates", RenaissanceChangeID.tblSystemDates)
  Private Shared _tblSystemStrings As StandardDataset = New StandardDataset("SystemStrings", RenaissanceChangeID.tblSystemStrings)
	Private Shared _tblTradeFileEmsxStatus As StandardDataset = New StandardDataset("TradeFileEmsxStatus", RenaissanceChangeID.tblTradeFileEmsxStatus)
  Private Shared _tblTradeFileExecutionReporting As StandardDataset = New StandardDataset("TradeFileExecutionReporting", RenaissanceChangeID.tblTradeFileExecutionReporting)
  Private Shared _tblTradeFileAcknowledgement As StandardDataset = New StandardDataset("TradeFileAcknowledgement", RenaissanceChangeID.tblTradeFileAcknowledgement)
	Private Shared _tblTradeFilePending As StandardDataset = New StandardDataset("TradeFilePending", RenaissanceChangeID.tblTradeFilePending)
	Private Shared _tblTradeStatus As StandardDataset = New StandardDataset("TradeStatus", RenaissanceChangeID.tblTradeStatus)
  Private Shared _tblTradeStatusGroup As StandardDataset = New StandardDataset("TradeStatusGroup", RenaissanceChangeID.tblTradeStatusGroup)
  Private Shared _tblTradeStatusGroupItems As StandardDataset = New StandardDataset("TradeStatusGroupItems", RenaissanceChangeID.tblTradeStatusGroupItems)
	Private Shared _tblTransaction As StandardDataset = New StandardDataset("Transaction", RenaissanceChangeID.tblTransaction)
  Private Shared _tblTransactionContra As StandardDataset = New StandardDataset("TransactionContra", RenaissanceChangeID.tblTransactionContra, True, New RenaissanceChangeID() {RenaissanceChangeID.tblTransaction}, 1, Nothing, New RenaissanceChangeID() {RenaissanceChangeID.tblTransaction})
  Private Shared _tblTransactionType As StandardDataset = New StandardDataset("TransactionType", RenaissanceChangeID.tblTransactionType)
  Private Shared _tblUnitHolderFees As StandardDataset = New StandardDataset("UnitHolderFees", RenaissanceChangeID.tblUnitHolderFees)
  Private Shared _tblUserPermissions As StandardDataset = New StandardDataset("UserPermissions", RenaissanceChangeID.tblUserPermissions)
	Private Shared _tblVeniceFormTooltips As StandardDataset = New StandardDataset("VeniceFormTooltips", RenaissanceChangeID.tblVeniceFormTooltips, False)
	Private Shared _tblWorkflowRules As StandardDataset = New StandardDataset("WorkflowRules", RenaissanceChangeID.tblWorkflowRules, False)
	Private Shared _tblWorkflows As StandardDataset = New StandardDataset("Workflows", RenaissanceChangeID.tblWorkflows, False)

  Private Shared _Mastername As StandardDataset = New StandardDataset("Mastername", RenaissanceChangeID.Mastername, False)
  Private Shared _Information As StandardDataset = New StandardDataset("Information", RenaissanceChangeID.Information, False)
  Private Shared _Performance As StandardDataset = New StandardDataset("Performance", RenaissanceChangeID.Performance, False)
  Private Shared _InstrumentPerformance As StandardDataset = New StandardDataset("InstrumentPerformance", RenaissanceChangeID.Performance, False)


  Private Sub New()
    ' A private `New` function stops the object being instantiated.
    ' The object is not designed to need to be instantiated, but should we explicitly stop people ?
  End Sub

  ' ReadOnly properties to Reveal the Local varaiables.

  Public Shared ReadOnly Property tblAttribution() As StandardDataset
    Get
      Return _tblAttribution
    End Get
  End Property

  Public Shared ReadOnly Property tblAutoUpdate() As StandardDataset
    Get
      Return _tblAutoUpdate
    End Get
  End Property

	Public Shared ReadOnly Property tblBenchmarkIndex() As StandardDataset
		Get
			Return _tblBenchmarkIndex
		End Get
	End Property

	Public Shared ReadOnly Property tblBenchmarkItems() As StandardDataset
		Get
			Return _tblBenchmarkItems
		End Get
	End Property

	Public Shared ReadOnly Property tblBestFX() As StandardDataset
		Get
			Return _tblBestFX
		End Get
	End Property

  Public Shared ReadOnly Property tblBestMarketPrice() As StandardDataset
    Get
      Return _tblBestMarketPrice
    End Get
  End Property

  Public Shared ReadOnly Property tblBookmark() As StandardDataset
    Get
      Return _tblBookmark
    End Get
  End Property

	Public Shared ReadOnly Property tblBroker() As StandardDataset
		Get
			Return _tblBroker
		End Get
	End Property

	Public Shared ReadOnly Property tblBrokerAccounts() As StandardDataset
		Get
			Return _tblBrokerAccounts
		End Get
	End Property

  Public Shared ReadOnly Property tblChangeControl() As StandardDataset
    Get
      Return _tblChangeControl
    End Get
  End Property

  Public Shared ReadOnly Property tblCompoundTransaction() As StandardDataset
    Get
      Return _tblCompoundTransaction
    End Get
  End Property

  Public Shared ReadOnly Property tblCompoundTransactionParameters() As StandardDataset
    Get
      Return _tblCompoundTransactionParameters
    End Get
  End Property

  Public Shared ReadOnly Property tblCompoundTransactionTemplate() As StandardDataset
    Get
      Return _tblCompoundTransactionTemplate
    End Get
  End Property

  Public Shared ReadOnly Property tblCounterparty() As StandardDataset
    Get
      Return _tblCounterparty
    End Get
  End Property

  Public Shared ReadOnly Property tblCovariance() As StandardDataset
    Get
      Return _tblCovariance
    End Get
  End Property

  Public Shared ReadOnly Property tblCovarianceList() As StandardDataset
    Get
      Return _tblCovarianceList
    End Get
  End Property

  Public Shared ReadOnly Property tblCTA_Simulation() As StandardDataset
    Get
      Return _tblCTA_Simulation
    End Get
  End Property

  Public Shared ReadOnly Property tblCTA_Folders() As StandardDataset
    Get
      Return _tblCTA_Folders
    End Get
  End Property

  Public Shared ReadOnly Property tblCTA_SimulationGroupList() As StandardDataset
    Get
      Return _tblCTA_SimulationGroupList
    End Get
  End Property

  Public Shared ReadOnly Property tblCTA_SimulationGroupItems() As StandardDataset
    Get
      Return _tblCTA_SimulationGroupItems
    End Get
  End Property

  Public Shared ReadOnly Property tblCurrency() As StandardDataset
    Get
      Return _tblCurrency
    End Get
  End Property

  Public Shared ReadOnly Property tblDataDictionary_TransactionFormLabels() As StandardDataset
    Get
      Return _tblDataDictionary_TransactionFormLabels
    End Get
  End Property

  Public Shared ReadOnly Property tblDataProviderInstrumentType() As StandardDataset
    Get
      Return _tblDataProviderInstrumentType
    End Get
  End Property

  Public Shared ReadOnly Property tblDataTask() As StandardDataset
    Get
      Return _tblDataTask
    End Get
  End Property

  Public Shared ReadOnly Property tblDataTaskType() As StandardDataset
    Get
      Return _tblDataTaskType
    End Get
  End Property

  Public Shared ReadOnly Property tblDealingPeriod() As StandardDataset
    Get
      Return _tblDealingPeriod
    End Get
  End Property

  Public Shared ReadOnly Property tblEstimate() As StandardDataset
    Get
      Return _tblEstimate
    End Get
  End Property

  Public Shared ReadOnly Property tblExposure() As StandardDataset
    Get
      Return _tblExposure
    End Get
  End Property

  Public Shared ReadOnly Property tblFeeDefinitions() As StandardDataset
    Get
      Return _tblFeeDefinitions
    End Get
  End Property

  Public Shared ReadOnly Property tblFlorenceDataValues() As StandardDataset
    Get
      Return _tblFlorenceDataValues
    End Get
  End Property

  Public Shared ReadOnly Property tblFlorenceEntity() As StandardDataset
    Get
      Return _tblFlorenceEntity
    End Get
  End Property

  Public Shared ReadOnly Property tblFlorenceGroup() As StandardDataset
    Get
      Return _tblFlorenceGroup
    End Get
  End Property

  Public Shared ReadOnly Property tblFlorenceItems() As StandardDataset
    Get
      Return _tblFlorenceItems
    End Get
  End Property

  Public Shared ReadOnly Property tblFlorenceStatus() As StandardDataset
    Get
      Return _tblFlorenceStatus
    End Get
  End Property

  Public Shared ReadOnly Property tblFlorenceStatusIDs() As StandardDataset
    Get
      Return _tblFlorenceStatusIDs
    End Get
  End Property

  Public Shared ReadOnly Property tblFlorenceWorkItems() As StandardDataset
    Get
      Return _tblFlorenceWorkItems
    End Get
  End Property

  Public Shared ReadOnly Property tblFlorenceWorkItemsData() As StandardDataset
    Get
      Return _tblFlorenceWorkItemsData
    End Get
  End Property

  Public Shared ReadOnly Property tblFund() As StandardDataset
    Get
      Return _tblFund
    End Get
  End Property

  Public Shared ReadOnly Property tblFundContactDetails() As StandardDataset
    Get
      Return _tblFundContactDetails
    End Get
  End Property

  Public Shared ReadOnly Property tblFundMilestones() As StandardDataset
    Get
      Return _tblFundMilestones
    End Get
  End Property

  Public Shared ReadOnly Property tblFundPerformanceFeePoints() As StandardDataset
    Get
      Return _tblFundPerformanceFeePoints
    End Get
  End Property

  Public Shared ReadOnly Property tblFundType() As StandardDataset
    Get
      Return _tblFundType
    End Get
  End Property

  Public Shared ReadOnly Property tblFX() As StandardDataset
    Get
      Return _tblFX
    End Get
  End Property

  Public Shared ReadOnly Property tblGenoaConstraintList() As StandardDataset
    Get
      Return _tblGenoaConstraintList
    End Get
  End Property

  Public Shared ReadOnly Property tblGenoaConstraintItems() As StandardDataset
    Get
      Return _tblGenoaConstraintItems
    End Get
  End Property

  Public Shared ReadOnly Property tblGenoaSavedSearchList() As StandardDataset
    Get
      Return _tblGenoaSavedSearchList
    End Get
  End Property

  Public Shared ReadOnly Property tblGenoaSavedSearchItems() As StandardDataset
    Get
      Return _tblGenoaSavedSearchItems
    End Get
  End Property

	Public Shared ReadOnly Property tblGreeks() As StandardDataset
		Get
			Return _tblGreeks
		End Get
	End Property

  Public Shared ReadOnly Property tblGroupList() As StandardDataset
    Get
      Return _tblGroupList
    End Get
  End Property

  Public Shared ReadOnly Property tblGroupItems() As StandardDataset
    Get
      Return _tblGroupItems
    End Get
  End Property

  Public Shared ReadOnly Property tblGroupItemData() As StandardDataset
    Get
      Return _tblGroupItemData
    End Get
  End Property

  Public Shared ReadOnly Property tblGroupsAggregated() As StandardDataset
    Get
      Return _tblGroupsAggregated
    End Get
  End Property

  Public Shared ReadOnly Property tblIndexMapping() As StandardDataset
    Get
      Return _tblIndexMapping
    End Get
  End Property

  Public Shared ReadOnly Property tblInstrument() As StandardDataset
    Get
      Return _tblInstrument
    End Get
  End Property

  Public Shared ReadOnly Property tblInstrumentNotes() As StandardDataset
    Get
      Return _tblInstrumentNotes
    End Get
  End Property

	Public Shared ReadOnly Property tblInstrumentFlorenceEntityLink() As StandardDataset
		Get
			Return _tblInstrumentFlorenceEntityLink
		End Get
	End Property

  Public Shared ReadOnly Property tblInstrumentNamesGrossUnits() As StandardDataset
    Get
      Return _tblInstrumentNamesGrossUnits
    End Get
  End Property

  Public Shared ReadOnly Property tblInstrumentType() As StandardDataset
    Get
      Return _tblInstrumentType
    End Get
  End Property

  Public Shared ReadOnly Property tblInvestorGroup() As StandardDataset
    Get
      Return _tblInvestorGroup
    End Get
  End Property

  Public Shared ReadOnly Property tblJob() As StandardDataset
    Get
      Return _tblJob
    End Get
  End Property

  Public Shared ReadOnly Property tblLimits() As StandardDataset
    Get
      Return _tblLimits
    End Get
  End Property

  Public Shared ReadOnly Property tblLimitType() As StandardDataset
    Get
      Return _tblLimitType
    End Get
  End Property

  Public Shared ReadOnly Property tblLiquidityLimit() As StandardDataset
    Get
      Return _tblLiquidityLimit
    End Get
  End Property

  Public Shared ReadOnly Property tblLocations() As StandardDataset
    Get
      Return _tblLocations
    End Get
  End Property

  Public Shared ReadOnly Property tblManagementCompany() As StandardDataset
    Get
      Return _tblManagementCompany
    End Get
  End Property

  Public Shared ReadOnly Property tblMarketInstruments() As StandardDataset
    Get
      Return _tblMarketInstruments
    End Get
  End Property

  Public Shared ReadOnly Property tblMarketPrices() As StandardDataset
    Get
      Return _tblMarketPrices
    End Get
  End Property

  Public Shared ReadOnly Property tblMeeting() As StandardDataset
    Get
      Return _tblMeeting
    End Get
  End Property

  Public Shared ReadOnly Property tblMeetingStatus() As StandardDataset
    Get
      Return _tblMeetingStatus
    End Get
  End Property

  Public Shared ReadOnly Property tblMeetingPeople() As StandardDataset
    Get
      Return _tblMeetingPeople
    End Get
  End Property

  Public Shared ReadOnly Property tblMonthlyComment() As StandardDataset
    Get
      Return _tblMonthlyComment
    End Get
  End Property

  Public Shared ReadOnly Property tblPeriod() As StandardDataset
    Get
      Return _tblPeriod
    End Get
  End Property

  Public Shared ReadOnly Property tblPerson() As StandardDataset
    Get
      Return _tblPerson
    End Get
  End Property

	Public Shared ReadOnly Property tblPendingTransactions() As StandardDataset
		Get
			Return _tblPendingTransactions
		End Get
	End Property

	Public Shared ReadOnly Property tblPertracCustomFields() As StandardDataset
		Get
			Return _tblPertracCustomFields
		End Get
	End Property

  Public Shared ReadOnly Property tblPertracCustomFieldData() As StandardDataset
    Get
      Return _tblPertracCustomFieldData
    End Get
  End Property

  Public Shared ReadOnly Property tblPertracFieldMapping() As StandardDataset
    Get
      Return _tblPertracFieldMapping
    End Get
  End Property

  Public Shared ReadOnly Property tblPFPCCustodyRecord() As StandardDataset
    Get
      Return _tblPFPCCustodyRecord
    End Get
  End Property

  Public Shared ReadOnly Property tblPFPCPortfolioReconcilliation() As StandardDataset
    Get
      Return _tblPFPCPortfolioReconcilliation
    End Get
  End Property

  Public Shared ReadOnly Property tblPFPCPortfolioValuation() As StandardDataset
    Get
      Return _tblPFPCPortfolioValuation
    End Get
  End Property

  Public Shared ReadOnly Property tblPFPCShareAllocation() As StandardDataset
    Get
      Return _tblPFPCShareAllocation
    End Get
  End Property

  Public Shared ReadOnly Property tblPortfolioData() As StandardDataset
    Get
      Return _tblPortfolioData
    End Get
  End Property

  Public Shared ReadOnly Property tblPortfolioIndex() As StandardDataset
    Get
      Return _tblPortfolioIndex
    End Get
  End Property

  Public Shared ReadOnly Property tblPortfolioItemTypes() As StandardDataset
    Get
      Return _tblPortfolioItemTypes
    End Get
  End Property

  Public Shared ReadOnly Property tblPortfolioType() As StandardDataset
    Get
      Return _tblPortfolioType
    End Get
  End Property

  Public Shared ReadOnly Property tblPrice() As StandardDataset
    Get
      Return _tblPrice
    End Get
  End Property

  Public Shared ReadOnly Property tblRecAvailablilities() As StandardDataset
    Get
      Return _tblRecAvailablilities
    End Get
  End Property

  Public Shared ReadOnly Property tblReferentialIntegrity() As StandardDataset
    Get
      Return _tblReferentialIntegrity
    End Get
  End Property

  Public Shared ReadOnly Property tblRelationship() As StandardDataset
    Get
      Return _tblRelationship
    End Get
  End Property

  Public Shared ReadOnly Property tblReport() As StandardDataset
    Get
      Return _tblReport
    End Get
  End Property

  Public Shared ReadOnly Property tblReportCustomisation() As StandardDataset
    Get
      Return _tblReportCustomisation
    End Get
  End Property

  Public Shared ReadOnly Property tblReportDetailLog() As StandardDataset
    Get
      Return _tblReportDetailLog
    End Get
  End Property

  Public Shared ReadOnly Property tblReportStatus() As StandardDataset
    Get
      Return _tblReportStatus
    End Get
  End Property

  Public Shared ReadOnly Property tblReportType() As StandardDataset
    Get
      Return _tblReportType
    End Get
  End Property

  Public Shared ReadOnly Property tblRiskData() As StandardDataset
    Get
      Return _tblRiskData
    End Get
  End Property

  Public Shared ReadOnly Property tblRiskDataCategory() As StandardDataset
    Get
      Return _tblRiskDataCategory
    End Get
  End Property

  Public Shared ReadOnly Property tblRiskDataCharacteristic() As StandardDataset
    Get
      Return _tblRiskDataCharacteristic
    End Get
  End Property

	Public Shared ReadOnly Property tblRiskDataCompoundCharacteristic() As StandardDataset
		Get
			Return _tblRiskDataCompoundCharacteristic
		End Get
	End Property

  Public Shared ReadOnly Property tblSelectCharacteristics() As StandardDataset
    Get
      Return _tblSelectCharacteristics
    End Get
  End Property

  Public Shared ReadOnly Property tblSavePortfolioDetails() As StandardDataset
    Get
      Return _tblSavePortfolioDetails
    End Get
  End Property

  Public Shared ReadOnly Property tblSectorLimit() As StandardDataset
    Get
      Return _tblSectorLimit
    End Get
  End Property

  Public Shared ReadOnly Property tblSelectMeeting() As StandardDataset
    Get
      Return _tblSelectMeeting
    End Get
  End Property

	Public Shared ReadOnly Property tblSelectFuturesNotional() As StandardDataset
		Get
			Return _tblSelectFuturesNotional
		End Get
	End Property

	Public Shared ReadOnly Property tblSelectTransaction() As StandardDataset
		Get
			Return _tblSelectTransaction
		End Get
	End Property

  Public Shared ReadOnly Property tblSelectCompoundTransaction() As StandardDataset
    Get
      Return _tblSelectCompoundTransaction
    End Get
  End Property

	Public Shared ReadOnly Property tblSophisStatusGroups() As StandardDataset
		Get
			Return _tblSophisStatusGroups
		End Get
	End Property

	Public Shared ReadOnly Property tblSubFund() As StandardDataset
		Get
			Return _tblSubFund
		End Get
	End Property

  Public Shared ReadOnly Property tblSubFundHeirarchy() As StandardDataset
    Get
      Return _tblSubFundHeirarchy
    End Get
  End Property

  Public Shared ReadOnly Property tblSystemDates() As StandardDataset
    Get
      Return _tblSystemDates
    End Get
  End Property

  Public Shared ReadOnly Property tblSystemStrings() As StandardDataset
    Get
      Return _tblSystemStrings
    End Get
  End Property

	Public Shared ReadOnly Property tblTradeFileAcknowledgement() As StandardDataset
		Get
			Return _tblTradeFileAcknowledgement
		End Get
	End Property

	Public Shared ReadOnly Property tblTradeFileEmsxStatus() As StandardDataset
		Get
			Return _tblTradeFileEmsxStatus
		End Get
	End Property

  Public Shared ReadOnly Property tblTradeFileExecutionReporting() As StandardDataset
    Get
      Return _tblTradeFileExecutionReporting
    End Get
  End Property

	Public Shared ReadOnly Property tblTradeFilePending() As StandardDataset
		Get
			Return _tblTradeFilePending
		End Get
	End Property

	Public Shared ReadOnly Property tblTradeStatus() As StandardDataset
		Get
			Return _tblTradeStatus
		End Get
	End Property

  Public Shared ReadOnly Property tblTradeStatusGroup() As StandardDataset
    Get
      Return _tblTradeStatusGroup
    End Get
  End Property

	Public Shared ReadOnly Property tblTradeStatusGroupItems() As StandardDataset
		Get
			Return _tblTradeStatusGroupItems
		End Get
	End Property

	Public Shared ReadOnly Property tblTransaction() As StandardDataset
		Get
			Return _tblTransaction
		End Get
	End Property

  Public Shared ReadOnly Property tblTransactionContra() As StandardDataset
    Get
      Return _tblTransactionContra
    End Get
  End Property

  Public Shared ReadOnly Property tblTransactionType() As StandardDataset
    Get
      Return _tblTransactionType
    End Get
  End Property

  Public Shared ReadOnly Property tblUnitHolderFees() As StandardDataset
    Get
      Return _tblUnitHolderFees
    End Get
  End Property

  Public Shared ReadOnly Property tblUserPermissions() As StandardDataset
    Get
      Return _tblUserPermissions
    End Get
  End Property

  Public Shared ReadOnly Property tblVeniceFormTooltips() As StandardDataset
    Get
      Return _tblVeniceFormTooltips
    End Get
  End Property

	Public Shared ReadOnly Property tblWorkflowRules() As StandardDataset
		Get
			Return _tblWorkflowRules
		End Get
	End Property

	Public Shared ReadOnly Property tblWorkflows() As StandardDataset
		Get
			Return _tblWorkflows
		End Get
	End Property

  Public Shared ReadOnly Property tblErrorLog() As StandardDataset
    Get
      Return _tblErrorLog
    End Get
  End Property

  Public Shared ReadOnly Property Mastername() As StandardDataset
    Get
      Return _Mastername
    End Get
  End Property

  Public Shared ReadOnly Property Information() As StandardDataset
    Get
      Return _Information
    End Get
  End Property

  Public Shared ReadOnly Property Performance() As StandardDataset
    Get
      Return _Performance
    End Get
  End Property

  Public Shared ReadOnly Property InstrumentPerformance() As StandardDataset
    Get
      Return _InstrumentPerformance
    End Get
  End Property

  Public Shared ReadOnly Property GetStandardDataset(ByVal pChangeID As RenaissanceChangeID) As StandardDataset
    Get
      Select Case pChangeID
        Case RenaissanceChangeID.KnowledgeDate
          Return Nothing

        Case RenaissanceChangeID.Connection
          Return Nothing

        Case RenaissanceChangeID.None
          Return Nothing

        Case RenaissanceChangeID.tblAttribution
          Return _tblAttribution

        Case RenaissanceChangeID.tblAutoUpdate
          Return _tblAutoUpdate

				Case RenaissanceChangeID.tblBenchmarkIndex
					Return _tblBenchmarkIndex

				Case RenaissanceChangeID.tblBenchmarkItems
					Return _tblBenchmarkItems

				Case RenaissanceChangeID.tblBestFX
					Return _tblBestFX

        Case RenaissanceChangeID.tblBestMarketPrice
          Return _tblBestMarketPrice

        Case RenaissanceChangeID.tblBookmark
          Return _tblBookmark

				Case RenaissanceChangeID.tblBroker
					Return _tblBroker

				Case RenaissanceChangeID.tblBrokerAccounts
					Return _tblBrokerAccounts

				Case RenaissanceChangeID.tblChangeControl
					Return _tblChangeControl

        Case RenaissanceChangeID.tblCompoundTransaction
          Return _tblCompoundTransaction

        Case RenaissanceChangeID.tblCompoundTransactionParameters
          Return _tblCompoundTransactionParameters

        Case RenaissanceChangeID.tblCompoundTransactionTemplate
          Return _tblCompoundTransactionTemplate

        Case RenaissanceChangeID.tblCounterparty
          Return _tblCounterparty

        Case RenaissanceChangeID.tblCovariance
          Return _tblCovariance

        Case RenaissanceChangeID.tblCovarianceList
          Return _tblCovarianceList

        Case RenaissanceChangeID.tblCTA_Simulation
          Return _tblCTA_Simulation

        Case RenaissanceChangeID.tblCTA_Folders
          Return _tblCTA_Folders

        Case RenaissanceChangeID.tblCTA_SimulationGroupList
          Return _tblCTA_SimulationGroupList

        Case RenaissanceChangeID.tblCTA_SimulationGroupItems
          Return _tblCTA_SimulationGroupItems

        Case RenaissanceChangeID.tblCurrency
          Return _tblCurrency

        Case RenaissanceChangeID.tblDataDictionary_TransactionFormLabels
          Return _tblDataDictionary_TransactionFormLabels

        Case RenaissanceChangeID.tblDataProviderInstrumentType
          Return _tblDataProviderInstrumentType

        Case RenaissanceChangeID.tblDataTask
          Return _tblDataTask

        Case RenaissanceChangeID.tblDataTaskType
          Return _tblDataTaskType

        Case RenaissanceChangeID.tblDealingPeriod
          Return _tblDealingPeriod

        Case RenaissanceChangeID.tblErrorLog
          Return _tblErrorLog

        Case RenaissanceChangeID.tblEstimate
          Return _tblEstimate

        Case RenaissanceChangeID.tblExposure
          Return _tblExposure

        Case RenaissanceChangeID.tblFeeDefinitions
          Return _tblFeeDefinitions

        Case RenaissanceChangeID.tblFlorenceDataValues
          Return _tblFlorenceDataValues

        Case RenaissanceChangeID.tblFlorenceEntity
          Return _tblFlorenceEntity

        Case RenaissanceChangeID.tblFlorenceGroup
          Return _tblFlorenceGroup

        Case RenaissanceChangeID.tblFlorenceItems
          Return _tblFlorenceItems

        Case RenaissanceChangeID.tblFlorenceStatus
          Return _tblFlorenceStatus

        Case RenaissanceChangeID.tblFlorenceStatusIDs
          Return _tblFlorenceStatusIDs

        Case RenaissanceChangeID.tblFlorenceWorkItems
          Return _tblFlorenceWorkItems

        Case RenaissanceChangeID.tblFlorenceWorkItemsData
          Return _tblFlorenceWorkItemsData

        Case RenaissanceChangeID.tblFund
          Return _tblFund

        Case RenaissanceChangeID.tblFundContactDetails
          Return _tblFundContactDetails

        Case RenaissanceChangeID.tblFundMilestones
          Return _tblFundMilestones

        Case RenaissanceChangeID.tblFundPerformanceFeePoints
          Return _tblFundPerformanceFeePoints

        Case RenaissanceChangeID.tblFundType
          Return _tblFundType

        Case RenaissanceChangeID.tblFX
          Return _tblFX

        Case RenaissanceChangeID.tblGenoaConstraintList
          Return _tblGenoaConstraintList

        Case RenaissanceChangeID.tblGenoaConstraintItems
          Return _tblGenoaConstraintItems

        Case RenaissanceChangeID.tblGenoaSavedSearchList
          Return _tblGenoaSavedSearchList

        Case RenaissanceChangeID.tblGenoaSavedSearchItems
          Return _tblGenoaSavedSearchItems

        Case RenaissanceChangeID.tblGroupsAggregated
          Return _tblGroupsAggregated

				Case RenaissanceChangeID.tblGreeks
					Return _tblGreeks

				Case RenaissanceChangeID.tblGroupList
					Return _tblGroupList

        Case RenaissanceChangeID.tblGroupItems
          Return _tblGroupItems

        Case RenaissanceChangeID.tblGroupItemData
          Return _tblGroupItemData

        Case RenaissanceChangeID.tblIndexMapping
          Return _tblIndexMapping

        Case RenaissanceChangeID.tblInstrument
          Return _tblInstrument

        Case RenaissanceChangeID.tblInstrumentNotes
          Return _tblInstrumentNotes

				Case RenaissanceChangeID.tblInstrumentFlorenceEntityLink
					Return _tblInstrumentFlorenceEntityLink

				Case RenaissanceChangeID.tblInstrumentNamesGrossUnits
					Return _tblInstrumentNamesGrossUnits

        Case RenaissanceChangeID.tblInstrumentType
          Return _tblInstrumentType

        Case RenaissanceChangeID.tblInvestorGroup
          Return _tblInvestorGroup

        Case RenaissanceChangeID.tblJob
          Return _tblJob

        Case RenaissanceChangeID.tblLocations
          Return _tblLocations

        Case RenaissanceChangeID.tblLimits
          Return _tblLimits

        Case RenaissanceChangeID.tblLimitType
          Return _tblLimitType

        Case RenaissanceChangeID.tblLiquidityLimit
          Return _tblLiquidityLimit

        Case RenaissanceChangeID.tblManagementCompany
          Return _tblManagementCompany

        Case RenaissanceChangeID.tblMarketInstruments
          Return _tblMarketInstruments

        Case RenaissanceChangeID.tblMarketPrices
          Return _tblMarketPrices

        Case RenaissanceChangeID.tblMeeting
          Return _tblMeeting

        Case RenaissanceChangeID.tblMeetingStatus
          Return _tblMeetingStatus

        Case RenaissanceChangeID.tblMeetingPeople
          Return _tblMeetingPeople

        Case RenaissanceChangeID.tblMonthlyComment
          Return _tblMonthlyComment

				Case RenaissanceChangeID.tblPendingTransactions	' No RenaissanceChangeID.tblPendingTransactions - I wonder why ? (NPP march 2013)
					Return _tblPendingTransactions

				Case RenaissanceChangeID.tblPeriod
					Return _tblPeriod

        Case RenaissanceChangeID.tblPerson
          Return _tblPerson

        Case RenaissanceChangeID.tblPertracCustomFields
          Return _tblPertracCustomFields

        Case RenaissanceChangeID.tblPertracCustomFieldData
          Return _tblPertracCustomFieldData

        Case RenaissanceChangeID.tblPertracFieldMapping
          Return _tblPertracFieldMapping

        Case RenaissanceChangeID.tblPFPCCustodyRecord
          Return _tblPFPCCustodyRecord

        Case RenaissanceChangeID.tblPFPCPortfolioReconcilliation
          Return _tblPFPCPortfolioReconcilliation

        Case RenaissanceChangeID.tblPFPCPortfolioValuation
          Return _tblPFPCPortfolioValuation

        Case RenaissanceChangeID.tblPFPCShareAllocation
          Return _tblPFPCShareAllocation

        Case RenaissanceChangeID.tblPortfolioData
          Return _tblPortfolioData

        Case RenaissanceChangeID.tblPortfolioIndex
          Return _tblPortfolioIndex

        Case RenaissanceChangeID.tblPortfolioItemTypes
          Return _tblPortfolioItemTypes

        Case RenaissanceChangeID.tblPortfolioType
          Return _tblPortfolioType

        Case RenaissanceChangeID.tblPrice
          Return _tblPrice

        Case RenaissanceChangeID.tblRecAvailablilities
          Return _tblRecAvailablilities

        Case RenaissanceChangeID.tblReferentialIntegrity
          Return _tblReferentialIntegrity

        Case RenaissanceChangeID.tblRelationship
          Return _tblRelationship

        Case RenaissanceChangeID.tblReport
          Return _tblReport

        Case RenaissanceChangeID.tblReportCustomisation
          Return _tblReportCustomisation

        Case RenaissanceChangeID.tblReportDetailLog
          Return _tblReportDetailLog

        Case RenaissanceChangeID.tblReportStatus
          Return _tblReportStatus

        Case RenaissanceChangeID.tblReportType
          Return _tblReportType

        Case RenaissanceChangeID.tblRiskData
          Return _tblRiskData

        Case RenaissanceChangeID.tblRiskDataCategory
          Return _tblRiskDataCategory

        Case RenaissanceChangeID.tblRiskDataCharacteristic
          Return _tblRiskDataCharacteristic

				Case RenaissanceChangeID.tblRiskDataCompoundCharacteristic
					Return _tblRiskDataCompoundCharacteristic

				Case RenaissanceChangeID.tblSelectCharacteristics
					Return _tblSelectCharacteristics

        Case RenaissanceChangeID.tblSavePortfolioDetails
          Return _tblSavePortfolioDetails

        Case RenaissanceChangeID.tblSectorLimit
          Return _tblSectorLimit

        Case RenaissanceChangeID.tblSelectMeeting
          Return _tblSelectMeeting

				Case RenaissanceChangeID.tblSelectFuturesNotional
					Return _tblSelectFuturesNotional

				Case RenaissanceChangeID.tblSelectTransaction
					Return _tblSelectTransaction

				Case RenaissanceChangeID.tblSelectCompoundTransaction
					Return _tblSelectCompoundTransaction

				Case RenaissanceChangeID.tblSophisStatusGroups
					Return _tblSophisStatusGroups

				Case RenaissanceChangeID.tblSubFund
					Return _tblSubFund

        Case RenaissanceChangeID.tblSubFundHeirarchy
          Return _tblSubFundHeirarchy

        Case RenaissanceChangeID.tblSystemDates
          Return _tblSystemDates

        Case RenaissanceChangeID.tblSystemStrings
          Return _tblSystemStrings

				Case RenaissanceChangeID.tblTradeFileAcknowledgement
					Return _tblTradeFileAcknowledgement

				Case RenaissanceChangeID.tblTradeFileEmsxStatus
					Return _tblTradeFileEmsxStatus

        Case RenaissanceChangeID.tblTradeFileExecutionReporting
          Return _tblTradeFileExecutionReporting

        Case RenaissanceChangeID.tblTradeFilePending
          Return _tblTradeFilePending

				Case RenaissanceChangeID.tblTradeStatus
					Return _tblTradeStatus

        Case RenaissanceChangeID.tblTradeStatusGroup
          Return _tblTradeStatusGroup

        Case RenaissanceChangeID.tblTradeStatusGroupItems
          Return _tblTradeStatusGroupItems

        Case RenaissanceChangeID.tblTransaction
          Return _tblTransaction

        Case RenaissanceChangeID.tblTransactionContra
          Return _tblTransactionContra

        Case RenaissanceChangeID.tblTransactionType
          Return _tblTransactionType

        Case RenaissanceChangeID.tblUnitHolderFees
          Return _tblUnitHolderFees

        Case RenaissanceChangeID.tblUserPermissions
          Return _tblUserPermissions

        Case RenaissanceChangeID.tblVeniceFormTooltips
          Return _tblVeniceFormTooltips

				Case RenaissanceChangeID.tblWorkflowRules
					Return _tblWorkflowRules

				Case RenaissanceChangeID.tblWorkflows
					Return _tblWorkflows

				Case RenaissanceChangeID.Mastername
					Return _Mastername

        Case RenaissanceChangeID.Information
          Return _Information

        Case RenaissanceChangeID.Performance
          Return _Performance

        Case RenaissanceChangeID.InstrumentPerformance
          Return _InstrumentPerformance

        Case Else
          Return Nothing
      End Select
    End Get
  End Property
End Class

#End Region

<Serializable()> Public Class RenaissanceUpdateClass
  Private _UpdateChangeIDs() As String
  Private _UpdateDetail() As String

  Public Sub New()
  End Sub

  Public Sub New(ByRef pChangeIDArray() As RenaissanceChangeID)
    Set_ChangeIDs(pChangeIDArray)
  End Sub

  Public Sub New(ByRef pChangeIDArray() As String)
    Set_ChangeIDs(pChangeIDArray)
  End Sub

  Public Sub New(ByRef pChangeIDArray() As RenaissanceChangeID, ByVal UpdateDetailArray() As String)
    Set_ChangeIDs(pChangeIDArray)
    Set_UpdateDetails(UpdateDetailArray)
  End Sub

  Public Sub New(ByRef pChangeIDArray() As String, ByVal UpdateDetailArray() As String)
    Set_ChangeIDs(pChangeIDArray)
    Set_UpdateDetails(UpdateDetailArray)
  End Sub

  Public ReadOnly Property ChangeIDs() As RenaissanceChangeID()
    Get
      Dim ChangeArray() As RenaissanceChangeID

      If Not (_UpdateChangeIDs Is Nothing) AndAlso (_UpdateChangeIDs.GetLength(0) > 0) Then
        Dim IndexCount As Integer
        Dim thisChangeID As RenaissanceChangeID
        ReDim ChangeArray(_UpdateChangeIDs.GetLength(0) - 1)

        For IndexCount = 0 To (_UpdateChangeIDs.GetLength(0) - 1)
          thisChangeID = RenaissanceChangeID.None

          Try
            thisChangeID = System.Enum.Parse(GetType(RenaissanceChangeID), _UpdateChangeIDs(IndexCount))
          Catch ex As Exception
          End Try
          ChangeArray(IndexCount) = thisChangeID
        Next

      Else
        ReDim ChangeArray(0)
        ChangeArray(0) = RenaissanceChangeID.None
      End If

      Return ChangeArray
    End Get
  End Property

	Public ReadOnly Property UpdateDetails() As String()
		Get
			Dim RVal(-1) As String

			Try
				If Not (_UpdateChangeIDs Is Nothing) AndAlso (_UpdateChangeIDs.GetLength(0) > 0) Then
					ReDim RVal(_UpdateChangeIDs.GetLength(0) - 1)

					If Not (_UpdateDetail Is Nothing) AndAlso (_UpdateDetail.GetLength(0) > 0) Then
						Array.Copy(_UpdateDetail, RVal, Math.Min(_UpdateDetail.GetLength(0), RVal.Length))
					End If
				End If
			Catch ex As Exception
				ReDim RVal(0)
			End Try

			Return RVal
		End Get
	End Property

	Public ReadOnly Property UpdateDetail(ByVal pChangeID As RenaissanceChangeID) As String
		Get
			Dim RVal As String = ""

			Try
				Dim IndexCount As Integer
				Dim ChangeName As String = pChangeID.ToString

				If (_UpdateChangeIDs IsNot Nothing) AndAlso (_UpdateDetail IsNot Nothing) AndAlso (_UpdateDetail.Length > 0) Then

					For IndexCount = 0 To (_UpdateChangeIDs.GetLength(0) - 1)

						If (_UpdateChangeIDs(IndexCount) = ChangeName) Then

							If (IndexCount < _UpdateDetail.Length) AndAlso (_UpdateDetail(IndexCount) IsNot Nothing) Then
								RVal = _UpdateDetail(IndexCount)
							End If

							Exit For

						End If

					Next

				End If

			Catch ex As Exception
				RVal = ""
			End Try

			Return RVal
		End Get
	End Property

  Public Function Set_ChangeIDs(ByRef pChangeIDArray() As RenaissanceChangeID) As Boolean
    Dim ChangeArray() As String
    Dim UpdateDetail() As String

    If Not (pChangeIDArray Is Nothing) Then
      If (pChangeIDArray.GetLength(0) > 0) Then
        ReDim ChangeArray(pChangeIDArray.GetLength(0) - 1)
        ReDim UpdateDetail(pChangeIDArray.GetLength(0) - 1)

        Try
          Dim IndexCount As Integer
          For IndexCount = 0 To (pChangeIDArray.GetLength(0) - 1)
            Try
              ChangeArray(IndexCount) = pChangeIDArray(IndexCount).ToString
            Catch ex As Exception
            End Try
          Next
        Catch ex As Exception
        End Try

        _UpdateChangeIDs = ChangeArray
        _UpdateDetail = UpdateDetail
        Return True
      End If
    End If

    _UpdateChangeIDs = Nothing
    Return False
  End Function

  Public Function Set_ChangeIDs(ByRef pChangeIDArray() As String) As Boolean

    ' Set ChangeIDs Array 

    _UpdateChangeIDs = pChangeIDArray

    ' Set _UpdateDetail array

    If (pChangeIDArray Is Nothing) Then
      _UpdateDetail = Nothing
    Else
      ReDim _UpdateDetail(pChangeIDArray.Length - 1)
    End If

  End Function

  Public Function Set_ChangeIDs(ByRef pChangeIDArray() As RenaissanceChangeID, ByVal UpdateDetailArray() As String) As Boolean
    Dim ChangeArray() As String

    If Not (pChangeIDArray Is Nothing) Then
      If (pChangeIDArray.GetLength(0) > 0) Then
        ReDim ChangeArray(pChangeIDArray.GetLength(0) - 1)

        Try
          Dim IndexCount As Integer
          For IndexCount = 0 To (pChangeIDArray.GetLength(0) - 1)
            Try
              ChangeArray(IndexCount) = pChangeIDArray(IndexCount).ToString
            Catch ex As Exception
            End Try
          Next
        Catch ex As Exception
        End Try

        _UpdateChangeIDs = ChangeArray

        Set_UpdateDetails(UpdateDetailArray)

        Return True
      End If
    End If

    _UpdateChangeIDs = Nothing
    Return False
  End Function

  Public Function Set_ChangeIDs(ByRef pChangeIDArray() As String, ByVal UpdateDetailArray() As String) As Boolean

    ' Set ChangeIDs Array 

    _UpdateChangeIDs = pChangeIDArray

    ' Set _UpdateDetail array

    If (pChangeIDArray Is Nothing) Then
      _UpdateDetail = Nothing
    Else
      Set_UpdateDetails(UpdateDetailArray)
    End If

  End Function

  Public Function Set_UpdateDetails(ByVal UpdateDetailArray() As String) As Boolean
    ' ******************************************************************************
    '
    ' ******************************************************************************

    Dim RVal As Boolean = False

    Try
      If (_UpdateChangeIDs Is Nothing) OrElse (_UpdateChangeIDs.Length <= 0) Then
        _UpdateDetail = Nothing
      Else
        If (_UpdateDetail Is Nothing) OrElse (_UpdateDetail.Length < _UpdateChangeIDs.Length) Then
          ReDim _UpdateDetail(_UpdateChangeIDs.Length - 1)
        Else
          Array.Clear(_UpdateDetail, 0, _UpdateDetail.Length)
        End If

        If Not (UpdateDetailArray Is Nothing) AndAlso (UpdateDetailArray.GetLength(0) > 0) Then
          Array.Copy(UpdateDetailArray, _UpdateDetail, Math.Min(_UpdateDetail.Length, UpdateDetailArray.GetLength(0)))
        End If

        RVal = True
      End If
    Catch ex As Exception
      RVal = False
    End Try

    Return RVal
  End Function

  Public Function Set_UpdateDetail(ByRef pChangeID As RenaissanceChangeID, ByVal UpdateDetail As String) As Boolean
    ' ******************************************************************************
    '
    ' ******************************************************************************

    Dim RVal As Boolean = False

    Try
      Dim IndexCount As Integer
      Dim ChangeName As String = pChangeID.ToString

      If (_UpdateChangeIDs IsNot Nothing) AndAlso (_UpdateDetail IsNot Nothing) AndAlso (_UpdateDetail.Length > 0) Then

        For IndexCount = 0 To (_UpdateChangeIDs.GetLength(0) - 1)

          If (_UpdateChangeIDs(IndexCount) = ChangeName) Then

            If (IndexCount < _UpdateDetail.Length) Then
              _UpdateDetail(IndexCount) = UpdateDetail
              RVal = True
            End If

            Exit For

          End If

        Next

      End If

    Catch ex As Exception
      RVal = False
    End Try

    Return RVal
  End Function

End Class

#Region " Form Handle Class and Form Handle Collection objects "

' Simple class designed to keep a record of a Form, Its purpose and whether it is currently being used.
Public Class RenaissanceFormHandle
  Private __Form As Form
  Private __FormType As Integer
  Private __InUse As Boolean


  ' Complex Constructor, Seeding the Form and the Purpose.
  Public Sub New(ByVal pForm As Form, ByVal pFormType As Integer)
    __Form = pForm
    __FormType = pFormType
    __InUse = True
  End Sub

  ' Complex Constructor, Seeding the Form, The Purpose and the 'InUse' flag
  Public Sub New(ByVal pForm As Form, ByVal pFormType As Integer, ByVal pInUse As Boolean)
    __Form = pForm
    __FormType = pFormType
    __InUse = pInUse
  End Sub

  ' Properties :
  ' The Form and Purpose properties are ReadOnly and may only be set in the Object Constructors.
  ' The InUse property may be Set or Got as necessary.
  ' 

  ' Form Property, Returns the form related to this record.
  Public ReadOnly Property Form() As Form
    Get
      Return __Form
    End Get
  End Property

  ' FormType, Returns the purpose associated with this Form record.
  Public ReadOnly Property FormType() As Integer
    Get
      Return __FormType
    End Get
  End Property

  ' InUse, Sets / Gets the property indicating whether this form is currently in use.
  Public Property InUse() As Boolean
    Get
      Return __InUse
    End Get
    Set(ByVal Value As Boolean)
      __InUse = Value
    End Set
  End Property

End Class

' Strongly typed collection (RenaissanceFormHandle) object designed to keep track of Renaissance Forms in use.
Public Class RenaissanceFormCollection
  ' ****************************************************************************************************************
  ' Strongly typed collection (RenaissanceFormHandle) object designed to keep track of Renaissance Forms in use.
  '
  ' A strongly typed collection was used for this purpose since it is more flexible than a normal array.
  ' The use of a collection means that arrays do not have to re-dimensioned and there are no arbitrary limits
  ' on the number of items.
  ' ****************************************************************************************************************

  Inherits System.Collections.CollectionBase

  Public Sub Add(ByVal pItem As RenaissanceFormHandle)
    ' Add new Form Handle to this collection

    Me.List.Add(pItem)

  End Sub

  Public Sub Remove(ByVal pIndex As Integer)
    ' Remove a Form Handle from this collection, specified by Index.

    If (pIndex > 0) And (pIndex < Me.Count) Then
      Me.List.RemoveAt(pIndex)
    End If
  End Sub

  Public Sub Remove(ByRef pIndex As RenaissanceFormHandle)
    ' Remove a Form Handle from this collection, specified by reference to a specific FormHandle object.

    If Me.List.Contains(pIndex) Then
      Me.List.Remove(pIndex)
    End If
  End Sub

  Public Sub Remove(ByRef pForm As Form)
    ' Remove Form Handles from this collection, specified by reference to a specific FORM object.
    ' This Remove variant will remove all items which reference a given form.

    Dim ItemsToDelete As New RenaissanceFormCollection
    Dim FormItem As RenaissanceFormHandle

    For Each FormItem In Me.List
      If FormItem.Form Is pForm Then
        ItemsToDelete.Add(FormItem)
      End If
    Next

    For Each FormItem In ItemsToDelete
      Me.List.Remove(FormItem)
    Next

    ItemsToDelete.Clear()
  End Sub

  Public Sub Hide(ByVal pIndex As Integer)
    ' Hide a Form Handle from this collection, specified by Index.

    Dim FormItem As RenaissanceFormHandle

    Try
      If (pIndex > 0) And (pIndex < Me.Count) Then
        FormItem = Me.List(pIndex)
        FormItem.InUse = False
      End If
    Catch ex As Exception
    End Try
  End Sub

  Public Sub Hide(ByRef pIndex As RenaissanceFormHandle)
    ' Hide a Form Handle from this collection, specified by reference to a specific FormHandle object.

    Try
      If Me.List.Contains(pIndex) Then
        pIndex.InUse = False
      End If
    Catch ex As Exception
    End Try
  End Sub

  Public Sub Hide(ByRef pForm As Form)
    ' Hide Form Handles from this collection, specified by reference to a specific FORM object.
    ' This Hide variant will Hide all items which reference a given form.

    Dim FormItem As RenaissanceFormHandle

    Try
      For Each FormItem In Me.List
        If FormItem.Form Is pForm Then
          FormItem.InUse = False
        End If
      Next
    Catch ex As Exception
    End Try

  End Sub

  Default Public Property Item(ByVal pIndex As Integer) As RenaissanceFormHandle
    ' Return a specified FormHandle

    Get
      Return CType(Me.List.Item(pIndex), RenaissanceFormHandle)
    End Get
    Set(ByVal Value As RenaissanceFormHandle)
      Me.List.Item(pIndex) = Value
    End Set
  End Property

  Public ReadOnly Property UnusedItem(ByVal pFormType As Integer) As RenaissanceFormHandle
    ' Return an unused form Handle of the requested type, or 'Nothing' if one does not exits.

    Get
      Dim FormItem As RenaissanceFormHandle

      For Each FormItem In Me.List
        If (FormItem.InUse = False) AndAlso (FormItem.FormType = pFormType) Then
          Return FormItem
        End If
      Next

      Return Nothing
    End Get
  End Property

  Public ReadOnly Property OpenItem(ByVal pFormType As Integer) As RenaissanceFormHandle
    ' Return the form Handle of an active Form of the requested type, or 'Nothing' if one does not exits.

    Get
      Dim FormItem As RenaissanceFormHandle

      For Each FormItem In Me.List
        If (FormItem.InUse = True) AndAlso (FormItem.FormType = pFormType) Then
          Return FormItem
        End If
      Next

      Return Nothing
    End Get
  End Property

  Public ReadOnly Property AllActiveItems() As ArrayList ' RenaissanceFormHandle
    ' Return an arraylist of all the Active FormItems in this collection

    Get
      Dim ActiveForms As New ArrayList
      Dim FormItem As RenaissanceFormHandle

      For Each FormItem In Me.List
        If (FormItem.InUse = True) Then
          ActiveForms.Add(FormItem)
        End If
      Next

      Return ActiveForms
    End Get
  End Property

  Public ReadOnly Property AllItems(Optional ByVal pFormType As Integer = (-1)) As ArrayList ' RenaissanceFormHandle
    ' Return an arraylist of all the FormItems in this collection

    Get
      Dim ActiveForms As New ArrayList
      Dim FormItem As RenaissanceFormHandle

      For Each FormItem In Me.List
        If (pFormType < 0) OrElse (FormItem.FormType = pFormType) Then
          ActiveForms.Add(FormItem)
        End If
      Next

      Return ActiveForms
    End Get
  End Property

  Public ReadOnly Property CountOf(ByVal pFormType As Integer) As Integer
    ' Return a count of the Given Form Type.

    Get
      Dim FormItem As RenaissanceFormHandle
      Dim Counter As Integer

      Counter = 0
      For Each FormItem In Me.List
        If (FormItem.FormType = pFormType) Then
          Counter += 1
        End If
      Next

      Return Counter
    End Get
  End Property

  Public ReadOnly Property ActiveItemsCount() As Integer
    ' Return a count of all Active FormItems.

    Get
      Dim RVal As Integer = 0
      Dim FormItem As RenaissanceFormHandle

      Try
        For Each FormItem In Me.List
          If (FormItem.InUse = True) Then
            RVal += 1
          End If
        Next
      Catch ex As Exception
      End Try

      Return RVal
    End Get
  End Property

End Class

#End Region

#Region " Standard (Instrument / Type / Reference etc) ID Enumerations "

Public Enum LOG_LEVELS As Integer
  [Warning] = 0     'Global Const LOG_Warning As Integer = 0
  [Audit] = (-1)    'Global Const LOG_Audit As Integer = (-1)
  [Changes] = (-2)  'Global Const LOG_Changes As Integer = (-2)
  [Error] = (-10)   'Global Const LOG_Error As Integer = (-10)

End Enum

' NOTE : Instrument, Types, Counterparties etc included in these enumerations are considered
' to be inviolate, that is not-editable.
' The Static Data Edit forms do (should) check the edited ID against the appropriate
' enumeration and block changes to matching IDs.
' Thus, do not add items to these enumerations if you wish to retain the freedom to directly 
' edit them.

Public Enum TransactionTypes As Integer
  [Consideration] = 1
  [Subscribe] = 2
  [Redeem] = 3
  [Buy] = 4
  [Sell] = 5
  [BuyFX] = 6
  [SellFX] = 7
  [Fee] = 8
  [Borrow] = 9
  [InterestPayment] = 10
  [InterestIncome] = 11
  [Repay] = 12
  [Expense_Increase] = 13
  [BuyFXForward] = 14
  [SellFXForward] = 15
  [Expense_Reduce] = 16
  [Provision_Increase] = 17
  [Provision_Reduce] = 18

  '[Max_Value] = 13
End Enum

Public Enum Instrument As Integer
  [USD] = 1
  [GBP] = 2
  [EUR] = 3

  '[Max_Value] = 3
End Enum

Public Enum InstrumentTypes As Integer
  [Fund] = 1
  [Unit] = 2
  [Cash] = 3
  [Share] = 4
  [Interest] = 5
  [Loan] = 6
  [Fee] = 7
  [Option] = 8
  [Margin] = 9
  [Expense] = 10
  [UnrealisedPnL] = 11
  [Index] = 12
  [CashFund] = 13
	[Notional] = 14
	[Future] = 15
	[Forward] = 16
	[Bond] = 17
	[ConvertibleBond] = 18

End Enum

Public Enum Counterparty As Integer
  [MARKET] = 1

  '[Max_Value] = 1
End Enum

Public Enum Currency As Integer
  [USD] = 1
  [GBP] = 2
  [EUR] = 3

  '[Max_Value] = 3
End Enum

Public Enum DealingPeriod As Integer
  [Annually] = 1
  [SemiAnnually] = 2
  [Quarterly] = 3
  [Monthly] = 4
  [Weekly] = 5
  [Daily] = 6
  [Fortnightly] = 7
End Enum

Public Enum FundType As Integer
  [Unit] = 1
  [Fee] = 2
  [Cash] = 3
  [Loan] = 4
  [Interest] = 5
  [CashFund] = 24
  [Notional] = 27
End Enum

Public Enum FlorenceStatusIDs As Integer
  [NoAction] = 0
  [NotRequired] = 1
  [NotAvailable] = 2
  [PartialResponse] = 3
  [RequestOutstanding] = 4
  [Completed] = 100
End Enum

Public Enum FlorenceDueDilligenceStatusIDs As Integer
	[InProgress] = 0
	[Declined] = 1
	[Approved] = 2
End Enum

Public Enum LimitType As Integer
  InstrumentCapitalUsage = 1
  SectorCapitalUsage = 2
  TotalCapitalUsage = 3
  RealisedDrawdown = 4
  InstrumentWeight = 5
  SectorWeight = 6
  SpecificInstrumentWeight = 7
  SpecificSectorWeight = 8
  NumberOfInstruments = 9
  SpecificInstrumentCapitalUsage = 10
  SpecificSectorCapitalUsage = 11
  SectorGroupWeight = 12
  SpecificSectorGroupWeight = 13
  DealingPeriod = 14
	CurrencyExposure = 15
    Characteristic = 16
    Ratio_5_10_40 = 17
End Enum

Public Enum PortfolioType As Integer
  ' Should this be a Bitmap ?

  [Default] = 0
  [SEC_Filing] = 1
  [IndexWeighting] = 2
  [X3] = 4
  [X4] = 8
  [X5] = 16
  [X6] = 32
  [X7] = 64



End Enum

Public Enum PortfolioItemType As Integer

  [Default] = 0
  [MarketInstrument] = 1
  [VeniceInstrument] = 2
  [Portfolio] = 3

End Enum

Public Enum DataProviderInstrumentType As Integer
  ' Mirrors tblDataProviderInstrumentType
  ' Enumeration name mirrors BBG Ticker convention. (necessary).

  [None] = 0
  [Govt] = 1
  [Corp] = 2
  [Mtge] = 3
  [M_Mkt] = 4 ' Note BBG code is actually M-Mkt
  [Muni] = 5
  [Pfd] = 6
  [Equity] = 7
  [Comdty] = 8
  [Index] = 9
  [Curncy] = 10
  [Client] = 11

End Enum

Public Enum PertracInformationFields As Integer
  ' Maps to the 'FCP_FieldName' column of the 'tblPertracFieldMapping' table.
  ' The Pertrac data calls uses this enumeration and the contents of the Mapping table
  ' to standardise access to the information in the Pertrac 'Information' table.
  '
  ' [DataPeriod] and [Mastername] are handled differently, they are pulled from the Mastername table.
  '
  [Address1]
  [Address2]
  [Administrator]
  [BloombergCode]
  [City]
  [CompanyName]
  [Contact]
  [ContactFax]
  [ContactPhone]
  [Country]
  [Currency]
  [DataPeriod]
  [DataVendorID]
  [DataVendorName]
  [Description]
  [Domicile]
  [Email]
  [Exchange]
  [FundAssets]
  [FirmAssets]
  [FundName]
  [Hurdle]
  [IncentiveFee]
  [InvestorType]
  [ISIN]
  [LastUpdated]
  [Lockup]
  [ManagementFee]
  [ManagerName]
  [MarketingContact]
  [MarketingFax]
  [MarketingPhone]
  [Mastername]
  [MinimumInvestment]
  [MorningstarCode]
  [MorningstarRating]
  [Notice]
  [Open]
  [Redemption]
  [ReutersCode]
  [Sedol]
  [State]
  [Strategy]
  [Subscription]
  [UserDescription]
  [Website]
  [ZipCode]


  [MaxValue]
End Enum

<FlagsAttribute()> _
 Public Enum AdministratorDatesFilter As Integer
	None = 0
  SubscriptionRedemption_IgnoreTransactionStatus = 1
	SubscriptionRedemption_IgnoreTodaysTrades = 2

  FXTradesSeparate = 64
  FXForwardsSeparate = 128

	UnitPriceVariantPeriod_Monthly = 256

End Enum

Public Enum StatusGroups As Integer
	' Not an exhaustive list, just some usefull ones.

	[Pending_FM] = 1
	[Pending_MO] = 2
	[Pending_All] = 10
  [PendingConfirmationMO] = 20

	[EOD] = 25098

End Enum

Public Enum TradeStatus As Integer
	' Not an exhaustive list, just some usefull ones.

	[ValidatedMO] = 1
	[SentToMiddleOffice] = 3
	[ElectronicMarket] = 4
	[ToBeSentToMO] = 5
	[CheckedFM] = 8
	[RefusedMO] = 9
  [CancelledFM] = 11
  [PendingConfirmationMO] = 14
  [PendingDerivativeMO] = 14
	[PendingFM] = 15
	[Reconciled] = 20
	[PendingMO] = 21
	[PendingEOD] = 22
	[ModifiedEOD] = 23
	[CheckedEOD] = 24
  '[ModifiedMO] = 25
  [WaitingToWriteToFile] = 40
  [WaitingToPlaceOrder] = 43
	[ExecutionComplete] = 44
  [ExecutionError] = 45
  [Cancelled] = 180
	[ValidatedAdministrator] = 185
	[RecievedAdministrator] = 187
	[AnomalyAdministrator] = 189
	[RejectedAdministrator] = 191

End Enum

Public Enum TradeStatusCancelled As Integer
  [CancelledFM] = 11
  [Cancelled] = 180
End Enum

Public Enum TradeStatusEOD As Integer
  ' Status' in the EOD Group. 

  [ValidatedMO] = 1
  [Reconciled] = 20
  [PendingMO] = 21
  [PendingEOD] = 22
  [ModifiedEOD] = 23
  [CheckedOED] = 24
  [ModifiedMO] = 25
  [ValidatedAdministrator] = 185

End Enum

Public Enum ExecutionBrokers As Integer
	' Enumeration to match the entries in the tblBrokers table and those used in the tblBrokerAccounts table

	[BNP_Paris] = 1
	[BNP_Luxembourg] = 2

End Enum

' *******************************************************
' Task Types used by the Aggregator
'
' It is ESSENTIAL that this enumeration matches the tblDataTaskType
' table within the Renaissance Database.
' These Enumerations are duplicated within RenaissanceGlobals, is is 
' ESSENTIAL that these Enumerations remain in-step.
' *******************************************************

' DEFINED IN 'NAPLESGLOBALS' !!!

'Public Enum DataTaskType As Integer
'  NoTask = 0
'  RealtimeSecurityCapture = 1
'  PeriodicSecurityCapture = 2
'  PortfolioCapture = 3
'  PageCapture = 4

'  EmailSingleStockChangeNotification = 10
'  EmailPortfolioStockChangeNotification = 11

'  EmailPeriodicInstrumentInformation = 20
'  EmailPage = 21

'End Enum

' DEFINED IN 'NAPLESGLOBALS' !!!

'Public Enum DataTaskDay As Byte
'  Monday = 1
'  Tuesday = 2
'  Wednesday = 4
'  Thursday = 8
'  Friday = 16
'  Saturday = 32
'  Sunday = 64
'  Clear = 128

'  EveryDay = 127
'  WeekDays = 31
'  WeekEnds = 96
'End Enum

Public Enum Relationship As Integer
  [GreaterThan] = 1
  [LessThan] = 2
End Enum

' Flags for the TransactionAdminStatus and TransactionCTFLAGS field.

' AdminStatus
Public Enum AdminStatus As Integer
  General_NonCT = 0
  General_CompoundTransaction = (-1)

  PerformanceFees = (-11)
  ManagementFees = (-10)
End Enum

Public Enum Months As Integer
  Jan = 1
  Feb = 2
  Mar = 3
  Apr = 4
  May = 5
  Jun = 6
  Jul = 7
  Aug = 8
  Sep = 9
  Oct = 10
  Nov = 11
  Dec = 12
End Enum

' Flags for the CompoundTransactionTemplate!TemplateFlags Field
' These flags are bit flags. 1, 2, 4, 8, 16 etc. Limit at 2^15 as the High order word is used for
' other purposes - Storing the CT Template Step Number

Public Enum CompoundTransactionFlags As Integer
  WithAddEditTransaction = 1
  ExcludeFromPriceCascadeUpdates = 2
  UsesIsTransferFlag = 4
End Enum

Public Enum Attribution_RecordPreference As Integer
  OnlyMilestone = 1
  OnlyNonMilestone = 2
  PreferMilestone = 3
  PreferNonMilestone = 4
End Enum

Public Enum RiskExposureFlags As Integer
  KnowledgeDateWholeDay = 1
	SystemKnowledgeDateWholeDay = 2
  TransactionStatusGroup = 4
  FXForwardsSeparate = 8
  FXTradesSeparate = 16
  FundLookthrough = 128 ' Do not change this without good cause. Referred to in SQL Code.
  AggregatedRisk = 256
End Enum

<FlagsAttribute()> _
 Public Enum RenaissanceDataType As Integer
  None = 0
  TextType = 1
  DateType = 2
  NumericType = 4
  PercentageType = 8
  BooleanType = 16
End Enum

Public Enum FlorenceLimitType As Integer
  Between = 1
  Outside = 2
  LessThan = 3
  GreaterThan = 4
  [Boolean] = 5
End Enum

Public Enum PertracCustomFieldTypes As Integer
  ' Relates to the 'CustomFieldType' field in the 'tblPertracCustomFields' table.

  CustomNumeric = 1
  CustomDate = 2
  CustomString = 3
  CustomBoolean = 4

  SingleReturn = 5
  PeriodReturn = 6
  ReturnDate = 7

  DrawDown = 8
  DrawUp = 9

  Volatility = 10
  StandardDeviation = 11
  SharpeRatio = 12
  Correlation = 13
  Beta = 14
  Alpha = 15

End Enum

Public Enum PertracCustomFieldTypes_NotCalculated As Integer
  ' Defines Which of the fields in 'PertracCustomFieldTypes' are not calculated fields.

  CustomNumeric = 1
  CustomDate = 2
  CustomString = 3
  CustomBoolean = 4

End Enum

Public Enum UnitPriceType As Integer
  '--			(1) GAV		Gross Asset Value Excl all expenses and fees.
  '--			(2) Basic NAV 	Excluding Mgmt & Incentive fees, but including all other expenses
  '--											NPP. 23/5/07. Change to include Mgmt fees from prevoius months. This appears to 
  '--											be correct and gives the correct AV from which to base this Months Management fees.
  '--			(3) GNAV	NAV, excluding only Performance (Incentive) fees.
  '--			(4) NAV		NAV, including all expenses etc. Also Null, 0

  NAV = 0
  GAV = 1
  BNAV = 2
  GNAV = 3
End Enum

Public Enum MeetingStatus As Integer

  Ordinary = 1
  RedFlag = 2
  Noteworthy = 3

End Enum

Public Enum RiskCharacteristicType As Integer
	Normal = 0
	Compound_Min = 1
	Compound_Max = 2
	Compound_Sum = 3
	Compound_Boolean = 4
	Compound_Average = 5
End Enum

Public Enum CalculationTypes_Profit As Integer

	FIFO = 1
	LIFO = 2
	AveragePrice = 3

End Enum

Public Enum BenchmarkCalculationMethod As Integer
	' For tblBenchmarkIndex.BenchmarkCalculationMethod

	BestIndex = 1
	WorstIndex = 2
	AverageIndex = 4

End Enum

Public Enum BenchmarkItemType As Integer
	' Partially mimics PertracInstrumentFlags
	'
	' Current range allows values 0 to 15.
	'

	PertracInstrument = 0	' Pertrac really has to be Zero as an absence of flags denotes a default type of Pertrac Instrument.

	Group_Median = 1
	Group_Mean = 2
	Group_Weighted = 3

	VeniceInstrument = 8 ' For legacy reasons (not now necessary I believe) Venice Instrument is 8 so that the single Bit set is bit 31 : i.e. the same as it used to be.

	CTA_Simulation = 10

	FixedReturn = 11	' eg 3% pa

End Enum

Public Enum BenchmarkItemCalculationMethod As Integer

	DailyCompounding = 1								' e.g. 3% compounds to 3.036% at end of year
	CompoundToAnnualisedFixedReturn = 2	' i.e. Daily compounding results in 3% at end of year

End Enum

Public Enum EMSX_OrderStatus As Integer

  ' Progress trades in ascending numeric order. Used when comparing existing order status to the one held in the Database.
  ' Also, this enumeration is referenced in the adp_tblTradeFileEmsxStatus_SelectActive Stored Procedure.

  Error_Recieved = -10
  None = 0                      ' Nothing yet done regarding this order.
  NotPlaced = 10                ' Order read by Kansas, noy yet placed with EMSX
  Placed_AwaitingResponse = 20  ' Order placed with EMSX, Response message not yet received.
  Placed_ResponseReceived = 30  ' Order placed and Response received.
  Routed_AwaitingResponse = 40  ' Order routed to broker, Response message not yet received.
  Routed_ResponseReceived = 50  ' Order routed to broker, Response message received.
  Order_Working = 60            ' Order placed, routed and one or more order messages received - thus is a working order.
  Order_Filled = 70             ' Order message indicating total fill has been received.
  Completed = 80                ' Ordr completed and updated in Venice.

  ' Cancelation status'

  Cancelled_RoutesPendingResponse = 100     ' Venice order status set to cancelled. Request to cancel routes has been issued.
  Cancelled_RoutesPendingConfirmation = 110 ' Request response received, broker responses pending.
  Cancelled_RoutesConfirmed = 120           ' Route cancellation response has been received.
  Cancelled_Route_Unexpected = 121          ' Route status is CANCEL, but I didn't ask for it.
  Cancelled_OrderPendingConfirmation = 130  ' Request to cancel order has been issued.
  Cancelled_OrderConfirmed = 140            ' Order cancellation response has been received.

  ' End of the line.

  Deleted = 1000                ' Order deletion message has been received. Will now be tidied away.

End Enum

Public Enum FeesType As Integer

  FixedValue = 1
  PercentageValue = 2

End Enum

Public Enum FeesPercentageType As Integer

  PreviousNAV = 1
  ValueAfterSubscriptions = 2
  ValueBeforeFees = 4

End Enum

#End Region

#Region " Genoa Enums"

Public Enum SpecialConstraintFields
  Sector = (-1)
  Liquidity = (-2)

  IndividualInstrumentLimit = (-15) ' Arbitrary number.
End Enum

Public Enum ConstraintType As Integer
  ctEqualTo = 1
  ctGreaterThan = 2
  ctLessThan = 3
End Enum

#End Region
