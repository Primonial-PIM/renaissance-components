﻿
''' <summary>
''' Class BNP_SimpleTradeReportingClass
''' </summary>
Public Class BNP_SimpleTradeReportingClass
  ''' <summary>
  ''' The _ culture name
  ''' </summary>
  Private _CultureName As String = ""
  ''' <summary>
  ''' The _ culture info
  ''' </summary>
  Private _CultureInfo As Globalization.CultureInfo
  ''' <summary>
  ''' The numeric precision
  ''' </summary>
  Private _NumericPrecision As Integer
  ''' <summary>
  ''' The double format string
  ''' </summary>
  Private DoubleFormatString As String = "###0.0#####"
  ''' <summary>
  ''' The _ is valid
  ''' </summary>
  Private _IsValid As Boolean = True ' Information only.
  ''' <summary>
  ''' The _ status string
  ''' </summary>
  Private _StatusString As String = "" ' Information only.

  Private Const _DateFormat As String = "yyyyMMdd"

  ''' <summary>
  ''' The trade terms
  ''' </summary>
  Private TradeTerms(15) As String

  ''' <summary>
  ''' Enum FDS_FieldPositions
  ''' </summary>
  Private Enum FieldPositions As Integer
    ClientPortfolioCode = 0
    OrderReference = 1
    ExecutionDate = 2
    SettlementDate = 3
    TradeType = 4
    ISIN = 5
    QuoteCountry = 6
    Quantity = 7
    QuantityType = 8
    Price = 9
    QuotationCurrency = 10
    BrokerComission = 11
    SettlementCurrency = 12
    BrokerBicCode = 13
    FXRate = 14
  End Enum

  Enum FieldSizes As Integer
    ClientPortfolioCode = 30
    OrderReference = 13
    ExecutionDate = 8
    SettlementDate = 8
    TradeType = 1
    ISIN = 12
    QuoteCountry = 3
    Quantity = 15
    QuantityType = 4
    Price = 14
    QuotationCurrency = 3
    BrokerComission = 8
    SettlementCurrency = 3
    BrokerBicCode = 11
    FXRate = 14
  End Enum

  ''' <summary>
  ''' Initializes a new instance of the <see cref="BNP_SimpleTradeReportingClass"/> class.
  ''' </summary>
  Public Sub New()
    _IsValid = True
    _StatusString = ""

    _CultureName = ""
    _NumericPrecision = 2
    _CultureInfo = New Globalization.CultureInfo(_CultureName)
    DoubleFormatString = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision - 1)))

    ' Defaults

    TradeTerms(FieldPositions.FXRate) = "1" ' Default

  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="BNP_SimpleTradeReportingClass"/> class.
  ''' </summary>
  ''' <param name="CultureName">Name of the culture.</param>
  ''' <param name="NumericPrecision">The numeric precision.</param>
  Public Sub New(ByVal CultureName As String, ByVal NumericPrecision As Integer)

    Me.New()

    _CultureName = CultureName
    _NumericPrecision = NumericPrecision
    _CultureInfo = New Globalization.CultureInfo(_CultureName)

    If (NumericPrecision < 1) Then
      DoubleFormatString = "###0"
    Else
      DoubleFormatString = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision - 1)))
    End If

  End Sub

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is valid.
  ''' </summary>
  ''' <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
  Public Property IsValid() As Boolean
    Get
      Return _IsValid
    End Get
    Set(ByVal value As Boolean)
      _IsValid = value
    End Set
  End Property

  ''' <summary>
  ''' Gets the Client Portfolio code.
  ''' </summary>
  ''' <value>The product code.</value>
  Public Property ClientPortfolioCode() As String
    Get
      Return TradeTerms(FieldPositions.ClientPortfolioCode)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.ClientPortfolioCode) = Left(value, FieldSizes.ClientPortfolioCode)
    End Set
  End Property

  ''' <summary>
  ''' Gets the Order Reference (TransactionParentID) code.
  ''' </summary>
  ''' <value>The product code.</value>
  Public Property OrderReference() As String
    Get
      Return TradeTerms(FieldPositions.OrderReference)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.OrderReference) = Left(value, FieldSizes.OrderReference)
    End Set
  End Property

  ''' <summary>
  ''' Gets / Sets the execution date.
  ''' </summary>
  ''' <value>The product code.</value>
  Public Property ExecutionDate() As Date
    Get
      Return Date.ParseExact(TradeTerms(FieldPositions.ExecutionDate), _DateFormat, Nothing)
    End Get
    Set(ByVal value As Date)
      TradeTerms(FieldPositions.ExecutionDate) = value.ToString(_DateFormat)
    End Set
  End Property

  ''' <summary>
  ''' Gets / Sets the settlement date.
  ''' </summary>
  ''' <value>The product code.</value>
  Public Property SettlementDate() As Date
    Get
      Return Date.ParseExact(TradeTerms(FieldPositions.SettlementDate), _DateFormat, Nothing)
    End Get
    Set(ByVal value As Date)
      TradeTerms(FieldPositions.SettlementDate) = value.ToString(_DateFormat)
    End Set
  End Property

  ''' <summary>
  ''' Gets the Order Reference (TransactionParentID) code.
  ''' </summary>
  ''' <value>The product code.</value>
  Public Property TradeType() As String
    Get
      Return TradeTerms(FieldPositions.OrderReference)
    End Get
    Set(ByVal value As String)

      Select Case value.ToUpper

        Case "A", "B", "ACHAT", "BUY"

          TradeTerms(FieldPositions.TradeType) = "A"

        Case "V", "S", "VENTE", "SELL"

          TradeTerms(FieldPositions.TradeType) = "V"

        Case Else

          TradeTerms(FieldPositions.TradeType) = ""

      End Select

    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the ISIN.
  ''' </summary>
  ''' <value>The ISIN.</value>
  Public Property ISIN() As String
    Get
      Return TradeTerms(FieldPositions.ISIN)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.ISIN) = Left(Trim(value), FieldSizes.ISIN)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the ISIN.
  ''' </summary>
  ''' <value>The ISIN.</value>
  Public Property QuoteCountry() As String
    Get
      Return TradeTerms(FieldPositions.QuoteCountry)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.QuoteCountry) = Left(Trim(value), FieldSizes.QuoteCountry)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Quantity value.
  ''' </summary>
  ''' <value>The Quantity value.</value>
  Public WriteOnly Property Quantity_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, 4) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = Math.Round(value, 2).ToString(DoubleFormatString, _CultureInfo)
      End If

      TradeTerms(FieldPositions.Quantity) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade quantity.
  ''' </summary>
  ''' <value>The Quantity.</value>
  Public Property Quantity() As String
    Get
      Return TradeTerms(FieldPositions.Quantity)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.Quantity) = Left(Trim(value), FieldSizes.Quantity)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Unit of measurement for Quantity.
  ''' </summary>
  ''' <value>The Quantity Type.</value>
  Public Property QuantityType() As String
    Get
      Return TradeTerms(FieldPositions.QuantityType)
    End Get
    Set(ByVal value As String)
      Select Case value.ToUpper

        Case "FAMT", "AMOUNT"

          TradeTerms(FieldPositions.QuantityType) = "FAMT"

        Case Else

          TradeTerms(FieldPositions.QuantityType) = "UNIT"

      End Select

    End Set
  End Property

  ''' <summary>
  ''' Sets the Price value.
  ''' </summary>
  ''' <value>The Price value.</value>
  Public WriteOnly Property Price_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, 4) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = Math.Round(value, 2).ToString(DoubleFormatString, _CultureInfo)
      End If

      TradeTerms(FieldPositions.Price) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade Price.
  ''' </summary>
  ''' <value>The Price.</value>
  Public Property Price() As String
    Get
      Return TradeTerms(FieldPositions.Price)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.Price) = Left(Trim(value), FieldSizes.Price)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the quotation currency.
  ''' </summary>
  ''' <value>The quotation currency.</value>
  Public Property QuotationCurrency() As String
    Get
      Return TradeTerms(FieldPositions.QuotationCurrency)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.QuotationCurrency) = Left(Trim(value), FieldSizes.QuotationCurrency)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Commission value.
  ''' </summary>
  ''' <value>The Commission value.</value>
  Public WriteOnly Property BrokerComission_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, 4) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = Math.Round(value, 2).ToString(DoubleFormatString, _CultureInfo)
      End If

      TradeTerms(FieldPositions.BrokerComission) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade Commission.
  ''' </summary>
  ''' <value>The Commission.</value>
  Public Property BrokerComission() As String
    Get
      Return TradeTerms(FieldPositions.BrokerComission)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BrokerComission) = Left(Trim(value), FieldSizes.BrokerComission)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the settlement currency.
  ''' </summary>
  ''' <value>The quotation currency.</value>
  Public Property SettlementCurrency() As String
    Get
      Return TradeTerms(FieldPositions.SettlementCurrency)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.SettlementCurrency) = Left(Trim(value), FieldSizes.SettlementCurrency)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Broker Bic Code.
  ''' </summary>
  ''' <value>The Broker Bic Code.</value>
  Public Property BrokerBicCode() As String
    Get
      Return TradeTerms(FieldPositions.BrokerBicCode)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BrokerBicCode) = Left(Trim(value), FieldSizes.BrokerBicCode)
    End Set
  End Property

  ''' <summary>
  ''' Sets the FXRate value.
  ''' </summary>
  ''' <value>The FXRate value.</value>
  Public WriteOnly Property FXRate_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, 4) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = Math.Round(value, 2).ToString(DoubleFormatString, _CultureInfo)
      End If

      TradeTerms(FieldPositions.FXRate) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade FXRate.
  ''' </summary>
  ''' <value>The FXRate.</value>
  Public Property FXRate() As String
    Get
      Return TradeTerms(FieldPositions.FXRate)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.FXRate) = Left(Trim(value), FieldSizes.FXRate)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the trade string.
  ''' </summary>
  ''' <value>The trade string.</value>
  Public Property TradeString() As String
    Get
      Return String.Join(";", TradeTerms)
    End Get
    Set(ByVal value As String)
      Dim TempArray() As String
      Dim Counter As Integer

      TempArray = value.Split(New Char() {";"c}, StringSplitOptions.None)

      If (TempArray.Length >= TradeTerms.Length) Then
        For Counter = 0 To (TradeTerms.Length - 1)
          If (TempArray.Length > Counter) Then
            TradeTerms(Counter) = TempArray(Counter)
          End If
        Next

      End If
    End Set
  End Property

End Class


''' <summary>
''' Class BNP_TradeReportingClass_Securities
''' </summary>
Public Class BNP_TradeReportingClass_Securities
  ''' <summary>
  ''' The _ culture name
  ''' </summary>
  Private _CultureName As String = ""
  ''' <summary>
  ''' The _ culture info
  ''' </summary>
  Private _CultureInfo As Globalization.CultureInfo
  ''' <summary>
  ''' The numeric precision
  ''' </summary>
  Private _NumericPrecision_Quantity As Integer
  Private _NumericPrecision_Cash As Integer
  Private _NumericPrecision_FX As Integer
  ''' <summary>
  ''' The double format string
  ''' </summary>
  Private DoubleFormatString_Quantity As String = "###0.0#####"
  Private DoubleFormatString_Cash As String = "###0.0#####"
  Private DoubleFormatString_FX As String = "###0.0#####"
  ''' <summary>
  ''' The _ is valid
  ''' </summary>
  Private _IsValid As Boolean = True ' Information only.
  ''' <summary>
  ''' The _ status string
  ''' </summary>
  Private _StatusString As String = "" ' Information only.

  Private Const _DateFormat_short As String = "yyyyMMdd"
  Private Const _TimeFormat As String = "HHmmss"
  Private Const _DateFormat_long As String = "yyyyMMddHHmmss"

  ''' <summary>
  ''' The trade terms
  ''' </summary>
  Private TradeTerms(72) As String

  ''' <summary>
  ''' Enum FDS_FieldPositions
  ''' </summary>
  Private Enum FieldPositions As Integer
    PortfolioCode = 0
    ExternalReferenceCode = 1
    TradeStatus = 2
    SecurityType = 3
    TradeDate = 4
    SettlementDate = 5
    TradeType = 6
    ISIN = 7
    QuoteCountry = 8
    Quantity = 9
    IndexationCoefficient = 10
    Price = 11
    PriceType = 12
    SecurityCurrency = 13
    GrossAmount = 14
    BrokerFees = 15
    BrokerFeesVAT = 16
    BrokerFeesCurrency = 17
    StockExchangeTax = 18
    AccruedInterest = 19
    BrokerNetAmount = 20
    SettlementCurrency = 21
    AssetManagementFees = 22
    AssetManagementFeesVAT = 23
    AssetManagementFeesCurrencyIndicator = 24
    NetAmount = 25
    BrokerCode = 26
    BrokerName = 27
    FinalBroker = 28
    FinalBrokerName = 29
    DeliveryPlaceReference = 30
    DeliveryPlaceDetails = 31
    DeliveryPlaceAccountReference = 32
    BlockCharacteristics = 33
    Comment = 34
    BlockReference = 35
    StampDuty = 36
    OtherTaxes = 37
    AssetManagerFeesIndicator = 38
    QuantityExpressionMode = 39
    Counter = 40
    CodificationType = 41
    TradeTime = 42
    PoolFactor = 43
    TraderID = 44
    ExchangeRate = 45
    AllocationNumberInBlock = 46
    New_Type = 47
    New_LegalType = 48
    New_PrePostCompte = 49
    New_FixedRate = 50
    New_FloatingRate = 51
    New_Code = 52
    New_ClientCode = 53
    New_Label = 54
    New_Currency = 55
    New_IssuerCode = 56
    New_IssuerName = 57
    New_IssueDate = 58
    New_MaturityDate = 59
    New_Margin = 60
    New_BasisForComputation = 61
    New_CapitalisationModus = 62
    New_HolidaysIncluded = 63
    New_Periodicity = 64
    New_DateOfFirstPayment = 65
    New_Rating = 66
    New_RateType = 67
    New_Comment = 68
    New_CotationPlace = 69
    New_NominalAmount = 70
    New_NominalCurrency = 71
  End Enum

  Enum FieldSizes As Integer
    PortfolioCode = 30
    ExternalReferenceCode = 13
    TradeStatus = 1
    SecurityType = 2
    TradeDate = 8
    SettlementDate = 8
    TradeType = 1
    ISIN = 12
    QuoteCountry = 2
    Quantity = 12
    IndexationCoefficient = 11
    Price = 15
    PriceType = 2
    SecurityCurrency = 3
    GrossAmount = 19
    BrokerFees = 9
    BrokerFeesVAT = 9
    BrokerFeesCurrency = 3
    StockExchangeTax = 9
    AccruedInterest = 21
    BrokerNetAmount = 19
    SettlementCurrency = 3
    AssetManagementFees = 9
    AssetManagementFeesVAT = 9
    AssetManagementFeesCurrencyIndicator = 1
    NetAmount = 19
    BrokerCode = 11
    BrokerName = 50
    FinalBroker = 11
    FinalBrokerName = 50
    DeliveryPlaceReference = 1
    DeliveryPlaceDetails = 1
    DeliveryPlaceAccountReference = 5
    BlockCharacteristics = 3
    Comment = 255
    BlockReference = 10
    StampDuty = 9
    OtherTaxes = 9
    AssetManagerFeesIndicator = 1
    QuantityExpressionMode = 4
    Counter = 2
    CodificationType = 7
    TradeTime = 6
    PoolFactor = 9
    TraderID = 10
    ExchangeRate = 15
    AllocationNumberInBlock = 11
    New_Type = 3
    New_LegalType = 4
    New_PrePostCompte = 1
    New_FixedRate = 13
    New_FloatingRate = 15
    New_Code = 12
    New_ClientCode = 8
    New_Label = 50
    New_Currency = 3
    New_IssuerCode = 12
    New_IssuerName = 50
    New_IssueDate = 8
    New_MaturityDate = 8
    New_Margin = 13
    New_BasisForComputation = 3
    New_CapitalisationModus = 1
    New_HolidaysIncluded = 1
    New_Periodicity = 2
    New_DateOfFirstPayment = 8
    New_Rating = 3
    New_RateType = 1
    New_Comment = 255
    New_CotationPlace = 2
    New_NominalAmount = 3
    New_NominalCurrency = 4
  End Enum

  ''' <summary>
  ''' Initializes a new instance of the <see cref="BNP_TradeReportingClass_Securities"/> class.
  ''' </summary>
  Public Sub New()
    _IsValid = True
    _StatusString = ""

    _CultureName = ""
    _NumericPrecision_Quantity = 2
    _NumericPrecision_Cash = 2
    _NumericPrecision_FX = 6
    _CultureInfo = New Globalization.CultureInfo(_CultureName)

    DoubleFormatString_Quantity = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision_Quantity - 1)))
    DoubleFormatString_Cash = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision_Cash - 1)))
    DoubleFormatString_FX = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision_FX - 1)))

    ' Defaults

    TradeTerms(FieldPositions.TradeStatus) = "E" ' Default
    TradeTerms(FieldPositions.Quantity) = "0" ' 
    TradeTerms(FieldPositions.IndexationCoefficient) = "1" ' 
    TradeTerms(FieldPositions.PriceType) = "20" ' Default : Units
    TradeTerms(FieldPositions.GrossAmount) = "0" ' 
    TradeTerms(FieldPositions.BrokerFees) = "0" ' 
    TradeTerms(FieldPositions.AccruedInterest) = "0" ' 
    TradeTerms(FieldPositions.BrokerNetAmount) = "0" ' 
    TradeTerms(FieldPositions.NetAmount) = "0" ' 
    TradeTerms(FieldPositions.CodificationType) = "ISIN" ' 
    TradeTerms(FieldPositions.PoolFactor) = "1" ' 
    TradeTerms(FieldPositions.TraderID) = "PAM_FR" ' 
    TradeTerms(FieldPositions.ExchangeRate) = "1" ' 
    TradeTerms(FieldPositions.AllocationNumberInBlock) = "001" ' 

  End Sub

  ''' <summary>
  ''' Initializes a new instance of the <see cref="BNP_TradeReportingClass_Securities"/> class.
  ''' </summary>
  ''' <param name="CultureName">Name of the culture.</param>
  Public Sub New(ByVal CultureName As String, ByVal NumericPrecision_Quantity As Integer, ByVal NumericPrecision_Cash As Integer, ByVal NumericPrecision_FX As Integer)

    Me.New()

    _CultureName = CultureName
    _NumericPrecision_Quantity = NumericPrecision_Quantity
    _NumericPrecision_Cash = NumericPrecision_Cash
    _NumericPrecision_FX = NumericPrecision_FX
    _CultureInfo = New Globalization.CultureInfo(_CultureName)

    If (_NumericPrecision_Quantity < 1) Then
      DoubleFormatString_Quantity = "###0"
    Else
      DoubleFormatString_Quantity = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision_Quantity - 1)))
    End If

    If (_NumericPrecision_Cash < 1) Then
      DoubleFormatString_Cash = "###0"
    Else
      DoubleFormatString_Cash = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision_Cash - 1)))
    End If

    If (_NumericPrecision_FX < 1) Then
      DoubleFormatString_FX = "###0"
    Else
      DoubleFormatString_FX = "###0.0" & (New String("#"c, Math.Min(19, _NumericPrecision_FX - 1)))
    End If

  End Sub

  ''' <summary>
  ''' Gets or sets a value indicating whether this instance is valid.
  ''' </summary>
  ''' <value><c>true</c> if this instance is valid; otherwise, <c>false</c>.</value>
  Public Property IsValid() As Boolean
    Get
      Return _IsValid
    End Get
    Set(ByVal value As Boolean)
      _IsValid = value
    End Set
  End Property

  ''' <summary>
  ''' Gets the Portfolio code.
  ''' </summary>
  ''' <value>The Portfolio code.</value>
  Public Property ClientPortfolioCode() As String
    Get
      Return TradeTerms(FieldPositions.PortfolioCode)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.PortfolioCode) = Left(value, FieldSizes.PortfolioCode)
    End Set
  End Property

  ''' <summary>
  ''' Gets the External Reference Code.
  ''' </summary>
  ''' <value>The Transaction Parent ID.</value>
  Public Property ExternalReferenceCode() As String
    Get
      Return TradeTerms(FieldPositions.ExternalReferenceCode)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.ExternalReferenceCode) = Left(value, FieldSizes.ExternalReferenceCode)
    End Set
  End Property

  ''' <summary>
  ''' Gets the External Reference Code.
  ''' </summary>
  ''' <value>The Transaction Parent ID.</value>
  Public Property TransactionID() As String
    Get
      Return TradeTerms(FieldPositions.ExternalReferenceCode)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.ExternalReferenceCode) = Left(value, FieldSizes.ExternalReferenceCode)
    End Set
  End Property

  ''' <summary>
  ''' Gets the Trade Status.
  ''' </summary>
  ''' <value>the Trade Status.</value>
  Public ReadOnly Property TradeStatus() As String
    Get

      Return TradeTerms(FieldPositions.TradeStatus)

    End Get
  End Property


  ''' <summary>
  ''' Gets the Secutity Type.
  ''' </summary>
  ''' <value>20 For Equities, Warrants, Tracker. 30 for Bonds.</value>
  Public Property SecutityType() As String
    Get
      Return TradeTerms(FieldPositions.SecurityType)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.SecurityType) = Left(value, FieldSizes.SecurityType)

      Select Case TradeTerms(FieldPositions.SecurityType)
        Case "30" ' Bonds
          Me.PriceType = "10"
        Case Else
          Me.PriceType = "20"
      End Select

    End Set
  End Property

  ''' <summary>
  ''' Sets the Secutity Type.
  ''' </summary>
  ''' <value>Renaissance InstrumentType enumeration.</value>
  Public WriteOnly Property SecutityType_Renaissance() As RenaissanceGlobals.InstrumentTypes
    Set(ByVal value As RenaissanceGlobals.InstrumentTypes)
      Select Case value
        Case InstrumentTypes.Bond, InstrumentTypes.ConvertibleBond
          TradeTerms(FieldPositions.SecurityType) = "30"
          Me.PriceType = "10"
        Case Else
          TradeTerms(FieldPositions.SecurityType) = "20"
          Me.PriceType = "20"
      End Select
    End Set
  End Property

  ''' <summary>
  ''' Gets / Sets the Trade date.
  ''' </summary>
  ''' <value>The Trade date.</value>
  Public Property TradeDate() As Date
    Get
      Return Date.ParseExact(TradeTerms(FieldPositions.TradeDate), _DateFormat_short, Nothing)
    End Get
    Set(ByVal value As Date)
      TradeTerms(FieldPositions.TradeDate) = value.ToString(_DateFormat_short)
    End Set
  End Property

  ''' <summary>
  ''' Gets / Sets the settlement date.
  ''' </summary>
  ''' <value>The settlement date.</value>
  Public Property SettlementDate() As Date
    Get
      Return Date.ParseExact(TradeTerms(FieldPositions.SettlementDate), _DateFormat_short, Nothing)
    End Get
    Set(ByVal value As Date)
      TradeTerms(FieldPositions.SettlementDate) = value.ToString(_DateFormat_short)
    End Set
  End Property

  ''' <summary>
  ''' Gets the Trade Type.
  ''' </summary>
  ''' <value>The Trade Type.</value>
  Public Property TradeType() As String
    Get
      Return TradeTerms(FieldPositions.TradeType)
    End Get
    Set(ByVal value As String)

      Select Case value.ToUpper

        Case "A", "B", "ACHAT", "BUY"

          TradeTerms(FieldPositions.TradeType) = "A"

        Case "V", "S", "VENTE", "SELL"

          TradeTerms(FieldPositions.TradeType) = "V"

        Case Else

          TradeTerms(FieldPositions.TradeType) = ""

      End Select

    End Set
  End Property

  ''' <summary>
  ''' Sets the Trade Type.
  ''' </summary>
  ''' <value>Renaissance TradeType enumeration.</value>
  Public WriteOnly Property TradeType_Renaissance() As RenaissanceGlobals.TransactionTypes
    Set(ByVal value As RenaissanceGlobals.TransactionTypes)
      Select Case value
        Case TransactionTypes.Buy
          TradeTerms(FieldPositions.TradeType) = "A"
        Case TransactionTypes.Sell
          TradeTerms(FieldPositions.TradeType) = "V"
        Case Else
          TradeTerms(FieldPositions.TradeType) = ""
      End Select
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the ISIN.
  ''' </summary>
  ''' <value>The ISIN.</value>
  Public Property ISIN() As String
    Get
      Return TradeTerms(FieldPositions.ISIN)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.ISIN) = Left(Trim(value), FieldSizes.ISIN)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Quote Country.
  ''' </summary>
  ''' <value>Country ISO 2 char code.</value>
  Public Property QuoteCountry() As String
    Get
      Return TradeTerms(FieldPositions.QuoteCountry)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.QuoteCountry) = Left(Trim(value), FieldSizes.QuoteCountry)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Quantity value.
  ''' </summary>
  ''' <value>The Quantity value.</value>
  Public WriteOnly Property Quantity_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Quantity) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = Math.Round(value, _NumericPrecision_Quantity).ToString(DoubleFormatString_Quantity, _CultureInfo)
      End If

      TradeTerms(FieldPositions.Quantity) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade quantity.
  ''' </summary>
  ''' <value>The Quantity.</value>
  Public Property Quantity() As String
    Get
      Return TradeTerms(FieldPositions.Quantity)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.Quantity) = Left(Trim(value), FieldSizes.Quantity)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Indexation Coefficient value.
  ''' </summary>
  ''' <value>The Indexation Coefficient value.</value>
  Public WriteOnly Property IndexationCoefficient_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, 9) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString("0.000000000", _CultureInfo)
      End If

      TradeTerms(FieldPositions.IndexationCoefficient) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade IndexationCoefficient.
  ''' </summary>
  ''' <value>The IndexationCoefficient.</value>
  Public Property IndexationCoefficient() As String
    Get
      Return TradeTerms(FieldPositions.IndexationCoefficient)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.IndexationCoefficient) = Left(Trim(value), FieldSizes.IndexationCoefficient)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Price value.
  ''' </summary>
  ''' <value>The Price value.</value>
  Public WriteOnly Property Price_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, 6) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = Math.Round(value, 6).ToString("#######0.000000", _CultureInfo)
      End If

      TradeTerms(FieldPositions.Price) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade Price.
  ''' </summary>
  ''' <value>The Price.</value>
  Public Property Price() As String
    Get
      Return TradeTerms(FieldPositions.Price)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.Price) = Left(Trim(value), FieldSizes.Price)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Price Type.
  ''' </summary>
  ''' <value>The Price Type. 10 for Nominal percent, 20 for Units</value>
  Public Property PriceType() As String
    Get
      Return TradeTerms(FieldPositions.PriceType)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.PriceType) = Left(Trim(value), FieldSizes.PriceType)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Security Currency.
  ''' </summary>
  ''' <value>The Security Currency.</value>
  Public Property SecurityCurrency() As String
    Get
      Return TradeTerms(FieldPositions.SecurityCurrency)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.SecurityCurrency) = Left(Trim(value), FieldSizes.SecurityCurrency)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Trade Gross Amount.
  ''' </summary>
  ''' <value>The Trade Gross Amount.</value>
  Public WriteOnly Property GrossAmount_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.GrossAmount) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade Gross Amount.
  ''' </summary>
  ''' <value>The Trade Gross Amount.</value>
  Public Property GrossAmount() As String
    Get
      Return TradeTerms(FieldPositions.GrossAmount)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.GrossAmount) = Left(Trim(value), FieldSizes.GrossAmount)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Broker Fees Amount.
  ''' </summary>
  ''' <value>The Broker Fees Amount.</value>
  Public WriteOnly Property BrokerFees_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.BrokerFees) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Broker Fees Amount.
  ''' </summary>
  ''' <value>The Broker Fees Amount.</value>
  Public Property BrokerFees() As String
    Get
      Return TradeTerms(FieldPositions.BrokerFees)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BrokerFees) = Left(Trim(value), FieldSizes.BrokerFees)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Broker Fees VAT Amount.
  ''' </summary>
  ''' <value>The Broker Fees VAT Amount.</value>
  Public WriteOnly Property BrokerFeesVAT_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.BrokerFeesVAT) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Broker Fees VAT Amount.
  ''' </summary>
  ''' <value>The Broker Fees VAT Amount.</value>
  Public Property BrokerFeesVAT() As String
    Get
      Return TradeTerms(FieldPositions.BrokerFeesVAT)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BrokerFeesVAT) = Left(Trim(value), FieldSizes.BrokerFeesVAT)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Broker Fees Currency.
  ''' </summary>
  ''' <value>The Broker Fees currency.</value>
  Public Property BrokerFeesCurrency() As String
    Get
      Return TradeTerms(FieldPositions.BrokerFeesCurrency)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BrokerFeesCurrency) = Left(Trim(value), FieldSizes.BrokerFeesCurrency)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Stock Exchange Tax Amount.
  ''' </summary>
  ''' <value>The Stock Exchange Tax Amount.</value>
  Public WriteOnly Property StockExchangeTax_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.StockExchangeTax) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Stock Exchange Tax Amount.
  ''' </summary>
  ''' <value>The Stock Exchange Tax Amount.</value>
  Public Property StockExchangeTax() As String
    Get
      Return TradeTerms(FieldPositions.StockExchangeTax)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.StockExchangeTax) = Left(Trim(value), FieldSizes.StockExchangeTax)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Broker Accrued Interest Amount.
  ''' </summary>
  ''' <value>The Broker Accrued Interest Amount.</value>
  Public WriteOnly Property AccruedInterest_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.AccruedInterest) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Accrued Interest Amount.
  ''' </summary>
  ''' <value>The Broker Accrued Interest Amount.</value>
  Public Property AccruedInterest() As String
    Get
      Return TradeTerms(FieldPositions.AccruedInterest)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.AccruedInterest) = Left(Trim(value), FieldSizes.AccruedInterest)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Broker Fees NET Amount.
  ''' </summary>
  ''' <value>The Broker Fees NET Amount.</value>
  Public WriteOnly Property BrokerNetAmount_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.BrokerNetAmount) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Broker Fees NET Amount.
  ''' </summary>
  ''' <value>The Broker Fees NET Amount.</value>
  Public Property BrokerNetAmount() As String
    Get
      Return TradeTerms(FieldPositions.BrokerNetAmount)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BrokerNetAmount) = Left(Trim(value), FieldSizes.BrokerNetAmount)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the settlement currency.
  ''' </summary>
  ''' <value>The settlement currency.</value>
  Public Property SettlementCurrency() As String
    Get
      Return TradeTerms(FieldPositions.SettlementCurrency)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.SettlementCurrency) = Left(Trim(value), FieldSizes.SettlementCurrency)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Asset Management Fees Amount.
  ''' </summary>
  ''' <value>The Asset Management Fees Amount.</value>
  Public WriteOnly Property AssetManagementFees_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.AssetManagementFees) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Asset Management Fees Amount.
  ''' </summary>
  ''' <value>The Asset Management Fees Amount.</value>
  Public Property AssetManagementFees() As String
    Get
      Return TradeTerms(FieldPositions.AssetManagementFees)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.AssetManagementFees) = Left(Trim(value), FieldSizes.AssetManagementFees)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Asset Management Fees VAT Amount.
  ''' </summary>
  ''' <value>The Asset Management Fees VAT Amount.</value>
  Public WriteOnly Property AssetManagementFeesVAT_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.AssetManagementFeesVAT) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Asset Management Fees VAT Amount.
  ''' </summary>
  ''' <value>The Asset Management Fees VAT Amount.</value>
  Public Property AssetManagementFeesVAT() As String
    Get
      Return TradeTerms(FieldPositions.AssetManagementFeesVAT)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.AssetManagementFeesVAT) = Left(Trim(value), FieldSizes.AssetManagementFeesVAT)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Asset Management Fees Currency Indicator.
  ''' </summary>
  ''' <value>0 : Transaction Currency, 1 : Portfolio Currency.</value>
  Public Property AssetManagementFeesCurrencyIndicator() As String
    Get
      Return TradeTerms(FieldPositions.AssetManagementFeesCurrencyIndicator)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.AssetManagementFeesCurrencyIndicator) = Left(Trim(value), FieldSizes.AssetManagementFeesCurrencyIndicator)
    End Set
  End Property

  ''' <summary>
  ''' Sets the NET Amount.
  ''' </summary>
  ''' <value>The NET Amount.</value>
  Public WriteOnly Property NetAmount_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.NetAmount) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the NET Amount.
  ''' </summary>
  ''' <value>The NET Amount.</value>
  Public Property NetAmount() As String
    Get
      Return TradeTerms(FieldPositions.NetAmount)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.NetAmount) = Left(Trim(value), FieldSizes.NetAmount)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Broker Bic Code.
  ''' </summary>
  ''' <value>The Broker Bic Code.</value>
  Public Property BrokerBicCode() As String
    Get
      Return TradeTerms(FieldPositions.BrokerCode)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BrokerCode) = Left(Trim(value), FieldSizes.BrokerCode)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Broker Name.
  ''' </summary>
  ''' <value>The Broker Name.</value>
  Public Property BrokerName() As String
    Get
      Return TradeTerms(FieldPositions.BrokerName)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BrokerName) = Left(Trim(value), FieldSizes.BrokerName)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Final Broker Bic Code.
  ''' </summary>
  ''' <value>The Final Broker Bic Code.</value>
  Public Property FinalBrokerBicCode() As String
    Get
      Return TradeTerms(FieldPositions.FinalBroker)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.FinalBroker) = Left(Trim(value), FieldSizes.FinalBroker)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Final Broker Bic Code.
  ''' </summary>
  ''' <value>The Final Broker Bic Code.</value>
  Public Property FinalBrokerName() As String
    Get
      Return TradeTerms(FieldPositions.FinalBrokerName)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.FinalBrokerName) = Left(Trim(value), FieldSizes.FinalBrokerName)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Delivery Place Reference.
  ''' </summary>
  ''' <value>The Final Delivery Place Reference.</value>
  Public Property DeliveryPlaceReference() As String
    Get
      Return TradeTerms(FieldPositions.DeliveryPlaceReference)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.DeliveryPlaceReference) = Left(Trim(value), FieldSizes.DeliveryPlaceReference)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Delivery Place Details(.
  ''' </summary>
  ''' <value>The Final Delivery Place Details(.</value>
  Public Property DeliveryPlaceDetails() As String
    Get
      Return TradeTerms(FieldPositions.DeliveryPlaceDetails)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.DeliveryPlaceDetails) = Left(Trim(value), FieldSizes.DeliveryPlaceDetails)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Delivery Place Account Reference.
  ''' </summary>
  ''' <value>The Final Delivery Place Account Reference.</value>
  Public Property DeliveryPlaceAccountReference() As String
    Get
      Return TradeTerms(FieldPositions.DeliveryPlaceAccountReference)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.DeliveryPlaceAccountReference) = Left(Trim(value), FieldSizes.DeliveryPlaceAccountReference)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Block Characteristics.
  ''' </summary>
  ''' <value>Number of lines in the block. By default 1 </value>
  Public Property BlockCharacteristics() As String
    Get
      Return TradeTerms(FieldPositions.BlockCharacteristics)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BlockCharacteristics) = Left(Trim(value), FieldSizes.BlockCharacteristics)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Comment.
  ''' </summary>
  ''' <value>Comment</value>
  Public Property Comment() As String
    Get
      Return TradeTerms(FieldPositions.Comment)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.Comment) = Left(Trim(value), FieldSizes.Comment)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Block Reference.
  ''' </summary>
  ''' <value>Block Reference</value>
  Public Property BlockReference() As String
    Get
      Return TradeTerms(FieldPositions.BlockReference)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.BlockReference) = Left(TradeTerms(FieldPositions.BlockReference), FieldSizes.BlockReference)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Stamp Duty Amount.
  ''' </summary>
  ''' <value>The Stamp Duty Amount.</value>
  Public WriteOnly Property StampDuty_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.StampDuty) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Stamp Duty Amount.
  ''' </summary>
  ''' <value>The Stamp Duty Amount.</value>
  Public Property StampDuty() As String
    Get
      Return TradeTerms(FieldPositions.StampDuty)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.StampDuty) = Left(Trim(value), FieldSizes.StampDuty)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Other Taxes Amount.
  ''' </summary>
  ''' <value>The Other Taxes Amount.</value>
  Public WriteOnly Property OtherTaxes_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_Cash) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString(DoubleFormatString_Cash, _CultureInfo)
      End If

      TradeTerms(FieldPositions.OtherTaxes) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Other Taxes Amount.
  ''' </summary>
  ''' <value>The Other Taxes Amount.</value>
  Public Property OtherTaxes() As String
    Get
      Return TradeTerms(FieldPositions.OtherTaxes)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.OtherTaxes) = Left(Trim(value), FieldSizes.OtherTaxes)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Asset Manager Fees Indicator.
  ''' </summary>
  ''' <value>Asset Manager Fees Indicator : O : Exempted, N : Not Exempted</value>
  Public Property AssetManagerFeesIndicator() As String
    Get
      Return TradeTerms(FieldPositions.AssetManagerFeesIndicator)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.AssetManagerFeesIndicator) = Left(TradeTerms(FieldPositions.AssetManagerFeesIndicator), FieldSizes.AssetManagerFeesIndicator)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Quantity Expression Mode.
  ''' </summary>
  ''' <value>Quantity Expression Mode</value>
  Public Property QuantityExpressionMode() As String
    Get
      Return TradeTerms(FieldPositions.QuantityExpressionMode)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.QuantityExpressionMode) = Left(TradeTerms(FieldPositions.QuantityExpressionMode), FieldSizes.QuantityExpressionMode)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Counter.
  ''' </summary>
  ''' <value>Counter</value>
  Public Property Counter() As String
    Get
      Return TradeTerms(FieldPositions.Counter)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.Counter) = Left(TradeTerms(FieldPositions.Counter), FieldSizes.Counter)
    End Set
  End Property


  ''' <summary>
  ''' Gets or sets the Codification Type.
  ''' </summary>
  ''' <value>Codification Type. ISIN or `NEW SEC`</value>
  Public Property CodificationType() As String
    Get
      Return TradeTerms(FieldPositions.CodificationType)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.CodificationType) = Left(TradeTerms(FieldPositions.CodificationType), FieldSizes.CodificationType)
    End Set
  End Property

  ''' <summary>
  ''' Gets / Sets the Trade date.
  ''' </summary>
  ''' <value>The Trade date.</value>
  Public WriteOnly Property TradeTime() As Date
    Set(ByVal value As Date)
      TradeTerms(FieldPositions.TradeTime) = value.ToString(_TimeFormat)
    End Set
  End Property

  ''' <summary>
  ''' Sets the Pool Factor value.
  ''' </summary>
  ''' <value>The Pool Factor value.</value>
  Public WriteOnly Property PoolFactor_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, 6) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = value.ToString("0.000000", _CultureInfo)
      End If

      TradeTerms(FieldPositions.PoolFactor) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Pool Factor.
  ''' </summary>
  ''' <value>The Pool Factor.</value>
  Public Property PoolFactor() As String
    Get
      Return TradeTerms(FieldPositions.PoolFactor)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.PoolFactor) = Left(Trim(value), FieldSizes.PoolFactor)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the TraderID.
  ''' </summary>
  ''' <value>TraderID</value>
  Public Property TraderID() As String
    Get
      Return TradeTerms(FieldPositions.TraderID)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.TraderID) = Left(value, FieldSizes.TraderID)
    End Set
  End Property

  ''' <summary>
  ''' Sets the FXRate value.
  ''' </summary>
  ''' <value>The FXRate value.</value>
  Public WriteOnly Property FXRate_Value() As Double
    Set(ByVal value As Double)
      Dim CleanedValue As String

      If Math.Round(value, 0) = Math.Round(value, _NumericPrecision_FX) Then ' Integer
        CleanedValue = value.ToString("###0", _CultureInfo)
      Else
        CleanedValue = Math.Round(value, _NumericPrecision_FX).ToString(DoubleFormatString_FX, _CultureInfo)
      End If

      TradeTerms(FieldPositions.ExchangeRate) = CleanedValue
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the Trade FXRate.
  ''' </summary>
  ''' <value>The FXRate.</value>
  Public Property FXRate() As String
    Get
      Return TradeTerms(FieldPositions.ExchangeRate)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.ExchangeRate) = Left(Trim(value), FieldSizes.ExchangeRate)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the AllocationNumberInBlock.
  ''' </summary>
  ''' <value>001</value>
  Public Property AllocationNumberInBlock() As String
    Get
      Return TradeTerms(FieldPositions.AllocationNumberInBlock)
    End Get
    Set(ByVal value As String)
      TradeTerms(FieldPositions.AllocationNumberInBlock) = Left(Trim(value), FieldSizes.AllocationNumberInBlock)
    End Set
  End Property

  ''' <summary>
  ''' Gets or sets the trade string.
  ''' </summary>
  ''' <value>The trade string.</value>
  Public Property TradeString() As String
    Get
      TradeTerms(FieldPositions.BlockReference) = Me.ExternalReferenceCode

      Return String.Join(";", TradeTerms)
    End Get
    Set(ByVal value As String)
      Dim TempArray() As String
      Dim Counter As Integer

      TempArray = value.Split(New Char() {";"c}, StringSplitOptions.None)

      If (TempArray.Length >= TradeTerms.Length) Then
        For Counter = 0 To (TradeTerms.Length - 1)
          If (TempArray.Length > Counter) Then
            TradeTerms(Counter) = TempArray(Counter)
          End If
        Next

      End If
    End Set
  End Property

End Class

