
Public Enum GL_PostingType As Integer
	None = 0
	AutomaticPositng = 1
	ManualPosting = 2
	YearEndAutomaticPosting = 5
	YearEndManualPosting = 6

End Enum

Public Enum GL_PostingPhase As Byte

	Phase_None = 0
	Phase_OpeningTrade = 1
	Phase_ClosingTrade = 2
	Phase_PeriodPosting = 4

End Enum

Public Enum GL_SettlementOrder As Byte

	None = 0
	AssetThenPayment = 1
	PaymentThenAsset = 2

End Enum

Public Enum GL_SettlementPhase As Byte

	Phase_None = 0
	Phase_ValuePostings = 1
	Phase_PreSettlementPostings = 2
	Phase_SettlementPostings = 4

End Enum

Public Enum GL_SettlementStatus As Byte

	None = 0
	Unsettled = 1
	OnSettlement = 2
	Settled = 3

End Enum

Public Enum GL_PostingTime As Byte

	None = 0
	TradeDate_Opening = 2
	TradeDate_Closing = 3
	EndOfPeriod = 4
	EndOfYear = 8
	OpeningTrade = 8
	ClosingTrade = 9

End Enum

Public Enum GL_PostingValues As Integer

	None = 0
	TransactionValue = 1
	MarketValue = 2
	Unrealised_PnL = 16
	Realised_PnL = 24
	Unrealised_FX_Pnl = 32
	Realised_FX_PnL = 40
	ChangeUnRealised_PnL = 80
	ChangeRealised_PnL = 88
	ChangeUnrealised_FX_PnL = 96
	ChangeRealised_FX_PnL = 104

	'													Change	FX_PnL	Asset_PnL		Realised		Value
	'TransactionValue																										1
	'MarketValue																												2
	'Unrealised PnL															16										 16
	'Realised PnL																16						8				 24
	'Unrealised FX Pnl									32														 32
	'Realised FX PnL										32										8				 40
	'Change UnRealised PnL			64							16										 80
	'Change Realised PnL				64							16						8				 88
	'Change Unrealised FX PnL		64			32														 96
	'Change Realised FX PnL			64			32										8				 104

End Enum