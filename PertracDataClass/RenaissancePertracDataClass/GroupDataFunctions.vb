﻿Option Strict On

Imports System.IO
Imports System.Data.SqlClient
Imports RenaissanceDataClass
Imports RenaissanceGlobals

Partial Public Class PertracDataClass

	Private Function BuildGroupReturnTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pGroupID As Integer, ByVal pSampleSize As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date) As DataTable
		' ******************************************************************************************
		'
		'
		'
		' ******************************************************************************************

		Dim RVal As New DataTable
		Dim DatesArray() As Date = Nothing
		Dim ReturnsArray() As Double = Nothing
		Dim NetDeltaArray() As RenaissancePertracAndStatsGlobals.NetDeltaItemClass = Nothing

		Dim NewTableRow As DataRow
		Dim ReturnCounter As Integer

		Try

			RVal.Columns.Add("Estimate", GetType(Boolean))
			RVal.Columns.Add("FundsManaged", GetType(Double))
			RVal.Columns.Add("ID", GetType(Integer))
			RVal.Columns.Add("NAV", GetType(Double))
			RVal.Columns.Add("PerformanceDate", GetType(Date))
			RVal.Columns.Add("PerformanceReturn", GetType(Double))

			If (_StatFunctionsObject IsNot Nothing) Then

				_StatFunctionsObject.GetGroupSeries(pNestingLevel + 1, pGroupID, NoCache, KnowledgeDate, StatsDatePeriod, DatesArray, ReturnsArray, NetDeltaArray, 0UL, Nothing, pSampleSize)

				If (DatesArray IsNot Nothing) AndAlso (ReturnsArray IsNot Nothing) Then

					For ReturnCounter = 0 To (DatesArray.Length - 1)
						If (ReturnCounter < ReturnsArray.Length) Then

							NewTableRow = RVal.NewRow
							NewTableRow("Estimate") = False
							NewTableRow("FundsManaged") = 0
							NewTableRow("ID") = pGroupID
							NewTableRow("NAV") = 0
							NewTableRow("PerformanceDate") = DatesArray(ReturnCounter)
							NewTableRow("PerformanceReturn") = ReturnsArray(ReturnCounter)

							RVal.Rows.Add(NewTableRow)
						End If
					Next

				End If

			End If

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Public Sub SetDynamicGroupMembers(ByVal pGroupID As Integer, ByVal pGroupMembers() As Integer, ByVal pGroupWeights() As Double)
		' ******************************************************************************************
		' Set Array of Integers representing the members for the Given Custom Group ID.
		'
		'
		' ******************************************************************************************


		Try
			If (_StatFunctionsObject IsNot Nothing) Then
				_StatFunctionsObject.SetDynamicGroupMembers(pGroupID, pGroupMembers, pGroupWeights)
			End If

		Catch ex As Exception
		End Try

	End Sub

  Public Sub GetDynamicGroupMembers(ByVal pGroupID As Integer, ByRef pGroupMembers() As Integer, ByRef pGroupWeights() As Double, ByRef pSampleSize As Integer, ByVal pLookThrough As Boolean)
    ' ******************************************************************************************
    ' Get Array of Integers representing the members for the Given Custom Group ID.
    '
    '
    ' ******************************************************************************************

    Dim StrippedGroupID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pGroupID))

    Try

      If (_StatFunctionsObject IsNot Nothing) Then
        _StatFunctionsObject.GetDynamicGroupMembers(pGroupID, pGroupMembers, pGroupWeights, pSampleSize, pLookThrough)
      End If

    Catch ex As Exception

    End Try

  End Sub

End Class
