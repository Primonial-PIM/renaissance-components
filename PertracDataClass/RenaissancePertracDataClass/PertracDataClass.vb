Option Strict On

Imports System.IO
Imports System.Data.SqlClient
Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions
Imports RenaissancePertracAndStatsGlobals
Imports RenaissancePertracAndStatsGlobals.Globals

Imports RenaissanceGlobals
Imports System.Text

Partial Public Class PertracDataClass

#Region " Locals"

	Private _MainForm As RenaissanceGlobals.StandardRenaissanceMainForm
	Private _StatFunctionsObject As RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider

	Private PertracDataCache As Hashtable	' Integer Keyed hash table used to cache Pertrac Returns data.
	Private PertracInformationCache As Hashtable ' Integer Keyed hash table used to cache Pertrac Instrument Information.
	Private InformationColumnCache As Dictionary(Of String, Integer()) = Nothing

	Private _MAX_DataCacheSize As Integer
	Public Const MIN_DataCacheSize As Integer = 100
	Public Const Default_MAX_DataCacheSize As Integer = 600

	Private _MAX_InformationCacheSize As Integer
	Public Const MIN_InformationCacheSize As Integer = 20
	Public Const Default_MAX_InformationCacheSize As Integer = 150

	Private InformationColumnMap() As Integer	' PertracInformationFields
	Private MiniDataPeriodCache As New Dictionary(Of Integer, DealingPeriod)

	Private Const Information_ColumnOrdinal As Integer = 0

	' Please note : 
	'
	' The 'FlagBitRangeStart' constant is implicitly used in creating Pseudo Pertrac Instruments
	' (Created in the Mastername Select function). Modify with care.
	' As of Mar 2009 it is assumed t be 27.
	'

	Private Const FlagBitRangeStart As Integer = 27	' Flag range starts at and includes bit 27 (Base Bit Zero, i.e. is the 28th bit.)
	Private Const FlagBitRangeLength As Integer = 4	' Flag range length in bits.

	Private Const ID_Mask_ExFlags As UInteger = CUInt((2 ^ FlagBitRangeStart) - 1)
	Private Const ID_Mask_FlagsOnly As UInteger = CUInt((2 ^ (FlagBitRangeStart + FlagBitRangeLength)) - (2 ^ FlagBitRangeStart))


#End Region

#Region " Properties"

	Private ReadOnly Property MainForm() As RenaissanceGlobals.StandardRenaissanceMainForm
		Get
			Return _MainForm
		End Get
	End Property

	Public Property MAX_DataCacheSize() As Integer
		Get
			Return _MAX_DataCacheSize
		End Get
		Set(ByVal value As Integer)
			If value >= MIN_DataCacheSize Then
				_MAX_DataCacheSize = value
			End If
		End Set
	End Property

	Public Property MAX_InformationCacheSize() As Integer
		Get
			Return _MAX_InformationCacheSize
		End Get
		Set(ByVal value As Integer)
			If value >= MIN_InformationCacheSize Then
				_MAX_InformationCacheSize = value
			End If
		End Set
	End Property

	Public Property StatFunctionsObject() As RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider
		Get
			Return _StatFunctionsObject
		End Get
		Set(ByVal value As RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider)
			_StatFunctionsObject = value
		End Set
	End Property

	Public ReadOnly Property AnnualPeriodCount(ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod) As Integer
		Get
			Select Case StatsDatePeriod
				Case DealingPeriod.Daily
					Return 261 ' Business days

				Case DealingPeriod.Weekly
					Return 52	' (365 / 7)

				Case DealingPeriod.Fortnightly
					Return 26	' (365 / 14)

				Case DealingPeriod.Monthly
					Return 12

				Case DealingPeriod.Quarterly
					Return 4

				Case DealingPeriod.SemiAnnually
					Return 2

				Case DealingPeriod.Annually
					Return 1

				Case Else
					Return 12
			End Select
		End Get
	End Property

#End Region

#Region " Enumerations"
	' Please note : 
	' Various of the 'PertracInstrumentFlags' enumeration values are used in creating Pseudo Pertrac Instruments
	' (Created in the Mastername Select function). Modify these enumeration values with care.
	' see tblPertracInstrumentFlags
	' see dbo.fn_GetMastername
	' see dbo.adp_tblGroupItems_SelectGroup

	Public Enum PertracInstrumentFlags As UInteger
		' Current range allows values 0 to 15.
		'
		' NOTE !! : 'NetDeltas_Contingent()' function uses this enumeration to determine the requirement to lookthrough Net Deltas.
		' Instrument values other than 'PertracInstrument' and 'VeniceInstrument' are assumed to be a Simulation or Group of some sort 
		' and to require lookthrough processing, so If you introduce a new 'base' instrument type then you will nedd to modify the 
		' NetDeltas function accordingly.
		'

		PertracInstrument = 0	' Pertrac really has to be Zero as an absence of flags denotes a default type of Pertrac Instrument.

		Group_Median = 1
		Group_Mean = 2
		Group_Weighted = 3
		Dynamic_Group_Median = 4
		Dynamic_Group_Mean = 5
		Dynamic_Group_Weighted = 6

		DrillDown_Group_Median = 7
		DrillDown_Group_Mean = 9

		VeniceInstrument = 8 ' For legacy reasons (not now necessary I believe) Venice Instrument is 8 so that the single Bit set is bit 31 : i.e. the same as it used to be.

		CTA_Simulation = 10

	End Enum

#End Region

#Region " Classes"

	Private Class PertracCacheObject
		Private _PertracID As Integer
		Private _InstrumentFlags As PertracDataClass.PertracInstrumentFlags
		Private _NativeDataPeriod As RenaissanceGlobals.DealingPeriod
		Public PertracData As DataTable
		Public LastAccessed As Date
		Public ToDelete As Boolean

		Public ReadOnly Property IsVenicePriceSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.VeniceInstrument) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public ReadOnly Property IsGroupPriceSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.Group_Mean) Or (_InstrumentFlags = PertracInstrumentFlags.Group_Median) Or (_InstrumentFlags = PertracInstrumentFlags.Group_Weighted) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public ReadOnly Property IsDynamicGroupPriceSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.Dynamic_Group_Mean) OrElse (_InstrumentFlags = PertracInstrumentFlags.Dynamic_Group_Median) OrElse (_InstrumentFlags = PertracInstrumentFlags.Dynamic_Group_Weighted) OrElse (_InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Median) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public ReadOnly Property IsDrilldownGroupPriceSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Median) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public ReadOnly Property IsCTA_SimulationSeries() As Boolean
			Get
				If (_InstrumentFlags = PertracInstrumentFlags.CTA_Simulation) Then
					Return True
				Else
					Return False
				End If
			End Get
		End Property

		Public Property PertracID() As Integer
			Get
				Return _PertracID
			End Get
			Set(ByVal value As Integer)

				Try
					If (value >= 0) Then
						Dim UIntID As UInteger = CUInt(value)
						_PertracID = value ' CInt(UIntID And Uint_30BitMask)
						_InstrumentFlags = PertracDataClass.GetFlagsFromID(CUInt(_PertracID))
					End If

				Catch ex As Exception
				End Try

			End Set
		End Property

		Public Property DataPeriod() As RenaissanceGlobals.DealingPeriod
			Get
				Return _NativeDataPeriod
			End Get
			Set(ByVal value As RenaissanceGlobals.DealingPeriod)
				_NativeDataPeriod = value
			End Set
		End Property

		Public ReadOnly Property ID_ExFlags() As Integer
			Get
				Return CInt(GetInstrumentFromID(CUInt(_PertracID)))
			End Get
		End Property

		Public Property VeniceInstrumentID() As Integer

			Get
				If (IsVenicePriceSeries) Then
					Return ID_ExFlags
				Else
					Return 0
				End If
			End Get

			Set(ByVal value As Integer)
				_PertracID = CInt(SetFlagsToID(CUInt(value), PertracInstrumentFlags.VeniceInstrument))
			End Set

		End Property

		Public Property CTA_SimulationID() As Integer

			Get
				If (IsCTA_SimulationSeries) Then
					Return ID_ExFlags
				Else
					Return 0
				End If
			End Get

			Set(ByVal value As Integer)
				_PertracID = CInt(SetFlagsToID(CUInt(value), PertracInstrumentFlags.CTA_Simulation))
			End Set

		End Property

		Public ReadOnly Property GroupID() As Integer

			Get
				If (IsGroupPriceSeries) Then
					Return ID_ExFlags
				Else
					Return 0
				End If
			End Get

		End Property

		Private Sub New()
			_PertracID = (-1)
			_InstrumentFlags = PertracInstrumentFlags.PertracInstrument
			_NativeDataPeriod = DealingPeriod.Monthly

			PertracData = Nothing
			LastAccessed = Now
			ToDelete = False
		End Sub

		Public Sub New(ByVal pPertracID As Integer, ByVal pNativeDataPeriod As RenaissanceGlobals.DealingPeriod)
			Me.New()

			PertracID = pPertracID
			_NativeDataPeriod = pNativeDataPeriod

		End Sub

		Public Sub New(ByVal pPertracID As Integer, ByVal pNativeDataPeriod As RenaissanceGlobals.DealingPeriod, ByVal pPertracData As DataTable)
			Me.New()

			PertracID = pPertracID
			_NativeDataPeriod = pNativeDataPeriod
			PertracData = pPertracData
		End Sub

	End Class

#End Region

#Region " Constructors"

	Private Sub New()
		' Prevent use of Default New() constructor
	End Sub

	Public Sub New(ByVal pMainForm As RenaissanceGlobals.StandardRenaissanceMainForm)

		' Set MainForm local
		_MainForm = pMainForm

		' Initialise Properties
		_MAX_DataCacheSize = Default_MAX_DataCacheSize
		_MAX_InformationCacheSize = Default_MAX_InformationCacheSize
		_StatFunctionsObject = Nothing

		' Initialise Pertrac Data Structures
		PertracDataCache = New Hashtable
		PertracInformationCache = New Hashtable

		AddHandler _MainForm.RenaissanceAutoUpdate, AddressOf Me.AutoUpdate

	End Sub

	Public Sub New(ByVal pMainForm As RenaissanceGlobals.StandardRenaissanceMainForm, ByVal pStatObject As RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider)

		Me.New(pMainForm)

		Me._StatFunctionsObject = pStatObject

	End Sub

	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' *************************************************************************************
		' Respond to Update Messages : Clear Caches as necessary.
		'
		' *************************************************************************************

		Try
			Dim thisUpdateDetail As String

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Then
				Call GetActivePertracInstruments(True, -999) ' Clear Active Instruments Cache
			End If

			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Information) Then
				thisUpdateDetail = e.UpdateDetail(RenaissanceChangeID.Information)

				SyncLock PertracInformationCache
					PertracInformationCache.Clear()
				End SyncLock
				SyncLock MiniDataPeriodCache
					MiniDataPeriodCache.Clear()
				End SyncLock
			End If

			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Performance) Then
				thisUpdateDetail = e.UpdateDetail(RenaissanceChangeID.Performance)

				SyncLock PertracDataCache
					ClearDataCache(False, False, False, False, False, thisUpdateDetail)	' 
				End SyncLock
			End If

			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) Then
				thisUpdateDetail = e.UpdateDetail(RenaissanceChangeID.tblGroupItems)

				SyncLock PertracDataCache
					ClearDataCache(False, True, False, False, False, thisUpdateDetail)	' Clear Cached Group Series, Not Pertrac prices.
				End SyncLock
			End If

			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItemData) Then
				thisUpdateDetail = e.UpdateDetail(RenaissanceChangeID.tblGroupItemData)

				SyncLock PertracDataCache
					ClearDataCache(False, True, False, False, False, thisUpdateDetail)	' Clear Cached Group Series, Not Pertrac prices.
				End SyncLock
			End If

			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation) Then
				thisUpdateDetail = e.UpdateDetail(RenaissanceChangeID.tblCTA_Simulation)

				SyncLock PertracDataCache
					ClearDataCache(False, False, False, False, True, thisUpdateDetail)	' Clear Cached Simulation Series, Not Pertrac prices.
				End SyncLock
			End If

			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblInstrument) Then
				thisUpdateDetail = e.UpdateDetail(RenaissanceChangeID.tblInstrument)

				SyncLock PertracDataCache
					ClearDataCache(True, False, False, False, False, thisUpdateDetail)	' Clear Cached Venice Series, Not Pertrac prices.
				End SyncLock
			End If

			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPrice) Then
				thisUpdateDetail = e.UpdateDetail(RenaissanceChangeID.tblInstrument)

				SyncLock PertracDataCache
					ClearDataCache(True, False, False, False, False, thisUpdateDetail)	' Clear Cached Venice Series, Not Pertrac prices.
				End SyncLock

			End If

			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) Then
				SyncLock PertracDataCache
					ClearDataCache(True, True, True, True, True, "") ' Clear Cached Prices.
				End SyncLock

				Call GetActivePertracInstruments(True, -999) ' Clear Active Instruments Cache
			End If

		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Initialisation Subs"

	Public Function Set_InformationColumnMap() As Dictionary(Of String, Integer())
		' *****************************************************************************
		' Create a dictionary object containing Integer arrays mapping the 
		' 'PertracInformationFields' Enumeration to column Ordinals in the 
		' 'DSInformation.tblInformationDataTable' table.
		'
		' *****************************************************************************

		Dim MappingDS As RenaissanceDataClass.DSPertracFieldMapping
		Dim MappingTable As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingDataTable
		Dim SortedMappingRows() As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow
		Dim thisMappingRow As RenaissanceDataClass.DSPertracFieldMapping.tblPertracFieldMappingRow
		' Dim tmpCommand As New SqlCommand

		' Instantiate new Dictionary

		Dim ThisInformationColumnCache As New Dictionary(Of String, Integer())

		' Get InformationTable
		Dim myInformationTbl As New RenaissanceDataClass.DSInformation.tblInformationDataTable

		' Get Mapping Table

		Try
			MappingDS = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracFieldMapping), DSPertracFieldMapping)

			If (MappingDS Is Nothing) Then
				Return ThisInformationColumnCache
				Exit Function
			End If

			MappingTable = MappingDS.tblPertracFieldMapping
			If (MappingTable.Rows.Count <= 0) Then
				Return ThisInformationColumnCache
				Exit Function
			End If

			SortedMappingRows = CType(MappingTable.Select("true", "DataProvider"), DSPertracFieldMapping.tblPertracFieldMappingRow())
			If (SortedMappingRows Is Nothing) OrElse (SortedMappingRows.Length <= 0) Then
				Return ThisInformationColumnCache
				Exit Function
			End If

		Catch ex As Exception
			Return Nothing
			Exit Function
		End Try

		' Cycle through mapping rows, creating and populating dictionary entries and 
		' mapping arrays as necessary.

		Try
			Dim thisDataProvider As String = ""
			Dim RowCounter As Integer
			Dim thisMappingArray() As Integer = Nothing
			Dim thisArrayIndex As Integer

			Dim MappingCounter As Integer

			' For each Row....
			For RowCounter = 0 To (SortedMappingRows.Length - 1)
				thisMappingRow = SortedMappingRows(RowCounter)

				' If this is the first row, or the DataProvider has changed, create a new Dictionary entry.

				If (RowCounter = 0) OrElse (thisDataProvider <> thisMappingRow.DataProvider) Then
					' Create new Dictionary Entry
					thisDataProvider = thisMappingRow.DataProvider

					ThisInformationColumnCache.Add(thisDataProvider.ToUpper, CType(Array.CreateInstance(GetType(Integer), PertracInformationFields.MaxValue - 1), Integer()))

					' Initialise Dictionary Entry
					thisMappingArray = ThisInformationColumnCache.Item(thisDataProvider.ToUpper)

					For MappingCounter = 0 To (thisMappingArray.Length - 1)
						thisMappingArray(MappingCounter) = (-1)
					Next

					' Hard Code a couple of mappings...

          If (myInformationTbl.Columns.Contains("DataPeriod")) Then
            thisArrayIndex = CInt(PertracInformationFields.DataPeriod)
            thisMappingArray(thisArrayIndex) = myInformationTbl.Columns.IndexOf("DataPeriod")
          End If
          If (myInformationTbl.Columns.Contains("Mastername")) Then
            thisArrayIndex = CInt(PertracInformationFields.Mastername)
            thisMappingArray(thisArrayIndex) = myInformationTbl.Columns.IndexOf("Mastername")
          End If
				End If

				' Set Appropriate element of the mapping array to the correct Column ordinal.

				Try
					thisArrayIndex = (-1)

					If (thisMappingRow.PertracField.Length > 0) Then
						If (myInformationTbl.Columns.Contains(thisMappingRow.PertracField)) Then
							thisArrayIndex = CInt(System.Enum.Parse(GetType(PertracInformationFields), thisMappingRow.FCP_FieldName, True))
							If (thisMappingArray.Length > thisArrayIndex) Then
								thisMappingArray(thisArrayIndex) = myInformationTbl.Columns.IndexOf(thisMappingRow.PertracField)
							End If
						End If
					End If
				Catch ex As Exception
					If (thisArrayIndex >= 0) Then
						thisMappingArray(thisArrayIndex) = (-1)
					End If
				End Try

			Next

		Catch ex As Exception
			Return Nothing
		End Try

		Return ThisInformationColumnCache

	End Function

#End Region

#Region " Cache Management Functions"

	Public Sub ClearDataCache()
		' ******************************************************************************************
		' Clear cache unselectively.
		' ******************************************************************************************

		ClearDataCache(False, False, False, False, False, "")

	End Sub

	Public Sub ClearDataCache(ByVal ClearVenicePriceSeries As Boolean, ByVal ClearGroupSeries As Boolean, ByVal ClearCustomGroupSeries As Boolean, ByVal ClearDrilldownGroupSeries As Boolean, ByVal ClearCTA_SimulationSeries As Boolean, ByVal InstrumentIdentifier As String)
		' ******************************************************************************************
		' Clear cache unselectively.
		' ******************************************************************************************

		ClearDataCache(ClearVenicePriceSeries, ClearGroupSeries, ClearCustomGroupSeries, ClearDrilldownGroupSeries, ClearCTA_SimulationSeries, InstrumentIdentifier, True)

	End Sub

	Public Sub ClearDataCache(ByVal ClearVenicePriceSeries As Boolean, ByVal ClearGroupSeries As Boolean, ByVal ClearCustomGroupSeries As Boolean, ByVal ClearDrilldownGroupSeries As Boolean, ByVal ClearCTA_SimulationSeries As Boolean, ByVal InstrumentIdentifier As String, ByVal pClearMiniDataPeriodCache As Boolean)
		' ******************************************************************************************
		' Routine to Clear the PertracDataCache, Selectively if specified.
		'
		' ******************************************************************************************

    'Dim ThisID As Integer = 0
    Dim IDArray(-1) As String
		Dim ID_Provided As Boolean = False

		If (pClearMiniDataPeriodCache) Then
			SyncLock MiniDataPeriodCache
				If (MiniDataPeriodCache IsNot Nothing) AndAlso (MiniDataPeriodCache.Count > 0) Then
					MiniDataPeriodCache.Clear()
				End If
			End SyncLock
		End If

    If (InstrumentIdentifier IsNot Nothing) AndAlso (Trim(InstrumentIdentifier).Length > 0) Then ' AndAlso (IsNumeric(InstrumentIdentifier)) Then
      IDArray = InstrumentIdentifier.Split(New Char() {","c, "|"c}, StringSplitOptions.RemoveEmptyEntries)

      If (IDArray.Length > 0) Then
        ID_Provided = True
      End If
    End If

    ' Default to one element array.
    If (IDArray.Length <= 0) Then
      IDArray = New String() {"0"}
    End If


		If (ID_Provided) OrElse (ClearVenicePriceSeries) OrElse (ClearGroupSeries) OrElse (ClearCustomGroupSeries) OrElse (ClearDrilldownGroupSeries) Then

      'Dim ItemCounter As Integer
      Dim ThisArrayElement As String
      Dim ThisID As Integer
			Dim KeyArray(PertracDataCache.Count) As Integer
			Dim KeyCount As Integer = 0
			Dim thisPertracCacheObject As PertracCacheObject

      For Each ThisArrayElement In IDArray
        If IsNumeric(Trim(ThisArrayElement)) Then
          ThisID = CInt(Trim(ThisArrayElement))
        Else
          ThisID = 0
        End If

        For Each thisPertracCacheObject In PertracDataCache.Values

          If (thisPertracCacheObject IsNot Nothing) Then

            Try
              If (KeyArray.Contains(thisPertracCacheObject.PertracID) = False) Then
                If (ClearVenicePriceSeries) AndAlso (thisPertracCacheObject.IsVenicePriceSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.VeniceInstrumentID = ThisID)) Then
                  KeyArray(KeyCount) = thisPertracCacheObject.PertracID
                  KeyCount += 1
                ElseIf (ClearGroupSeries) AndAlso (thisPertracCacheObject.IsGroupPriceSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.GroupID = ThisID)) Then
                  KeyArray(KeyCount) = thisPertracCacheObject.PertracID
                  KeyCount += 1
                ElseIf (ClearCustomGroupSeries) AndAlso (thisPertracCacheObject.IsDynamicGroupPriceSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.GroupID = ThisID)) Then
                  KeyArray(KeyCount) = thisPertracCacheObject.PertracID
                  KeyCount += 1
                ElseIf (ClearDrilldownGroupSeries) AndAlso (thisPertracCacheObject.IsDrilldownGroupPriceSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.GroupID = ThisID)) Then
                  KeyArray(KeyCount) = thisPertracCacheObject.PertracID
                  KeyCount += 1
                ElseIf (ClearCTA_SimulationSeries) AndAlso (thisPertracCacheObject.IsCTA_SimulationSeries) AndAlso ((Not ID_Provided) OrElse (thisPertracCacheObject.CTA_SimulationID = ThisID)) Then
                  KeyArray(KeyCount) = thisPertracCacheObject.PertracID
                  KeyCount += 1
                ElseIf (Not (ClearVenicePriceSeries Or ClearGroupSeries Or ClearCustomGroupSeries)) AndAlso (ID_Provided) AndAlso (thisPertracCacheObject.PertracID = ThisID) Then
                  KeyArray(KeyCount) = thisPertracCacheObject.PertracID
                  KeyCount += 1
                ElseIf (thisPertracCacheObject.IsDynamicGroupPriceSeries) Then
                  ' Always flush Dynamic Group series. Not possible to tell easily what they depend on.

                  KeyArray(KeyCount) = thisPertracCacheObject.PertracID
                  KeyCount += 1
                End If
              End If

            Catch ex As Exception
            End Try

          End If

        Next ' For Each thisPertracCacheObject

      Next ' For Each ThisArrayElement

			Dim ItemCounter As Integer

			If (KeyCount > 0) Then
				SyncLock PertracDataCache
          For ItemCounter = 0 To (KeyCount - 1)
            Try
              If (PertracDataCache.ContainsKey(KeyArray(ItemCounter))) Then
                PertracDataCache.Remove(KeyArray(ItemCounter))
              End If
            Catch ex As Exception
            End Try
          Next
				End SyncLock
			End If

		Else

			' Clear All 

			Try
				SyncLock PertracDataCache
					PertracDataCache.Clear()
				End SyncLock
			Catch ex As Exception
			End Try

		End If



	End Sub


	Public Sub ClearInformationCache(Optional ByVal pUpdateDetail As String = "")
    ' ************************************************************************************************
    ' Clear the Information Cache.
    ' pUpdateDetail, if given, may be a ',' or '|' separated list of integer identifiers.
    ' ************************************************************************************************
    Try
      Dim IDArray(-1) As String
      Dim ThisArrayElement As String
      Dim ThisID As Integer

      Try
        If (Trim(pUpdateDetail).Length > 0) Then
          IDArray = pUpdateDetail.Split(New Char() {","c, "|"c}, StringSplitOptions.RemoveEmptyEntries)
        End If
      Catch ex As Exception
      End Try

      SyncLock PertracInformationCache
        If (IDArray.Length > 0) Then
          For Each ThisArrayElement In IDArray
            Try
              If IsNumeric(Trim(ThisArrayElement)) Then
                ThisID = CInt(Trim(ThisArrayElement))

                If (PertracInformationCache.ContainsKey(ThisID)) Then
                  PertracInformationCache.Remove(ThisID)
                End If

              End If
            Catch ex As Exception
            End Try
          Next
        Else
          PertracInformationCache.Clear()
        End If
      End SyncLock
    Catch ex As Exception
    End Try

	End Sub

	Private Class PertracCacheDateComparer
		Implements IComparer

		Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
			Dim X_CacheItem As PertracCacheObject
			Dim Y_CacheItem As PertracCacheObject

			Try
				If (TypeOf x Is PertracCacheObject) Then
					X_CacheItem = CType(x, PertracCacheObject)
				Else
					Return 0
				End If

				If (TypeOf y Is PertracCacheObject) Then
					Y_CacheItem = CType(y, PertracCacheObject)
				Else
					Return 0
				End If

				Return X_CacheItem.LastAccessed.CompareTo(Y_CacheItem.LastAccessed)
			Catch ex As Exception

				Return 0
			End Try

		End Function
	End Class

	Private Sub TrimPertracCache(ByVal pPertracCache As Hashtable, ByVal pNewCacheSize As Integer)
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Try
			If (pPertracCache.Count <= pNewCacheSize) Then
				Exit Sub
			End If
		Catch ex As Exception
		End Try

		Try
			If (pNewCacheSize <= 0) Then
				SyncLock pPertracCache
					pPertracCache.Clear()
				End SyncLock

				Exit Sub
			End If
		Catch ex As Exception
		End Try

		SyncLock pPertracCache
			Try
				Dim ValueArray(pPertracCache.Count - 1) As PertracCacheObject
				Dim ItemCounter As Integer

				pPertracCache.Values.CopyTo(ValueArray, 0)
				Array.Sort(ValueArray, New PertracCacheDateComparer)

				For ItemCounter = 0 To ((pPertracCache.Count - pNewCacheSize) - 1)
					pPertracCache.Remove(ValueArray(ItemCounter).PertracID)
				Next
			Catch ex As Exception
			End Try
		End SyncLock

	End Sub

#End Region

#Region " Data Functions"

	Public Function GetPertracDataPeriod(ByVal pFirstPertracID As Integer, ByVal pSecondPertracID As Integer) As DealingPeriod
		' *****************************************************************************
		' 
		' Return the coarsest Native date perid of two Pertrac IDs.
		'
		' *****************************************************************************

		Dim RVal As DealingPeriod = DealingPeriod.Monthly

		If (pFirstPertracID > 0) AndAlso (pSecondPertracID > 0) Then
			Dim FirstDP As DealingPeriod = GetPertracDataPeriodRecursive(pFirstPertracID, 0)
			Dim SecondDP As DealingPeriod = GetPertracDataPeriodRecursive(pSecondPertracID, 0)

			If AnnualPeriodCount(FirstDP) < AnnualPeriodCount(SecondDP) Then
				Return FirstDP
			Else
				Return SecondDP
			End If

		ElseIf (pFirstPertracID > 0) Then

			Return GetPertracDataPeriodRecursive(pFirstPertracID, 0)

		ElseIf (pSecondPertracID > 0) Then

			Return GetPertracDataPeriodRecursive(pSecondPertracID, 0)

		End If

		Return RVal

	End Function

	Public Function GetPertracDataPeriod(ByVal pPertracID As Integer) As DealingPeriod
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Return GetPertracDataPeriodRecursive(pPertracID, 0)

	End Function

	Public Sub Set_MiniDataPeriodCache(ByVal pPertracID As Integer, ByVal pDataPeriod As DealingPeriod)
		' *****************************************************************************
		' Allow an entry in the MiniDataPeriodCache to be set.
		'
		' Presented as a workaround to improve performance in the Genoa / Optimisation / Marginals code.
		'
		' *****************************************************************************

		Try

			SyncLock MiniDataPeriodCache
				If (Not MiniDataPeriodCache.ContainsKey(pPertracID)) Then
					MiniDataPeriodCache.Add(pPertracID, pDataPeriod)
				End If
			End SyncLock

		Catch ex As Exception
		End Try

	End Sub

	Public Function GetPertracDataPeriodRecursive(ByVal pPertracID As Integer, ByVal pLevel As Integer) As DealingPeriod
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Dim DataCommand As SqlCommand = Nothing
		Dim DataTable As DataTable = Nothing

		Try

			If (pPertracID <= 0) OrElse (pLevel > 10) Then

				Return DealingPeriod.Monthly

			Else

				' Check the Pertrac Data cache for this item. Return Data Period from there if possible.

				SyncLock PertracDataCache

					If (PertracDataCache.Contains(pPertracID)) Then

						Return CType(PertracDataCache(pPertracID), PertracCacheObject).DataPeriod

					End If

				End SyncLock

				' Check Mini-Cache.
				' This Mini Cache is used to prevent repeated DB queries during the Stats Cache Creation process where
				' This function may be called multiple times before the Pertrac Data Cache is populated.

				SyncLock MiniDataPeriodCache

					If MiniDataPeriodCache.ContainsKey(pPertracID) Then

						Return MiniDataPeriodCache(pPertracID)

					End If

					If (MiniDataPeriodCache.Count > 50) Then ' 50 is arbitrary.

						MiniDataPeriodCache.Clear()

					End If

				End SyncLock


			End If

			' Not in cache, retrieve from DB.

			Dim thisFlags As PertracDataClass.PertracInstrumentFlags
			Dim DataPeriodField As String = ""

			thisFlags = GetFlagsFromID(pPertracID)

			Try
				DataCommand = New SqlCommand
				DataTable = New DataTable

				DataCommand.Connection = MainForm.GetRenaissanceConnection()

				Select Case thisFlags

					Case PertracInstrumentFlags.PertracInstrument

						DataCommand.CommandType = CommandType.StoredProcedure
						DataCommand.CommandText = "[dbo].[adp_tblMastername_SelectCommand]"
						DataCommand.CommandTimeout = 600
						DataCommand.Parameters.Add("@ID", SqlDbType.Int).Value = GetInstrumentFromID(pPertracID)
						DataPeriodField = "DataPeriod"

					Case PertracInstrumentFlags.VeniceInstrument

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [InstrumentID], [InstrumentDataPeriod] FROM [dbo].[fn_tblInstrument_SelectSingle](" & GetInstrumentFromID(pPertracID).ToString & ", '" & MainForm.Main_Knowledgedate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT) & "')"
						DataCommand.CommandTimeout = 600
						DataPeriodField = "InstrumentDataPeriod"

					Case PertracInstrumentFlags.CTA_Simulation

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [SimulationID], [SimulationDataPeriod] FROM [dbo].[fn_tblCTA_Simulation_SelectKD]('" & MainForm.Main_Knowledgedate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT) & "') WHERE [SimulationID] = " & GetInstrumentFromID(pPertracID).ToString
						DataCommand.CommandTimeout = 600
						DataPeriodField = "SimulationDataPeriod"

					Case PertracInstrumentFlags.DrillDown_Group_Median, PertracInstrumentFlags.DrillDown_Group_Mean

						' ****************************************************************************
						' Hardcode Drilldown groups as Monthly
						'
						' This is done partly for performance reasons and partly because, at present, the 
						' Drilldown forms only use a Monthly Period.
						'
						' If this is changed, then allow selection to the next section which is coded to 
						' handle DrillDown Groups also.
						'
						' ****************************************************************************

						Return DealingPeriod.Monthly

					Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted, _
							 PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted
						' PertracInstrumentFlags.DrillDown_Group_Mean, PertracInstrumentFlags.DrillDown_Group_Median, _

						' ****************************************************************************
						' If the Statistics Object is set, use the Group members functions to establish a native Data Period.
						' Otherwise, assume Monthly.
						' ****************************************************************************

						If (_StatFunctionsObject Is Nothing) Then

							Return DealingPeriod.Monthly

						Else

							Dim GroupMembers() As Integer = Nothing
							Dim GroupWeights() As Double = Nothing
							Dim GroupIndex As Integer

							Select Case thisFlags

								Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted

									_StatFunctionsObject.GetGroupMembers(CInt(GetInstrumentFromID(pPertracID)), GroupMembers, GroupWeights)

								Case PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted

									_StatFunctionsObject.GetDynamicGroupMembers(CInt(GetInstrumentFromID(pPertracID)), GroupMembers, GroupWeights, -1, False)

								Case PertracInstrumentFlags.DrillDown_Group_Median, PertracInstrumentFlags.DrillDown_Group_Mean

									_StatFunctionsObject.GetDynamicGroupMembers(CInt(GetInstrumentFromID(pPertracID)), GroupMembers, GroupWeights, -1, False)

							End Select

							If (GroupMembers Is Nothing) OrElse (GroupMembers.Length <= 0) Then
								Return DealingPeriod.Monthly
							End If

							' Get DataPeriod for each Group Member and return the Coarsest Date Period 

							Dim RVal As DealingPeriod = DealingPeriod.Daily
							Dim ThisDataPeriod As DealingPeriod

							For GroupIndex = 0 To (GroupMembers.Length - 1)
								ThisDataPeriod = GetPertracDataPeriodRecursive(GroupMembers(GroupIndex), pLevel + 1)

								If (ThisDataPeriod <> RVal) AndAlso (AnnualPeriodCount(ThisDataPeriod) < AnnualPeriodCount(RVal)) Then
									RVal = ThisDataPeriod

									' Monthly Get Out
									' Assume period will not be worse than Monthly and exit For it we get to Monthly.

									If AnnualPeriodCount(RVal) <= AnnualPeriodCount(DealingPeriod.Monthly) Then
										Exit For
									End If

								End If
							Next

							' Add to MiniCache.

							SyncLock MiniDataPeriodCache
								If (Not MiniDataPeriodCache.ContainsKey(pPertracID)) Then
									MiniDataPeriodCache.Add(pPertracID, RVal)
								End If
							End SyncLock

							Return RVal

						End If

					Case Else

						' Default period for unrecognised instrument type.

						Return DealingPeriod.Monthly

				End Select

				If (DataCommand.CommandText.Length > 0) Then
					SyncLock DataCommand.Connection
						DataTable.Load(DataCommand.ExecuteReader)
					End SyncLock
				End If

			Catch ex As Exception
			Finally
				Try
					If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
						DataCommand.Connection.Close()
						DataCommand.Connection = Nothing
					End If
				Catch ex As Exception
				End Try
			End Try

			If (DataTable IsNot Nothing) AndAlso (DataTable.Rows.Count > 0) AndAlso (DataPeriodField.Length > 0) Then
				Try
					SyncLock MiniDataPeriodCache
						If (Not MiniDataPeriodCache.ContainsKey(pPertracID)) Then
							MiniDataPeriodCache.Add(pPertracID, CType(DataTable.Rows(0)(DataPeriodField), DealingPeriod))
						End If
					End SyncLock
				Catch ex As Exception
				End Try

				Return CType(DataTable.Rows(0)(DataPeriodField), DealingPeriod)
			End If

		Catch ex As Exception
		End Try

		Return DealingPeriod.Monthly

	End Function

	Public Function GetPertracDataPeriod(ByVal pPertracID As ULong) As DealingPeriod

		Dim PertracID As Integer = CInt(pPertracID And Ulong_31BitMask)
		Dim BackFillID As Integer = CInt((pPertracID >> 32) And Ulong_31BitMask)

		Return GetPertracDataPeriod(PertracID, BackFillID)

	End Function

	Public Function GetPertracDataPeriod(ByVal pFirstPertracID As ULong, ByVal pSecondPertracID As ULong) As DealingPeriod

		If (pFirstPertracID > 0) AndAlso (pSecondPertracID > 0) Then

			Dim FirstDP As DealingPeriod = GetPertracDataPeriod(pFirstPertracID)
			Dim SecondDP As DealingPeriod = GetPertracDataPeriod(pSecondPertracID)

			If AnnualPeriodCount(FirstDP) < AnnualPeriodCount(SecondDP) Then
				Return FirstDP
			Else
				Return SecondDP
			End If

		ElseIf (pFirstPertracID > 0) Then

			Return GetPertracDataPeriod(pFirstPertracID)

		ElseIf (pSecondPertracID > 0) Then

			Return GetPertracDataPeriod(pSecondPertracID)

		End If

	End Function

	Public Function GetPertracDataPeriods(ByVal pPertracIDs() As Integer) As DealingPeriod()
		' *****************************************************************************
		'
		'
		' *****************************************************************************

		Dim RVal(-1) As DealingPeriod

		Try
			If (pPertracIDs Is Nothing) OrElse (pPertracIDs.Length <= 0) Then
				Return RVal
			End If

			ReDim RVal(pPertracIDs.Length - 1)
			Dim IndexDictionary As New Dictionary(Of Integer, Integer)
			Dim Index As Integer

			Dim UserPertracCounter As Integer = 0
			Dim VeniceInstrumentCounter As Integer = 0
			Dim GroupInstrumentCounter As Integer = 0
			Dim SimulationInstrumentCounter As Integer = 0
			Dim UserPertracs As ArrayList = Nothing
			Dim VeniceInstruments As ArrayList = Nothing
			Dim GroupInstruments As ArrayList = Nothing
			Dim SimulationInstruments As ArrayList = Nothing
			Dim thisFlags As PertracDataClass.PertracInstrumentFlags
			Dim DataPeriodFound As Boolean
			Dim ThisPertracID As Integer

			SyncLock MiniDataPeriodCache
				SyncLock PertracDataCache

					For Index = 0 To (pPertracIDs.Length - 1)

						ThisPertracID = pPertracIDs(Index)
						DataPeriodFound = False

						' First chech caches for the required data priod.
						' Failing that, organise the retrieval of the required information.

						If (ThisPertracID <= 0) Then

							RVal(Index) = DealingPeriod.Monthly
							DataPeriodFound = True

						Else

							' Check the Pertrac Data cache for this item. Return Data Period from there if possible.


							If (PertracDataCache.Contains(ThisPertracID)) Then

								RVal(Index) = CType(PertracDataCache(ThisPertracID), PertracCacheObject).DataPeriod
								DataPeriodFound = True

							End If

							' Check Mini-Cache.
							' This Mini Cache is used to prevent repeated DB queries during the Stats Cache Creation process where
							' This function may be called multiple times before the Pertrac Data Cache is populated.


							If MiniDataPeriodCache.ContainsKey(ThisPertracID) Then

								RVal(Index) = MiniDataPeriodCache(ThisPertracID)
								DataPeriodFound = True

							End If

						End If

						' Not in the cache, organise retrieval. 

						If (Not DataPeriodFound) Then

							thisFlags = GetFlagsFromID(ThisPertracID)

							Select Case thisFlags

								Case PertracInstrumentFlags.PertracInstrument

									If (GetInstrumentFromID(ThisPertracID) < 1048576) Then
										RVal(Index) = DealingPeriod.Monthly	' Original Pertrac instrument, assume Monthly.
									Else

										If (UserPertracs Is Nothing) Then
											UserPertracs = New ArrayList
										End If

										UserPertracs.Add(ThisPertracID)
										IndexDictionary.Add(ThisPertracID, Index)
										UserPertracCounter += 1

									End If

								Case PertracInstrumentFlags.VeniceInstrument

									If (VeniceInstruments Is Nothing) Then
										VeniceInstruments = New ArrayList
									End If

									VeniceInstruments.Add(ThisPertracID)
									IndexDictionary.Add(ThisPertracID, Index)
									VeniceInstrumentCounter += 1

								Case PertracInstrumentFlags.CTA_Simulation

									If (SimulationInstruments Is Nothing) Then
										SimulationInstruments = New ArrayList
									End If

									SimulationInstruments.Add(ThisPertracID)
									IndexDictionary.Add(ThisPertracID, Index)
									SimulationInstrumentCounter += 1

								Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted, _
										 PertracInstrumentFlags.DrillDown_Group_Mean, PertracInstrumentFlags.DrillDown_Group_Median, _
										 PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted

									RVal(Index) = GetPertracDataPeriodRecursive(ThisPertracID, 0)	' DealingPeriod.Monthly

									' ****************************************************************************
									' Groups do not yet allow you to specify a periodicity, but if they did ....
									' ****************************************************************************

									'If (GroupInstruments Is Nothing) Then
									'  GroupInstruments = New ArrayList
									'End If

									'GroupInstruments.Add(ThisPertracID)
									'IndexDictionary.Add(ThisPertracID, Index)

									'GroupInstrumentCounter += 1

								Case Else	' Assume Monthly

									RVal(Index) = DealingPeriod.Monthly

							End Select

						End If

					Next

				End SyncLock
			End SyncLock

			' Get UserPertrac, Venice, Simulation and Standard Group Native Data Periods.

			If (UserPertracCounter + VeniceInstrumentCounter + SimulationInstrumentCounter + GroupInstrumentCounter) > 0 Then

				Dim DataCommand As New SqlCommand
				Dim DataTable As New DataTable
				Dim ID_String As StringBuilder
				Dim ThisID As Integer

				Try
					DataCommand.Connection = MainForm.GetRenaissanceConnection()

					If (UserPertracCounter > 0) Then
						Dim WholePertracIDs() As Integer
						Dim StrippedIDs(UserPertracs.Count - 1) As Integer

						WholePertracIDs = CType(UserPertracs.ToArray(GetType(Integer)), Integer())
						ID_String = New StringBuilder

						For Index = 0 To (WholePertracIDs.Length - 1)
							StrippedIDs(Index) = CInt(GetInstrumentFromID(CInt(WholePertracIDs(Index))) - 1048576)

							If (Index > 0) Then
								ID_String.Append(",")
							End If
							ID_String.Append(StrippedIDs(Index).ToString)
						Next

						UserPertracs.Clear()

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [InstrumentID], [DataPeriod] FROM [MASTERSQL].[dbo].[tblInstrumentList] WHERE [InstrumentID] IN (" & ID_String.ToString & ")"
						DataCommand.CommandTimeout = 600

						DataTable.Clear()
						DataTable.Columns.Clear()

						SyncLock DataCommand.Connection
							DataTable.Load(DataCommand.ExecuteReader)
						End SyncLock

						For Each thisRow As DataRow In DataTable.Rows
							ThisID = CInt(thisRow("InstrumentID"))

							For Index = 0 To (WholePertracIDs.Length - 1)

								If (StrippedIDs(Index) = ThisID) Then
									RVal(IndexDictionary(WholePertracIDs(Index))) = CType(CInt(thisRow("DataPeriod")), DealingPeriod)
								End If

							Next

						Next

					End If

					If (VeniceInstrumentCounter > 0) Then

						Dim WholePertracIDs() As Integer
						Dim StrippedIDs(VeniceInstruments.Count - 1) As Integer

						WholePertracIDs = CType(VeniceInstruments.ToArray(GetType(Integer)), Integer())
						ID_String = New StringBuilder

						For Index = 0 To (WholePertracIDs.Length - 1)
							StrippedIDs(Index) = CInt(GetInstrumentFromID(CInt(WholePertracIDs(Index))) - 1048576)

							If (Index > 0) Then
								ID_String.Append(",")
							End If
							ID_String.Append(StrippedIDs(Index).ToString)
						Next

						VeniceInstruments.Clear()

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [InstrumentID], [InstrumentDataPeriod] FROM [dbo].[fn_tblInstrument_SelectKD]('" & MainForm.Main_Knowledgedate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT) & "') WHERE [InstrumentID] IN (" & ID_String.ToString & ")"
						DataCommand.CommandTimeout = 600

						DataTable.Clear()
						DataTable.Columns.Clear()

						SyncLock DataCommand.Connection
							DataTable.Load(DataCommand.ExecuteReader)
						End SyncLock

						For Each thisRow As DataRow In DataTable.Rows
							ThisID = CInt(thisRow("InstrumentID"))

							For Index = 0 To (WholePertracIDs.Length - 1)

								If (StrippedIDs(Index) = ThisID) Then
									RVal(IndexDictionary(WholePertracIDs(Index))) = CType(CInt(thisRow("InstrumentDataPeriod")), DealingPeriod)
								End If

							Next

						Next

					End If

					If (SimulationInstrumentCounter > 0) Then

						Dim WholePertracIDs() As Integer
						Dim StrippedIDs(SimulationInstruments.Count - 1) As Integer

						WholePertracIDs = CType(SimulationInstruments.ToArray(GetType(Integer)), Integer())
						ID_String = New StringBuilder

						For Index = 0 To (WholePertracIDs.Length - 1)
							StrippedIDs(Index) = CInt(GetInstrumentFromID(CInt(WholePertracIDs(Index))))

							If (Index > 0) Then
								ID_String.Append(",")
							End If
							ID_String.Append(StrippedIDs(Index).ToString)
						Next

						SimulationInstruments.Clear()

						DataCommand.CommandType = CommandType.Text
						DataCommand.CommandText = "SELECT [SimulationID], [SimulationDataPeriod] FROM [dbo].[fn_tblCTA_Simulation_SelectKD]('" & MainForm.Main_Knowledgedate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT) & "') WHERE [SimulationID] IN (" & ID_String.ToString & ")"
						DataCommand.CommandTimeout = 600

						DataTable.Clear()
						DataTable.Columns.Clear()

						SyncLock DataCommand.Connection
							DataTable.Load(DataCommand.ExecuteReader)
						End SyncLock

						For Each thisRow As DataRow In DataTable.Rows
							ThisID = CInt(thisRow("SimulationID"))

							For Index = 0 To (WholePertracIDs.Length - 1)

								If (StrippedIDs(Index) = ThisID) Then
									RVal(IndexDictionary(WholePertracIDs(Index))) = CType(CInt(thisRow("SimulationDataPeriod")), DealingPeriod)
								End If

							Next

						Next

					End If

					If (GroupInstrumentCounter > 0) Then

						' ****************************************************************************
						' Groups do not yet allow you to specify a periodicity, but if they did ....
						' ****************************************************************************

						'Dim WholePertracIDs() As Integer
						'Dim StrippedIDs(GroupInstruments.Count - 1) As Integer

						'WholePertracIDs = CType(GroupInstruments.ToArray(GetType(Integer)), Integer())
						'ID_String = New StringBuilder

						'For Index = 0 To (WholePertracIDs.Length - 1)
						'  StrippedIDs(Index) = CInt(GetInstrumentFromID(CInt(WholePertracIDs(Index))))

						'  If (Index > 0) Then
						'    ID_String.Append(",")
						'  End If
						'  ID_String.Append(StrippedIDs(Index).ToString)
						'Next

						'GroupInstruments.Clear()

						'DataCommand.CommandType = CommandType.Text
						'DataCommand.CommandText = "SELECT [GroupListID], [GroupListDataPeriod] FROM [dbo].[fn_tblGroupList_SelectKD]('" & MainForm.Main_Knowledgedate.ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT) & "') WHERE [GroupListID] IN (" & ID_String.ToString & ")"
						'DataCommand.CommandTimeout = 600

						'DataTable.Clear()
						'DataTable.Columns.Clear()

						'SyncLock DataCommand.Connection
						'  DataTable.Load(DataCommand.ExecuteReader)
						'End SyncLock

						'For Each thisRow As DataRow In DataTable.Rows
						'  ThisID = CInt(thisRow("GroupListID"))

						'  For Index = 0 To (WholePertracIDs.Length - 1)

						'    If (StrippedIDs(Index) = ThisID) Then
						'      RVal(IndexDictionary(WholePertracIDs(Index))) = CType(CInt(thisRow("GroupListDataPeriod")), DealingPeriod)
						'    End If

						'  Next

						'Next

					End If

				Catch ex As Exception
				Finally

					Try
						If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
							DataCommand.Connection.Close()
							DataCommand.Connection = Nothing
						End If
					Catch ex As Exception
					End Try

				End Try

				' Default value, just in case.

				For Index = 0 To (RVal.Length - 1)
					If CInt(RVal(Index)) = 0 Then
						RVal(Index) = DealingPeriod.Monthly
					End If
				Next
			End If

		Catch ex As Exception

		End Try

		Return RVal

	End Function

	Public Function CoarsestDataPeriod(ByVal pDP1 As DealingPeriod, ByVal pDP2 As DealingPeriod) As DealingPeriod

		Try

			If (AnnualPeriodCount(pDP1) < AnnualPeriodCount(pDP2)) Then
				Return pDP1
			Else
				Return pDP2
			End If

		Catch ex As Exception
		End Try

		Return pDP1

	End Function

	Public Sub LoadPertracTables(ByVal pPertracIDs() As Integer)
		' *****************************************************************************
		' Routine to Bulk-Load a set of Pertrac tables.
		' There seems to be a large overhead in getting data from the Server, thus getting the Data for
		' 20 funds takes the same time as for One Fund. 
		' Thus it made sense to enable the class to bulk-load data.
		'
		' If Bit31 is Set then the Instrument is assumed to be a Venice Instrument rather than a PERTRAC
		' Instrument.
		'
		' 'GetPertracTable()' only uses the StatsDatePeriod parameter for Group and Simulation instruments
		' So a default 'Monthly' value is provided here.
		' *****************************************************************************

		' Validate PertracID array.

		If (pPertracIDs Is Nothing) OrElse (pPertracIDs.Length <= 0) Then
			Exit Sub
		End If

		Dim IDsToGet As String = ""
		Dim PertracCounter As Integer
		Dim RowCounter As Integer
		Dim PertracDataPeriods() As DealingPeriod

		Dim MissingIDs(pPertracIDs.Length - 1) As Integer
		Dim DataCommand As New SqlCommand
		Dim DataTable As New DataTable

		Try
			SyncLock PertracDataCache

				' Build 'IN' string of missing (not already loaded) PertracIDs

				For PertracCounter = 0 To (pPertracIDs.Length - 1)
					MissingIDs(PertracCounter) = 0

					If Not (PertracDataCache.Contains(pPertracIDs(PertracCounter))) Then

						' Don't Pre - Load Group or Simulation series.

						Select Case GetFlagsFromID(CUInt(pPertracIDs(PertracCounter)))

							Case PertracInstrumentFlags.VeniceInstrument

								' Venice ID or Group

								Dim thisCacheObject As New PertracCacheObject(pPertracIDs(PertracCounter), GetPertracDataPeriod(pPertracIDs(PertracCounter)))

								thisCacheObject.PertracData = GetPertracTable(DealingPeriod.Monthly, pPertracIDs(PertracCounter))

								' Not necessary : Std call to GetPertracTable() adds table to PertracDataCache
								' PertracDataCache.Add(pPertracIDs(PertracCounter), thisCacheObject)

							Case PertracInstrumentFlags.PertracInstrument

								' Normal Pertrac ID
								If (IDsToGet.Length > 0) Then
									IDsToGet &= ","
								End If

								IDsToGet &= pPertracIDs(PertracCounter).ToString
								MissingIDs(PertracCounter) = pPertracIDs(PertracCounter)

						End Select

					End If
				Next

				If (IDsToGet.Length <= 0) Then
					Exit Sub
				End If

				' Get Bulk Data.

				DataCommand.CommandType = CommandType.Text
				DataCommand.CommandText = "SELECT [ID], [Date], [Return], [FundsManaged], [NAV], [Estimate], [LastUpdated] FROM [MASTERSQL].[dbo].[Performance] WHERE [ID] IN (" & IDsToGet & ")"
				DataCommand.CommandTimeout = 600

				Try
					DataCommand.Connection = MainForm.GetRenaissanceConnection()

					SyncLock DataCommand.Connection
						DataTable.Load(DataCommand.ExecuteReader)
					End SyncLock
				Catch ex As Exception
				Finally
					Try
						If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
							DataCommand.Connection.Close()
							DataCommand.Connection = Nothing
						End If
					Catch ex As Exception
					End Try
				End Try

				' If data was sucessfully loaded...

				If (DataTable.Rows.Count > 0) Then
					Dim SelectedRows() As DataRow

					PertracDataPeriods = GetPertracDataPeriods(MissingIDs)

					' For each ID...

					For PertracCounter = 0 To (MissingIDs.Length - 1)
						If MissingIDs(PertracCounter) > 0 Then

							' Select Sub-Set of Returns.

							SelectedRows = DataTable.Select("ID=" & MissingIDs(PertracCounter), "Date")

							' Build ID Specific table.

							If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
								Dim ThisDataRow As DataRow
								Dim NewDataRow As DSPerformance.tblPerformanceRow
								Dim NewTable As New DSPerformance.tblPerformanceDataTable

								Try
									For RowCounter = 0 To (SelectedRows.Length - 1)
										ThisDataRow = SelectedRows(RowCounter)

										NewDataRow = NewTable.NewtblPerformanceRow
										NewDataRow.ID = CInt(ThisDataRow("ID"))
										NewDataRow.PerformanceDate = CDate(ThisDataRow("Date"))
										NewDataRow.PerformanceReturn = CDbl(ThisDataRow("Return"))
										NewDataRow.FundsManaged = CDbl(ThisDataRow("FundsManaged"))
										NewDataRow.NAV = CDbl(ThisDataRow("NAV"))
										NewDataRow.Estimate = CBool(ThisDataRow("Estimate"))
										NewDataRow.LastUpdated = CDate(ThisDataRow("LastUpdated"))

										NewTable.Rows.Add(NewDataRow)
									Next
								Catch ex As Exception
								End Try

								' Add to Cache.

								If (NewTable IsNot Nothing) Then
									Dim thisCacheObject As PertracCacheObject

									NewTable.TableName = MissingIDs(PertracCounter).ToString

									thisCacheObject = New PertracCacheObject(MissingIDs(PertracCounter), PertracDataPeriods(PertracCounter), NewTable)
									PertracDataCache.Add(MissingIDs(PertracCounter), thisCacheObject)
								End If
							End If
						End If
					Next

				End If

			End SyncLock
		Catch ex As Exception
		End Try

	End Sub

	Public Function GetPertracTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As Integer) As DataTable
		' *****************************************************************************
		'
		' *****************************************************************************

		Try
			Return GetPertracTable(StatsDatePeriod, pPertracID, False, MainForm.Main_Knowledgedate)
		Catch ex As Exception
		End Try

		Return Nothing

	End Function

	Public Function GetPertracTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pPertracID As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date) As DataTable

		Return GetPertracTable(StatsDatePeriod, 0, pPertracID, -1, NoCache, KnowledgeDate)

	End Function

	Public Function GetPertracTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pPertracID As Integer, ByRef pSampleSize As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date) As DataTable
		' *****************************************************************************
		' Function to return a DataTable with Pertrac data for the given Instrument.
		'
		' Simple Data Caching is performed using a hash table.
		'
		' High order bits may be set consistent with 'FlagBitRangeStart', 'FlagBitRangeLength' 
		' and Instrument Flag conventions.
		'
		' *****************************************************************************

		Dim DataCommand As New SqlCommand
		Dim DataTable As New DataTable
		Dim thisCacheObject As PertracCacheObject

		' Retrieve Pertrac Index data.

		' Manage Cache size.
		Try
			If (Not NoCache) Then
				SyncLock PertracDataCache
					If (PertracDataCache.Count > MAX_DataCacheSize) Then
						Call TrimPertracCache(PertracDataCache, CInt(MAX_DataCacheSize / 2))
					End If
				End SyncLock
			End If
		Catch ex As Exception
		End Try

		Try
			If (pPertracID > 0) Then

				SyncLock PertracDataCache

					If (Not NoCache) AndAlso PertracDataCache.Contains(pPertracID) Then

						' Get Table from cache.

						thisCacheObject = CType(PertracDataCache(pPertracID), PertracCacheObject)
						DataTable = thisCacheObject.PertracData
						thisCacheObject.LastAccessed = Now()

					Else
						' Load Data.

						thisCacheObject = New PertracCacheObject(pPertracID, GetPertracDataPeriod(pPertracID))

						If (thisCacheObject.IsGroupPriceSeries) OrElse (thisCacheObject.IsDynamicGroupPriceSeries) Then

							' Build Data Table reflecting specified Group 

							DataTable = BuildGroupReturnTable(StatsDatePeriod, pNestingLevel, thisCacheObject.PertracID, pSampleSize, NoCache, KnowledgeDate)	 ' Id, Including Flags

						ElseIf (thisCacheObject.IsCTA_SimulationSeries) Then

							' Build Data Table reflecting specified Simulation 

							DataTable = BuildCTASimulationReturnTable(StatsDatePeriod, pNestingLevel, thisCacheObject.PertracID, NoCache, KnowledgeDate)	' Id, Including Flags

						Else
							' For Pertrac or Venice Instruments, retrieve price data directly.

							If (thisCacheObject.IsVenicePriceSeries) Then
								' Venice Instrument Data.

								DataCommand.CommandType = CommandType.StoredProcedure
								DataCommand.CommandText = "adp_tblPrice_SelectPertracStyle"
								DataCommand.Parameters.Add("@PriceInstrument", SqlDbType.Int).Value = thisCacheObject.VeniceInstrumentID
								DataCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = KnowledgeDate ' MainForm.Main_Knowledgedate ' RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW

							Else
								' Pertrac Data.

								DataCommand.CommandType = CommandType.StoredProcedure
								DataCommand.CommandText = "adp_tblPerformance_SelectCommand"
								DataCommand.Parameters.Add("@ID", SqlDbType.Int).Value = thisCacheObject.ID_ExFlags

							End If

							Try
								DataCommand.Connection = MainForm.GetRenaissanceConnection()

								SyncLock DataCommand.Connection
									DataTable.Load(DataCommand.ExecuteReader)
								End SyncLock

							Catch ex As Exception
							Finally
								Try
									If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
										DataCommand.Connection.Close()
										DataCommand.Connection = Nothing
									End If
								Catch ex As Exception
								End Try
							End Try

						End If

						If (DataTable IsNot Nothing) Then
							DataTable.TableName = pPertracID.ToString

							If (Not NoCache) Then
								thisCacheObject.PertracData = DataTable
								PertracDataCache.Add(pPertracID, thisCacheObject)
							Else
								thisCacheObject = Nothing
							End If
						End If

					End If
				End SyncLock

			End If

		Catch ex As Exception
		End Try

		Return DataTable
	End Function

	Public Function GetInformationRow(ByVal pPertracID As Integer) As RenaissanceDataClass.DSInformation.tblInformationRow

		Return GetInformationRow(pPertracID, True)

	End Function

	Dim GetInformationRowLock As New Threading.ReaderWriterLock

	Public Function GetInformationRow(ByVal pPertracID As Integer, ByVal pCacheItem As Boolean) As RenaissanceDataClass.DSInformation.tblInformationRow
		' *****************************************************************************
		' Function to return an Information Row for the given Instrument.
		'
		' *****************************************************************************

		Try
			GetInformationRowLock.AcquireWriterLock(1000)

			If (GetInformationRowLock.IsWriterLockHeld) Then


				Dim DataCommand As New SqlCommand
				Dim InformationDataTable As RenaissanceDataClass.DSInformation.tblInformationDataTable = Nothing
				Dim InformationRow As RenaissanceDataClass.DSInformation.tblInformationRow = Nothing
				Dim thisCacheObject As PertracCacheObject
				Dim ThisInstrumentFlags As PertracDataClass.PertracInstrumentFlags
				Dim ThisPertracID As Integer

				' Retrieve Pertrac Index data.

				Try
					SyncLock PertracInformationCache
						If (PertracInformationCache.Count > MAX_InformationCacheSize) Then
							Call TrimPertracCache(PertracInformationCache, CInt(MAX_InformationCacheSize / 2))
						End If
					End SyncLock
				Catch ex As Exception
				End Try

				Try
					If (pPertracID <= 0) Then
						pPertracID = (0)
					End If

					ThisPertracID = CInt(PertracDataClass.GetInstrumentFromID(CUInt(pPertracID)))
					ThisInstrumentFlags = PertracDataClass.GetFlagsFromID(CUInt(pPertracID))

					SyncLock PertracInformationCache
						If PertracInformationCache.Contains(pPertracID) Then

							thisCacheObject = CType(PertracInformationCache(pPertracID), PertracCacheObject)
							InformationDataTable = CType(thisCacheObject.PertracData, DSInformation.tblInformationDataTable)
							thisCacheObject.LastAccessed = Now()
						Else

							Select Case ThisInstrumentFlags

								Case PertracInstrumentFlags.PertracInstrument
									' Standard Pertrac Instrument

									InformationDataTable = New RenaissanceDataClass.DSInformation.tblInformationDataTable

									DataCommand.CommandType = CommandType.StoredProcedure
									DataCommand.CommandText = "adp_tblInformation_SelectCommand"
									DataCommand.Parameters.Add("@ID", SqlDbType.Int).Value = CInt(ThisPertracID)

									Try
										DataCommand.Connection = MainForm.GetRenaissanceConnection()

										SyncLock DataCommand.Connection
											InformationDataTable.Load(DataCommand.ExecuteReader)
										End SyncLock

									Catch ex As Exception
									Finally
										Try
											If (DataCommand IsNot Nothing) AndAlso (DataCommand.Connection IsNot Nothing) Then
												DataCommand.Connection.Close()
												DataCommand.Connection = Nothing
											End If
										Catch ex As Exception
										End Try
									End Try

								Case PertracInstrumentFlags.VeniceInstrument

									'1033	1033	RENAISSANCE	Address1				F2							Address 1					0	1
									'1034	1034	RENAISSANCE	Address2				F3							Address 2					0	1
									'1035	1035	RENAISSANCE	Administrator		F18							Administrator			0	1
									'1036	1036	RENAISSANCE	City						F4							City							0	1
									'1037	1037	RENAISSANCE	CompanyName			F1							Manager						0	1
									'1038	1038	RENAISSANCE	Contact					F8							Contact						0	1
									'1039	1039	RENAISSANCE	ContactFax			F10							Fax								0	1
									'1040	1040	RENAISSANCE	ContactPhone		F9							Phone Number			0	1
									'1041	1041	RENAISSANCE	Country					F7							Country						0	1
									'1042	1042	RENAISSANCE	Currency																					0	1
									'1043	1043	RENAISSANCE	DataVendorID		DataVendorID		DataVendorID			0	1
									'1044	1044	RENAISSANCE	DataVendorName	DataVendorName	DataVendorName		0	1
									'1045	1045	RENAISSANCE	Description			Description			Description				0	1
									'1046	1046	RENAISSANCE	Domicile				F39							Investor Types		0	1
									'1047	1047	RENAISSANCE	Email						F11							Email							0	1
									'1048	1048	RENAISSANCE	Exchange																					0	1
									'1049	1049	RENAISSANCE	FirmAssets			F34							Firm Assets				1	1
									'1050	1050	RENAISSANCE	FundAssets			F33							Fund Assets				1	1
									'1051	1051	RENAISSANCE	FundName				F20							Fund Name					0	1
									'1052	1052	RENAISSANCE	Hurdle					F26							Hurdle Rate				0	1
									'1053	1053	RENAISSANCE	IncentiveFee		F24							Incentive Fee			1	0.01
									'1054	1054	RENAISSANCE	LastUpdated			LastUpdated			LastUpdated				0	1
									'1055	1055	RENAISSANCE	Lockup					F29							Lockup						0	1
									'1056	1056	RENAISSANCE	ManagementFee		F22							Management Fee		1	0.01
									'1057	1057	RENAISSANCE	ManagerName			F15							Manager						0	1
									'1058	1058	RENAISSANCE	MarketingContactF12							Marketing contact	0	1
									'1059	1059	RENAISSANCE	MarketingFax		F14							Fax								0	1
									'1060	1060	RENAISSANCE	MarketingPhone	F13							Phone Number			0	1
									'1061	1061	RENAISSANCE	MinimumInvestment	F23						Minimum Investment	1	1
									'1062	1062	RENAISSANCE	Notice					F28							Advance Notice		0	1
									'1063	1063	RENAISSANCE	Open																							0	1
									'1064	1064	RENAISSANCE	Redemption			F27							Redemption				0	1
									'1065	1065	RENAISSANCE	State						F5							State							0	1
									'1066	1066	RENAISSANCE	Strategy				F41							Strategy Description	0	1
									'1067	1067	RENAISSANCE	Subscription		F25							Subscription			0	1
									'1068	1068	RENAISSANCE	UserDescription	UserDescription	UserDescription		0	1
									'1069	1069	RENAISSANCE	Website					F19							Website						0	1
									'1070	1070	RENAISSANCE	ZipCode					F6							Zip Code					0	1

									InformationDataTable = New RenaissanceDataClass.DSInformation.tblInformationDataTable

									If (Not InformationDataTable.Columns.Contains("Mastername")) Then
										InformationDataTable.Columns.Add(New DataColumn("Mastername", GetType(String)))
									End If
									If (Not InformationDataTable.Columns.Contains("DataPeriod")) Then
										InformationDataTable.Columns.Add(New DataColumn("DataPeriod", GetType(String)))
									End If

									InformationRow = InformationDataTable.NewtblInformationRow
									InformationRow.DataVendorName = "RENAISSANCE"
									InformationRow.DataVendorID = ThisPertracID.ToString
                  InformationRow.ID = pPertracID
                  InformationRow.LastUpdated = Now.Date

									Dim ThistblInstrumentRow As DSInstrument.tblInstrumentRow
									Dim ThistblFundTypeRow As DSFundType.tblFundTypeRow
									Try
										ThistblInstrumentRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblInstrument, CInt(ThisPertracID)), DSInstrument.tblInstrumentRow)

										InformationRow.Description = ThistblInstrumentRow.InstrumentDescription
										InformationRow.UserDescription = ThistblInstrumentRow.InstrumentDescription
										InformationRow("Mastername") = ThistblInstrumentRow.InstrumentDescription
										If (InformationDataTable.Columns.Contains("InstrumentDataPeriod")) Then
											InformationRow("DataPeriod") = CType(ThistblInstrumentRow("InstrumentDataPeriod"), DealingPeriod).ToString
										Else
											InformationRow("DataPeriod") = "Monthly"
                    End If
                    InformationRow.LastUpdated = ThistblInstrumentRow.DateEntered.Date

										ThistblFundTypeRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblFundType, CInt(ThistblInstrumentRow.InstrumentFundType)), DSFundType.tblFundTypeRow)
										If (ThistblFundTypeRow IsNot Nothing) Then
											InformationRow("F41") = ThistblFundTypeRow.FundTypeDescription
										Else
											InformationRow("F41") = ""
										End If
										ThistblFundTypeRow = Nothing
										ThistblInstrumentRow = Nothing

									Catch ex As Exception
										InformationRow.Description = "Venice Instrument"
										InformationRow.UserDescription = "Venice Instrument"
										InformationRow("Mastername") = "Venice Instrument"
										InformationRow("DataPeriod") = "Monthly"
										InformationRow("F41") = ""
									End Try

                  InformationDataTable.Rows.Add(InformationRow)

								Case PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted, PertracInstrumentFlags.DrillDown_Group_Median

									InformationDataTable = New RenaissanceDataClass.DSInformation.tblInformationDataTable
									If (Not InformationDataTable.Columns.Contains("Mastername")) Then
										InformationDataTable.Columns.Add(New DataColumn("Mastername", GetType(String)))
									End If
									If (Not InformationDataTable.Columns.Contains("DataPeriod")) Then
										InformationDataTable.Columns.Add(New DataColumn("DataPeriod", GetType(String)))
									End If

									InformationRow = InformationDataTable.NewtblInformationRow
									InformationRow.DataVendorName = "RENAISSANCE"
									InformationRow.DataVendorID = ThisPertracID.ToString
									InformationRow.Description = "Dynamic Group " & ThisPertracID.ToString & ", " & ThisInstrumentFlags.ToString
									InformationRow.UserDescription = "Dynamic Group"
									InformationRow.AuditID = pPertracID
									InformationRow.ID = pPertracID
									InformationRow.LastUpdated = Now.Date
									InformationRow("Mastername") = InformationRow.Description
									InformationRow("DataPeriod") = "Monthly"

									For Each ThisColIndex As DataColumn In InformationDataTable.Columns
										If ThisColIndex.ColumnName.StartsWith("C") AndAlso IsNumeric(ThisColIndex.ColumnName.Substring(1, 1)) Then
											InformationRow(ThisColIndex) = 0
										End If
										If ThisColIndex.ColumnName.StartsWith("F") AndAlso IsNumeric(ThisColIndex.ColumnName.Substring(1, 1)) Then
											InformationRow(ThisColIndex) = ""
										End If
									Next

									'More_Code_Here("Allow for Dynamic Groups to be given a name ?")

									InformationRow("F41") = "Genoa Group"

                  InformationDataTable.Rows.Add(InformationRow)

								Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted

									InformationDataTable = New RenaissanceDataClass.DSInformation.tblInformationDataTable
									If (Not InformationDataTable.Columns.Contains("Mastername")) Then
										InformationDataTable.Columns.Add(New DataColumn("Mastername", GetType(String)))
									End If
									If (Not InformationDataTable.Columns.Contains("DataPeriod")) Then
										InformationDataTable.Columns.Add(New DataColumn("DataPeriod", GetType(String)))
									End If

									InformationRow = InformationDataTable.NewtblInformationRow
									InformationRow.DataVendorName = "RENAISSANCE"
									InformationRow.DataVendorID = ThisPertracID.ToString
									InformationRow.AuditID = pPertracID
									InformationRow.ID = pPertracID
									InformationRow.LastUpdated = Now.Date

									For Each ThisColIndex As DataColumn In InformationDataTable.Columns
										If ThisColIndex.ColumnName.StartsWith("C") AndAlso IsNumeric(ThisColIndex.ColumnName.Substring(1, 1)) Then
											InformationRow(ThisColIndex) = 0
										End If
										If ThisColIndex.ColumnName.StartsWith("F") AndAlso IsNumeric(ThisColIndex.ColumnName.Substring(1, 1)) Then
											InformationRow(ThisColIndex) = ""
										End If
									Next

									Dim ThistblGroupRow As DSGroupList.tblGroupListRow
									Try
										ThistblGroupRow = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblGroupList, CInt(ThisPertracID)), DSGroupList.tblGroupListRow)

										InformationRow.Description = ThistblGroupRow.GroupListName
										InformationRow.UserDescription = ThistblGroupRow.GroupListName
										InformationRow("Mastername") = ThistblGroupRow.GroupListName
										InformationRow("DataPeriod") = "Monthly"
										InformationRow("F41") = "Genoa Group"

									Catch ex As Exception
										InformationRow.Description = "Renaissance Group"
										InformationRow.UserDescription = "Renaissance Group"
										InformationRow("Mastername") = InformationRow.Description
										InformationRow("DataPeriod") = "Monthly"
										InformationRow("F41") = "Genoa Group"
									End Try

									InformationDataTable.Rows.Add(InformationRow)

								Case PertracInstrumentFlags.CTA_Simulation

									InformationDataTable = New RenaissanceDataClass.DSInformation.tblInformationDataTable
									If (Not InformationDataTable.Columns.Contains("Mastername")) Then
										InformationDataTable.Columns.Add(New DataColumn("Mastername", GetType(String)))
									End If
									If (Not InformationDataTable.Columns.Contains("DataPeriod")) Then
										InformationDataTable.Columns.Add(New DataColumn("DataPeriod", GetType(String)))
									End If

									InformationRow = InformationDataTable.NewtblInformationRow
									InformationRow.DataVendorName = "RENAISSANCE"
									InformationRow.DataVendorID = ThisPertracID.ToString
									InformationRow.AuditID = pPertracID
									InformationRow.ID = pPertracID
									InformationRow.LastUpdated = Now.Date

									For Each ThisColIndex As DataColumn In InformationDataTable.Columns
										If ThisColIndex.ColumnName.StartsWith("C") AndAlso IsNumeric(ThisColIndex.ColumnName.Substring(1, 1)) Then
											InformationRow(ThisColIndex) = 0
										End If
										If ThisColIndex.ColumnName.StartsWith("F") AndAlso IsNumeric(ThisColIndex.ColumnName.Substring(1, 1)) Then
											InformationRow(ThisColIndex) = ""
										End If
									Next

									Dim ThistblCTA_Simulation As DSCTA_Simulation.tblCTA_SimulationRow
									Try
										ThistblCTA_Simulation = CType(LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCTA_Simulation, CInt(ThisPertracID)), DSCTA_Simulation.tblCTA_SimulationRow)

										InformationRow.Description = ThistblCTA_Simulation.SimulationName
										InformationRow.UserDescription = ThistblCTA_Simulation.SimulationName
										InformationRow("Mastername") = InformationRow.Description
										InformationRow("DataPeriod") = CType(ThistblCTA_Simulation.SimulationDataPeriod, DealingPeriod).ToString
										InformationRow("F41") = "Simulation"

									Catch ex As Exception
										InformationRow.Description = "CTA Simulation"
										InformationRow.UserDescription = "CTA Simulation"
										InformationRow("Mastername") = "CTA Simulation"
										InformationRow("DataPeriod") = "Monthly"
										InformationRow("F41") = "Simulation"
									End Try

									InformationDataTable.Rows.Add(InformationRow)

								Case Else

									InformationDataTable = Nothing

							End Select

							If (InformationDataTable IsNot Nothing) Then
								InformationDataTable.TableName = pPertracID.ToString

								If (pCacheItem) Then
									thisCacheObject = New PertracCacheObject(pPertracID, GetPertracDataPeriod(pPertracID), InformationDataTable)
									PertracInformationCache.Add(pPertracID, thisCacheObject)
								End If
							End If

						End If
					End SyncLock

				Catch ex As Exception
				End Try

				Try
					If (InformationDataTable IsNot Nothing) AndAlso (InformationDataTable.Rows.Count > 0) Then
						Return CType(InformationDataTable.Rows(0), DSInformation.tblInformationRow)
					End If
				Catch ex As Exception
				End Try
			End If

		Catch ex As Exception
		Finally
			If (GetInformationRowLock.IsWriterLockHeld) Then
				GetInformationRowLock.ReleaseWriterLock()
			End If
		End Try

		Return Nothing

	End Function

	Public Function GetInformationValue(ByVal pPertracID As Integer, ByVal pInformationItem As PertracInformationFields) As Object

		Return GetInformationValue(pPertracID, pInformationItem, True)

	End Function

	Public Function GetInformationValue(ByVal pPertracID As Integer, ByVal pInformationItem As PertracInformationFields, ByVal pCacheItem As Boolean) As Object
		' *****************************************************************************
		' Function to return an Information item for the given Instrument.
		'
		' *****************************************************************************

		Dim thisRow As RenaissanceDataClass.DSInformation.tblInformationRow

		Try
			thisRow = GetInformationRow(pPertracID, pCacheItem)

			If (thisRow IsNot Nothing) Then
				Return GetInformationValue(thisRow, pInformationItem)
			End If
		Catch ex As Exception
		End Try

		Return Nothing

	End Function

	Public Function GetInformationValues(ByVal pPertracID As Integer) As Object()

		Return GetInformationValues(pPertracID, True)

	End Function

	Public Function GetInformationValues(ByVal pPertracID As Integer, ByVal pCacheItem As Boolean) As Object()
		' *****************************************************************************
		' Function to return an Information item for the given Instrument.
		'
		' *****************************************************************************

		Dim thisRow As RenaissanceDataClass.DSInformation.tblInformationRow

		Try
			thisRow = GetInformationRow(pPertracID, pCacheItem)

			If (thisRow IsNot Nothing) Then
				Return GetInformationValues(thisRow)
			End If
		Catch ex As Exception
		End Try

		Return Nothing

	End Function

	Public Function GetInformationValue(ByRef pInformationRow As RenaissanceDataClass.DSInformation.tblInformationRow, ByVal pInformationItem As PertracInformationFields) As Object
		' *****************************************************************************
		' Function to return an Information item for the given Information table row.
		'
		' *****************************************************************************
		Dim RVal As Object = Nothing
		Dim MappingArray() As Integer

		Try
			If (InformationColumnCache Is Nothing) Then
				InformationColumnCache = Set_InformationColumnMap()
			End If
		Catch ex As Exception
			Return Nothing
		End Try

		Try

			If (InformationColumnCache.ContainsKey(pInformationRow.DataVendorName.ToUpper) = False) Then
				Return RVal
			End If

			MappingArray = InformationColumnCache.Item(pInformationRow.DataVendorName.ToUpper)

			Return pInformationRow.Item(MappingArray(CInt(pInformationItem)))

		Catch ex As Exception
		End Try

		Return Nothing
	End Function

	Public Function GetInformationValues(ByRef pInformationRow As RenaissanceDataClass.DSInformation.tblInformationRow) As Object()
		' *****************************************************************************
		' Function to return all Information items for the given Information table row.
		'
		' *****************************************************************************
		Dim RVal() As Object = Nothing
		Dim MappingArray() As Integer
		Dim FieldArray() As Integer
		Dim FieldCounter As Integer

		If (InformationColumnCache Is Nothing) Then
			InformationColumnCache = Set_InformationColumnMap()

			If (InformationColumnCache Is Nothing) Then
				Return Nothing
			End If
		End If

		Try
			ReDim RVal(PertracInformationFields.MaxValue - 1)

			If (InformationColumnCache.ContainsKey(pInformationRow.DataVendorName.ToUpper) = False) Then
				Return RVal
			End If

			MappingArray = InformationColumnCache.Item(pInformationRow.DataVendorName.ToUpper)

			FieldArray = CType(System.Enum.GetValues(GetType(PertracInformationFields)), Integer())

			Dim InformationFieldValue As Integer
			Dim TableColumnOrdinal As Integer


			For FieldCounter = 0 To (FieldArray.Length - 1)
				InformationFieldValue = FieldArray(FieldCounter)
				If (InformationFieldValue >= 0) AndAlso (InformationFieldValue < MappingArray.Length) Then
					TableColumnOrdinal = MappingArray(InformationFieldValue)
				Else
					TableColumnOrdinal = (-1)
				End If

				If (TableColumnOrdinal >= 0) AndAlso (InformationFieldValue < CInt(PertracInformationFields.MaxValue)) AndAlso (InformationFieldValue < RVal.Length) Then
					If (TableColumnOrdinal < pInformationRow.ItemArray.Length) Then
						RVal(InformationFieldValue) = pInformationRow.Item(TableColumnOrdinal)
					End If
				End If
			Next

			Return RVal
		Catch ex As Exception
		End Try

		Return Nothing
	End Function

	Public Sub GetReturnsArray(ByVal pNestingLevel As Integer, ByVal pPertracID As Integer, ByRef pSampleSize As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByVal StatsDatePeriod As DealingPeriod, ByRef pDateArray() As Date, ByRef pReturnsArray() As Double, ByRef pNAVArray() As Double, ByRef pIsMilestoneArray() As Boolean)
		' **************************************************************************************
		' Return Date, Return, NAV etc arrays for the given Pertrac ID
		'
		'
		' **************************************************************************************

		Dim DataStartDate As Date
		Dim DataTable As DataTable = Nothing
		Dim SortedRows(-1) As DataRow
		Dim ArrayLength As Integer
		Dim RealReturnArray() As Boolean = Nothing

		Dim PerformanceDateOrdinal As Integer
		Dim ReturnOrdinal As Integer
		Dim IsMilestoneOrdinal As Integer = (-1)
		Dim NAVOrdinal As Integer
		Dim ArrayIndex As Integer
		Dim RowCounter As Integer

		Dim IsVeniceID As Boolean = False
		Dim IsNavSeries As Boolean = False
		If PertracDataClass.GetFlagsFromID(CUInt(Math.Max(pPertracID, 0))) = PertracDataClass.PertracInstrumentFlags.VeniceInstrument Then
			IsVeniceID = True
		ElseIf PertracDataClass.GetFlagsFromID(CUInt(Math.Max(pPertracID, 0))) = PertracDataClass.PertracInstrumentFlags.CTA_Simulation Then
			IsNavSeries = True
		End If


		Try
			If pPertracID <= 0 Then
				DataTable = GetPertracTable(StatsDatePeriod, 0)
			Else
				DataTable = GetPertracTable(StatsDatePeriod, pNestingLevel + 1, pPertracID, pSampleSize, NoCache, KnowledgeDate)
			End If

			' Ensure the data is in date order

			If (DataTable.Columns.Contains("PerformanceDate")) Then
				SortedRows = DataTable.Select("True", "PerformanceDate")
			End If

		Catch ex As Exception
			ReDim SortedRows(-1)
		End Try

		If (SortedRows.Length <= 0) Then
			' Exit with default values

			pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
			pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
			Exit Sub
		End If

		' Get Data Column Ordinals, used as a performance improver.

		Try
			PerformanceDateOrdinal = DataTable.Columns.IndexOf("PerformanceDate")
			ReturnOrdinal = DataTable.Columns.IndexOf("PerformanceReturn")
			NAVOrdinal = DataTable.Columns.IndexOf("NAV")

			If (DataTable.Columns.Contains("PriceIsMilestone")) Then
				IsMilestoneOrdinal = DataTable.Columns.IndexOf("PriceIsMilestone")
			End If
		Catch ex As Exception
			pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
			pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
			Exit Sub
		End Try

		' Initialise Data Arrays

		Try

			DataStartDate = CDate(SortedRows(0)(PerformanceDateOrdinal))

			If (IsVeniceID) AndAlso (SortedRows.Length > 1) Then
				' Some (Many) Venice price series start with a default starting value on 1 Jan 1970, 
				' Check for this and eliminate it.

				Dim StartIndex As Integer = 0

				' If the NAVs of the first two dates are the same, then ignore the first one, otherwise dont.

				While (StartIndex < (SortedRows.Length - 1)) AndAlso ((CDate(SortedRows(StartIndex + 1)(PerformanceDateOrdinal)) - CDate(SortedRows(StartIndex)(PerformanceDateOrdinal))).TotalDays > 50) AndAlso (Math.Abs(CDbl(SortedRows(StartIndex + 1)(NAVOrdinal)) - CDbl(SortedRows(StartIndex)(NAVOrdinal))) < 0.00000001#)
					DataStartDate = CDate(SortedRows(StartIndex + 1)(PerformanceDateOrdinal))

					StartIndex += 1
				End While

			End If

			DataStartDate = FitDateToPeriod(StatsDatePeriod, DataStartDate, False)	' move to start of period.

		Catch ex As Exception
			pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
			pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
			pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
			Exit Sub
		End Try

		If (IsVeniceID) OrElse (IsNavSeries) Then

			' If it's a Venice Series, Build returns from NAV data as returns are not always present for Venice Instruments.
			' Also Build IsMilestone Array (Only for Venice Instruments).

			' Fill the returns array using the latest value in each period.
			' Note, this may not work if the StatsDatePeriod does not match the returns period in the pertrac DB.

			Dim ThisRow As DataRow

			ArrayLength = GetPeriodCount(StatsDatePeriod, DataStartDate, CDate(SortedRows(SortedRows.Length - 1)(PerformanceDateOrdinal)))

			' NAVArray
			' RealReturnArray
			' IsMilestoneArray

			RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())	' Flag to indicate 'real' returns - don't fill.
			pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
			pNAVArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())

			Try
				Dim InitIndex As Integer

				If (RealReturnArray IsNot Nothing) AndAlso (RealReturnArray.Length > 0) Then
					For InitIndex = 0 To (RealReturnArray.Length - 1)
						RealReturnArray(InitIndex) = False
					Next
				End If
			Catch ex As Exception
			End Try


			For RowCounter = 0 To (SortedRows.Length - 1)
				ThisRow = SortedRows(RowCounter)
				ArrayIndex = Math.Max(GetPriceIndex(StatsDatePeriod, DataStartDate, CDate(ThisRow(PerformanceDateOrdinal))), 0)

				pNAVArray(ArrayIndex) = CDbl(ThisRow(NAVOrdinal))
				RealReturnArray(ArrayIndex) = True
				If (IsMilestoneOrdinal >= 0) Then
					pIsMilestoneArray(ArrayIndex) = CBool(ThisRow(IsMilestoneOrdinal))
				End If
			Next

			' Check for missing prices - Forward fill and Back fill.

			For RowCounter = 1 To (pNAVArray.Length - 1)
				If (pNAVArray(RowCounter) = 0) AndAlso (RealReturnArray(RowCounter) = False) Then
					pNAVArray(RowCounter) = pNAVArray(RowCounter - 1)
				End If
			Next

			For RowCounter = (pNAVArray.Length - 2) To 0
				If (pNAVArray(RowCounter) = 0) AndAlso (RealReturnArray(RowCounter) = False) Then
					pNAVArray(RowCounter) = pNAVArray(RowCounter + 1)
				End If
			Next

			' Venice Obsolete data points fix.
      ' OK, We are trying to eliminate the PreStart Default (often '100.0') fund valuations in Venice.
			' Now these values play an important role for Venice, but thay are not valid for the purpose of Statistics
			' or The Reporter App.

			If (IsVeniceID) Then

				Try
					Dim InitIndex As Integer
					InitIndex = 0

					While (InitIndex < (RealReturnArray.Length - 2)) AndAlso (Not (RealReturnArray(InitIndex) And RealReturnArray(InitIndex + 1)) AndAlso (Math.Abs(pNAVArray(InitIndex) - pNAVArray(InitIndex + 1)) < 0.00000001#))
						InitIndex += 1
					End While

					If (InitIndex > 0) Then
						DataStartDate = AddPeriodToDate(StatsDatePeriod, DataStartDate, InitIndex)
						DataStartDate = FitDateToPeriod(StatsDatePeriod, DataStartDate, False)	' move to start of period.

						ArrayLength -= InitIndex

						Dim OldDoubleArray() As Double
						Dim OldBooleanArray() As Boolean

						OldDoubleArray = pNAVArray
						pNAVArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
						Array.Copy(OldDoubleArray, InitIndex, pNAVArray, 0, ArrayLength + 1)

						OldBooleanArray = RealReturnArray
						RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
						Array.Copy(OldBooleanArray, InitIndex, RealReturnArray, 0, ArrayLength + 1)

						OldBooleanArray = pIsMilestoneArray
						pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
						Array.Copy(OldBooleanArray, InitIndex, pIsMilestoneArray, 0, ArrayLength + 1)

						OldDoubleArray = Nothing
						OldBooleanArray = Nothing
					End If
				Catch ex As Exception
					pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
					pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
					pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
					pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
					Exit Sub
				End Try

			End If

			' Note ArrayLength returns the actual number of periods, thus dimensioning to 'ArrayLength + 1'
			' Creates space for the initial NAV figure.

			pDateArray = CType(Array.CreateInstance(GetType(Date), ArrayLength + 1), Date())
			pReturnsArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())

			' Now Generate Return series.

			For RowCounter = 1 To (pNAVArray.Length - 1)
				If (pNAVArray(RowCounter - 1) <> 0) Then
					pReturnsArray(RowCounter) = (pNAVArray(RowCounter) / pNAVArray(RowCounter - 1)) - 1.0#
				Else
					pReturnsArray(RowCounter) = 0
				End If
			Next

		Else
      ' Not Venice Instrument.

			Try

				' Note ArrayLength returns the actual number of periods, thus dimensioning to 'ArrayLength + 1'
				' Creates space for the initial NAV figure.

				ArrayLength = GetPeriodCount(StatsDatePeriod, DataStartDate, CDate(SortedRows(SortedRows.Length - 1)(PerformanceDateOrdinal)))

				RealReturnArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())	' Flag to indicate 'real' returns - don't fill.
				pDateArray = CType(Array.CreateInstance(GetType(Date), ArrayLength + 1), Date())
				pReturnsArray = CType(Array.CreateInstance(GetType(Double), ArrayLength + 1), Double())
				pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), ArrayLength + 1), Boolean())
				pNAVArray = Nothing

				Try
					Dim InitIndex As Integer

					If (RealReturnArray IsNot Nothing) AndAlso (RealReturnArray.Length > 0) Then
						For InitIndex = 0 To (RealReturnArray.Length - 1)
							RealReturnArray(InitIndex) = False
						Next
					End If
				Catch ex As Exception
				End Try

			Catch ex As Exception
				pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
				pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
				Exit Sub
			End Try

			' Build Returns Array

			DataStartDate = DataStartDate.AddMonths(-1)

			Try
				' Fill the returns array using the latest value in each period.
				' Note, this may not work if the StatsDatePeriod does not match the returns period in the pertrac DB.

				Dim ThisRow As DataRow

				For RowCounter = 0 To (SortedRows.Length - 1)
					ThisRow = SortedRows(RowCounter)
					ArrayIndex = GetPriceIndex(StatsDatePeriod, DataStartDate, CDate(ThisRow(PerformanceDateOrdinal)))

					pReturnsArray(ArrayIndex) = CDbl(ThisRow(ReturnOrdinal))
					RealReturnArray(ArrayIndex) = True

				Next
			Catch ex As Exception
				pDateArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
				pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
				pIsMilestoneArray = CType(Array.CreateInstance(GetType(Boolean), 0), Boolean())
				Exit Sub
			End Try

		End If

	End Sub

#End Region

#Region " Information Functions"

	Public Function GetPertracInstruments(Optional ByVal PertracID As Integer = 0) As DataView
		' ***********************************************************************************
		' Return a list of all Pertrac Instruments with Name and Source information.
		'
		'
		' ***********************************************************************************
		Dim RVal As DataView = Nothing

		Try
			Dim tmpCommand As New SqlCommand
			Dim tmpTable As New DataTable("PertracInstruments")

			Try
				tmpCommand.CommandType = CommandType.StoredProcedure
				tmpCommand.CommandText = "spu_PertracInstruments"
				tmpCommand.Connection = MainForm.GetRenaissanceConnection
				tmpCommand.CommandTimeout = 600

				If (PertracID > 0) Then
					tmpCommand.Parameters.Add(New SqlParameter("@ID", SqlDbType.Int)).Value = PertracID
				End If

				tmpTable.Load(tmpCommand.ExecuteReader)
			Catch ex As Exception
			Finally
				Try
					If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
						tmpCommand.Connection.Close()
						tmpCommand.Connection = Nothing
					End If
				Catch ex As Exception
				End Try
			End Try

			RVal = New DataView(tmpTable)
			RVal.Sort = "ListDescription"
		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Public Function GetActivePertracInstruments(Optional ByVal ForceRefresh As Boolean = False, Optional ByVal pFundID As Integer = (-1)) As DataTable
		' ***********************************************************************************
		' Return a list of Pertrac Instruments associated with a given Venice Fund.
		'
		'
		' ***********************************************************************************

		Return GetActivePertracInstruments(ForceRefresh, pFundID, RenaissanceGlobals.Globals.Renaissance_BaseDate)

	End Function

	Public Function GetActivePertracInstruments(ByVal ForceRefresh As Boolean, ByVal pFundID As Integer, ByVal pValueDate As Date) As DataTable
		' ***********************************************************************************
		' Return a list of Pertrac Instruments associated with a given Venice Fund.
		'
		'
		' ***********************************************************************************
		Static LastFundID As Integer
		Static LastValueDate As Date
		Static RVal As DataTable

		If (pFundID <= (-999)) Then
			Try

				LastFundID = (-999)

				If (RVal IsNot Nothing) Then
					RVal.Rows.Clear()
				End If

			Catch ex As Exception
			Finally
				RVal = Nothing
			End Try
			Return Nothing
		End If

		Try
			Dim tmpCommand As New SqlCommand
			Dim tmpTable As New DataTable("PertracInstruments")

			If (pFundID < 0) Then
				pFundID = LastFundID
			End If

			If (ForceRefresh = False) AndAlso (LastFundID = pFundID) AndAlso (LastValueDate = pValueDate) AndAlso (RVal IsNot Nothing) Then
				Try
					Return RVal
				Catch ex As Exception
				End Try
			End If

			LastFundID = pFundID
			LastValueDate = pValueDate

			Try
				tmpCommand.CommandType = CommandType.StoredProcedure
				tmpCommand.CommandText = "spu_ActiveFundPertracInstruments"
				tmpCommand.Connection = MainForm.GetRenaissanceConnection
				tmpCommand.Parameters.Add("@FundID", SqlDbType.Int).Value = pFundID
				tmpCommand.Parameters.Add("@ValueDate", SqlDbType.DateTime).Value = LastValueDate
				tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
				tmpCommand.CommandTimeout = 600

				tmpTable.Load(tmpCommand.ExecuteReader)
			Catch ex As Exception
			Finally
				Try
					If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
						tmpCommand.Connection.Close()
						tmpCommand.Connection = Nothing
					End If
				Catch ex As Exception
				End Try
			End Try

			RVal = tmpTable
			Return RVal
		Catch ex As Exception
		End Try

		Return Nothing
	End Function

#End Region

	Public Shared Function GetFlagsFromID(ByVal pPertraciD As UInteger) As PertracDataClass.PertracInstrumentFlags
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

		Return CType(((pPertraciD And ID_Mask_FlagsOnly) >> FlagBitRangeStart), PertracDataClass.PertracInstrumentFlags)

	End Function

	Public Shared Function GetFlagsFromID(ByVal pPertraciD As Integer) As PertracDataClass.PertracInstrumentFlags
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

    If (pPertraciD < 0) Then
      Return PertracDataClass.PertracInstrumentFlags.PertracInstrument
    Else
      Return GetFlagsFromID(CUInt(pPertraciD))
    End If

	End Function

	Public Shared Function GetInstrumentFromID(ByVal pPertraciD As UInteger) As UInteger
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

		Return CUInt(pPertraciD And ID_Mask_ExFlags)

	End Function

	Public Shared Function GetInstrumentFromID(ByVal pPertraciD As Integer) As UInteger
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

    If (pPertraciD < 0) Then
      Return 0UI
    Else
      Return CUInt(CUInt(Math.Abs(pPertraciD)) And ID_Mask_ExFlags)
    End If

	End Function

	Public Shared Function SetFlagsToID(ByVal pPertraciD As UInteger, ByVal pFlags As PertracDataClass.PertracInstrumentFlags) As UInteger
		' ********************************************************************************
		' Function to strip the InstrumentFlags from a Combined Pertrac ID 
		'
		' ********************************************************************************

		Return CType((pPertraciD And ID_Mask_ExFlags) Or (pFlags << FlagBitRangeStart), UInteger)

	End Function


End Class
