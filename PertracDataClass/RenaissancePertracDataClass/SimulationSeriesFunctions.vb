﻿Option Strict On

Imports System.IO
Imports System.Data.SqlClient
Imports RenaissanceDataClass
Imports RenaissanceGlobals
Imports RenaissancePertracAndStatsGlobals

Partial Public Class PertracDataClass

  Private Function BuildCTASimulationReturnTable(ByVal StatsDatePeriod As DealingPeriod, ByVal pNestingLevel As Integer, ByVal pSimulationID As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date) As DSPerformance.tblPerformanceDataTable
    ' ******************************************************************************************
    '
    ' 
    '
    ' ******************************************************************************************

    Dim RVal As New DSPerformance.tblPerformanceDataTable
    Dim DatesArray() As Date = Nothing
    Dim ReturnsArray() As Double = Nothing
    Dim NetDeltaItem As NetDeltaItemClass = Nothing
    Dim NAVArray() As Double = Nothing
    Dim NewTableRow As DSPerformance.tblPerformanceRow
    Dim ReturnCounter As Integer

    Try

      If (_StatFunctionsObject IsNot Nothing) Then

        _StatFunctionsObject.GetSimulationSeries(pNestingLevel + 1, pSimulationID, NoCache, KnowledgeDate, StatsDatePeriod, DatesArray, ReturnsArray, NAVArray, NetDeltaItem, 0UL, Nothing)

        If (DatesArray IsNot Nothing) AndAlso (ReturnsArray IsNot Nothing) Then

          For ReturnCounter = 0 To (DatesArray.Length - 1)
            If (ReturnCounter < ReturnsArray.Length) Then

              NewTableRow = RVal.NewtblPerformanceRow
              NewTableRow.Estimate = False
              NewTableRow.FundsManaged = 0
              NewTableRow.ID = CInt(GetFlagsFromID(CUInt(pSimulationID)))
              NewTableRow.PerformanceDate = DatesArray(ReturnCounter)
              NewTableRow.PerformanceReturn = ReturnsArray(ReturnCounter)
              NewTableRow.NAV = NAVArray(ReturnCounter)

              RVal.Rows.Add(NewTableRow)
            End If
          Next

        End If

      End If

    Catch ex As Exception
      RVal = New DSPerformance.tblPerformanceDataTable
    End Try

    Return RVal

  End Function


End Class
