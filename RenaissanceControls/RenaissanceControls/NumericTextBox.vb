Imports System.Globalization

<CLSCompliant(True)> _
Public Class NumericTextBox

  ' *********************************************************************
  ' The motivation behind this control was to provide a simple value edit
  ' box that displayed the resulting value in a definable format.
  ' Strangely, this basic control seems un-available in the .Net environment.
  ' So here it is. The Numeric and Percentage Text Box controls are identical 
  ' except for the default Text Format. They are discrete object types because 
  ' it was convenient in Florence to be able to differentiate the two objects
  ' using to 'TypeOf' operator.
  ' This control maintains a 'Value' property of type double.
  ' When the control gains focus, the value is rendered in simple form, making it
  ' easy to edit. When Focus is lost, the control text is converted to a numeric
  ' value which is saved in the Value property. This value is then displayed
  ' in the given TextFormat. Simple!
  ' 
  ' The RenaissanceTag property is a convenience originating from the TextCombo
  ' Control. The Common Combo selection functions use the existing Tag property to 
  ' maintain the Selected Index value, and Florence found it convenient to have
  ' a second Tag property with which to maintain WorkItem values.
  ' *********************************************************************

  ' Basic Functionallity :-
  Inherits TextBox

  ' Provides secondary Tag property :-
  Implements RenaissanceTagProperty

  ' Does what it says :-
  Public Event ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs)

  ' Does what it says :-
  Public Event PercentageValue(ByVal sender As Object, ByVal e As System.EventArgs)
  ' Public Event NotPercentageValue(ByVal sender As Object, ByVal e As System.EventArgs)

  ' Enables the underlying TextChanged Event to be suppressed when Text is altered during
  ' the GotFocus and LostFocus Events.
  Public Shadows Event TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

  '
  Private _RenaissanceTag As Object
  Private _MyValue As Double
  Private _SelectTextOnFocus As Boolean = False

  ' When True, suppresses the TextChanged Events and prevents the New Text being
  ' parsed back to the Value property. 
  Private _TextPassthrough As Boolean = False

  ' Default Text Format.
  Private _TextFormat As String = "#,##0.00"

  Private Function GlobaliseNumber(ByVal myNumber As String) As String

    Try
      Dim DecimalChar As String = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator

      If (DecimalChar = ".") Then
        Return myNumber.Replace(",", "").Replace(" ", "")
      Else
        Return myNumber.Replace(".", DecimalChar).Replace(" ", "")
      End If

    Catch ex As Exception
      Return myNumber
    End Try

  End Function

  'Public Property Value() As Double
  '  ' *********************************************************************
  '  ' Basic Numeric value maintained by this control.
  '  ' *********************************************************************
  '  Get
  '    Return _MyValue
  '  End Get

  '  Set(ByVal NewValue As Double)
  '    _MyValue = NewValue

  '    If Not Me.Focused Then
  '      _TextPassthrough = True
  '      Me.Text = _MyValue.ToString(_TextFormat)
  '    Else
  '      _TextPassthrough = True
  '      Me.Text = _MyValue.ToString()
  '    End If

  '    RaiseEvent ValueChanged(Me, New EventArgs())
  '    RaiseEvent TextChanged(Me, New EventArgs())
  '  End Set
  'End Property

#Region " Value Property "

  Private ValueLock As New Object
  Public Property Value() As Double
    ' *********************************************************************
    ' Basic Numeric value maintained by this control.
    '
    ' Coded to be thread safe (I hope)
    ' *********************************************************************
    Get
      Return GetValue()
    End Get

    Set(ByVal NewValue As Double)
      SetValue(NewValue)
    End Set
  End Property

  Private Delegate Sub SetValueDelegate(ByVal NewValue As Double, ByVal DontSetText As Boolean)

  Private Sub SetValue(ByVal NewValue As Double, Optional ByVal DontSetText As Boolean = False)
    If InvokeRequired Then
      Dim d As New SetValueDelegate(AddressOf SetValue)
      Invoke(d, New Object() {NewValue, DontSetText})
    Else
      SyncLock ValueLock
        _MyValue = NewValue

        If Not Me.Focused Then
          _TextPassthrough = True
          SetText(_MyValue.ToString(_TextFormat))
        Else
          _TextPassthrough = True
          SetText(_MyValue.ToString())
        End If

        RaiseEvent ValueChanged(Me, New EventArgs())
        RaiseEvent TextChanged(Me, New EventArgs())
      End SyncLock
    End If
  End Sub

  Private Delegate Function GetValueDelegate() As Double

  Private Function GetValue() As Double
    If InvokeRequired Then
      Dim d As New GetValueDelegate(AddressOf GetValue)
      Return CDbl(Invoke(d))
    Else
      SyncLock ValueLock
        Return _MyValue
      End SyncLock
    End If
  End Function

#End Region

#Region " TextFormat Property "

  Private TextFormatLock As New Object

  Public Property TextFormat() As String
    ' *********************************************************************
    ' Format in which to display the Value when the control does not have Focus.
    '
    ' Coded to be thread safe (I hope)
    ' *********************************************************************
    Get
      Return GetTextFormat()
    End Get
    Set(ByVal NewValue As String)
      SetTextFormat(NewValue)
    End Set
  End Property

  Private Delegate Sub SetTextFormatDelegate(ByVal NewValue As String)

  Private Sub SetTextFormat(ByVal NewValue As String)
    If InvokeRequired Then
      Dim d As New SetTextFormatDelegate(AddressOf SetTextFormat)
      Invoke(d, New Object() {NewValue})
    Else
      SyncLock TextFormatLock
        _TextFormat = NewValue

        If Not Me.Focused Then
          _TextPassthrough = True
          SetText(_MyValue.ToString(_TextFormat))
        End If
      End SyncLock
    End If
  End Sub

  Private Delegate Function GetTextFormatDelegate() As String

  Private Function GetTextFormat() As String
    If InvokeRequired Then
      Dim d As New GetTextFormatDelegate(AddressOf GetTextFormat)
      Return CStr(Invoke(d))
    Else
      SyncLock TextFormatLock
        Return _TextFormat
      End SyncLock
    End If
  End Function

#End Region


  'Public Overrides Property Text() As String
  '  ' *********************************************************************
  '  ' Set the Text property of the underlying control.
  '  ' If not suppressed, attempt to parse the Text and set the control Value
  '  ' aswell.
  '  ' *********************************************************************
  '  Get
  '    Return GlobaliseNumber(MyBase.Text)
  '  End Get

  '  Set(ByVal NewValue As String)
  '    If (_TextPassthrough) Then
  '      MyBase.Text = NewValue
  '      _TextPassthrough = False
  '      Exit Property
  '    End If

  '    Dim TempString As String = GlobaliseNumber(NewValue.Trim)
  '    Dim Divisor As Double = 1.0

  '    If TempString.EndsWith("%") Then
  '      TempString = TempString.Substring(0, TempString.Length - 1)
  '      Divisor = 100.0
  '    End If

  '    If IsNumeric(TempString) Then
  '      _MyValue = (CDbl(TempString)) / Divisor
  '      RaiseEvent ValueChanged(Me, New EventArgs())
  '    End If

  '    MyBase.Text = NewValue
  '  End Set
  'End Property

  Private TextLock As New Object

  Public Overrides Property Text() As String
    ' *********************************************************************
    ' Set the Text property of the underlying control.
    ' If not suppressed, attempt to parse the Text and set the control Value
    ' aswell.
    '
    ' Coded to be thread safe (I hope)
    ' *********************************************************************
    Get

      Return GetText()
    End Get

    Set(ByVal NewValue As String)

      SetText(NewValue)
    End Set
  End Property

  Private Delegate Sub SetTextDelegate(ByVal NewValue As String)

  Private Sub SetText(ByVal NewValue As String)
    If InvokeRequired Then
      Dim d As New SetTextDelegate(AddressOf SetText)
      Invoke(d, New Object() {NewValue})
    Else
      SyncLock TextLock
        If (_TextPassthrough) Then
          MyBase.Text = NewValue
          _TextPassthrough = False
          Exit Sub
        End If

        Dim TempString As String = GlobaliseNumber(NewValue.Trim)
        Dim Divisor As Double = 1.0#

        If TempString.EndsWith("%") Then
          TempString = TempString.Substring(0, TempString.Length - 1)
          Divisor = 100.0
        End If

        If IsNumeric(TempString) Then
          SetValue((CDbl(TempString)) / Divisor, True) ' Don't Recurse to SetText
          RaiseEvent ValueChanged(Me, New EventArgs())
        End If

        MyBase.Text = NewValue
      End SyncLock
    End If

  End Sub

  Private Delegate Function GetTextDelegate() As String

  Private Function GetText() As String
    If InvokeRequired Then
      Dim d As New GetTextDelegate(AddressOf GetText)
      Return CStr(Invoke(d))
    Else
      SyncLock TextLock
        Return GlobaliseNumber(MyBase.Text) ' MyBase.Text '  GlobaliseNumber(MyBase.Text)
      End SyncLock
    End If
  End Function

  Public Property SelectTextOnFocus() As Boolean
    Get
      Return _SelectTextOnFocus
    End Get
    Set(ByVal value As Boolean)
      _SelectTextOnFocus = value
    End Set
  End Property

  Protected Function SetValueFromText() As Boolean
    ' *********************************************************************
    '
    '
    ' *********************************************************************

    Try
      Dim TempString As String = GlobaliseNumber(Me.Text.Trim)
      Dim Divisor As Double = 1.0
      Dim IsPercentage As Boolean = False

      If TempString.EndsWith("%") Then
        TempString = TempString.Substring(0, TempString.Length - 1)
        Divisor = 100.0
        IsPercentage = True
      End If

      If IsNumeric(TempString) Then
        If (_MyValue.ToString <> (CDbl(TempString) / Divisor).ToString) Then
          _MyValue = (CDbl(TempString) / Divisor)
          RaiseEvent ValueChanged(Me, New EventArgs())
        End If

        _TextPassthrough = True
        Me.Text = _MyValue.ToString(_TextFormat)

        If (IsPercentage) Then
          RaiseEvent PercentageValue(Me, New EventArgs())
        Else
          ' RaiseEvent NotPercentageValue(Me, New EventArgs())
        End If

        Return True
      End If



    Catch ex As Exception
    End Try

    Return False
  End Function

  Private Sub MyBaseKeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
    ' *********************************************************************
    '
    '
    ' *********************************************************************

    Try
      If e.KeyChar = Chr(13) Then
        If (SetValueFromText()) Then
          e.Handled = True
        End If

        'Dim TempString As String = GlobaliseNumber(Me.Text.Trim)
        'Dim Divisor As Double = 1.0

        'If TempString.EndsWith("%") Then
        '  TempString = TempString.Substring(0, TempString.Length - 1)
        '  Divisor = 100.0
        'End If

        'If IsNumeric(TempString) Then
        '  If (_MyValue.ToString <> (CDbl(TempString) / Divisor).ToString) Then
        '    _MyValue = (CDbl(TempString) / Divisor)
        '    RaiseEvent ValueChanged(Me, New EventArgs())
        '    e.Handled = True
        '  End If
        'End If

      End If
    Catch ex As Exception

    End Try
  End Sub

  Protected Overrides Sub OnGotFocus(ByVal e As System.EventArgs)
    ' *********************************************************************
    ' Change displayed Text to reflect an editable Value, rather than the given Display format.
    ' *********************************************************************
    Try
      MyBase.OnGotFocus(e)

      _TextPassthrough = True
      Me.Text = GlobaliseNumber(_MyValue.ToString)

      If (_SelectTextOnFocus) Then
        Me.SelectionStart = 0
        Me.SelectionLength = Me.Text.Length

      End If

    Catch ex As Exception
    End Try

  End Sub

  Protected Overrides Sub OnLostFocus(ByVal e As System.EventArgs)
    ' *********************************************************************
    ' On Exiting the control, Parse the control Text to set the Control Value
    ' and then display the Control Value in the given format.
    ' *********************************************************************

    MyBase.OnLostFocus(e)

    Dim TempString As String = GlobaliseNumber(Me.Text.Trim)
    Dim Divisor As Double = 1.0
    Dim IsPercentage As Boolean = False

    Try

      If TempString.EndsWith("%") Then
        TempString = TempString.Substring(0, TempString.Length - 1)
        Divisor = 100.0
        IsPercentage = True
      End If

      If IsNumeric(TempString) Then
        If (_MyValue.ToString <> (CDbl(TempString) / Divisor).ToString) Then
          _MyValue = (CDbl(TempString) / Divisor)
          RaiseEvent ValueChanged(Me, New EventArgs())
        End If
      End If

      _TextPassthrough = True
      Me.Text = _MyValue.ToString(_TextFormat)

      If (IsPercentage) Then
        RaiseEvent PercentageValue(Me, New EventArgs())
      Else
        ' RaiseEvent NotPercentageValue(Me, New EventArgs())
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub MyBaseTextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.TextChanged
    ' *********************************************************************
    ' Override the underlying 'TextChanged' event so that it is possible to suppress this event when the 
    ' text is changed for purely cosmetic reasons during 'GotFocus' and 'LostFocus'
    '
    ' *********************************************************************

    If (_TextPassthrough = False) Then
      RaiseEvent TextChanged(Me, e)
    End If
  End Sub



#Region " RenaissanceTag Property "

  Private RenaissanceTagLock As New Object
  Public Property RenaissanceTag() As Object Implements RenaissanceTagProperty.RenaissanceTag
    ' *********************************************************************
    ' Secondary Tag property.
    ' Added for consistenct with the TextCombo control. The Combo control has a second Tag property because the 
    ' main Tag property is used by the Common Combo selection routines and it was necessary to record additional
    ' information.
    '
    ' *********************************************************************
    Get
      SyncLock RenaissanceTagLock
        Return GetRenaissanceTag()
      End SyncLock
    End Get
    Set(ByVal value As Object)
      SetRenaissanceTag(value)
    End Set
  End Property

  Private Delegate Sub SetRenaissanceTagDelegate(ByVal NewValue As Object)

  Private Sub SetRenaissanceTag(ByVal NewValue As Object)
    If InvokeRequired Then
      Dim d As New SetRenaissanceTagDelegate(AddressOf SetRenaissanceTag)
      Invoke(d, New Object() {NewValue})
    Else
      SyncLock RenaissanceTagLock
        _RenaissanceTag = NewValue
      End SyncLock
    End If
  End Sub

  Private Delegate Function GetRenaissanceTagDelegate() As Object

  Private Function GetRenaissanceTag() As Object
    If InvokeRequired Then
      Dim d As New GetRenaissanceTagDelegate(AddressOf GetRenaissanceTag)
      Return Invoke(d)
    Else
      SyncLock RenaissanceTagLock
        Return _RenaissanceTag
      End SyncLock
    End If
  End Function

#End Region



End Class
