<CLSCompliant(True)> _
Public Class DateCombo
  Inherits System.Windows.Forms.ComboBox

  Implements RenaissanceTagProperty

  Private _RenaissanceTag As Object

  Public Property RenaissanceTag() As Object Implements RenaissanceTagProperty.RenaissanceTag
    Get
      Return _RenaissanceTag
    End Get
    Set(ByVal value As Object)
      _RenaissanceTag = value
    End Set
  End Property

End Class
