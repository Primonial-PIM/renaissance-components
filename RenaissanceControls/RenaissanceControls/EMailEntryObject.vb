Public Class EMailEntryObject
	Inherits System.Object

	Private _PersonID As Integer
	Private _PersonName As String
	Private _EMail As String
	Private _Tag As Object

	Public Overrides Function ToString() As String
		Return _PersonName
	End Function

	Private Sub New()
		MyBase.New()
	End Sub

	Public Sub New(ByVal pPersonID As Integer, ByVal pPersonName As String, ByVal pEMail As String)
		_PersonID = pPersonID
		_PersonName = pPersonName
		_EMail = pEMail
	End Sub

	Public Property PersonID() As Integer
		Get
			Return _PersonID
		End Get
		Set(ByVal value As Integer)
			_PersonID = value
		End Set
	End Property

	Public Property PersonName() As String
		Get
			Return _PersonName
		End Get
		Set(ByVal value As String)
			_PersonName = value
		End Set
	End Property

	Public Property EMail() As String
		Get
			Return _EMail
		End Get
		Set(ByVal value As String)
			_EMail = value
		End Set
	End Property

	Public Property Tag() As Object
		Get
			Return _Tag
		End Get
		Set(ByVal value As Object)
			_Tag = value
		End Set
	End Property

End Class
