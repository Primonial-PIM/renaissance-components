﻿Option Strict On

Imports System.IO
Imports System.Data.SqlClient
Imports RenaissanceDataClass
Imports RenaissanceGlobals
Imports RenaissancePertracDataClass
Imports RenaissancePertracDataClass.PertracDataClass
Imports CTASimulationFunctions

Imports System.Text
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports RenaissancePertracAndStatsGlobals

Partial Public Class StatFunctions

  Private _TradeSimulator As CTASimulationFunctions.TradeSimulation = Nothing

  Private Sub GetSimulationSeries(ByVal pNestingLevel As Integer, ByVal pSimulationID As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByVal pStatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByRef pDatesArray() As Date, ByRef pReturnsArray() As Double, ByRef pNAVArray() As Double, ByRef pNetDeltaItem As NetDeltaItemClass, ByVal ParentStatsID As ULong, ByRef DependencyList As List(Of ULong)) Implements RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider.GetSimulationSeries
    ' ******************************************************************************************
    '
    '
    ' ******************************************************************************************

    Try
      Dim ThisSimulation As CTASimulationFunctions.SimulationParameterClass
      Dim Formatter As New BinaryFormatter

      '

      Try

        If (pNetDeltaItem Is Nothing) Then
          pNetDeltaItem = New NetDeltaItemClass
        End If

      Catch ex As Exception
      End Try

      ' First Get Simulation details

      Try

        Dim StrippedSimulationID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pSimulationID))
        Dim SimulationTable As New RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationDataTable
        Dim SimulationItem As RenaissanceDataClass.DSCTA_Simulation.tblCTA_SimulationRow
        Dim SimulationCommand As New SqlCommand

        Try

          SimulationCommand.CommandType = CommandType.StoredProcedure
          SimulationCommand.CommandText = "adp_tblCTA_Simulation_SelectCommand"
          SimulationCommand.Parameters.Add("@SimulationID", SqlDbType.Int).Value = CInt(StrippedSimulationID)
          SimulationCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
          SimulationCommand.CommandTimeout = 600

          SimulationCommand.Connection = MainForm.GetRenaissanceConnection()

          SyncLock SimulationCommand.Connection
            SimulationTable.Load(SimulationCommand.ExecuteReader)
          End SyncLock

        Catch ex As Exception
          SimulationTable = Nothing
        Finally
          Try
            If (SimulationCommand IsNot Nothing) AndAlso (SimulationCommand.Connection IsNot Nothing) Then
              SimulationCommand.Connection.Close()
              SimulationCommand.Connection = Nothing
            End If

            SimulationCommand = Nothing
          Catch ex As Exception
          End Try

        End Try

        ' 

        If (SimulationTable Is Nothing) OrElse (SimulationTable.Rows.Count <= 0) Then
          pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
          pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
          pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
          Exit Sub
        End If

        ' Instantiate Trade Simulator if necessary

        If (_TradeSimulator Is Nothing) Then
          _TradeSimulator = New TradeSimulation()
        End If

        ' Populate ThisSimulation Class

        SimulationItem = CType(SimulationTable.Rows(0), DSCTA_Simulation.tblCTA_SimulationRow)

        'ThisSimulation = CType(Formatter.Deserialize(New System.IO.MemoryStream(Convert.FromBase64String(SimulationItem.SimulationDefinition))), CTASimulationFunctions.SimulationParameterClass)
        ThisSimulation = CTASimulationFunctions.SimulationParameterClass.DeSerialiseParameterClass(SimulationItem.SimulationDefinition)

        ThisSimulation.InitialNAV = 100.0# ' To Normalise with most NAV Series.
        ThisSimulation.SeriesToUpdate = True
        ThisSimulation.InterestRateToUpdate = True

        pNetDeltaItem.PertracId = CULng(ThisSimulation.InstrumentID)

        ' Run Simulation

        _TradeSimulator.RunTrandingSimulation(Me, ThisSimulation, ParentStatsID, DependencyList)

        ' Copy out results series

        ' Get First Active 'PortfolioHistory()' element.

        Dim SimulationResultsStartIndex As Integer
        Dim CopyResultsIndex As Integer

        For DateIndex As Integer = 0 To (ThisSimulation.PortfolioHistory.Length - 1)
          If (ThisSimulation.PortfolioHistory(DateIndex) IsNot Nothing) Then
            SimulationResultsStartIndex = DateIndex
            Exit For
          End If
        Next

        If (ThisSimulation.InstrumentDate Is Nothing) OrElse (ThisSimulation.InstrumentDate.Length <= 0) Then

          pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
          pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
          pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())

        Else

          pDatesArray = CType(Array.CreateInstance(GetType(Date), ThisSimulation.PortfolioHistory.Length - SimulationResultsStartIndex), Date())
          pReturnsArray = CType(Array.CreateInstance(GetType(Double), pDatesArray.Length), Double())
          pNAVArray = CType(Array.CreateInstance(GetType(Double), pDatesArray.Length), Double())
          pNetDeltaItem.DeltaArray = CType(Array.CreateInstance(GetType(Double), pDatesArray.Length), Double())

          CopyResultsIndex = 0
          For Index As Integer = SimulationResultsStartIndex To (ThisSimulation.PortfolioHistory.Length - 1)
            pDatesArray(CopyResultsIndex) = ThisSimulation.PortfolioHistory(Index).PortfolioDate
            pNAVArray(CopyResultsIndex) = ThisSimulation.PortfolioHistory(Index).NAVClose
            pNetDeltaItem.DeltaArray(CopyResultsIndex) = ThisSimulation.PortfolioHistory(Index).AssetClose / ThisSimulation.PortfolioHistory(Index).NAVClose

            If (CopyResultsIndex = 0) Then
              pReturnsArray(CopyResultsIndex) = 0.0#
            Else
              pReturnsArray(CopyResultsIndex) = (pNAVArray(CopyResultsIndex) / pNAVArray(CopyResultsIndex - 1)) - 1.0#
            End If

            CopyResultsIndex += 1
          Next

        End If

      Catch ex As Exception

        pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
        pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
        pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
        pNetDeltaItem.DeltaArray = CType(Array.CreateInstance(GetType(Double), 0), Double())

      End Try

    Catch ex As Exception

      pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
      pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
      pNAVArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
      pNetDeltaItem.DeltaArray = CType(Array.CreateInstance(GetType(Double), 0), Double())

    End Try

  End Sub


End Class
