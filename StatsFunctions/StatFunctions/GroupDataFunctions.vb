﻿Option Strict On

Imports System.IO
Imports System.Data.SqlClient
Imports RenaissanceDataClass
Imports RenaissanceGlobals
Imports RenaissancePertracDataClass
Imports RenaissancePertracDataClass.PertracDataClass
Imports System.Text
Imports RenaissancePertracAndStatsGlobals

Partial Public Class StatFunctions
  Implements RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider

  Private Class GroupMemberData
    Public StartDate As Date
    Public EndDate As Date
    Public StartIndex As Integer
    Public EndIndex As Integer
    Public ReturnSeries() As Double
    Public MemberWeight As Double
  End Class

  'Private Sub More_Code_Here(Optional ByVal pInfo As String = "")
  'End Sub

  Private Sub GetGroupSeries(ByVal pNestingLevel As Integer, ByVal pGroupID As Integer, ByVal NoCache As Boolean, ByVal KnowledgeDate As Date, ByVal StatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByRef pDatesArray() As Date, ByRef pReturnsArray() As Double, ByRef pNetDeltaArray() As NetDeltaItemClass, ByVal ParentStatsID As ULong, ByRef DependencyList As List(Of ULong), ByVal pSampleSize As Integer) Implements RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider.GetGroupSeries
    ' ******************************************************************************************
    '
    ' At this time, Drilldown Groups are assumed to be DatePeriod - Monthly.
    '
    ' ******************************************************************************************

    Dim InstrumentFlags As PertracDataClass.PertracInstrumentFlags
    Dim StrippedGroupID As Integer
    Dim InstrumentCounter As Integer
    Dim DateIndexCounter As Integer
    Dim IsWeighted As Boolean = False
    Dim IsAverage As Boolean = False
    Dim FullTotalOfPositiveBasketWeights As Double = 1.0#
    Dim FullTotalOfNegativeBasketWeights As Double = 1.0#
    Dim DontUseVariableApproach As Boolean = False

    ' Temporary Arrays

    Dim GroupMembers() As Integer = Nothing
    Dim GroupWeights() As Double = Nothing
    Dim MemberData() As GroupMemberData
    Dim MaxPeriodsToCalculate As Integer = 0

    Try
			' Exit if no group specified, or if nesting is too deep (Anti-recursion)

      If (pGroupID = 0) Then
				pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
        pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
        pNetDeltaArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), 0), NetDeltaItemClass())

        Exit Sub
      End If

      If (pNestingLevel > MAX_GROUP_NESTINGLEVEL) Then
        pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
        pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
        pNetDeltaArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), 0), NetDeltaItemClass())

        Exit Sub
      End If

      InstrumentFlags = PertracDataClass.GetFlagsFromID(CUInt(pGroupID))
      StrippedGroupID = CInt(PertracDataClass.GetInstrumentFromID(CUInt(pGroupID)))

      ' Get Group Members and Weights

      Select Case InstrumentFlags

        Case PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted, PertracInstrumentFlags.DrillDown_Group_Median, PertracInstrumentFlags.DrillDown_Group_Mean
					' Dynamic and Drill-Down Groups.

          GetDynamicGroupMembers(StrippedGroupID, GroupMembers, GroupWeights, pSampleSize, False)
          MaxPeriodsToCalculate = GetDynamicGroupMaxPeriods(StrippedGroupID)

          For InstrumentCounter = 0 To (GroupMembers.Length - 1)
            If (PertracDataClass.GetFlagsFromID(GroupMembers(InstrumentCounter)) <> PertracInstrumentFlags.PertracInstrument) Then
              DontUseVariableApproach = True
              Exit For
            End If
          Next

          If (InstrumentFlags = PertracInstrumentFlags.Dynamic_Group_Weighted) Then

            IsWeighted = True
            FullTotalOfPositiveBasketWeights = 0.0#
            FullTotalOfNegativeBasketWeights = 0.0#

            If (GroupWeights IsNot Nothing) Then

              ' Aggregate Long and Short Weights.

              For InstrumentCounter = 0 To (GroupWeights.Length - 1)
                If (GroupWeights(InstrumentCounter) > 0.0#) Then
                  FullTotalOfPositiveBasketWeights += GroupWeights(InstrumentCounter)
                Else
                  FullTotalOfNegativeBasketWeights += Math.Abs(GroupWeights(InstrumentCounter))
                End If
              Next

              ' Normalise to the larger weighting - implicitly eliminate gearing - Necessary as it is not possible to specify Equity at present).

              For InstrumentCounter = 0 To (GroupWeights.Length - 1)
                If (FullTotalOfPositiveBasketWeights > 0.0#) AndAlso (FullTotalOfPositiveBasketWeights > FullTotalOfNegativeBasketWeights) Then
                  GroupWeights(InstrumentCounter) /= FullTotalOfPositiveBasketWeights
                ElseIf (FullTotalOfNegativeBasketWeights > 0.0#) AndAlso (FullTotalOfNegativeBasketWeights > FullTotalOfPositiveBasketWeights) Then
                  GroupWeights(InstrumentCounter) /= FullTotalOfNegativeBasketWeights
                End If
              Next

            End If

          Else
            ' Basket is not Weighted - Mean or Median

            If (GroupWeights IsNot Nothing) Then

              FullTotalOfPositiveBasketWeights = CDbl(GroupWeights.Length)
              FullTotalOfNegativeBasketWeights = 0.0#

              For InstrumentCounter = 0 To (GroupWeights.Length - 1)
                GroupWeights(InstrumentCounter) = 1.0#
              Next

            Else

              FullTotalOfPositiveBasketWeights = 0.0#
              FullTotalOfNegativeBasketWeights = 0.0#

            End If

            If (InstrumentFlags = PertracInstrumentFlags.Dynamic_Group_Mean) OrElse (InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Mean) Then
              IsAverage = True
            End If

          End If

				Case PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median, PertracInstrumentFlags.Group_Weighted
					' `Normal` Genoa goups.

					GetGroupMembers(pGroupID, GroupMembers, GroupWeights)
					MaxPeriodsToCalculate = GetDynamicGroupMaxPeriods(StrippedGroupID)

					If (InstrumentFlags = PertracInstrumentFlags.Group_Weighted) Then

						IsWeighted = True
						FullTotalOfPositiveBasketWeights = 0.0#
						FullTotalOfNegativeBasketWeights = 0.0#

						' Aggregate Long and Short Weights.

						For InstrumentCounter = 0 To (GroupWeights.Length - 1)
							If (GroupWeights(InstrumentCounter) > 0.0#) Then
								FullTotalOfPositiveBasketWeights += GroupWeights(InstrumentCounter)
							Else
								FullTotalOfNegativeBasketWeights += Math.Abs(GroupWeights(InstrumentCounter))
							End If
						Next

						' Normalise to the larger weighting - implicitly eliminate gearing - Necessary as it is not possible to specify Equity at present).

						If (FullTotalOfPositiveBasketWeights > 0.0#) AndAlso (FullTotalOfPositiveBasketWeights > FullTotalOfNegativeBasketWeights) Then
							For InstrumentCounter = 0 To (GroupWeights.Length - 1)
								GroupWeights(InstrumentCounter) /= FullTotalOfPositiveBasketWeights
							Next
						ElseIf (FullTotalOfNegativeBasketWeights > 0.0#) AndAlso (FullTotalOfNegativeBasketWeights > FullTotalOfPositiveBasketWeights) Then
							For InstrumentCounter = 0 To (GroupWeights.Length - 1)
								GroupWeights(InstrumentCounter) /= FullTotalOfNegativeBasketWeights
							Next
						End If

					Else
						' Basket is not Weighted - Mean or Median

						FullTotalOfPositiveBasketWeights = CDbl(GroupWeights.Length)
						FullTotalOfNegativeBasketWeights = 0.0#

						For InstrumentCounter = 0 To (GroupWeights.Length - 1)
							GroupWeights(InstrumentCounter) = 1.0#
						Next

						If (InstrumentFlags = PertracInstrumentFlags.Group_Mean) Then
							IsAverage = True
						End If

					End If

        Case Else

          ' Not a recognied group type.

          pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
          pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())

          Exit Sub

      End Select

      If (GroupMembers Is Nothing) OrElse (GroupMembers.Length <= 0) Then

        pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
        pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())
        pNetDeltaArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), 0), NetDeltaItemClass())

        Exit Sub

      End If

      If (GroupMembers.Length = 1) Then
        ' Get Single Stock Data

        pDatesArray = DateSeries(StatsDatePeriod, CULng(GroupMembers(0)), False, -1, 1.0#, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
        pReturnsArray = ReturnSeries(StatsDatePeriod, CULng(GroupMembers(0)), False, -1, 1.0#, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
        pNetDeltaArray = NetDeltas(StatsDatePeriod, CULng(GroupMembers(0)), False, -1, 1.0#, True, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data, False)

        If (ParentStatsID > 0) Then
          If (DependencyList IsNot Nothing) Then
            DependencyList.Add(SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(GroupMembers(0)), False)))
          Else
            SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(GroupMembers(0)), False))
          End If
        End If

        Exit Sub
      End If

			' Variable approach : 
			'
      ' Construct Group series differently for DrillDown_Group_Median type instruments.
      ' (Designed with Drill-Down forms in mind).
      '
      ' NOTE : ONLY HANDLES DATEPERIOD TYPE MONTHLY AT PRESENT !!!!!
      ' Other Period types will be handled as per small groups - THIS WILL BE SLOW !
      '

      If (Not DontUseVariableApproach) AndAlso (StatsDatePeriod = DealingPeriod.Monthly) AndAlso ((InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Median) OrElse (InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Mean)) Then

        Const PERTRAC_INSTRUMENT_CUTOFF As Integer = 1048576
        Const GROUP_INSTRUMENT_CUTOFF As Integer = 134217728
        Const RETURNED_DATES_AT_END_OF_PERIOD As Boolean = False ' Dates at start if False

        Dim HasPertracSeries As Boolean = False
        Dim HasRenaissanceSeries As Boolean = False
        Dim HasGroupSeries As Boolean = False
        Dim HasVeniceSeries As Boolean = False
        Dim HasSimulationSeries As Boolean = False
        Dim GroupSeriesLookup As Dictionary(Of Integer, Integer) = Nothing
        Dim VeniceSeriesLookup As Dictionary(Of Integer, Integer) = Nothing
        Dim SimulationSeriesLookup As Dictionary(Of Integer, Integer) = Nothing
        Dim GroupSeriesCount As Integer = 0
        Dim VeniceSeriesCount As Integer = 0
        Dim SimulationSeriesCount As Integer = 0
        Dim GroupDateSeries() As Array = Nothing
        Dim VeniceDateSeries() As Array = Nothing
        Dim SimulationDateSeries() As Array = Nothing
        Dim GroupReturnSeries() As Array = Nothing
        Dim VeniceReturnSeries() As Array = Nothing
        Dim SimulationReturnSeries() As Array = Nothing
        Dim VeniceCounter As Integer
        Dim VeniceIDs() As Integer = Nothing
        Dim SimulationCounter As Integer
        Dim SimulationIDs() As Integer = Nothing
        Dim GroupCounter As Integer
        Dim GroupIDs() As Integer = Nothing
        Dim NetDeltaInstrumentDictionary As New Dictionary(Of ULong, Integer)

        Dim DataCommand As New SqlCommand
        Dim ThisDataReader As SqlDataReader = Nothing
        Dim TempDatesTable As New DataTable
        Dim EarliestDate As Date = Now.Date
        Dim LatestDate As Date = RenaissanceGlobals.Globals.Renaissance_BaseDate
        Dim IndexOfEarliestDate As Integer '
        Dim IndexOfLatestDate As Integer '
        Dim ActiveInstrumentCount As Integer
        ' Dim IndexHash As SortedDictionary(Of Integer, Integer) = Nothing
        ' Dim ThisIndex As Integer 
        Dim ThisID As Integer
        ' Dim LastID As Integer
        Dim ThisReturn As Double
        Dim ThisDate As Date

        ActiveInstrumentCount = 0

        Try
          DataCommand.Connection = MainForm.GetRenaissanceConnection
          DataCommand.CommandType = CommandType.Text
          DataCommand.CommandTimeout = 120

          ' 
          'Dim GroupMembers() As Integer = Nothing
          'Dim GroupWeights() As Double = Nothing

          ' Initialise Temporary tables and Datasets.

          DataCommand.CommandText = "IF object_id('tempdb..#tblGroupPertracIDs') IS NOT NULL DROP TABLE #tblGroupPertracIDs"
          DataCommand.ExecuteNonQuery()

          DataCommand.CommandText = "CREATE TABLE #tblGroupPertracIDs ([InstrumentID] [int] NOT NULL , PRIMARY KEY ([InstrumentID]))"
          DataCommand.ExecuteNonQuery()

          DataCommand.CommandText = "IF object_id('tempdb..#tblGroupRenaissanceIDs') IS NOT NULL DROP TABLE #tblGroupRenaissanceIDs"
          DataCommand.ExecuteNonQuery()

          DataCommand.CommandText = "CREATE TABLE #tblGroupRenaissanceIDs ([InstrumentID] [int] NOT NULL , PRIMARY KEY ([InstrumentID]))"
          DataCommand.ExecuteNonQuery()

          Dim tblGroupPertracIDs As New DataTable
          Dim tblGroupRenaissanceIDs As New DataTable

          tblGroupPertracIDs.Columns.Add("InstrumentID", GetType(Integer))
          tblGroupRenaissanceIDs.Columns.Add("InstrumentID", GetType(Integer))

          ' Build Lookup Classes

          ' Average, Use only Members with weights.

          Dim ThisInstrumentFlags As PertracDataClass.PertracInstrumentFlags

          For InstrumentCounter = 0 To (GroupMembers.Length - 1)

            If (Not IsWeighted) OrElse (GroupWeights(InstrumentCounter) <> 0.0#) Then

              ThisID = CInt(PertracDataClass.GetInstrumentFromID(CUInt(GroupMembers(InstrumentCounter))))
              ThisInstrumentFlags = CType(PertracDataClass.GetFlagsFromID(CUInt(GroupMembers(InstrumentCounter))), PertracDataClass.PertracInstrumentFlags)

              ' Set Index Hash.
              ' Note, not required for Median calculations, so omit it if necessary.

              'If (InstrumentFlags <> PertracInstrumentFlags.DrillDown_Group_Median) Then
              '  If (IndexHash Is Nothing) Then
              '    IndexHash = New SortedDictionary(Of Integer, Integer)
              '  End If

              '  If (Not IndexHash.ContainsKey(GroupMembers(InstrumentCounter))) Then
              '    IndexHash.Add(GroupMembers(InstrumentCounter), ActiveInstrumentCount)
              '  End If
              'End If

              Select Case ThisInstrumentFlags

                Case PertracInstrumentFlags.PertracInstrument

                  If (ThisID < PERTRAC_INSTRUMENT_CUTOFF) Then
                    HasPertracSeries = True

                    tblGroupPertracIDs.Rows.Add(New Object() {ThisID})
                  ElseIf (ThisID < GROUP_INSTRUMENT_CUTOFF) Then
                    HasRenaissanceSeries = True

                    tblGroupRenaissanceIDs.Rows.Add(New Object() {ThisID})
                  End If

                Case PertracInstrumentFlags.VeniceInstrument

                  If (Not HasVeniceSeries) Then
                    HasVeniceSeries = True
                    VeniceSeriesLookup = New Dictionary(Of Integer, Integer)
                  End If

                  If (Not VeniceSeriesLookup.ContainsKey(GroupMembers(InstrumentCounter))) Then
                    VeniceSeriesLookup.Add(GroupMembers(InstrumentCounter), VeniceSeriesCount)
                    VeniceSeriesCount += 1
                  End If

                Case PertracInstrumentFlags.CTA_Simulation

                  If (Not HasSimulationSeries) Then
                    HasSimulationSeries = True
                    SimulationSeriesLookup = New Dictionary(Of Integer, Integer)
                  End If

                  If (Not SimulationSeriesLookup.ContainsKey(GroupMembers(InstrumentCounter))) Then
                    SimulationSeriesLookup.Add(GroupMembers(InstrumentCounter), SimulationSeriesCount)
                    SimulationSeriesCount += 1
                  End If

                Case PertracInstrumentFlags.DrillDown_Group_Mean, PertracInstrumentFlags.DrillDown_Group_Median, _
                     PertracInstrumentFlags.Dynamic_Group_Mean, PertracInstrumentFlags.Dynamic_Group_Median, PertracInstrumentFlags.Dynamic_Group_Weighted, _
                     PertracInstrumentFlags.Group_Mean, PertracInstrumentFlags.Group_Median

                  If (Not HasGroupSeries) Then
                    HasGroupSeries = True
                    GroupSeriesLookup = New Dictionary(Of Integer, Integer)
                  End If

                  If (Not GroupSeriesLookup.ContainsKey(GroupMembers(InstrumentCounter))) Then
                    GroupSeriesLookup.Add(GroupMembers(InstrumentCounter), GroupSeriesCount)
                    GroupSeriesCount += 1
                  End If

              End Select

            End If

          Next

          ' Set temporary tables

          Dim myBulkCopy As New SqlBulkCopy(DataCommand.Connection)

          If (HasPertracSeries) Then
            myBulkCopy.DestinationTableName = "#tblGroupPertracIDs"
            myBulkCopy.WriteToServer(tblGroupPertracIDs)
          End If

          If (HasRenaissanceSeries) Then
            myBulkCopy.DestinationTableName = "#tblGroupRenaissanceIDs"
            myBulkCopy.WriteToServer(tblGroupRenaissanceIDs)
          End If

          myBulkCopy = Nothing

          ' Hmmm, How long should this series go on for ?
          ' 

          ThisDataReader = Nothing
          Try

            If (HasPertracSeries) Then

              DataCommand.CommandText = "SELECT MIN([Date]) AS MinDate, MAX([Date]) AS MaxDate FROM MASTERSQL.dbo.Performance WHERE [ID] IN (Select InstrumentID FROM #tblGroupPertracIDs)"

              ThisDataReader = DataCommand.ExecuteReader

              While ThisDataReader.Read

                Try
                  If (Not (DBNull.Value.Equals(ThisDataReader(0)))) Then

                    ThisDate = CDate(ThisDataReader(0))

                    If (ThisDate < EarliestDate) Then
                      EarliestDate = ThisDate
                    End If

                    ThisDate = CDate(ThisDataReader(1))

                    If (ThisDate > LatestDate) Then
                      LatestDate = ThisDate
                    End If

                  End If


                Catch ex As Exception
                End Try

              End While

            End If

          Catch ex As Exception
          Finally
            If (Not ThisDataReader Is Nothing) Then
              ThisDataReader.Close()
              ThisDataReader = Nothing
            End If
          End Try

          Try
            If (HasRenaissanceSeries) Then

              DataCommand.CommandText = "SELECT MIN([ReturnDate]) AS MinDate, MAX([ReturnDate]) AS MaxDate FROM MASTERSQL.dbo.tblInstrumentPerformance WHERE [InstrumentID] IN (Select InstrumentID FROM #tblGroupRenaissanceIDs)"

              ThisDataReader = DataCommand.ExecuteReader

              While ThisDataReader.Read

                Try
                  If (Not (DBNull.Value.Equals(ThisDataReader(0)))) Then

                    ThisID = CInt(ThisDataReader(0)) + PERTRAC_INSTRUMENT_CUTOFF
                    ThisDate = CDate(ThisDataReader(1))

                    If (ThisDate < EarliestDate) Then
                      EarliestDate = ThisDate
                    End If

                    ThisDate = CDate(ThisDataReader(2))

                    If (ThisDate > LatestDate) Then
                      LatestDate = ThisDate
                    End If

                  End If

                Catch ex As Exception
                End Try

              End While

            End If

          Catch ex As Exception
          Finally
            If (Not ThisDataReader Is Nothing) Then
              ThisDataReader.Close()
              ThisDataReader = Nothing
            End If
          End Try

          ' Get Venice, Group & Simulation series.

          If (HasVeniceSeries) Then
            If (VeniceSeriesLookup IsNot Nothing) AndAlso (VeniceSeriesLookup.Count > 0) Then
              VeniceIDs = VeniceSeriesLookup.Keys.ToArray

              ReDim VeniceDateSeries(VeniceSeriesLookup.Count - 1)
              ReDim VeniceReturnSeries(VeniceSeriesLookup.Count - 1)
              Dim ThisDateSeries() As Date

              For VeniceCounter = 0 To (VeniceIDs.Length - 1)
                VeniceDateSeries(VeniceSeriesLookup(VeniceIDs(VeniceCounter))) = Me.DateSeries(StatsDatePeriod, pNestingLevel, CULng(VeniceIDs(VeniceCounter)), False, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
                VeniceReturnSeries(VeniceSeriesLookup(VeniceIDs(VeniceCounter))) = Me.ReturnSeries(StatsDatePeriod, pNestingLevel, CULng(VeniceIDs(VeniceCounter)), False, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data, 1.0#)

                If (ParentStatsID > 0) Then

                  If (DependencyList IsNot Nothing) Then
                    DependencyList.Add(SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(VeniceIDs(VeniceCounter)), False)))
                  Else
                    SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(VeniceIDs(VeniceCounter)), False))
                  End If

                End If

                ThisDateSeries = CType(VeniceDateSeries(VeniceSeriesLookup(VeniceIDs(VeniceCounter))), Date())

                If (ThisDateSeries IsNot Nothing) AndAlso (ThisDateSeries.Length > 0) Then
                  EarliestDate = dMIN(ThisDate, ThisDateSeries(0))
                  LatestDate = dMAX(ThisDate, ThisDateSeries(ThisDateSeries.Length - 1))
                Else
                  VeniceIDs(VeniceCounter) = 0
                End If

              Next
            Else
              HasVeniceSeries = False
            End If
          End If

          If (HasSimulationSeries) Then
            If (SimulationSeriesLookup IsNot Nothing) AndAlso (SimulationSeriesLookup.Count > 0) Then
              SimulationIDs = SimulationSeriesLookup.Keys.ToArray

              ReDim SimulationDateSeries(SimulationSeriesLookup.Count - 1)
              ReDim SimulationReturnSeries(SimulationSeriesLookup.Count - 1)
              Dim ThisDateSeries() As Date

              For SimulationCounter = 0 To (SimulationIDs.Length - 1)
                SimulationDateSeries(SimulationSeriesLookup(SimulationIDs(SimulationCounter))) = Me.DateSeries(StatsDatePeriod, pNestingLevel, CULng(SimulationIDs(SimulationCounter)), False, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
                SimulationReturnSeries(SimulationSeriesLookup(SimulationIDs(SimulationCounter))) = Me.ReturnSeries(StatsDatePeriod, pNestingLevel, CULng(SimulationIDs(SimulationCounter)), False, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data, 1.0#)

                If (ParentStatsID > 0) Then

                  If (DependencyList IsNot Nothing) Then
                    DependencyList.Add(SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(SimulationIDs(SimulationCounter)), False)))
                  Else
                    SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(SimulationIDs(SimulationCounter)), False))
                  End If

                End If

                ThisDateSeries = CType(SimulationDateSeries(SimulationSeriesLookup(SimulationIDs(SimulationCounter))), Date())

                If (ThisDateSeries IsNot Nothing) AndAlso (ThisDateSeries.Length > 0) Then
                  EarliestDate = dMIN(ThisDate, ThisDateSeries(0))
                  LatestDate = dMAX(ThisDate, ThisDateSeries(ThisDateSeries.Length - 1))
                Else
                  SimulationIDs(SimulationCounter) = 0
                End If

              Next
            Else
              HasSimulationSeries = False
            End If
          End If

          If (HasGroupSeries) Then
            If (GroupSeriesLookup IsNot Nothing) AndAlso (GroupSeriesLookup.Count > 0) Then
              GroupIDs = GroupSeriesLookup.Keys.ToArray

              ReDim GroupDateSeries(GroupSeriesLookup.Count - 1)
              ReDim GroupReturnSeries(GroupSeriesLookup.Count - 1)
              Dim ThisDateSeries() As Date

              For GroupCounter = 0 To (GroupIDs.Length - 1)
                GroupDateSeries(GroupSeriesLookup(GroupIDs(GroupCounter))) = Me.DateSeries(StatsDatePeriod, pNestingLevel, CULng(GroupIDs(GroupCounter)), False, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
                GroupReturnSeries(GroupSeriesLookup(GroupIDs(GroupCounter))) = Me.ReturnSeries(StatsDatePeriod, pNestingLevel, CULng(GroupIDs(GroupCounter)), False, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data, 1.0#)
                If (ParentStatsID > 0) Then

                  If (DependencyList IsNot Nothing) Then
                    DependencyList.Add(SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(GroupIDs(GroupCounter)), False)))
                  Else
                    SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(GroupIDs(GroupCounter)), False))
                  End If

                End If

                ThisDateSeries = CType(GroupDateSeries(GroupSeriesLookup(GroupIDs(GroupCounter))), Date())

                If (ThisDateSeries IsNot Nothing) AndAlso (ThisDateSeries.Length > 0) Then
                  EarliestDate = dMIN(ThisDate, ThisDateSeries(0))
                  LatestDate = dMAX(ThisDate, ThisDateSeries(ThisDateSeries.Length - 1))
                Else
                  GroupIDs(GroupCounter) = 0
                End If

              Next
            Else
              HasGroupSeries = False
            End If
          End If

          ' OK.....

          ' OK Resize destination arrays

          If MaxPeriodsToCalculate > 0 Then
            EarliestDate = dMAX(EarliestDate, RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(StatsDatePeriod, LatestDate, -Math.Max(MaxPeriodsToCalculate, 12)))
          End If

          EarliestDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(StatsDatePeriod, EarliestDate, RETURNED_DATES_AT_END_OF_PERIOD)
          LatestDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(StatsDatePeriod, LatestDate, RETURNED_DATES_AT_END_OF_PERIOD)
          Dim CurrentDate As Date = EarliestDate

          IndexOfEarliestDate = 1
          IndexOfLatestDate = Math.Max(RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(StatsDatePeriod, EarliestDate, LatestDate), 0) + IndexOfEarliestDate

          pDatesArray = CType(Array.CreateInstance(GetType(Date), Math.Max(0, IndexOfLatestDate) + 1), Date())
          pReturnsArray = CType(Array.CreateInstance(GetType(Double), pDatesArray.Length), Double())
          pNetDeltaArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), GroupMembers.Length), NetDeltaItemClass())

          ' initialise Net Delta Arrays

          For InstrumentIndex As Integer = 0 To (GroupMembers.Length - 1)
            pNetDeltaArray(InstrumentIndex) = New NetDeltaItemClass(CULng(GroupMembers(InstrumentIndex)), CType(Array.CreateInstance(GetType(Double), pDatesArray.Length), Double()))

            If (Not NetDeltaInstrumentDictionary.ContainsKey(CULng(GroupMembers(InstrumentIndex)))) Then
              NetDeltaInstrumentDictionary.Add(CULng(GroupMembers(InstrumentIndex)), InstrumentIndex)
            End If
          Next

          ' Dim TempIDs(GroupMembers.Length - 1) As Integer ' Use GroupMembers() Instead
          Dim TempReturns(GroupMembers.Length - 1) As Double
          Dim TempIDs(GroupMembers.Length - 1) As Double
          'Dim TempActiveToDateFlag(GroupMembers.Length - 1) As Boolean
          'Dim TempActiveThisDateFlag(GroupMembers.Length - 1) As Boolean
          Dim QueryStartDate As String
          ' Dim QueryEndDate As String
          Dim PeriodStartDate As Date
          Dim PeriodEndDate As Date

          ' Start to Get data...

          ' Set Start Date. Set as One period earlier then the 'Earliest' Date so that the first return is Zero - Matching
          ' the general StatsClass convention (Leaves room for an initial NAV value).

          pDatesArray(0) = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(StatsDatePeriod, RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(StatsDatePeriod, EarliestDate, -1), RETURNED_DATES_AT_END_OF_PERIOD)

          For DateIndexCounter = 1 To IndexOfLatestDate
            ' Set Dates
            pDatesArray(DateIndexCounter) = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(StatsDatePeriod, pDatesArray(DateIndexCounter - 1), 1)
          Next

          ' *************************************
          ' Retrieve and process Returns Data
          ' *************************************

          QueryStartDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(StatsDatePeriod, pDatesArray(1), False).ToString(RenaissanceGlobals.Globals.QUERY_SHORTDATEFORMAT)

          ' Set Command Query to retrieve pricing data :
          ' Note data should be returned in [ID], [Date] order.

          If (HasPertracSeries And HasRenaissanceSeries) Then
            ' Query combined Pertrac and Renaissance (User Series) data.

            DataCommand.CommandText = "SELECT [ID] AS InstrumentID, [Date] AS ReturnDate, [Return] AS PerformanceReturn FROM MASTERSQL.dbo.Performance WHERE ([ID] IN (Select InstrumentID FROM #tblGroupPertracIDs)) AND ([Date] >= '" & QueryStartDate & "') UNION ALL SELECT ([InstrumentID] + " & PERTRAC_INSTRUMENT_CUTOFF.ToString & ") AS InstrumentID, [ReturnDate] AS ReturnDate, [ReturnPercent] AS PerformanceReturn FROM MASTERSQL.dbo.tblInstrumentPerformance WHERE ([InstrumentID] IN (Select InstrumentID FROM #tblGroupRenaissanceIDs)) AND ([ReturnDate] >= '" & QueryStartDate & "') ORDER BY [ReturnDate], [InstrumentID]"

          ElseIf (HasRenaissanceSeries) Then
            ' Query only Renaissance (User Series) data.

            DataCommand.CommandText = "SELECT ([InstrumentID] + " & PERTRAC_INSTRUMENT_CUTOFF.ToString & ") AS InstrumentID, [ReturnDate] AS ReturnDate, [ReturnPercent] AS PerformanceReturn FROM MASTERSQL.dbo.tblInstrumentPerformance WHERE ([InstrumentID] IN (Select InstrumentID FROM #tblGroupRenaissanceIDs)) AND ([ReturnDate] >= '" & QueryStartDate & "') ORDER BY [ReturnDate], [InstrumentID]"

          Else
            ' Query Only Pertrac data.

            DataCommand.CommandText = "SELECT [ID] AS InstrumentID, [Date] AS ReturnDate, [Return] AS PerformanceReturn FROM MASTERSQL.dbo.Performance WHERE ([ID] IN (Select InstrumentID FROM #tblGroupPertracIDs)) AND ([Date] >= '" & QueryStartDate & "') ORDER BY [ReturnDate], [InstrumentID]"

          End If

          Try
            Dim ReadSuccess As Boolean
            Dim PeriodChanged As Boolean
            Dim InstrumentHash As New SortedDictionary(Of Integer, Integer)
            Dim ReturnIndex As Integer
            Dim SumReturn As Double
            Dim ThisDateSeries() As Date
            Dim ThisReturnsSeries() As Double
            Dim ThisReturnsIndex As Integer

            ThisDataReader = DataCommand.ExecuteReader

            If (ThisDataReader.HasRows) Then
              ReadSuccess = ThisDataReader.Read
            Else
              ReadSuccess = False
            End If
            PeriodChanged = False

            For DateIndexCounter = 1 To IndexOfLatestDate

              ' Create Data Set for this Date period.

              PeriodStartDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(StatsDatePeriod, pDatesArray(DateIndexCounter), False)
              PeriodEndDate = RenaissanceUtilities.DatePeriodFunctions.FitDateToPeriod(StatsDatePeriod, pDatesArray(DateIndexCounter), True)
              PeriodChanged = False

              ActiveInstrumentCount = 0

              ' Clear Temporary arrays.

              Array.Clear(TempReturns, 0, TempReturns.Length)
              Array.Clear(TempIDs, 0, TempReturns.Length)

              If (InstrumentHash.Count > 0) Then
                InstrumentHash.Clear()
              End If

              While (ReadSuccess) AndAlso (Not PeriodChanged)
                Try
                  ThisID = CInt(ThisDataReader(0))
                  ThisDate = CDate(ThisDataReader(1))
                  ThisReturn = CDbl(ThisDataReader(2))


                  If (ThisDate >= PeriodStartDate) Then

                    If (ThisDate <= PeriodEndDate) Then

                      If (InstrumentHash.ContainsKey(ThisID)) Then
                        ReturnIndex = InstrumentHash(ThisID)
                      Else
                        ReturnIndex = InstrumentHash.Count
                        InstrumentHash.Add(ThisID, ReturnIndex)
                        ActiveInstrumentCount += 1
                      End If

                      TempReturns(ReturnIndex) = ThisReturn
                      TempIDs(ReturnIndex) = ThisID
                    Else

                      PeriodChanged = True

                    End If

                  End If

                Catch ex As Exception
                End Try

                If (Not PeriodChanged) Then
                  ReadSuccess = ThisDataReader.Read
                End If

              End While

              ' Add on Returns for Non-Pertrac Instruments and Group Instruments

              If (HasVeniceSeries) Then

                For VeniceCounter = 0 To (VeniceIDs.Length - 1)
                  ThisID = VeniceIDs(VeniceCounter)

                  If (ThisID > 0) Then

                    If (InstrumentHash.ContainsKey(ThisID)) Then
                      ReturnIndex = InstrumentHash(ThisID)
                    Else
                      ReturnIndex = InstrumentHash.Count
                      InstrumentHash.Add(ThisID, ReturnIndex)
                      ActiveInstrumentCount += 1
                    End If

                    ThisDateSeries = CType(VeniceDateSeries(GroupSeriesLookup(ThisID)), Date())
                    ThisReturnsSeries = CType(VeniceReturnSeries(GroupSeriesLookup(ThisID)), Double())

                    If (ThisDateSeries IsNot Nothing) AndAlso (ThisReturnsSeries IsNot Nothing) Then
                      ThisReturnsIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(StatsDatePeriod, ThisDateSeries(0), PeriodEndDate)

                      If (ThisReturnsIndex >= 0) AndAlso (ThisReturnsIndex < ThisReturnsSeries.Length) Then

                        TempReturns(ReturnIndex) = ThisReturnsSeries(ThisReturnsIndex)
                        TempIDs(ReturnIndex) = ThisID

                      End If

                    Else
                      VeniceIDs(VeniceCounter) = 0
                    End If

                  End If

                Next

              End If

              If (HasGroupSeries) Then

                For GroupCounter = 0 To (GroupIDs.Length - 1)
                  ThisID = GroupIDs(GroupCounter)

                  If (ThisID > 0) Then

                    If (InstrumentHash.ContainsKey(ThisID)) Then
                      ReturnIndex = InstrumentHash(ThisID)
                    Else
                      ReturnIndex = InstrumentHash.Count
                      InstrumentHash.Add(ThisID, ReturnIndex)
                      ActiveInstrumentCount += 1
                    End If

                    ThisDateSeries = CType(GroupDateSeries(GroupSeriesLookup(ThisID)), Date())
                    ThisReturnsSeries = CType(GroupReturnSeries(GroupSeriesLookup(ThisID)), Double())

                    If (ThisDateSeries IsNot Nothing) AndAlso (ThisReturnsSeries IsNot Nothing) Then
                      ThisReturnsIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(StatsDatePeriod, ThisDateSeries(0), PeriodEndDate)

                      If (ThisReturnsIndex >= 0) AndAlso (ThisReturnsIndex < ThisReturnsSeries.Length) Then

                        TempReturns(ReturnIndex) = ThisReturnsSeries(ThisReturnsIndex)
                        TempIDs(ReturnIndex) = ThisID

                      End If

                    Else
                      GroupIDs(GroupCounter) = 0
                    End If

                  End If

                Next

              End If

              If (HasSimulationSeries) Then

                For SimulationCounter = 0 To (SimulationIDs.Length - 1)
                  ThisID = SimulationIDs(SimulationCounter)

                  If (ThisID > 0) Then

                    If (InstrumentHash.ContainsKey(ThisID)) Then
                      ReturnIndex = InstrumentHash(ThisID)
                    Else
                      ReturnIndex = InstrumentHash.Count
                      InstrumentHash.Add(ThisID, ReturnIndex)
                      ActiveInstrumentCount += 1
                    End If

                    ThisDateSeries = CType(SimulationDateSeries(GroupSeriesLookup(ThisID)), Date())
                    ThisReturnsSeries = CType(SimulationReturnSeries(GroupSeriesLookup(ThisID)), Double())

                    If (ThisDateSeries IsNot Nothing) AndAlso (ThisReturnsSeries IsNot Nothing) Then
                      ThisReturnsIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(StatsDatePeriod, ThisDateSeries(0), PeriodEndDate)

                      If (ThisReturnsIndex >= 0) AndAlso (ThisReturnsIndex < ThisReturnsSeries.Length) Then

                        TempReturns(ReturnIndex) = ThisReturnsSeries(ThisReturnsIndex)
                        TempIDs(ReturnIndex) = ThisID

                      End If

                    Else
                      SimulationIDs(SimulationCounter) = 0
                    End If

                  End If

                Next

              End If

              '

              If (InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Median) Then

                Try


                  If (ActiveInstrumentCount > 0) Then

                    ' Sort the captured returns.

                    Array.Sort(TempReturns, TempIDs, 0, ActiveInstrumentCount)

                    ' Derive the Median

                    If (ActiveInstrumentCount Mod 2) = 0 Then
                      ' Even Number of returns - Median is the average of the middle two.

                      pReturnsArray(DateIndexCounter) = (TempReturns((ActiveInstrumentCount \ 2) - 1) + TempReturns(ActiveInstrumentCount \ 2)) / 2.0# ' Note '\' integer division.
                      pNetDeltaArray(NetDeltaInstrumentDictionary(CULng(TempIDs((ActiveInstrumentCount \ 2))))).DeltaArray(DateIndexCounter) = 0.5#
                      pNetDeltaArray(NetDeltaInstrumentDictionary(CULng(TempIDs((ActiveInstrumentCount \ 2) - 1)))).DeltaArray(DateIndexCounter) = 0.5#

                    Else
                      ' Odd Number of returns - Median is the middle return.

                      pReturnsArray(DateIndexCounter) = TempReturns(ActiveInstrumentCount \ 2) ' Note '\' integer division.
                      pNetDeltaArray(NetDeltaInstrumentDictionary(CULng(TempIDs((ActiveInstrumentCount \ 2))))).DeltaArray(DateIndexCounter) = 1.0#

                    End If

                  Else
                    pReturnsArray(DateIndexCounter) = 0.0#
                  End If

                Catch ex As Exception
                End Try

              ElseIf (InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Mean) Then

                Try

                  If (ActiveInstrumentCount > 0) Then

                    SumReturn = 0.0#

                    For InstrumentCounter = 0 To (ActiveInstrumentCount - 1)

                      SumReturn += TempReturns(InstrumentCounter)
                      pNetDeltaArray(NetDeltaInstrumentDictionary(CULng(TempIDs(InstrumentCounter)))).DeltaArray(DateIndexCounter) = 1.0# / CDbl(ActiveInstrumentCount)

                    Next

                    pReturnsArray(DateIndexCounter) = SumReturn / CDbl(ActiveInstrumentCount)

                  End If

                Catch ex As Exception
                End Try

                'ElseIf (InstrumentFlags = PertracInstrumentFlags.DrillDown_Group_Weighted) Then

                '  More_Code_Here()

              End If

            Next ' DateIndexCounter

          Catch ex As Exception
          Finally
            If (Not ThisDataReader Is Nothing) Then
              ThisDataReader.Close()
              ThisDataReader = Nothing
            End If
          End Try

          ' Tidy the Temporary tables.

          DataCommand.CommandText = "IF object_id('tempdb..#tblGroupPertracIDs') IS NOT NULL DROP TABLE #tblGroupPertracIDs"
          DataCommand.ExecuteNonQuery()

          DataCommand.CommandText = "IF object_id('tempdb..#tblGroupRenaissanceIDs') IS NOT NULL DROP TABLE #tblGroupRenaissanceIDs"
          DataCommand.ExecuteNonQuery()

        Catch ex As Exception
        Finally
          If (DataCommand IsNot Nothing) Then
            Try
              If (DataCommand.Connection IsNot Nothing) Then
                DataCommand.Connection.Close()
              End If
            Catch ex As Exception
            Finally
              DataCommand.Connection = Nothing
            End Try
          End If
        End Try

      Else
        ' Conventional Series Construction :
        ' - Get each instrument series and build composite.
        '
        ' OK, now Get the Group Member Price Series.
        ' Remember 'pNestingLevel'

        ' Group Load Group Member Pertrac series.

        PertracData.LoadPertracTables(GroupMembers)

        ' 

        ReDim MemberData(GroupMembers.Length - 1)
        Dim TempDate() As Date
        Dim TempReturns() As Double
        Dim TempMemberDataItem As GroupMemberData
        Dim EarliestDate As Date = RenaissanceGlobals.Globals.Renaissance_EndDate_Data
        Dim LatestDate As Date = RenaissanceGlobals.Globals.Renaissance_BaseDate
        Dim IndexOfEarliestDate As Integer '
        Dim IndexOfLatestDate As Integer '

        For InstrumentCounter = 0 To (GroupMembers.Length - 1)
					If (Not IsWeighted) OrElse (GroupWeights(InstrumentCounter) <> 0.0#) Then
						' If Now Weighted Group or Weight is not 0 then :

						TempDate = Me.DateSeries(StatsDatePeriod, pNestingLevel, CULng(GroupMembers(InstrumentCounter)), False, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data)
						TempReturns = Me.ReturnSeries(StatsDatePeriod, pNestingLevel, CULng(GroupMembers(InstrumentCounter)), False, AnnualPeriodCount(StatsDatePeriod), 1.0#, pSampleSize, False, RenaissanceGlobals.Globals.Renaissance_BaseDate, RenaissanceGlobals.Globals.Renaissance_EndDate_Data, 1.0#)

						If (ParentStatsID > 0) Then
							If (DependencyList IsNot Nothing) Then
								DependencyList.Add(SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(GroupMembers(InstrumentCounter)), False)))
							Else
								SetDependsOnMe(ParentStatsID, GetStatCacheKey(StatsDatePeriod, CULng(GroupMembers(InstrumentCounter)), False))
							End If
						End If
					Else
						' Is Weighted and weight is Zero, Don't bother getting the returns series.
						TempDate = Nothing
						TempReturns = Nothing
					End If

          If (TempDate IsNot Nothing) AndAlso (TempDate.Length > 0) Then
            TempMemberDataItem = New GroupMemberData
            TempMemberDataItem.StartDate = TempDate(0)
            TempMemberDataItem.EndDate = TempDate(TempDate.Length - 1)
            TempMemberDataItem.ReturnSeries = TempReturns
            TempMemberDataItem.MemberWeight = GroupWeights(InstrumentCounter)
            TempMemberDataItem.StartIndex = -1
            TempMemberDataItem.EndIndex = -1

            MemberData(InstrumentCounter) = TempMemberDataItem

            If (TempMemberDataItem.StartDate < EarliestDate) Then
              EarliestDate = TempMemberDataItem.StartDate
            End If
            If (TempMemberDataItem.EndDate > LatestDate) Then
              LatestDate = TempMemberDataItem.EndDate
            End If

          Else
            MemberData(InstrumentCounter) = Nothing
          End If
        Next

        IndexOfEarliestDate = 0
        IndexOfLatestDate = Math.Max(RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(StatsDatePeriod, EarliestDate, LatestDate), 0)

        For InstrumentCounter = 0 To (GroupMembers.Length - 1)
          TempMemberDataItem = MemberData(InstrumentCounter)

          If (TempMemberDataItem IsNot Nothing) Then

            TempMemberDataItem.StartIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(StatsDatePeriod, EarliestDate, TempMemberDataItem.StartDate)
            TempMemberDataItem.EndIndex = RenaissanceUtilities.DatePeriodFunctions.GetPriceIndex(StatsDatePeriod, EarliestDate, TempMemberDataItem.EndDate)
          End If
        Next

        ' OK Resize destination arrays

        pDatesArray = CType(Array.CreateInstance(GetType(Date), Math.Max(0, IndexOfLatestDate - IndexOfEarliestDate) + 1), Date())
        pReturnsArray = CType(Array.CreateInstance(GetType(Double), pDatesArray.Length), Double())
        pNetDeltaArray = CType(Array.CreateInstance(GetType(NetDeltaItemClass), GroupMembers.Length), NetDeltaItemClass())

        For InstrumentIndex As Integer = 0 To (GroupMembers.Length - 1)
          pNetDeltaArray(InstrumentIndex) = New NetDeltaItemClass(CULng(GroupMembers(InstrumentIndex)), CType(Array.CreateInstance(GetType(Double), pDatesArray.Length), Double()))
        Next

        '
        Dim ActiveInstrumentCount As Integer
        Dim ActiveInstrumentReturns(GroupMembers.Length) As Double
        Dim ActiveInstrumentIndexes(GroupMembers.Length) As Integer
        Dim SumWeight As Double
        Dim SumWeightedReturn As Double
        Dim CurrentDate As Date = EarliestDate
        Dim ThisReturn As Double

        ' For Each Date...

        For DateIndexCounter = IndexOfEarliestDate To IndexOfLatestDate

          ActiveInstrumentCount = 0
          SumWeight = 0.0#
          SumWeightedReturn = 0.0#
          pDatesArray(DateIndexCounter) = CurrentDate

          ' For Each Instrument...

          For InstrumentCounter = 0 To (GroupMembers.Length - 1)

            TempMemberDataItem = MemberData(InstrumentCounter)

            If (TempMemberDataItem IsNot Nothing) AndAlso (TempMemberDataItem.StartIndex <= DateIndexCounter) Then

              ' Get this period return, assume a return of Zero if current date is after the final series date.
              ' Assumed Zero returns are not counted for the calculation of the Median return.

              If (TempMemberDataItem.EndIndex >= DateIndexCounter) Then
                ThisReturn = TempMemberDataItem.ReturnSeries(DateIndexCounter - TempMemberDataItem.StartIndex)
              Else
                ThisReturn = 0.0#
              End If

              If (IsWeighted) Then

                SumWeight += TempMemberDataItem.MemberWeight
                SumWeightedReturn += (ThisReturn * TempMemberDataItem.MemberWeight)

                pNetDeltaArray(InstrumentCounter).DeltaArray(DateIndexCounter) = TempMemberDataItem.MemberWeight

              ElseIf (IsAverage) Then

                SumWeight += 1.0#
                SumWeightedReturn += ThisReturn

                pNetDeltaArray(InstrumentCounter).DeltaArray(DateIndexCounter) = 1.0#

              Else ' Median, only use this return if it is a 'Real' return.

                If (TempMemberDataItem.EndIndex >= DateIndexCounter) Then
                  ActiveInstrumentReturns(ActiveInstrumentCount) = TempMemberDataItem.ReturnSeries(DateIndexCounter - TempMemberDataItem.StartIndex)
                  ActiveInstrumentIndexes(ActiveInstrumentCount) = InstrumentCounter
                  ActiveInstrumentCount += 1
                End If

              End If

            End If

          Next

          If (IsWeighted) Then
            ' Weights were normalised previously, so no return adjustment is necessary here...
            ' except that some time series could be missing entirely, in which case 'Sumweight' will be < 1.0

            If (SumWeight = 0.0#) Then
              pReturnsArray(DateIndexCounter) = 0.0#

              For InstrumentCounter = 0 To (GroupMembers.Length - 1)
                pNetDeltaArray(InstrumentCounter).DeltaArray(DateIndexCounter) = 0.0#
              Next
            Else
              pReturnsArray(DateIndexCounter) = SumWeightedReturn / SumWeight

              For InstrumentCounter = 0 To (GroupMembers.Length - 1)
                pNetDeltaArray(InstrumentCounter).DeltaArray(DateIndexCounter) /= SumWeight
              Next
            End If

          ElseIf (IsAverage) Then
            ' For simple average series, 'SumWeight' is the number of instruments aggregated for this period.

            If (SumWeight > 0.0#) Then
              pReturnsArray(DateIndexCounter) = SumWeightedReturn / SumWeight

              For InstrumentCounter = 0 To (GroupMembers.Length - 1)
                pNetDeltaArray(InstrumentCounter).DeltaArray(DateIndexCounter) /= SumWeight
              Next
            End If

          Else ' Median

            Array.Sort(ActiveInstrumentReturns, ActiveInstrumentIndexes, 0, ActiveInstrumentCount)

            If (ActiveInstrumentCount <= 0) Then
              pReturnsArray(DateIndexCounter) = 0.0#
            Else
              If (ActiveInstrumentCount Mod 2) = 0 Then
                ' Even Number of returns - Median is the average of the middle two.

                pReturnsArray(DateIndexCounter) = (ActiveInstrumentReturns((ActiveInstrumentCount \ 2) - 1) + ActiveInstrumentReturns(ActiveInstrumentCount \ 2)) / 2.0# ' Note '\' integer division.
                pNetDeltaArray(ActiveInstrumentIndexes((ActiveInstrumentCount \ 2) - 1)).DeltaArray(DateIndexCounter) = 0.5#
                pNetDeltaArray(ActiveInstrumentIndexes(ActiveInstrumentCount \ 2)).DeltaArray(DateIndexCounter) = 0.5#

              Else
                ' Odd Number of returns - Median is the middle return.

                pReturnsArray(DateIndexCounter) = ActiveInstrumentReturns(ActiveInstrumentCount \ 2) ' Note '\' integer division.
                pNetDeltaArray(ActiveInstrumentIndexes(ActiveInstrumentCount \ 2)).DeltaArray(DateIndexCounter) = 1.0#


              End If
            End If
          End If

          CurrentDate = RenaissanceUtilities.DatePeriodFunctions.AddPeriodToDate(StatsDatePeriod, CurrentDate, 1)
        Next

      End If

    Catch ex As Exception

      pDatesArray = CType(Array.CreateInstance(GetType(Date), 0), Date())
      pReturnsArray = CType(Array.CreateInstance(GetType(Double), 0), Double())

    End Try

  End Sub

  Private Sub GetGroupMembers(ByVal pGroupID As Integer, ByRef pGroupMembers() As Integer, ByRef pGroupWeights() As Double) Implements RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider.GetGroupMembers
    ' ******************************************************************************************
    ' Get Array of Integers representing the members for the Given Renaissance Group ID, together
    ' with the associated weights.
    '
    '
    ' ******************************************************************************************

    Try
      Dim StrippedGroupID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pGroupID))
      Dim GroupItems As New RenaissanceDataClass.DSGroupsAggregated.tblGroupsAggregatedDataTable
      Dim GroupItem As RenaissanceDataClass.DSGroupsAggregated.tblGroupsAggregatedRow
      Dim GroupCommand As New SqlCommand

      ' Get Group Items

      Try
        GroupCommand.CommandType = CommandType.StoredProcedure
        GroupCommand.CommandText = "adp_tblGroupsAggregated_SelectCommand"
        GroupCommand.Parameters.Add("@GroupListID", SqlDbType.Int).Value = CInt(StrippedGroupID)
        GroupCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = RenaissanceGlobals.Globals.KNOWLEDGEDATE_NOW
        GroupCommand.CommandTimeout = 600

        GroupCommand.Connection = MainForm.GetRenaissanceConnection()

        SyncLock GroupCommand.Connection
          GroupItems.Load(GroupCommand.ExecuteReader)
        End SyncLock
      Catch ex As Exception
      Finally
        Try
          If (GroupCommand IsNot Nothing) AndAlso (GroupCommand.Connection IsNot Nothing) Then
            GroupCommand.Connection.Close()
            GroupCommand.Connection = Nothing
          End If

          GroupCommand = Nothing
        Catch ex As Exception
        End Try
      End Try

      If (GroupItems IsNot Nothing) AndAlso (GroupItems.Rows.Count > 0) Then
        Dim ItemCount As Integer = 0

        pGroupMembers = CType(Array.CreateInstance(GetType(Integer), GroupItems.Rows.Count), Integer())
        pGroupWeights = CType(Array.CreateInstance(GetType(Double), GroupItems.Rows.Count), Double())

        For Each GroupItem In GroupItems.Rows
          pGroupMembers(ItemCount) = GroupItem.GroupPertracCode
          pGroupWeights(ItemCount) = GroupItem.GroupNewHolding

          ItemCount += 1
        Next

      Else
        pGroupMembers = CType(Array.CreateInstance(GetType(Integer), 0), Integer())
        pGroupWeights = CType(Array.CreateInstance(GetType(Double), 0), Double())
      End If

    Catch ex As Exception
      pGroupMembers = CType(Array.CreateInstance(GetType(Integer), 0), Integer())
      pGroupWeights = CType(Array.CreateInstance(GetType(Double), 0), Double())
    End Try

  End Sub

  Public Sub SetDynamicGroupMembers(ByVal pGroupID As Integer, ByVal pGroupMembers() As Integer, ByVal pGroupWeights() As Double) Implements RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider.SetDynamicGroupMembers
    ' ******************************************************************************************
    '
    '
    ' ******************************************************************************************

    SetDynamicGroupMembers(pGroupID, pGroupMembers, pGroupWeights, (-1))

  End Sub

  Public Sub SetDynamicGroupMembers(ByVal pGroupID As Integer, ByVal pGroupMembers() As Integer, ByVal pGroupWeights() As Double, ByVal MaxPeriodCount As Integer)
    ' ******************************************************************************************
    ' Set Array of Integers representing the members for the Given Custom Group ID.
    '
    '
    ' ******************************************************************************************

    Try
      DynamicGroupMembersLock.AcquireWriterLock(DynamicGroupMembersTimeout)

      If (DynamicGroupMembersLock.IsWriterLockHeld) Then

        Dim StrippedGroupID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pGroupID))

        If (DynamicGroupDefinitions.ContainsKey(StrippedGroupID)) Then
          Dim ExistingDefinition As DynamicGroupDefinitionClass
          Dim MembersChanged As Boolean

          ExistingDefinition = DynamicGroupDefinitions(StrippedGroupID)

          If (pGroupMembers IsNot Nothing) Then
            ' Set Group members

            If DynamicGroupDefinitions(StrippedGroupID).SetDynamicGroup(pGroupMembers, pGroupWeights, MembersChanged) Then
              ClearCache(False, False, True, True, False, StrippedGroupID.ToString)
              PertracData.ClearDataCache(False, False, True, True, False, StrippedGroupID.ToString, MembersChanged)
            End If

          Else
            ' Clear Entry if Members array is Nothing

            Try

              ClearCache(False, False, True, True, False, StrippedGroupID.ToString)
              PertracData.ClearDataCache(False, False, True, True, False, StrippedGroupID.ToString)

              ExistingDefinition.SetDynamicGroup(Nothing, Nothing, MembersChanged)

              DynamicGroupDefinitions.Remove(StrippedGroupID)

            Catch ex As Exception
            End Try
          End If

        Else
          If (pGroupMembers IsNot Nothing) Then
            DynamicGroupDefinitions.Add(StrippedGroupID, New DynamicGroupDefinitionClass(pGroupMembers, pGroupWeights, MaxPeriodCount))
          End If
        End If

      End If

    Catch ex As Exception
    Finally
      If (DynamicGroupMembersLock.IsWriterLockHeld) Then
        DynamicGroupMembersLock.ReleaseWriterLock()
      End If
    End Try

  End Sub

  Public Function GetDynamicGroupMemberCount(ByVal pGroupID As Integer) As Integer
    ' ******************************************************************************************
    '
    '
    ' ******************************************************************************************

    Dim RVal As Integer = 0

    Try
      DynamicGroupMembersLock.AcquireReaderLock(DynamicGroupMembersTimeout)

      If (DynamicGroupMembersLock.IsReaderLockHeld) Then

        Dim StrippedGroupID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pGroupID))

        If (DynamicGroupDefinitions.ContainsKey(StrippedGroupID)) Then

          Dim ThisGroupDefinition As DynamicGroupDefinitionClass

          ThisGroupDefinition = DynamicGroupDefinitions(StrippedGroupID)

          RVal = ThisGroupDefinition.MemberCount

        End If

      End If

    Catch ex As Exception

    Finally

      If (DynamicGroupMembersLock.IsReaderLockHeld) Then
        DynamicGroupMembersLock.ReleaseReaderLock()
      End If

    End Try

    Return RVal

  End Function

  Public Function GetDynamicGroupMaxPeriods(ByVal pGroupID As Integer) As Integer
    ' ******************************************************************************************
    '
    '
    ' ******************************************************************************************

    Dim RVal As Integer = 0

    Try
      DynamicGroupMembersLock.AcquireReaderLock(DynamicGroupMembersTimeout)

      If (DynamicGroupMembersLock.IsReaderLockHeld) Then

        Dim StrippedGroupID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pGroupID))

        If (DynamicGroupDefinitions.ContainsKey(StrippedGroupID)) Then

          Dim ThisGroupDefinition As DynamicGroupDefinitionClass

          ThisGroupDefinition = DynamicGroupDefinitions(StrippedGroupID)

          RVal = ThisGroupDefinition.MaxPeriodCount

        End If

      End If

    Catch ex As Exception

    Finally

      If (DynamicGroupMembersLock.IsReaderLockHeld) Then
        DynamicGroupMembersLock.ReleaseReaderLock()
      End If

    End Try

    Return RVal

  End Function

  Public Function GetDynamicGroupMember_First(ByVal pGroupID As Integer) As Integer
    ' ******************************************************************************************
    '
    ' Get a sample Member (so it is possible to get the associated grouping information on the frmDrillDown).
    '
    ' ******************************************************************************************

    Dim RVal As Integer = 0

    Try
      DynamicGroupMembersLock.AcquireReaderLock(DynamicGroupMembersTimeout)

      If (DynamicGroupMembersLock.IsReaderLockHeld) Then

        Dim StrippedGroupID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pGroupID))

        If (DynamicGroupDefinitions.ContainsKey(StrippedGroupID)) Then

          Dim ThisGroupDefinition As DynamicGroupDefinitionClass

          ThisGroupDefinition = DynamicGroupDefinitions(StrippedGroupID)

          If (ThisGroupDefinition.MemberCount > 0) Then
            RVal = ThisGroupDefinition.GetGroupMembers(0)
          End If

        End If

      End If

    Catch ex As Exception

    Finally

      If (DynamicGroupMembersLock.IsReaderLockHeld) Then
        DynamicGroupMembersLock.ReleaseReaderLock()
      End If

    End Try

    Return RVal

  End Function

  Public Sub GetDynamicGroupMembers(ByVal pGroupID As Integer, ByRef pGroupMembers() As Integer, ByRef pGroupWeights() As Double, ByVal pSampleSize As Integer, ByVal pLookthrough As Boolean) Implements RenaissancePertracAndStatsGlobals.GroupPriceSeriesProvider.GetDynamicGroupMembers
    ' ******************************************************************************************
    ' Get Array of Integers representing the members for the Given Custom Group ID.
    ' If a SampleSize is specified and that size is less than the Group Member count, then
    ' return a repeatable subset of the Group members.
    '
    ' pLookthrough not used at present.
    '
    ' ******************************************************************************************
    Const Minimum_Set_Size As Integer = 20 ' 20 is an arbitrary minimum set size.

    Try

      DynamicGroupMembersLock.AcquireReaderLock(DynamicGroupMembersTimeout)

      If (DynamicGroupMembersLock.IsReaderLockHeld) Then

        Dim StrippedGroupID As UInteger = PertracDataClass.GetInstrumentFromID(CUInt(pGroupID))

        If (DynamicGroupDefinitions.ContainsKey(StrippedGroupID)) Then
          Dim ThisGroupDefinition As DynamicGroupDefinitionClass

          ThisGroupDefinition = DynamicGroupDefinitions(StrippedGroupID)


          If (pSampleSize <= 0) OrElse (pSampleSize >= ThisGroupDefinition.MemberCount) OrElse (ThisGroupDefinition.MemberCount <= Minimum_Set_Size) Then

            If (ThisGroupDefinition.MemberCount <= 0) Then

              pGroupMembers = CType(Array.CreateInstance(GetType(Integer), 0), Integer())
              pGroupWeights = CType(Array.CreateInstance(GetType(Double), 0), Double())

            Else

              pGroupMembers = CType(Array.CreateInstance(GetType(Integer), ThisGroupDefinition.MemberCount), Integer())
              pGroupWeights = CType(Array.CreateInstance(GetType(Double), ThisGroupDefinition.MemberCount), Double())

              Array.Copy(ThisGroupDefinition.GetGroupMembers, pGroupMembers, ThisGroupDefinition.MemberCount)
              Array.Copy(ThisGroupDefinition.GetGroupWeights, pGroupWeights, ThisGroupDefinition.MemberCount)

            End If

          Else

            If (pSampleSize < Minimum_Set_Size) Then
              pSampleSize = Minimum_Set_Size
            End If

            ' Build derived return sets

            Dim TempGroupMembers() As Integer = ThisGroupDefinition.GetGroupMembers
            Dim TempGroupWeights() As Double = ThisGroupDefinition.GetGroupWeights
            Dim DerivedIndex As Integer
            Dim ReferenceIndex As Double

            If (TempGroupMembers Is Nothing) Then
              pGroupMembers = Nothing
              pGroupWeights = Nothing

              Exit Sub
            End If

            pGroupMembers = CType(Array.CreateInstance(GetType(Integer), pSampleSize), Integer())
            pGroupWeights = CType(Array.CreateInstance(GetType(Double), pSampleSize), Double())
            ReferenceIndex = 0

            For DerivedIndex = 0 To (pSampleSize - 1)
              pGroupMembers(DerivedIndex) = TempGroupMembers(CInt(ReferenceIndex))
              pGroupWeights(DerivedIndex) = TempGroupWeights(CInt(ReferenceIndex))

              ReferenceIndex = ((CDbl(TempGroupMembers.Length) - 1.0#) * CDbl(DerivedIndex + 1)) / CDbl(pSampleSize - 1)
            Next

          End If

        Else

          pGroupMembers = Nothing
          pGroupWeights = Nothing

        End If

      Else

        pGroupMembers = Nothing
        pGroupWeights = Nothing

      End If

    Catch ex As Exception

      pGroupMembers = Nothing
      pGroupWeights = Nothing

    Finally

      If (DynamicGroupMembersLock.IsReaderLockHeld) Then
        DynamicGroupMembersLock.ReleaseReaderLock()
      End If

    End Try

  End Sub

  Public Overloads Function dMAX(ByVal V1 As Date, ByVal V2 As Date) As Date
    Try
      If (V1 > V2) Then
        Return V1
      Else
        Return V2
      End If
    Catch ex As Exception
    End Try

    Return V2
  End Function

  Public Overloads Function dMIN(ByVal V1 As Date, ByVal V2 As Date) As Date
    Try
      If (V1 < V2) Then
        Return V1
      Else
        Return V2
      End If
    Catch ex As Exception
    End Try

    Return V2
  End Function

End Class
